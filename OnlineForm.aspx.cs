﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;

public partial class payment_OnlineForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack )
        {
            
            DateTime Now = DateTime.Now;
            txtDate.Text  = Now.ToShortDateString();
            
            dropdownlist();
            ShowDropDownEvent();
            refCode();

        }

    }

    protected void serialnumbr()
    {
        string gamount = Amount.Text;

        string setamount = gamount.Replace(".", "");

        string concertsignature = MerchantKey.Value + MerchantCode.Value + RefNo.Text + setamount + Currency.Text;

        Signature.Value = ComputeHash(concertsignature);
       
        //Response.Write(Signature.Value);
       
    }
   
    public static string ComputeHash(string Key)
    {
        SHA1CryptoServiceProvider objSHA1 = new SHA1CryptoServiceProvider();
        objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Key.ToCharArray()));
        byte[] buffer = objSHA1.Hash;
        string HashValue = System.Convert.ToBase64String(buffer);
        return HashValue;
    }

    protected void Prod_EditChanged(object sender, EventArgs e)
    {
        lblDesignation.Text= txtDesignation.SelectedValue;
    }

    protected void dropdownlist()
    {
        txtDesignation.Items.Clear();
        txtDesignation.Items.Add(new ListItem("---------Please Select---------", "0"));
        txtDesignation.Items.Add(new ListItem("Trainee", "Trainee"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Govt/University)", "Ophthalmologist(Govt/University)"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Private)", "Ophthalmologist(Private)"));
        txtDesignation.Items.Add(new ListItem("Eyecare Proffessional", "Eyecare_Proffessional"));
    }

    protected void Event_EditChanged(object sender, EventArgs e)
    {
        string dropEvent = DropDownListEvent.SelectedValue;

        bizEvent bevent = new bizEvent();
        DataSet ds;
        ds = bevent.SelectDetailevent (dropEvent);

        if (ds.Tables[0].Rows.Count > 0)
        {
            Currency.Text = ds.Tables[0].Rows[0]["ECurrency"].ToString();
            Amount.Text = ds.Tables[0].Rows[0]["EFees"].ToString();
            lblEventID.Text = ds.Tables[0].Rows[0]["EventID"].ToString();
            lblEventTitle.Text = ds.Tables[0].Rows[0]["EventTitle"].ToString();
            ProdDesc.Value = ds.Tables[0].Rows[0]["EventDescription"].ToString();
        }
        serialnumbr();
    }

    protected void ShowDropDownEvent()
    {
        bizEvent  bt = new bizEvent ();

        DataSet ds_template = bt.Selectevent ();

        ListItem li;
        DropDownListEvent.Items.Clear();

        if (ds_template.Tables[0].Rows.Count > 0)
        {
            li = new ListItem();
            li.Value = "";
            li.Text = "---------Please Select---------";
            DropDownListEvent.Items.Add(li);

            for (int i = 0; i < ds_template.Tables[0].Rows.Count; i++)
            {
                li = new ListItem();
                li.Value = ds_template.Tables[0].Rows[i]["EventID"].ToString();
                li.Text = ds_template.Tables[0].Rows[i]["EventTitle"].ToString();
                DropDownListEvent.Items.Add(li);
            }
        }
    }

    protected string refCode()
    {
        string tryRefcode;
        bizCode bRefCode = new bizCode();
        DataSet ds;
        ds = bRefCode.SelectReferenceCode();

        string refcode = ds.Tables[0].Rows[0]["Approval_Code"].ToString();
        int refintcode= Convert .ToInt32(refcode)  + 1;
        RefNo.Text = Convert.ToString(refintcode);
        tryRefcode = Convert.ToString(refintcode);

        bizCode updaterefCoed = new bizCode();
        int saverefcode = updaterefCoed.Updaterefcode(tryRefcode);

        return tryRefcode;
    }
   
    protected void Btnsubmit_Click(object sender, EventArgs e)
    {
        string gRefCode = refCode();
        RefNo.Text = refCode();
        if (RefNo.Text != "")
        {
            bizCode updaterefCoed = new bizCode();
            int refcode = updaterefCoed.Updaterefcode(gRefCode);
            bizpaymentform insertPForm = new bizpaymentform();
            int i = insertPForm.Insertpaymentform(UserName.Text, lblDesignation.Text, txtIC.Text, txtplcPractice.Text, txtAddress.Value, txtCity.Text, txtState.Text, txtPoscode.Text, UserEmail.Text, UserContact.Text, Amount.Text, txtpaymentoption.Text, gRefCode, txtDate.Text, lblEventID.Text, lblEventTitle.Text);

            //Response.Write("<script>alert('Submit Successful!');</script>");
            //Response.Write("<script>window.location='tmppage.aspx';</script>");
            
        }
        
    }

    
}
