﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using MSO_BizLayer;
using MyDate_Val;


public partial class SearchMember : System.Web.UI.Page
{
    private BizMember bm = new BizMember();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserCookies"] == null)
        {
            Response.Redirect("LostCookies.aspx");
        }
        else
        {
            string ProfileID;

            ProfileID = Request.Cookies["UserCookies"].Value;
        }
    }
    protected void btn_Search_Click(object sender, EventArgs e)
    {
        searchtypelbl.Text = "0";
        DataSet Ds_MemberList;
        Dg_MemberList.CurrentPageIndex = 0;
        Ds_MemberList = bm.MemberList(txt_MemberName.Text, txt_IC.Text, ddl_memb_type.SelectedValue, "true", memb_state.SelectedValue, addres.Text, searchtypelbl.Text, sortlbl.Text, MembershipID.Text);

        if (Ds_MemberList.Tables[0].Rows.Count < 1)
        {
            Dg_MemberList.DataSource = null;
            Dg_MemberList.DataBind();
        }
        else
        {
            Dg_MemberList.DataSource = Ds_MemberList;
            Dg_MemberList.DataBind();
        }


    }
    protected void Link_Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("AdminMain.aspx");
    }

    protected void Dg_MemberList_pageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
    {
        
        DataSet Ds_MemberList;

        Ds_MemberList = bm.MemberList(txt_MemberName.Text, txt_IC.Text, ddl_memb_type.SelectedValue, "false", memb_state.SelectedValue, addres.Text, searchtypelbl.Text, sortlbl.Text, MembershipID.Text);

        Dg_MemberList.CurrentPageIndex = e.NewPageIndex;
        Dg_MemberList.DataSource = Ds_MemberList;
        Dg_MemberList.DataBind();

    }

    protected void SortCommand(Object sender, DataGridSortCommandEventArgs e)
    {
        DataSet Ds_MemberList;
        sortlbl.Text = e.SortExpression;
        Ds_MemberList = bm.MemberList(txt_MemberName.Text, txt_IC.Text, ddl_memb_type.SelectedValue, "false", memb_state.SelectedValue, addres.Text, searchtypelbl.Text, sortlbl.Text, MembershipID.Text);
        Dg_MemberList.DataSource = Ds_MemberList;
        Dg_MemberList.DataBind();

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        searchtypelbl.Text = "1"; 
        DataSet Ds_MemberList;
        Dg_MemberList.CurrentPageIndex = 0;
        Ds_MemberList = bm.MemberList(txt_MemberName.Text, txt_IC.Text, ddl_memb_type.SelectedValue, "false", memb_state.SelectedValue, addres.Text, searchtypelbl.Text, sortlbl.Text, MembershipID.Text);
        Dg_MemberList.DataSource = Ds_MemberList;
        Dg_MemberList.DataBind();
    }
    public string getStatus(object sid)
    {
        String statusid = Convert.ToString(sid);
        string stat = "";
        if (statusid == "0")
        {
            stat = "Active";
        }
        else if (statusid == "1")
        {
            stat = "Resigned";
        }
        else if (statusid == "2")
        {
            stat = "Ceased";
        }
        return stat;
    }
}
