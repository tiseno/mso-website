﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlineForm.aspx.cs" Inherits="payment_OnlineForm" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" ><%--action='http://localhost/msotest/msotestpostback.php' method='POST'--%>
    <div>
    
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%"  >
    
    
    
    
    
    <tr><td align ="center" style ="padding:10px 0px 10px 0px;">
    <table style="width:80%;"><tr><td align ="left"><img src="images/logo.jpg" alt="" /></td></tr></table>
    </td></tr>
    
    <tr><td><table style="width:100%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 130px" align="left">Online Form</td></tr></table>
      </td></tr>
      
    <tr><td align ="center" style ="background-color:#E0EBF1; padding:20px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="80%" >
    
    
    
    <tr>
	 <td align="left"><img src="images/box_left_top.png" /></td>
	 <td style="background-image:url(images/box_line_vec.png); background-repeat:repeat-x;"></td>
	 <td><img src="images/box_right_top.png" /></td>
</tr>

<tr>
<td style="background-image:url(images/box_left_h.png); background-repeat:repeat-y"></td>
<td style="background-image:url(images/content_white.png); background-repeat:repeat;">

<table border="0" cellpadding ="0" cellspacing ="0">

<tr><td colspan ="6" class ="formheader"align ="center" ><br />Event</td></tr>
    <tr><td colspan ="6" class ="smallstep" align ="left" >Step 1 : Choose Event</td></tr>
    
    <tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
      
      <tr><td align ="left" class="style1" >Event</td><td class="style2">:</td>
    <td align ="left">
    <asp:DropDownList ID="DropDownListEvent" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Event_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
    <asp:Label ID="lblEventTitle" runat="server" Visible ="false"  Text=""></asp:Label>
    <asp:Label ID="lblEventID" runat="server" Visible ="false"  Text=""></asp:Label>
    </td>
    
    <td align ="left" class="style1" >RefNo</td>
    <td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="RefNo" ReadOnly ="true" runat="server"></asp:TextBox></td>
    </tr>
    
    <tr><td align ="left" class="style1" >Event Description</td><td class="style2">:</td>
    <td align ="left" colspan ="6">
        <textarea id ="ProdDesc" readonly ="readonly" runat="server" cols="80" name="elm1" rows="20" style="width: 250px; height:60px; "></textarea>
    </td></tr>
     
    <tr><td align ="left" class="style1" >Currency</td><td class="style2">:</td>
    <td align ="left" colspan ="6">
        <asp:TextBox ID="Currency" ReadOnly ="true"  runat="server"></asp:TextBox>
    </td></tr>
    
    <tr><td align ="left" class="style1" >Payment Amount</td><td class="style2">:</td>
    <td align ="left" class="style3">
    
    <asp:TextBox ID="Amount" ReadOnly ="true"  runat="server"></asp:TextBox>
    </td>
    <td align ="left" class="style1" >Payment Type</td><td class="style2">:</td>
    <td align ="left" >
    <%--<asp:TextBox ID="txtpaymentoption" ReadOnly ="true" runat="server"></asp:TextBox>--%>
        <asp:Label ID="txtpaymentoption" runat="server" Text="Credit Card"></asp:Label> <br />
    </td>
    </tr> 
    
    
    
    <tr><td colspan ="6" class ="formheader"align ="center" >User Information</td></tr>
    <tr><td colspan ="6" class ="smallstep" align ="left" >Step 2 : Fill in Your Personal Detail</td></tr>
    
    <tr><td colspan ="6"><table class="Dline"align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
    
    <tr><td align ="left" class="style1" >Name</td><td class="style2">:</td>
    <td  align ="left" class="style3">
    <asp:TextBox ID="UserName" Width="200px" runat="server" ></asp:TextBox>
    
    </td>
    <td align ="left" class="style1" >Date</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:Label ID="txtDate" runat="server" Text=""></asp:Label></td></tr> 
    
    <tr><td align ="left" class="style1" >Designation</td><td class="style2">:</td>
    <td class ="style3 " align ="left" >
    <asp:DropDownList ID="txtDesignation" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
    <asp:Label ID="lblDesignation" runat="server" Visible ="false"  Text=""></asp:Label> </td>
    
    <td align ="left" class="style1" >IC/Passport No</td><td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtIC" runat="server"></asp:TextBox></td>
    </tr> 
    
    <tr><td align ="left" class="style1" >Hospital/Clinic/Place of Practice</td>
        <td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <asp:TextBox ID="txtplcPractice" Width="200px" runat="server"></asp:TextBox></td></tr> 
    
    <tr><td align ="left" class="style1" rowspan ="3">Address</td><td class="style2" rowspan ="3">:</td>
    <td align ="left" rowspan ="3">
    <textarea id ="txtAddress" runat="server" cols="80" name="elm1" rows="20" style="width: 250px;height:60px; "></textarea> 
    </td>
    
    <td align ="left" class="style1" >State</td><td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtState" runat="server"></asp:TextBox></td>
    </tr>
    
    
    <tr>    
    <td align ="left" class="style1" >Poscode</td>
    <td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtPoscode" runat="server"></asp:TextBox></td>
    </tr> 
    
    <tr><td align ="left" class="style1" >City</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td></tr> 
    
    <tr><td align ="left" class="style1" >Hand Phone Number</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:TextBox ID="UserContact" runat="server"></asp:TextBox></td>
    
    <td align ="left" class="style1" >Email Address</td>
    <td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="UserEmail" runat="server"></asp:TextBox></td></tr> 
    
    <tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
      
      <tr><td align="center" colspan ="6">
      <table style ="padding:0px 10px 10px 10px; width:100%;">
      <tr><td align ="left" style="width:50px;">
        <asp:CheckBox ID="CheckBoxConfirm" runat="server" />
      </td><td align="left" style=" font-family:Verdana; font-size:10pt;">Please Check To Confirm.</td></tr>
      <tr><td align ="left" style =" padding:20px 0px 10px 0px;" colspan="2" >
      <asp:Button ID="Btnsubmit" runat="server" Text="Proceed" onclick="Btnsubmit_Click" /></td></tr></table>
      
          <%--PostBackUrl="http://localhost/msotest/msotestpostback.php"--%><%-- PostBackUrl="https://www.mobile88.com/ePayment/entry.asp"--%>
      </td></tr>
    
    <tr><td colspan ="6"><table>
    <tr>
    <td align ="left" class="style3">
    <asp:HiddenField ID="MerchantCode" runat="server" Value="M03184" />
    <asp:HiddenField ID="PaymentId" runat="server" Value="2" />
    <asp:HiddenField ID="Remark" runat="server" Value="" />
    <asp:HiddenField ID="Lang" runat="server" Value="ISO-8859-1" />
    <asp:HiddenField ID="ResponseURL" runat="server" Value="http://www.mso.org.my/payment/response.aspx" />
    <asp:HiddenField ID="MerchantKey" runat="server" Value="Fg0ESGVFKx" />
    <asp:HiddenField ID="Signature" runat="server"/>
    
    </td>

    
    </tr> 
    </table></td></tr>

</table>

</td>
<td style="background-image:url(images/box_right_h.png); background-repeat:repeat-y;"></td>
</tr>

<tr>
     <td><img src="images/box_left_btm.png" /></td>
     <td style="background-image:url(images/box_line_vec2.png); background-repeat:repeat-x;"></td>
     <td><img src="images/box_right_btm.png" /></td>
</tr>



    
    
    
    </table> 
    </td></tr>
    
    
    
      
      
    
      
      
   </table>
    
    </div>
    </form>
</body>
</html>
