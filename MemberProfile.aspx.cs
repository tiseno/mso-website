﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using MSO_BizLayer;

public partial class MemberProfile : System.Web.UI.Page
{
    private BizMember bm = new BizMember();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserCookies"] == null)
        {
            Response.Redirect("LostCookies.aspx");
        }
        else
        {
            string ProfileID;
            ProfileID = Request.Cookies["UserCookies"].Value;

            //DataSet getID = new DataSet();
            //getID = bm.get_id(ProfileID);

            DataSet get_detail = new DataSet();
            get_detail = bm.MemberDetail(ProfileID);

            //txt_MemberType.Text = get_detail.Tables[0].Rows[0]["Application"].ToString();
            txt_MemberType.Text = get_detail.Tables[0].Rows[0]["memberType"].ToString();
            txt_username.Text = get_detail.Tables[0].Rows[0]["username"].ToString();
            txt_password.Text = get_detail.Tables[0].Rows[0]["pw"].ToString();
            txt_Name.Text = get_detail.Tables[0].Rows[0]["Memb_Name"].ToString();
            txt_Nationality.Text = get_detail.Tables[0].Rows[0]["Nationality"].ToString();
            txt_IC.Text = get_detail.Tables[0].Rows[0]["NRIC"].ToString();
            txt_TitleOthers.Text = get_detail.Tables[0].Rows[0]["Memb_Title"].ToString();
            txt_DOB.Text = get_detail.Tables[0].Rows[0]["DOB"].ToString();
            txt_gender.Text = get_detail.Tables[0].Rows[0]["Gend"].ToString();
            txt_HomeAddress.Text = get_detail.Tables[0].Rows[0]["Home_Address"].ToString();
            txt_HomeCity.Text = get_detail.Tables[0].Rows[0]["Home_City"].ToString();
            txt_HomeCode.Text = get_detail.Tables[0].Rows[0]["Home_Code"].ToString();
            txt_HomeState.Text = get_detail.Tables[0].Rows[0]["Home_State"].ToString();
            txt_HomeCountry.Text = get_detail.Tables[0].Rows[0]["Home_country"].ToString();
            txt_HomeFax.Text = get_detail.Tables[0].Rows[0]["H_Fax"].ToString();
            txt_HomePhone.Text = get_detail.Tables[0].Rows[0]["H_Phone"].ToString();
            txt_MobilePhone.Text = get_detail.Tables[0].Rows[0]["M_Phone"].ToString();

            txt_MailingAdd.Text = get_detail.Tables[0].Rows[0]["Mailing_Address"].ToString();
            txt_CommMethod.Text = get_detail.Tables[0].Rows[0]["Comm_Method"].ToString();
            txt_MMC_No.Text = get_detail.Tables[0].Rows[0]["MMC_No"].ToString();

            txt_OfficeAddress.Text = get_detail.Tables[0].Rows[0]["Office_Address"].ToString();
            txt_OfficeCity.Text = get_detail.Tables[0].Rows[0]["Office_City"].ToString();
            txt_OfficeCode.Text = get_detail.Tables[0].Rows[0]["Office_Code"].ToString();
            txt_OfficeState.Text = get_detail.Tables[0].Rows[0]["Office_State"].ToString();
            txt_OfficeCountry.Text = get_detail.Tables[0].Rows[0]["Office_Country"].ToString();
            txt_OfficeFax.Text = get_detail.Tables[0].Rows[0]["O_Fax"].ToString();
            txt_OfficePhone.Text = get_detail.Tables[0].Rows[0]["O_Phone"].ToString();

            txt_RegisDate.Text = get_detail.Tables[0].Rows[0]["DOR"].ToString();
            txt_Email1.Text = get_detail.Tables[0].Rows[0]["Email1"].ToString();
            txt_Email2.Text = get_detail.Tables[0].Rows[0]["Email2"].ToString();


            string no;
            no = get_detail.Tables[0].Rows[0]["Profile_No"].ToString();
            DataSet get_Memb_Edu = new DataSet();
            get_Memb_Edu = bm.MemberEducation(no);

            if (get_Memb_Edu.Tables["Level"].Rows.Count > 0)
            {
                txt_Country1.Text = get_Memb_Edu.Tables["Level"].Rows[0]["Country"].ToString();
                txt_Degree1.Text = get_Memb_Edu.Tables["Level"].Rows[0]["Degree"].ToString();
                txt_University1.Text = get_Memb_Edu.Tables["Level"].Rows[0]["University"].ToString();
                txt_Date1.Text = get_Memb_Edu.Tables["Level"].Rows[0]["Date_Join"].ToString();
            }
            if (get_Memb_Edu.Tables["Level"].Rows.Count > 1)
            {
                txt_Country2.Text = get_Memb_Edu.Tables["Level"].Rows[1]["Country"].ToString();
                txt_Degree2.Text = get_Memb_Edu.Tables["Level"].Rows[1]["Degree"].ToString();
                txt_University2.Text = get_Memb_Edu.Tables["Level"].Rows[1]["University"].ToString();
                txt_Date2.Text = get_Memb_Edu.Tables["Level"].Rows[1]["Date_Join"].ToString();
            }
            if (get_Memb_Edu.Tables["Level"].Rows.Count > 2)
            {
                txt_Country3.Text = get_Memb_Edu.Tables["Level"].Rows[2]["Country"].ToString();
                txt_Degree3.Text = get_Memb_Edu.Tables["Level"].Rows[2]["Degree"].ToString();
                txt_University3.Text = get_Memb_Edu.Tables["Level"].Rows[2]["University"].ToString();
                txt_Date3.Text = get_Memb_Edu.Tables["Level"].Rows[2]["Date_Join"].ToString();
            }
            if (get_Memb_Edu.Tables["Level"].Rows.Count > 3)
            {
                txt_Country4.Text = get_Memb_Edu.Tables["Level"].Rows[3]["Country"].ToString();
                txt_Degree4.Text = get_Memb_Edu.Tables["Level"].Rows[3]["Degree"].ToString();
                txt_University4.Text = get_Memb_Edu.Tables["Level"].Rows[3]["University"].ToString();
                txt_Date4.Text = get_Memb_Edu.Tables["Level"].Rows[3]["Date_Join"].ToString();
            }

        }
        
    }
    protected void Link_Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("AdminMain.aspx");
    }
}
