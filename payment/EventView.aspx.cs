﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;

public partial class EventView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            ViewDetail();
        }

    }

    protected void ViewDetail()
    {
        bizEvent  bs = new bizEvent ();
        DataSet ds;
        ds = bs.SelectDetailevent(Request.QueryString["EventID"]);

        if (ds.Tables[0].Rows.Count > 0)
        {

            txtEventTitle.Text = ds.Tables[0].Rows[0]["EventTitle"].ToString();
            txtEventDescription.Value = ds.Tables[0].Rows[0]["EventDescription"].ToString();
            lblCurrency.Text = ds.Tables[0].Rows[0]["ECurrency"].ToString();
            txtFees.Text = ds.Tables[0].Rows[0]["EFees"].ToString();

        }
    }

   
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        string EventID = Request.QueryString["EventID"];
        Response.Redirect("EventDetailEdit.aspx?EventID=" + EventID);
    }
    protected void btnback_Click1(object sender, EventArgs e)
    {
        Response.Redirect("ListEventSearch.aspx");
    }
}
