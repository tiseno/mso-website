﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;


public partial class EventTypeadd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //alllistEvent();
            ShowDropDownEvent();
            dropdownlist();
        }
    }

    protected void alllistEvent()
    {
        bizEvent bs = new bizEvent();
        DataSet ds;

        ds = bs.SelectAllEventInTypeB();
        dg_SearchControl.DataSource = ds;
        dg_SearchControl.DataBind();
    }

    protected string URL(object EventTypeID)
    {
        return "EventTypeEdit.aspx?EventTypeID=" + EventTypeID;
    }

    protected void dg_SearchControl_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dg_SearchControl.CurrentPageIndex = e.NewPageIndex;
        bizEvent bs = new bizEvent();
        DataSet ds;

        ds = bs.SelectHoldEventTypeB(lblEventTypeID.Value);
        dg_SearchControl.DataSource = ds;
        dg_SearchControl.DataBind();
    }

    protected void Prod_EditChanged(object sender, EventArgs e)
    {
        //lblCurrency.Value = txtCurrency.SelectedValue;
        string dropCurrency= txtCurrency.SelectedValue;
        lblCurrency.Value = dropCurrency;

        if (dropCurrency != "0")
        {
            ErP7.Visible = false;
        }
        else if (dropCurrency == "0")
        {
            ErP7.Visible = true;
        }
    }

    protected void dropdownlist()
    {
        txtCurrency.Items.Clear();
        //txtCurrency.Items.Add(new ListItem("Please Select", "0"));
        txtCurrency.Items.Add(new ListItem("MYR", "MYR"));
        //txtCurrency.Items.Add(new ListItem("USD", "USD"));

    }

    protected void EventType_EditChanged(object sender, EventArgs e)
    {
        string dropEvent = DropEventType.SelectedValue;
        lblEventTypeID.Value = dropEvent;

        if (dropEvent != "0")
        {
            bizEvent bs = new bizEvent();
            DataSet ds;

            ds = bs.SelectHoldEventTypeB(dropEvent);
            dg_SearchControl.DataSource = ds;
            dg_SearchControl.DataBind();
            ErP6.Visible = false;
        }
        else if (dropEvent == "0")
        {
            ErP6.Visible = true;
            ShowDropDownEvent();
        }
    }

    protected void ShowDropDownEvent()
    {
        bizEvent bt = new bizEvent();

        DataSet ds_template = bt.SelectAllEventInTypeB();

        ListItem li;
        DropEventType.Items.Clear();

        if (ds_template.Tables[0].Rows.Count > 0)
        {
            li = new ListItem();
            li.Value = "0";
            li.Text = "---------Please Select---------";
            DropEventType.Items.Add(li);

            for (int i = 0; i < ds_template.Tables[0].Rows.Count; i++)
            {
                li = new ListItem();
                li.Value = ds_template.Tables[0].Rows[i]["EventID"].ToString();
                li.Text = ds_template.Tables[0].Rows[i]["EventTitle"].ToString();
                DropEventType.Items.Add(li);
            }
        }
    }

    protected void Status_editchange(object sender, EventArgs e)
    {
        lblStatus.Text = radioStatus.SelectedValue;
        
    }

    public static bool IsInteger(string theValue)
    {
        try
        {
            Convert.ToInt32(theValue);
            return true;
        }
        catch
        {
            return false;
        }
    }

    protected void btnEventTypeAdd_Click(object sender, EventArgs e)
    {
        if (lblEventTypeID.Value != "")
        {
            if (txtEventTypeTitle.Text != "")
            {
                if (lblCurrency.Value != "")
                {
                string gamountFees = txtFees.Text;
                string setamount = gamountFees.Replace(".", "");

                if (setamount != "" && IsInteger(setamount) == true)
                {
                    
                    string gfees = txtFees.Text;
                    string gfeesdot = Convert.ToDecimal(String.Format("{0:0.00}", Convert.ToDecimal(gfees))).ToString();

                    bizEvent addevent = new bizEvent();
                    int i = addevent.insertEventType(txtEventTypeTitle.Text, lblCurrency.Value, gfeesdot, lblEventTypeID.Value, "True");

                    bizEvent bs = new bizEvent();
                    DataSet ds;

                    ds = bs.SelectHoldEventTypeB(lblEventTypeID.Value);
                    dg_SearchControl.DataSource = ds;
                    dg_SearchControl.DataBind();

                    //Response.Write("<script>alert('Add Successful!');</script>");
                    Alert.Show("Add Successful!");


                    ErP.Visible = false;
                }
                else
                {
                    ErP.Visible = true;

                }
                    ErP7.Visible = false;
                }

                else
                {
                    ErP7.Visible = true;
                }
                ErP5.Visible = false;
            }
            else
            {
                ErP5.Visible = true;
            }
            ErP6.Visible = false;
        }
        else
        {
            ErP6.Visible = true;
        }
    }
}
