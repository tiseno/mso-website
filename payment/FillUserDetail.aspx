﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FillUserDetail.aspx.cs" Inherits="FillUserDetail" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" ><%--action='http://localhost/msotest/msotestpostback.php' method='POST'--%>
    <div>
    
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    
    <tr><td align ="center" style ="padding:10px 10px 10px 10px;">
    <table style="width:80%;"><tr><td align ="left"><img src="images/logo.jpg" /></td></tr></table>
    </td></tr>
    
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Online Form</td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :10px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="80%" >
    
    
    
    <tr><td colspan ="6" class ="formheader"align ="center" >User Information</td></tr>
    <tr><td colspan ="6" class ="smallstep" align ="left" >Step 2 : Fill in Your Personal Detail</td></tr>
    
    <tr><td colspan ="6"><table class="Dline"align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
    
    <tr><td align ="left" class="style1" >Name</td><td class="style2">:</td>
    <td  align ="left" class="style3">
    <asp:TextBox ID="UserName" Width="200px" runat="server" ></asp:TextBox>
    
    </td>
    <td align ="left" class="style1" >Date</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:Label ID="txtDate" runat="server" Text=""></asp:Label></td></tr> 
    
    <tr><td align ="left" class="style1" >Designation</td><td class="style2">:</td>
    <td class ="style3 " align ="left" >
    <asp:DropDownList ID="txtDesignation" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
    <asp:Label ID="lblDesignation" runat="server" Visible ="false"  Text=""></asp:Label> </td>
    
    <td align ="left" class="style1" >IC/Passport No</td><td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtIC" runat="server"></asp:TextBox></td>
    </tr> 
    
    <tr><td align ="left" class="style1" >Hospital/Clinic/Place of Practice</td>
        <td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <asp:TextBox ID="txtplcPractice" Width="200px" runat="server"></asp:TextBox></td></tr> 
    
    <tr><td align ="left" class="style1" rowspan ="3">Address</td><td class="style2" rowspan ="3">:</td>
    <td align ="left" rowspan ="3">
    <textarea id ="txtAddress" runat="server" cols="80" name="elm1" rows="20" style="width: 250px;height:60px; "></textarea> 
    </td>
    
    <td align ="left" class="style1" >State</td><td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtState" runat="server"></asp:TextBox></td>
    </tr>
    
    
    <tr>    
    <td align ="left" class="style1" >Poscode</td>
    <td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtPoscode" runat="server"></asp:TextBox></td>
    </tr> 
    
    <tr><td align ="left" class="style1" >City</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td></tr> 
    
    <tr><td align ="left" class="style1" >Hand Phone Number</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:TextBox ID="UserContact" runat="server"></asp:TextBox></td>
    
    <td align ="left" class="style1" >Email Address</td>
    <td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="UserEmail" runat="server"></asp:TextBox></td></tr> 
    
    
    <tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
    
    
    <tr><td align="left" colspan ="6">
      <table style ="padding:0px 3px 0px 10px; width :80%;">
      
      <tr><td align ="left" style =" padding-left :5px;" >
      <asp:Button ID="Btnsubmit" runat="server" Text="Next"  onclick="Btnsubmit_Click" /></td></tr></table>
      
          <%--PostBackUrl="http://localhost/msotest/msotestpostback.php"--%><%-- PostBackUrl="https://www.mobile88.com/ePayment/entry.asp"--%>
      </td></tr>
    
    
    <tr><td colspan ="6"><table>
    <tr>
    <td align ="left" class="style3">
    
    <asp:HiddenField ID="MerchantKey" runat="server"/>
    <asp:HiddenField ID="RefNo" runat="server"/>  <asp:HiddenField ID="ProdDesc" runat="server"/>
    <asp:HiddenField ID="Currency" runat="server"/>  <asp:HiddenField ID="Amount" runat="server"/>
    
    <asp:HiddenField ID="paymentoption" runat="server"/>  <asp:HiddenField ID="lblEventID" runat="server"/>  <asp:HiddenField ID="lblEventTitle" runat="server"/>
    </td>

    
    
    
    </tr> 
    </table></td></tr>
    
    </table> 
    </td></tr>
    
    
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:10px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>
      
      
    
      
      
   </table>
    
    </div>
    </form>
</body>
</html>

