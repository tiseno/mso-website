﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventDetailEdit.aspx.cs" Inherits="EventDetailEdit" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/updateevent.js" ></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    
    
    <tr><td><table style="width:100%; background-color :#d9d6d6;    height:20px;" align="center">
      <tr><td style="color:#323232;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Event Detail</td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="left" style ="padding :10px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%" >
    <%--<tr><td  class ="formheader"align ="left" >Event Edit</td></tr>--%>
    
    <%--<tr><td ><table class="Dline"  align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <%--<tr><td ><table style="width:100%; height:2px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; " align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <tr><td align ="left">
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="60%" >
      
      <tr><td align ="left" class="style1" >Event Title</td><td class="style2">:</td>
      <td  align ="left" style =" width :450px;">
      <asp:TextBox ID="txtEventTitle" Width="200px" runat="server"></asp:TextBox> 
          <asp:HiddenField ID="lblEventTypeID" runat="server"  />
                                            </td>
      </tr> 
      
      <tr><td align ="left" class="style1" valign ="top" >Event Description</td><td class="style2"  valign ="top" >:</td>
      <td  align ="left" >
      <textarea id ="txtEventDescription" runat="server" cols="80" name="elm1" rows="20" style="width: 250px;height:60px; "></textarea>
      </td></tr> 
      
            
      <%--<tr><td align ="left" class="style1" >Currency</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <asp:DropDownList ID="txtCurrency" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
      <asp:HiddenField ID="lblCurrency" runat="server"  />
      </td></tr> --%>

      
      <%--<tr><td align ="left" class="style1" >Fees</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <asp:TextBox ID="txtFees" Width="200px" runat="server"></asp:TextBox>
      <asp:Label ID="ErP" runat="server" style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" Font-Bold="true" Text="Invalid Value!"></asp:Label></td>
      </tr>--%> 
      
      
      <tr><td align ="left" class="style1" >Status</td><td class="style2">:</td>
      <td  align ="left" >
      <asp:RadioButtonList ID="radioStatus" Runat="server" Font-Size="8pt" Font-Names="Verdana" BorderWidth="0" CellPadding="0" CellSpacing="0" OnSelectedIndexChanged="Status_EventEditchange" RepeatLayout="flow" RepeatColumns="3">
            <asp:ListItem Text="Yes" Value="True" ></asp:ListItem>
            <asp:ListItem Text="No" Value="False"></asp:ListItem>
        </asp:RadioButtonList>    
        <asp:Label ID="lblStatus1" runat="server" Visible ="false"  Text=""></asp:Label> 
      </td>
      </tr> 
      
      <tr><td align ="left" colspan ="3" style ="padding :10px 20px 10px 0px;">
      <%--<asp:Button ID="btnback" runat="server" Text="Back" onclick="btnback_Click1" />--%><%--OnClientClick="return validateForm(this)"--%>
      <asp:Button ID="btnupdate" runat="server" Text="Update" Width ="80px" onclick="btnupdate_Click" />
          <br />
      <asp:Button ID="btndelete" runat="server" Visible ="false" Text="Delete" onclick="btndelete_Click" /></td>
      </tr> 
      
      </table>
      </td></tr></table> </td></tr>
      
      <tr><td><table style="width:100%; background-color :#d9d6d6; height:20px;" align="center">
      <tr><td style="color:#323232;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Event Type</td></tr></table>  <br />
      </td></tr>
      
      <tr><td align ="left" style ="padding :10px 10px 10px 10px;">
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%" >
      
      <tr><td align ="left">
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="60%" >
      <tr><td align ="left" class="style1">Event Type Name</td> 
      <td class="style2">:</td> 
      <td align ="left" style =" width :450px;">
      <asp:TextBox ID="txtEventTypeTitle" Width="200px" runat="server"></asp:TextBox>
    
      <asp:Label ID="ErEventType0" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
    
          <asp:Label ID="ErP5" runat="server" 
            style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
            Font-Bold="true" Text="Please Insert Event Type Name!"></asp:Label> 
      </td></tr>
      
      <tr><td align ="left" class="style1" >Currency</td><td class="style2">:</td>
      <td  align ="left">
      <asp:DropDownList ID="txtCurrency" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_CurrencyEditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
      <asp:Label ID="ErEventType1" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
    
          <asp:Label ID="ErP7" runat="server" 
            style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
            Font-Bold="true" Text="Please Choose Currency!"></asp:Label> 
      <asp:HiddenField ID="lblCurrency" runat="server"  />
      </td></tr> 
      
      <tr><td align ="left" class="style1" >Fees</td><td class="style2">:</td>
      <td  align ="left" >
      <asp:TextBox ID="txtFees" Width="200px" runat="server"></asp:TextBox>
      <asp:Label ID="ErEventType2" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
      <asp:Label ID="ErP" runat="server" style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" Font-Bold="true" Text="Invalid Value!"></asp:Label></td>
      </tr> 
      
      <tr><td align ="left" class="style1" >&nbsp;</td><td class="style2">&nbsp;</td>
      <td  align ="left">
      <asp:RadioButtonList ID="RadioEventType" Runat="server" Font-Size="8pt" Font-Names="Verdana" Visible ="false" OnSelectedIndexChanged="Status_EventTypeEditchange" BorderWidth="0" CellPadding="0" CellSpacing="0" RepeatLayout="flow" RepeatColumns="3">
            <asp:ListItem Text="Yes" Value="True" ></asp:ListItem>
            <asp:ListItem Text="No" Value="False"></asp:ListItem>
        </asp:RadioButtonList>   
        <asp:Label ID="lblStatus" runat="server" Visible ="false"  Text=""></asp:Label> 
      </td>
      </tr> 
      
      <tr><td align="left" colspan="6" style=" font-family:tahoma,arial; font-size:8pt;">
    <br /><br />
          <asp:Label ID="ErP4" runat="server"
            style="color:Red; vertical-align:top; font-family:tahoma,arial; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> &nbsp;=Fields must be fill.
            <br />
    
    <br /></td></tr>
      
      <tr><td align ="left" colspan ="3" style ="padding :10px 20px 10px 0px;">
      <asp:Button ID="btnEventTypeAdd" runat="server" Text="Add" Width ="80px" 
              onclick="btnEventTypeAdd_Click"/></td>
      </tr> 
      </table></td></tr></table> </td> </tr> 
      
      
    
    <tr><td align ="center" >
    
 <asp:Datagrid ID="dg_SearchControl" runat="server" OnPageIndexChanged="dg_SearchControl_PageIndexChanged" Width ="100%" 
        AutoGenerateColumns="False" 
        BackColor="White" 
        BorderColor="#eeeeee" 
        BorderWidth="2px" 
        CellPadding="4" 
        ForeColor="Black" 
        ItemStyle-VerticalAlign="Top" 
        PageSize="10" 
        AllowPaging="true" 
        PagerStyle-Position="Bottom" 
        PagerStyle-Mode="NextPrev"
        ><%--tis is for paging--%>
        
            <FooterStyle BackColor="#CCCC99" />
            <SelectedItemStyle BackColor="#CE5D5A" Font-Size ="Small"  Font-Bold="False" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
            Mode="NumericPages" />
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#efefef" />
        
       <Columns>
        
            <asp:TemplateColumn HeaderText="No"><itemtemplate>
            <%#Container.ItemIndex + dg_SearchControl.CurrentPageIndex * dg_SearchControl.PageSize + 1%>
            </itemtemplate>
            <HeaderStyle  HorizontalAlign="Center"/>
            <ItemStyle HorizontalAlign="Center"/>
            </asp:TemplateColumn>

            <asp:TemplateColumn HeaderText="Event Type Title" Visible ="True" >
            <ItemTemplate>
            <asp:Hyperlink runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventTypeName")%>' NavigateUrl='<%#URL(DataBinder.Eval(Container.DataItem, "EventTypeID"))%>'  ID="Hyperlink1"/></ItemTemplate>
            </asp:TemplateColumn>

            <%--<asp:BoundColumn DataField="EventDescription" HeaderText="EventDescription"></asp:BoundColumn>--%>
            <asp:BoundColumn DataField="ECurrency" HeaderText="Currency"></asp:BoundColumn>
            <asp:BoundColumn DataField="EFees" HeaderText="Fees"></asp:BoundColumn>
            <asp:BoundColumn DataField="EventTypeAtivation" HeaderText="Status"></asp:BoundColumn>
            
            
            
            </Columns>
            <HeaderStyle BackColor="#d9d6d6" Font-Bold="false" Font-Size ="small" ForeColor="#323232" />
            </asp:Datagrid>   
    
    
    </td></tr>
    
    </table> 
  
    
    <%--<tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      

    </div>
    </form>
</body>
</html>
