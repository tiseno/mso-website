﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintReceipt.aspx.cs" Inherits="PrintReceipt" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSO</title>
     <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/txtvalidation.js" ></script>
</head>
<script type="text/javascript">
function poponload()
{

   testwindow = window.open ("response.aspx","mywindow","menubar=1,resizable=1,width=350,height=250");
    //testwindow = //window.open("", "mywindow", "location=1,status=1,scrollbars=1,width=100,height=100");
    testwindow.moveTo(0, 0);
}
</script>

<body onload="window.print();" ><%--onload="javascript: poponload()"style="background-color:#E0EBF1;"--%>
<%----%>
    <form id="form1" runat="server">
    <div>
 
    <table width ="100%" style=" margin:0px 0px 0px 0px; padding:10px 10px 10px 10px;">

      
    <%--<tr><td align ="center" id="preview" runat="server" style ="background-color:#E0EBF1; padding:10px 10px 10px 10px;" >--%>
    <tr><td align ="center" id="Td1" runat="server" style ="padding:10px 10px 10px 10px;" >
    <table border ="0" cellpadding="0"  cellspacing="0" width ="50%">
    
    <%--<tr>
	 <td align="right" style=" width:18px; background-image:url(../images/box_left_top.png); background-repeat:no-repeat;">
         &nbsp;</td>
	 <td style="background-image:url(../images/box_line_vec.png); background-repeat:repeat-x;">
         &nbsp;</td>
	 <td align="left" style=" background-image:url(../images/box_right_top.png); width :18px; background-repeat:no-repeat;">
         &nbsp;</td>
</tr>--%>
    
<%--    <tr>
<td align ="right" style=" background-image:url(../images/box_left_h.png); width :18px; background-repeat:repeat-y">
    &nbsp;</td>--%>
<td style=" background-image:url(../images/content_white.png); background-repeat:repeat;" align="center">

<table border="0" cellpadding ="0" cellspacing ="0" style="width:100%;">
<%--<tr><td align ="left"><img src="images/logo.jpg" /></td></tr>--%>
    
    <tr><td align ="center" style ="padding:10px 0px 20px 0px;">
    <table style="width:60%;"><tr><td align ="left"><img src="../images/logo.jpg" width="500px" height="50px" alt="" /></td></tr></table>
    </td></tr>
    
    <tr><td align ="center"><table style="width:100%; height:20px; " align="center">
      <tr><td style=" font-family :tahoma,arial; font-size :10pt; font-weight:bold; padding:0px 0px 0px 25px" align="left">3rd Annual Scientific Meeting</td></tr></table>
      </td></tr>
    
    <%--<tr><td style=" font-family:Verdana; font-weight:bold; font-size :10pt;" align ="center" >
    <table style="width:60%;"><tr><td align ="left">Receipt</td></tr></table></td></tr>--%>
    
    <tr><td align="center" >
    <table border="0" cellpadding ="0" cellspacing ="0" width ="90%">
    
    
      
    <tr><td colspan="6" style=" font-family:Verdana; padding:0px 0px 10px 0px; font-weight:bold; font-size :10pt;" align ="center">
    <asp:Label ID="lblThankYouPayment" Visible ="false" style="color:Red; font-family:tahoma,arial; font-weight :bold ; font-size:12pt;" runat="server" Text="Thank You For Your Payment."></asp:Label>
    </td></tr>
    
    <tr><td colspan="6" style=" font-family:Verdana; padding:0px 0px 10px 0px; font-weight:bold; font-size :10pt;" align ="left">Receipt</td></tr>
    
    <tr><td class ="rcptstyle" align ="left" >Ref No.</td>
    <td class ="style2" >:</td>
    <td align ="left" class="responsestyle" style ="width :230px;">
    <asp:Label ID="RefNo" runat="server" Text=""></asp:Label>  </td>
    
    <td style="padding-left:50px; width:80px; font-family :Verdana; font-size :10pt;" align ="left" >Date</td>
    <td class ="style2" >:</td>
    <td align ="left" style="font-family :Verdana; font-size :10pt; width:100px;">
    <asp:Label ID="txtDate" runat="server" Text=""></asp:Label> </td>
    </tr>
  
    <tr><td class ="rcptstyle" align ="left" >Receipt No.</td><td class ="style2" >:</td>
    <td align ="left" class ="receiptstyle">
    <asp:Label ID="txtreceiptNo" runat="server" Text=""></asp:Label> </td>
    
    
    <td  style="padding-left:50px; width:80px; font-family :tahoma,arial; font-size :10pt;" align ="left" >Time</td><td class ="style2" >
                                                        <asp:Label ID="Label4" Font-Size ="8pt" Font-Names="tahoma,arial" 
                                                            runat="server" Text=":"></asp:Label></td>
    <td style="font-family :Verdana; font-size :10pt;  width:100px"  align ="left" >
    <asp:Label ID="lblTime" runat="server" Text=""></asp:Label> </td>
    
    </tr>
    
    <tr><td class ="rcptstyle" align ="left" >Name</td><td class ="style2" >:</td>
    <td align ="left" class ="receiptstyle" colspan="4">
    <asp:Label ID="lblPaymentName" runat="server" Text=""></asp:Label> </td>
    </tr>
    
    <tr><td class ="rcptstyle" align ="left" >Event Name</td><td class ="style2" >:</td>
    <td align ="left" style="width:150px;" class="responsestyle"  colspan="4">
    <asp:Label ID="lblEventName" runat="server" Text=""></asp:Label></td>
    </tr>
    
    <tr><td class ="rcptstyle" align ="left" >Fees</td><td class ="style2" >:</td>
    <td align ="left" colspan="4">
    <asp:Label ID="eCurrency" runat="server" Text=""></asp:Label> &nbsp;<asp:Label ID="Amount" runat="server" Text=""></asp:Label> </td></tr>
    
    <%--<tr><td  class ="rcptstyle"align ="left" >Amount</td><td class ="style2" >:</td>
    <td align ="left" class ="receiptstyle" colspan="4">
        &nbsp;</td></tr>--%>
    
    <tr><td class ="rcptstyle" align ="left" >
    <asp:Label ID="lblErrorDescTitle" Visible ="false" Font-Size ="10pt" Font-Names="tahoma,arial" runat="server" Text="Error Desc"></asp:Label></td><td class ="style2" >
    <asp:Label ID="lblDot" Visible ="false" Font-Size ="8pt" Font-Names="tahoma,arial" runat="server" Text=":"></asp:Label></td>
    <td align ="left" style="height :30px; font-family: Verdana; font-size :10pt;" colspan="4">
    <asp:Label ID="ErrDesc" Font-Size ="10pt" Font-Names="tahoma,arial" runat="server" Text=""></asp:Label>
    <%--<textarea id ="ErrDesc" readonly ="readonly" runat="server" cols="80" name="elm1" rows="20" style="width: 250px; height:60px; "></textarea>--%>
    </td></tr>
    
    <tr><td class ="rcptstyle" align ="left" >&nbsp;</td><td class ="style2" >&nbsp;</td>
    <td align ="left"style="height :30px; font-family: Verdana; font-size :10pt;" colspan="4">
    <asp:Label ID="Remark" runat="server" Visible ="false" style="width: 250px; height:60px;" Text=""></asp:Label> 
    <%--<textarea id ="Remark" readonly ="readonly" runat="server" cols="80" name="elm1" rows="20" style="width: 250px; height:60px;"></textarea>--%>
    </td></tr>
    <%--<tr><td class ="style1" align ="left" >TransId</td><td class ="style2" >:</td>
    <td align ="left" colspan="4">
    <asp:Label ID="TransId" runat="server" Text=""></asp:Label> </td></tr>
    <tr><td class ="style1" align ="left" >AuthCode</td><td class ="style2" >:</td>
    <td align ="left" colspan="4">
    <asp:Label ID="AuthCode" runat="server" Text=""></asp:Label> </td></tr>--%>
    
    
    
    <tr><td colspan="6" >
    <%--<asp:Label ID="Signature" runat="server" Text=""></asp:Label>--%> 
    <asp:HiddenField ID="Signature" runat="server" />
    <asp:HiddenField ID="eStatus" runat="server" />
    <asp:HiddenField ID="MerchantCode" runat="server" />
    <asp:HiddenField ID="PaymentId" runat="server" />
    
    </td></tr>
    </table>    
    </td></tr>

    <tr><td align="center"> <span  style =" font-family :Verdana ; font-size :6pt;">
    <br /><br /><br />
    Mailing address:
Malaysian Society of Ophthalmology 
TST 26, <br />
Pejabat Pos Damansara Jaya, 
Selangor D.E., 
Malaysia. 
<br /><br />
Office:
Malaysian Society of Ophthalmology 
40a, Jalan SS21/58, <br />
Damansara Utama, 
47400 Petaling Jaya, 
Selangor D. E.,
Malaysia
<br /><br />

Email: msophth@gmail.com
Phone Number : 012-2403318 
Tel / fax: 03-77107282<br /><br />

</span>
    </td> </tr> 
    
    </table>
    
   </td>
<%--<td align="left" style=" background-image:url(../images/box_right_h.png); width :20px; background-repeat:repeat-y;">
    &nbsp;</td>--%>
</tr>

<%--<tr>
     <td style=" background-image:url(../images/box_left_btm.png); width :20px; background-repeat:no-repeat;">
         &nbsp;</td>
     <td style=" background-image:url(../images/box_line_vec2.png); background-repeat:repeat-x;">&nbsp;
     </td>
     <td align="left" style=" background-image:url(../images/box_right_btm.png); width :20px; background-repeat:no-repeat;">
         &nbsp;</td>
</tr> --%>


    </table>
    </td></tr>
    
    </table>

    
    </div>
    </form>
</body>
</html>