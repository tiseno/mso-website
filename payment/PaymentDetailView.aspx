﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentDetailView.aspx.cs" Inherits="PaymentDetailView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Online Payment</td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :10px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="80%" >
    <tr><td colspan ="6" class ="formheader"align ="center" >User Information</td></tr>
    
    <tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
    
    <tr><td align ="left" class="style1" >Name</td><td class="style2">:</td>
    <td  align ="left" class="style3">
    <asp:Label ID="txtName" runat="server" Text=""></asp:Label> </td>
    <td align ="left" class="style1" >Date</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:Label ID="txtDate" runat="server" Text=""></asp:Label></td></tr> 
    
    <tr><td align ="left" class="style1" >Designation</td><td class="style2">:</td>
    <td class ="style3 " align ="left" >
    <asp:Label ID="txtDesignation" runat="server"  Text=""></asp:Label> </td>
    
    <td align ="left" class="style1" >IC/Passport No</td><td class="style2">:</td>
    <td align ="left" >
    <asp:Label ID="txtIC" runat="server"  Text=""></asp:Label>
    </tr> 
    
    <tr></tr> 
    
    <tr><td align ="left" class="style1" >Hospital/Clinic/Place of Practice</td>
        <td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <asp:Label ID="txtplcPractice" runat="server"  Text=""></asp:Label> </td></tr> 
    
    <tr><td align ="left" class="style1" >Address</td><td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <textarea id ="txtAddress" runat="server" cols="80" name="elm1" rows="20" style="width: 250px;height:60px; "></textarea> 
     </td></tr>
    
    
    <tr><td align ="left" class="style1" >State</td><td class="style2">:</td>
    <td class="style3" align ="left" >
    <asp:Label ID="txtState" runat="server"  Text=""></asp:Label>  </td>
    
    <td align ="left" class="style1" >Poscode</td>
    <td class="style2">:</td>
    <td align ="left" >
    <asp:Label ID="txtPoscode" runat="server"  Text=""></asp:Label> </td>
    </tr> 
    
    <tr><td align ="left" class="style1" >City</td><td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <asp:Label ID="txtCity" runat="server"  Text=""></asp:Label> </td></tr> 
    
    <tr><td align ="left" class="style1" >Hand Phone Number</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:Label ID="txthpno" runat="server"  Text=""></asp:Label> </td>
    
    <td align ="left" class="style1" >Email Address</td>
    <td class="style2">:</td>
    <td  align ="left" >
    <asp:Label ID="txtemail" runat="server"  Text=""></asp:Label> </td></tr> 
    
    <tr><td colspan ="6" class ="formheader"align ="center" ><br />Payment Information</td></tr>
    
    <tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
      
      <tr><td align ="left" class="style1" >Event</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:Label ID="lblevent" runat="server" Text=""></asp:Label>
    </td>
    <td align ="left" class="style1" ></td><td class="style2"></td>
    <td align ="left" >
        <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
    </td>
    </tr> 
      
      <tr><td align ="left" class="style1" >Reference Code</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:Label ID="txtReferenceCode" runat="server" Text=""></asp:Label>
    </td>
    <td align ="left" class="style1" >Receipt Code</td><td class="style2">:</td>
    <td align ="left" >
        <asp:Label ID="txtReceiptCode" runat="server" Text=""></asp:Label>
    </td>
    </tr> 
    
    <tr><td align ="left" class="style1" >Payment Amount</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:Label ID="txtpaymentamount" runat="server"  Text=""></asp:Label>  </td>
    <td align ="left" class="style1" >Payment Type</td><td class="style2">:</td>
    <td align ="left" >
        <asp:Label ID="txtpaymentoption" runat="server" Text="Credit Card"></asp:Label><br />
    </td>
    </tr> 
 
    
    </table> 
    </td></tr>
    
    
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>
      
      <tr><td align="center">
      <table style ="padding:10px 10px 10px 10px; width :80%;">
      <tr><td align ="left" style =" padding-left :10px;" >
      <asp:Button ID="btnback" runat="server" Text="Back" onclick="btnback_Click" /></td>
      <td align ="right" style =" padding-right :10px;">
      <asp:Button ID="BtnEdit" runat="server" Text="Edit" onclick="BtnEdit_Click" /></td></tr></table>
          
      </td></tr>
      
   </table>
    
    
    </div>
    </form>
</body>
</html>
