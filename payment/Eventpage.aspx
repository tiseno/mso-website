﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Eventpage.aspx.cs" Inherits="Eventpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    <tr><td><table style="width:100%; background-color :#d9d6d6;  height:20px; " align="center">
      <tr><td style="color:#323232;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">
          Event Search </td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :10px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%" >
    <%--<tr><td  class ="formheader"align ="left" >Search Event</td></tr>--%>
    
    <%--<tr><td ><table style="width:100%; height:2px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; " align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <tr><td align ="left">
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="40%" >
      
      <tr><td align ="left" class="style1" >Event Title</td><td class="style2">:</td>
      <td  align ="left">
      <asp:TextBox ID="txtEventTitle" Width="200px" runat="server"></asp:TextBox></td>
      </tr> 
      
      <%--<tr><td align ="left" class="style1" >Currency</td><td class="style2">:</td>
      <td  align ="left">
      <asp:DropDownList ID="txtCurrency" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
      <asp:Label ID="lblCurrency" runat="server" Visible ="false"  Text=""></asp:Label>
      </td></tr> --%>
      
      <%--<tr><td align ="left" class="style1" >Date</td><td class="style2">:</td>
      <td  align ="left">
      <asp:TextBox ID="txtDate" Width="200px" runat="server"></asp:TextBox>
      </td>
      </tr>--%> 
      
      <%--<tr><td align ="left" class="style1" >Receipt No</td><td class="style2">:</td>
      <td  align ="left">
      <asp:TextBox ID="txtReceiptNo" Width="200px" runat="server"></asp:TextBox></td>
      </tr>--%> 
      
      <tr><td align ="left" class="style1" >Status</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <asp:RadioButtonList ID="radioStatus" Runat="server" Font-Size="8pt" Font-Names="Verdana"  OnSelectedIndexChanged="Status_editchange" BorderWidth="0" CellPadding="0" CellSpacing="0" RepeatLayout="flow" RepeatColumns="3">
            <asp:ListItem Text="Yes" Value="true" ></asp:ListItem>
            <asp:ListItem Text="No" Value="false"></asp:ListItem>
        </asp:RadioButtonList>   
        <asp:Label ID="lblStatus" runat="server" Visible ="false"  Text=""></asp:Label> 
      </td>
      </tr> 
      
      <tr><td align ="left" colspan ="3" style ="padding :10px 20px 10px 0px;">
      <asp:Button ID="btnsearch" runat="server" Text="Search" onclick="btnsearch_Click"/></td>
      </tr> 
      
      </table>
      </td></tr>
    
    <tr><td align ="left" >
    
 <asp:Datagrid ID="dg_SearchControl" runat="server" OnPageIndexChanged="dg_SearchControl_PageIndexChanged" Width ="60%" 
        AutoGenerateColumns="False" 
        BackColor="White" 
        BorderColor="#eeeeee" 
        BorderWidth="2px" 
        CellPadding="4" 
        ForeColor="Black" 
        ItemStyle-VerticalAlign="Top" 
        PageSize="10" 
        AllowPaging="true" 
        PagerStyle-Position="Bottom" 
        PagerStyle-Mode="NextPrev"
        ><%--tis is for paging--%>
        
            <FooterStyle BackColor="#CCCC99" />
            <SelectedItemStyle BackColor="#CE5D5A" Font-Size ="Small"  Font-Bold="False" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
            Mode="NumericPages" />
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#efefef" />
        
       <Columns>
        
            <asp:TemplateColumn HeaderText="No"><itemtemplate>
            <%#Container.ItemIndex + dg_SearchControl.CurrentPageIndex * dg_SearchControl.PageSize + 1%>
            </itemtemplate>
            <HeaderStyle  HorizontalAlign="Center"/>
            <ItemStyle HorizontalAlign="Center"/>
            </asp:TemplateColumn>

            <asp:TemplateColumn HeaderText="Event Title" Visible ="True" >
            <ItemTemplate>
            <asp:Hyperlink runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventTitle")%>' NavigateUrl='<%#URL(DataBinder.Eval(Container.DataItem, "EventID"))%>'  ID="Hyperlink1"/></ItemTemplate>
            </asp:TemplateColumn>

            <%--<asp:BoundColumn DataField="EventDescription" HeaderText="EventDescription"></asp:BoundColumn>--%>
            <%--<asp:BoundColumn DataField="ECurrency" HeaderText="Currency"></asp:BoundColumn>
            <asp:BoundColumn DataField="EFees" HeaderText="Fees"></asp:BoundColumn>--%>
            <asp:BoundColumn DataField="EStatus" HeaderText="Status"></asp:BoundColumn>
            
            
            
            </Columns>
            <HeaderStyle BackColor ="#d9d6d6" Font-Bold="false" Font-Size ="small" ForeColor="#323232" />
            </asp:Datagrid>   
    
    </td></tr>
    
    
    </table> 
    </td></tr>
    
    
    <%--<tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
     
      
   </table>
    
    </div>
    </form>
</body>
</html>
