﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;

public partial class ListEventSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            alllistEvent();
        }
    }

    protected void alllistEvent()
    {
        bizEvent bs = new bizEvent ();
        DataSet ds;

        ds = bs.SelectAllEvent ();
        dg_SearchControl.DataSource = ds;
        dg_SearchControl.DataBind();
    }

    protected string URL(object EventID)
    {
        return "EventView.aspx?EventID=" + EventID;
    }

    protected void dg_SearchControl_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dg_SearchControl.CurrentPageIndex = e.NewPageIndex;
        bizEvent  bsC = new bizEvent ();
        DataSet dsC;

        dsC = bsC.SelectAllEvent ();
        dg_SearchControl.DataSource = dsC;
        dg_SearchControl.DataBind();
    }
}
