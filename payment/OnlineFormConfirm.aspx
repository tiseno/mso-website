﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlineFormConfirm.aspx.cs" Debug="true" Inherits="OnlineFormConfirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSO</title>
     <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" >
    <div>
    
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    
    <tr><td align ="center" style ="padding:10px 0px 10px 0px;">
    <table style="width:68%;"><tr><td align ="center"><img src="../images/logo.jpg" /></td></tr></table>
    </td></tr>
    
    <%--<tr><td><table style="width:100%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 130px" align="left"></td></tr></table> 
      </td></tr>--%>
      
      <tr><td align ="center" >
    <table border="0" width="100%;" style="background-color:#2a718d; color:White;">
    <tr><td align ="left" style ="color:White ; font-family :tahoma,arial ; font-size :10pt; font-weight :bold ; padding-left :255px;">3rd Annual Scientific Meeting </td></tr></table>
    </td></tr>
      
    <%--<tr><td align ="center" style ="background-color:#E0EBF1; padding:20px 10px 10px 10px;">--%>
    <tr><td align ="center" style ="padding:0px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="60%" >
    
    
    
    <%--<tr>
	 <td align="right" style="width:10px;"><img src="../images/box_left_top.png" /></td>
	 <td style="background-image:url(../images/box_line_vec.png); background-repeat:repeat-x;"></td>
	 <td align="left" style="width:10px;"><img src="../images/box_right_top.png" /></td>
</tr>
--%>
<tr>
<%--<td style="background-image:url(../images/box_left_h.png); background-repeat:repeat-y"></td>--%>
<td style="background-image:url(../images/content_white.png); background-repeat:repeat;" align="center">

<table border="0" width="95%" cellpadding ="0" cellspacing ="0">


    <tr><td colspan ="6" class ="formheader"align ="left" ><br />Payment Detail</td></tr>

    
    <tr><td colspan ="6" class ="formheader"align ="left" ><br />Event</td></tr>
    
    <%--<tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <tr><td align ="left" class="style1" >Event Title</td><td class="style2">:</td>
    <td align ="left">
    <asp:HiddenField ID="lblEventTitle" runat="server" />
    <asp:Label ID="lblEventTitlelbl" runat="server" Text=""></asp:Label>  
   <%-- <asp:TextBox ID="lblEventTitle" BorderStyle="None" ReadOnly ="true" runat="server"></asp:TextBox>--%>
    <asp:Label ID="lblEventID" runat="server" Visible ="false"  Text=""></asp:Label>
    </td>
    
    <td align ="left" class="style1" ></td>
    <td class="style2"></td>
    <td  align ="left" ><asp:HiddenField ID="RefNo" runat="server"  />
    <%--<asp:TextBox ID="RefNo" BorderStyle="None" ReadOnly ="true" runat="server"></asp:TextBox>--%></td>
    </tr>
    
    <tr><td align ="left"  class="style1" >Event Description</td><td class="style2">:</td>
    <td align ="left" colspan ="6">
    <asp:HiddenField ID="ProdDesc" runat="server" />
    <asp:Label ID="lblProdDesc" runat="server" Text=""></asp:Label>  
    <%--<textarea id ="ProdDesc" readonly ="readonly" runat="server" cols="80" name="elm1" rows="20" style="width: 250px; font-family:Verdana; font-size:10pt; border-style: none; overflow: hidden; height:20px;"></textarea>--%>
    </td></tr>
     
    <tr><td align ="left" class="style1" >Fees</td><td class="style2">:</td>
    <td align ="left" colspan ="6">
    <asp:HiddenField ID="Currency" runat="server" />
    <asp:Label ID="lblCurrency" runat="server" Text=""></asp:Label> 
        <%--<asp:TextBox ID="Currency" BorderStyle="None" ReadOnly ="true"  runat="server"></asp:TextBox>--%>&nbsp;
    <asp:Label ID="lblAmount" runat="server" Text=""></asp:Label> 
    <asp:HiddenField ID="Amount" runat="server" />
    <asp:Label ID="txtpaymentoption" Visible ="false" runat="server" Text="Credit Card"></asp:Label>
    </td></tr>
    
    <%--<tr>
    <td align ="left" class="style1" ></td><td class="style2"></td>
    <td align ="left" colspan ="6">
    </td>
    </tr>--%>
    
    <%--<tr><td align ="left" class="style1" >&nbsp;</td><td class="style2">&nbsp;</td>
    <td align ="left" class="style3" colspan="6">
    </td>
    <br />
    </tr> --%>
    
    <tr><td colspan ="6" class ="formheader"align ="left" ><br /><br />User Information</td></tr>
    
    <%--<tr><td colspan ="6"><table class="Dline"align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
    
    <tr><td align ="left" class="style1" >Name</td><td class="style2">:</td>
    <td  align ="left" class="style3">
    <asp:HiddenField ID="UserName" runat="server" />
    <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="UserName" BorderStyle="None" Width="200px" ReadOnly ="true" runat="server" ></asp:TextBox>--%>
    
    </td>
    <td align ="left" class="style1" >Date</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:Label ID="txtDate" runat="server" Text=""></asp:Label></td></tr> 
    
    <tr><td align ="left" class="style1" >Designation</td><td class="style2">:</td>
    <td class ="style3 " align ="left" >
    <asp:HiddenField ID="txtDesignation" runat="server" />
    <asp:Label ID="lbltxtDesignation" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="txtDesignation" BorderStyle="None" ReadOnly="true" Width="200px" runat="server" ></asp:TextBox>--%>
    <asp:Label ID="lblDesignation" runat="server" Visible ="false"  Text=""></asp:Label> </td>
    
    <td align ="left" class="style1" >IC/Passport No</td><td class="style2">:</td>
    <td align ="left" >
    <asp:HiddenField ID="txtIC" runat="server" />
    <asp:Label ID="lbltxtIC" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="txtIC" BorderStyle="None" ReadOnly="true" runat="server"></asp:TextBox>--%></td>
    </tr> 
    
    <tr><td align ="left" class="style1" >Place of Practice</td>
        <td class="style2" >:</td>
    <td colspan ="6" align ="left" >
    <asp:HiddenField ID="txtplcPractice" runat="server" />
    <asp:Label ID="lbltxtplcPractice" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="txtplcPractice" BorderStyle="None" ReadOnly="true" Width="200px" runat="server"></asp:TextBox>--%></td></tr> 
    
    <tr><td align ="left" style="padding:0px 0px 0px 0px" valign="top" rowspan ="2">Address</td>
    <td class="style2" valign="top" rowspan ="2">:</td>
    <td align ="left" rowspan ="2">
      <asp:HiddenField ID="txtAddress" runat="server" />
    <asp:Label ID="lbltxtAddress" runat="server" Text=""></asp:Label> 
    <%--<textarea id ="txtAddress" runat="server" readonly="readonly" cols="80" name="elm1" rows="20" style="width: 250px; border-style: none; overflow:hidden; height:60px; "></textarea> --%>
    </td>
    
    <td align ="left" class="style1" >State</td><td class="style2">:</td>
    <td align ="left" >
    <asp:HiddenField ID="txtState" runat="server" />
    <asp:Label ID="lbltxtState" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="txtState" BorderStyle="None" ReadOnly="true" runat="server"></asp:TextBox>--%></td>
    </tr>
    
    
    <tr>    
    
    <td align ="left" class="style1" >City</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:HiddenField ID="txtCity" runat="server" />
    <asp:Label ID="lbltxtCity" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="txtCity" BorderStyle="None" ReadOnly="true" runat="server"></asp:TextBox>--%></td>
    </tr> 
    
    <tr>
    <td align ="left" class="style1" >Hand Phone No.</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:HiddenField ID="UserContact" runat="server" />
    <asp:Label ID="lblUserContact" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="UserContact" BorderStyle="None" ReadOnly="true" runat="server"></asp:TextBox>--%></td>
    
    
    <td align ="left" class="style1" >Poscode</td>
    <td class="style2">:</td>
    <td align ="left" >
    <asp:HiddenField ID="txtPoscode" runat="server" />
    <asp:Label ID="lbltxtPoscode" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="txtPoscode" BorderStyle="None" ReadOnly="true" runat="server"></asp:TextBox>--%></td>
    </tr> 
    
    <tr>
    
    <td align ="left" class="style1" >Email Address</td>
    <td class="style2">:</td>
    <td  align ="left" colspan="6">
    <asp:HiddenField ID="UserEmail" runat="server" />
    <asp:Label ID="lblUserEmail" runat="server" Text=""></asp:Label> 
    <%--<asp:TextBox ID="UserEmail" BorderStyle="None" ReadOnly="true" runat="server"></asp:TextBox>--%><br /></td></tr> 
    
    <tr><td align="left" colspan="6">
      <table border="0" cellpadding="0" cellspacing="0" style ="padding:0px 10px 10px 0px; width :80%;">
      <tr><td align ="left"><br /><br />
      <asp:Button ID="Btnsubmit" runat="server" Text="Proceed To Payment" PostBackUrl="https://www.mobile88.com/ePayment/entry.asp" onclick="Btnsubmit_Click" /></td></tr></table>
      <%--PostBackUrl="http://localhost:4756/MSOonline/response.aspx"--%>
          <%--PostBackUrl="http://localhost/msotest/msotestpostback.php"--%><%-- --%>
      </td></tr>
    
    <tr><td colspan ="6"><table>
    <tr>
    <td align ="left" class="style3">
    <asp:HiddenField ID="MerchantCode" runat="server" Value="M04062" />
    <asp:HiddenField ID="PaymentId" runat="server" />
    <asp:HiddenField ID="Remark" runat="server" />
    <asp:HiddenField ID="Lang" runat="server" Value="ISO-8859-1" />
    <asp:HiddenField ID="ResponseURL" runat="server" Value="http://www.mso.org.my/payment/response.aspx" />
    <asp:HiddenField ID="MerchantKey" runat="server" Value="OXxi6oAcBB" />
    <asp:HiddenField ID="Signature" runat="server"/>
    
    </td>

    
    </tr> 
    </table></td></tr>
    
    </table> 
    
    
    </td>
<%--<td style="background-image:url(../images/box_right_h.png); background-repeat:repeat-y;"></td>--%>
</tr>

<%--<tr>
     <td><img src="../images/box_left_btm.png" /></td>
     <td style="background-image:url(../images/box_line_vec2.png); background-repeat:repeat-x;"></td>
     <td align="left"><img src="../images/box_right_btm.png" /></td>
</tr>--%>


   
    </table> 
    </td></tr>
    
    
      
      
   </table>
    
    </div>
    </form>
</body>
</html>
