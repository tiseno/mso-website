﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;

public partial class PaymentDetailView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            DateTime Now = DateTime.Now;
            txtDate.Text = Now.ToShortDateString();

            
            ViewDetail();
            
        }

    }

    protected void ViewDetail()
    {
        bizpaymentform bs = new bizpaymentform();
        DataSet ds;
        ds = bs.SelectPaymentDetail(Request.QueryString["UserDetailID"]);

        if (ds.Tables[0].Rows.Count > 0)
        {

            txtName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();
            txtDesignation.Text = ds.Tables[0].Rows[0]["Designation"].ToString();
            txtIC.Text = ds.Tables[0].Rows[0]["IC_Passport_No"].ToString();
            txtplcPractice.Text = ds.Tables[0].Rows[0]["Place_Practice"].ToString();

            txtAddress.Value = ds.Tables[0].Rows[0]["User_Address"].ToString();
            txtCity.Text = ds.Tables[0].Rows[0]["City"].ToString();
            txtState.Text = ds.Tables[0].Rows[0]["State"].ToString();

            txtPoscode.Text = ds.Tables[0].Rows[0]["PostCode"].ToString();
            txtemail.Text = ds.Tables[0].Rows[0]["User_Email"].ToString();
            txthpno.Text = ds.Tables[0].Rows[0]["Hand_Phone_No"].ToString();
            txtpaymentamount.Text = ds.Tables[0].Rows[0]["Payment_Amount"].ToString();

            txtpaymentoption.Text = ds.Tables[0].Rows[0]["Payment_Option"].ToString();
            txtReferenceCode.Text = ds.Tables[0].Rows[0]["Reference_ID"].ToString();
            txtReceiptCode.Text = ds.Tables[0].Rows[0]["Receipt_No"].ToString();
            txtDate.Text = ds.Tables[0].Rows[0]["PaymentDate"].ToString();
            lblevent.Text = ds.Tables[0].Rows[0]["UserDetail.EventTitle"].ToString();
        }
    }
  

    protected void BtnEdit_Click(object sender, EventArgs e)
    {
        string UserDetailID = Request.QueryString["UserDetailID"];
        Response.Redirect("PaymentDetailEdit.aspx?UserDetailID=" + UserDetailID );
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("ListAllPayment.aspx");
    }
}
