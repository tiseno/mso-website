﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="admin_Control.aspx.cs" Inherits="admin_Control" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="en" />
	<meta name="robots" content="noindex,nofollow" />
    <title>Admin Control Panel</title>
    
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../css/reset.css" /> <!-- RESET -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../css/main.css" /> <!-- MAIN STYLE SHEET -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../css/2col.css" title="2col" /> <!-- DEFAULT: 2 COLUMNS -->
	<link rel="alternate stylesheet" media="screen,projection" type="text/css" href="../css/1col.css" title="1col" /> <!-- ALTERNATE: 1 COLUMN -->
	<!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]--> <!-- MSIE6 -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../css/style2.css" /> <!-- GRAPHIC THEME -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../css/mystyle.css" /> <!-- WRITE YOUR CSS CODE HERE -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/switcher.js"></script>
	<script type="text/javascript" src="../js/toggle.js"></script>
	<script type="text/javascript" src="../js/ui.core.js"></script>
	<script type="text/javascript" src="../js/ui.tabs.js"></script>    
    <script type="text/javascript">
        $(document).ready(function() {
            $(".tabs > ul").tabs();
        });
    </script>

    <script language="javascript" type="text/javascript">
    <!--
        function autoResize(id) {
            var newheight;
            var newwidth;

            if (document.getElementById) {
                newheight = document.getElementById(id).contentWindow.document.body.scrollHeight;
                newwidth = document.getElementById(id).contentWindow.document.body.scrollWidth;
            }

            document.getElementById(id).height = (newheight) + "px";
            document.getElementById(id).width = (newwidth) + "px";
        }
    //-->
    </script>    
</head>

<body>
<form id="form1" runat="server">
<div id="main">

	<!-- Tray -->
	<div id="tray" class="box">
	<p class="f-right"><strong><%--Hi--%> &nbsp;<asp:Label ID="display_name"  Visible="false" runat="server" Text=""></asp:Label></strong>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        
        <%--Language Version:--%><strong>
                <asp:LinkButton ID="Link_English"  Visible="false" runat="server"><%--English--%></asp:LinkButton></strong>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><asp:LinkButton ID="Link_Logout" 
                runat="server" Visible="false" onclick="Link_Logout_Click"><%--Logout--%></asp:LinkButton></strong></p>
    </div> <!--  /tray -->

	<hr class="noscreen" />

	<!-- Menu -->
	<div id="menu" class="box">

		<ul class="box f-right">
			<li></li>
		</ul>

		<ul class="box">
			<li id="menu-active"></li> <!-- Active -->
		</ul>
        </div> <!-- /header -->

	<hr class="noscreen" />

	<!-- Columns -->
	<div id="cols" class="box">

		<!-- Aside (Left Column) -->
		<div id="aside" class="box">

			<div class="padding box" align="center">

				<!-- Logo (Max. width = 200px) -->
				<br />
				<p id="logo"><img src="../images/smalllogo.jpg" border="0" alt="mso" /></p>

                <!-- Search -->
				<!-- Create a new project -->
<%--                <table><tr><td ><p id="btn-create"><a href="#" target="_blank" s>&nbsp;&nbsp;&nbsp;Website 
                    Preview</a></p></td></tr></table>--%>
                    
                    <%--<table id="DealerImage" runat="server" visible="false"><tr>
                    <td ><img  src="images/user.gif" /></td><td></td>
                    </tr>
                    </table>--%>
                    
                    <%--resize the width here--%>
                    <%--<table id="DealerDetail" runat="server" visible="false" width="195px" cellspacing="0" cellpadding="0">
                    <tr>
                    <td align="left" >Username</td>--%>
                    <%--resize the width here--%>
                    <%--<td style="width:30px;">:</td><td align="left">
                    <asp:Label ID="lbl_username" runat="server" Text=""></asp:Label>
                    <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label></td>
                    </tr>
                    <tr>
                    <td align="left">State</td><td>:</td>
                    <td align="left"><asp:Label ID="lbl_state" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                    <td align="left">City</td><td>:</td>
                    <td align="left"><asp:Label ID="lbl_city" runat="server" Text=""></asp:Label>
                    <asp:Label ID="Label2" runat="server" Text="" Visible="false"></asp:Label></td>
                    </tr>
                    </table>--%>
                    
                    </div><!-- /padding -->
			    <ul class="box">
				<li></li>
				<li id="submenu-active"><table><tr><td><a>Sitemap</a></td></tr></table>  <%--Active--%> 
					<ul>
				    <%--<li><asp:HyperLink ID="HyperLink6" runat="server" target="main" NavigateUrl="admin_Main.aspx"  Visible="false" >Control Panel</asp:HyperLink></li>--%>
				    <li><asp:HyperLink ID="HyperLink7" runat="server" target="main" NavigateUrl="Eventadd.aspx"  Visible="true" >Add Event</asp:HyperLink></li>
				    <li><asp:HyperLink ID="HyperLink6" runat="server" target="main" NavigateUrl="EventTypeadd.aspx"  Visible="true" >Event Payment Type</asp:HyperLink></li>
					<li><asp:HyperLink ID="HyperLink1" runat="server" target="main" NavigateUrl="Eventpage.aspx" Visible="true" >Search Event</asp:HyperLink></li>
					<li><asp:HyperLink ID="HyperLink2" runat="server" target="main" NavigateUrl="ListAllPayment.aspx" Visible="true"  >Search Payment</asp:HyperLink></li>
					<li><asp:HyperLink ID="HyperLink3" runat="server" target="main" NavigateUrl="registration_form.aspx" Visible="false" >Student Registration Form</asp:HyperLink></li>
					<li><asp:HyperLink ID="HyperLink8" runat="server" target="main" NavigateUrl="import_excelFile.aspx" Visible="false" >Import via Excel Sheet</asp:HyperLink></li>
					<li><asp:HyperLink ID="HyperLink4" runat="server" target="main" NavigateUrl="search_Page.aspx?batchID=&stud_id=" Visible="false" >Search Manager</asp:HyperLink></li>
				    <li><asp:HyperLink ID="HyperLink5" runat="server" target="main" NavigateUrl="tmppage.aspx"  Visible="false" >Template Manager</asp:HyperLink></li>
				   
					</ul>
				</li>
			</ul>

		</div> <!-- /aside -->

		<hr class="noscreen" />

		<!-- Content (Right Column) -->
		<div id="content" class="box">

			<h1>Administration Control Panel</h1>

			<!-- Headings -->
            <p></p>
            
            <iframe id="AdminControl"  runat="server" name="main" frameborder="0" scrolling="yes" width="100%" height="970" ></iframe>
<%--src="dealerView2.aspx" visible="false"--%>
            </div> <!-- /content -->

	</div> <!-- /cols -->

	<hr class="noscreen" />

	<!-- Footer -->
	<div id="footer" class="box">

		<p class="f-left">&copy; 2011 <a href="#">AMI</a>, All Rights Reserved &reg;</p>

	</div> <!-- /footer -->

</div> <!-- /main -->
</form>
</body>
</html>
