﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentDetailEdit.aspx.cs" Inherits="PaymentDetailEdit" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
<link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
<script type="text/javascript" src="../js/txtvalidation.js" ></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    
    <%--<tr><td align ="center" style ="padding:10px 10px 10px 10px;">
    <table style="width:80%;"><tr><td align ="left"><img src="images/logo.jpg" /></td></tr></table>
    </td></tr>--%>
    <tr><td><table border ="0" cellpadding ="0" cellspacing ="0" style="width:100%; background-color :#d9d6d6; height:20px;" align="center">
      <tr><td style="color:#323232; font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left">Payment Detail</td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :10px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%" >
    <tr><td colspan ="6" class ="formheader"align ="left" >User Information</td></tr>
    
    <%--<tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      <tr><td colspan ="6"><table style="width:100%; height:1px; background-color: Gray;" align="center">
      <tr><td style="color:#323232;font-size:9pt;font-weight:bold;" align="left"></td></tr></table>  <br />
      </td></tr>
    
    <tr><td align ="left" class="style1" >Name</td><td class="style2">:</td>
    <td  align ="left" class="style3">
    <asp:TextBox ID="UserName" Width="200px" runat="server"></asp:TextBox></td>
    <td align ="left" class="style1" >Date</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
    <%--<asp:Label ID="txtDate" runat="server" Text=""></asp:Label>--%></td></tr> 
    
    <tr><td align ="left" class="style1" >Designation</td><td class="style2">:</td>
    <td class ="style3 " align ="left" >
    <asp:DropDownList ID="txtDesignation" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
    <%--<asp:Label ID="lblDesignation" runat="server" Visible ="false"  Text=""></asp:Label>--%>
    <asp:HiddenField ID="lblDesignation" runat="server"  /> </td>
    
    <td align ="left" class="style1" >IC/Passport No</td><td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtIC" runat="server"></asp:TextBox></td>
    </tr> 
    
    <tr><td align ="left" class="style1" >Place of Practice</td>
        <td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <asp:TextBox ID="txtplcPractice" Width="200px" runat="server"></asp:TextBox></td></tr> 
    
    <tr><td align ="left" class="style1" >Address</td><td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <textarea id ="txtAddress" runat="server" cols="80" name="elm1" rows="20" style="width: 250px;height:60px; "></textarea> 
    <%--<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>--%></td></tr>
    
    
    <tr><td align ="left" class="style1" >State</td><td class="style2">:</td>
    <td class="style3" align ="left" >
    <asp:TextBox ID="txtState" runat="server"></asp:TextBox></td>
    
    <td align ="left" class="style1" >Poscode</td>
    <td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtPoscode" runat="server"></asp:TextBox></td>
    </tr> 
    
    <tr><td align ="left" class="style1" >City</td><td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td></tr> 
    
    <tr><td align ="left" class="style1" >Hand Phone Number</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:TextBox ID="UserContact" MaxLength ="13" runat="server"></asp:TextBox> 
    
          <asp:Label ID="ErP" runat="server" style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" Font-Bold="true" Text="Invalid Value!"></asp:Label> 
                                </td>
    
    <td align ="left" class="style1" >Email Address</td>
    <td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="UserEmail" runat="server"></asp:TextBox> 
    
                                        </td></tr>
                                        
                                        
                                        
    <tr><td align ="left" class="style1" >&nbsp;</td><td class="style2">&nbsp;</td>
    <td align ="left" class="style3">
        &nbsp;</td>
    
    <td align ="left" class="style1" ></td>
    <td class="style2"></td>
    <td  align ="left" >
    
          <asp:Label ID="ErPemail" runat="server" 
            style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
            Font-Bold="true" Text="Invalid e-mail Address!"></asp:Label> 
    
     </td></tr> 
                                         
    
    <tr><td colspan ="6" class ="formheader"align ="left" ><br />Payment Information</td></tr>
    
    <%--<tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      <tr><td colspan ="6"><table style="width:100%; height:1px; background-color: Gray ;" align="center">
      <tr><td style="color:#323232;font-size:9pt;font-weight:bold;" align="left"></td></tr></table>  <br />
      </td></tr>
      
      <tr><td align ="left" class="style1" >Event</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:Label ID="lblevent" runat="server" Text=""></asp:Label>
    </td>
    <td align ="left" class="style1" >Receipt Code</td><td class="style2">:</td>
    <td align ="left" >
        <asp:Label ID="txtReceiptCode" runat="server" Text=""></asp:Label>
    </td>
    </tr> 
      
      <tr><td align ="left" class="style1" >Reference Code</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:Label ID="txtReferenceCode" runat="server" Text=""></asp:Label>
    </td>
    
    <td align ="left" class="style1" >Payment Status</td><td class="style2">:</td>
    <td align ="left" >
        <asp:Label ID="lblPaymentStatus" runat="server" Text=""></asp:Label>
    </td>
    </tr> 
    
    <tr><td align ="left" class="style1" >Payment Amount</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:Label ID="amount" runat="server" Text=""></asp:Label>
    <%--<asp:TextBox ID="txtpaymentamount" runat="server"></asp:TextBox>--%></td>
    <td align ="left" class="style1" >Payment Type</td><td class="style2">:</td>
    <td align ="left" >
        <asp:Label ID="txtpaymentoption" runat="server" Text="Credit Card"></asp:Label><br />
    </td>
    </tr> 
    
    </table> 
    </td></tr>
    
    
    <%--<tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <tr><td align="center">
      <table style ="padding:10px 10px 10px 10px; width :100%;">
      <tr>
      <%--<td align ="left" style =" padding-left :10px;" >
      <asp:Button ID="btnback" runat="server" Text="Back" onclick="btnback_Click"/></td>--%>
      
      <td align="left" style =" padding-right :10px;">
      <asp:Button ID="Btnupdate" runat="server" Text="Update" OnClientClick ="return validateForm(this);" onclick="Btnupdate_Click" />
          <asp:Button ID="BtnDelete" runat="server" Visible ="false" Text="Delete" onclick="BtnDelete_Click"/>
          </td></tr></table>
          
      </td></tr>
      
   </table>
    </div>
    </form>
</body>
</html>
