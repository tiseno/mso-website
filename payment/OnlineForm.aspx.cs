﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;


public partial class payment_OnlineForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack )
        {
            
            DateTime Now = DateTime.Now;
            string str = Now.ToString("dd/MM/yyyy");
            string FullDateTime = Now.ToString("dd/MM/yyyy HH:mm:ss tt");
            HideDateTime.Value = FullDateTime;
            txtDate.Text = str;

            dropdownlistState();
            dropdownlist();
            ShowDropDownEvent();
            //refCode();
            

        }
        
    }

    protected void radioEditEventTypeChange(object sender, EventArgs e)
    {
        bizEvent bevent = new bizEvent();
        DataSet ds;
        ds = bevent.SelectDetailEventTypeB(radioEventType.SelectedValue);

        if (ds.Tables[0].Rows.Count > 0)
        {
            ProdDesc.Text = ds.Tables[0].Rows[0]["EventDescription"].ToString();
            Currency.Text = ds.Tables[0].Rows[0]["ECurrency"].ToString();
            Amount.Text = ds.Tables[0].Rows[0]["EFees"].ToString();
            //lblEventID.Value = ds.Tables[0].Rows[0]["EventID"].ToString();
            lblEventTitle.Value = ds.Tables[0].Rows[0]["EventTitle"].ToString();
        }
    }

    protected void radioSelectedEvent()
    {
    }

    protected void Prod_EditChanged(object sender, EventArgs e)
    {
        lblDesignation.Value = txtDesignation.SelectedValue;
        if (lblDesignation.Value != "0")
        {
            ErP8.Visible = false;
        }
        else if (lblDesignation.Value == "0")
        {

            ErP8.Visible = true;

        }

    }

    protected void dropdownlistState()
    {
        txtState.Items.Clear();
        txtState.Items.Add(new ListItem("-------Please Select-------", "0"));
        txtState.Items.Add(new ListItem("Johor", "Johor"));
        txtState.Items.Add(new ListItem("Kelantan", "Kelantan"));
        txtState.Items.Add(new ListItem("Kedah", "Kedah"));
        txtState.Items.Add(new ListItem("Wilayah Persekutuan", "Wilayah Persekutuan"));
        txtState.Items.Add(new ListItem("Melaka", "Melaka"));
        txtState.Items.Add(new ListItem("Negeri Sembilan", "Negeri Sembilan"));
        txtState.Items.Add(new ListItem("Perak", "Perak"));
        txtState.Items.Add(new ListItem("Perlis", "Perlis"));
        txtState.Items.Add(new ListItem("Pulau Pinang", "Pulau Pinang"));
        txtState.Items.Add(new ListItem("Selangor", "Selangor"));
        txtState.Items.Add(new ListItem("Sabah	", "Sabah"));
        txtState.Items.Add(new ListItem("Sarawak", "Sarawak"));
        txtState.Items.Add(new ListItem("Terengganu", "Terengganu"));
        txtState.Items.Add(new ListItem("Pahang", "Pahang"));
        txtState.Items.Add(new ListItem("Overseas", "Overseas"));

    }

    protected void State_EditChanged(object sender, EventArgs e)
    {
        lblState.Value = txtState.SelectedValue;
        
    }

    protected void dropdownlist()
    {
        txtDesignation.Items.Clear();
        txtDesignation.Items.Add(new ListItem("-----------Please Select-----------", "0"));
        txtDesignation.Items.Add(new ListItem("Trainee", "Trainee"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Govt/University)", "Ophthalmologist(Govt/University)"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Private)", "Ophthalmologist(Private)"));
        txtDesignation.Items.Add(new ListItem("Eyecare Professional", "Eyecare_Professional"));
    }

    protected void Event_EditChanged(object sender, EventArgs e)
    {
        Currency.Text = "";
        Amount.Text = "";
        ProdDesc.Text = "";

        string dropEvent = DropDownListEvent.SelectedValue;
        if (dropEvent != "0")
        {
            bizEvent bevent = new bizEvent();
            DataSet ds;
            ds = bevent.SelectDetailevent(dropEvent);

            if (ds.Tables[0].Rows.Count > 0)
            {
                //Currency.Text = ds.Tables[0].Rows[0]["ECurrency"].ToString();
                //Amount.Text = ds.Tables[0].Rows[0]["EFees"].ToString();
                lblEventID.Value = ds.Tables[0].Rows[0]["EventID"].ToString();
                lblEventTitle.Value = ds.Tables[0].Rows[0]["EventTitle"].ToString();
                //ProdDesc.Text = ds.Tables[0].Rows[0]["EventDescription"].ToString();
                
            }

            bizEvent bs2 = new bizEvent();
            DataSet ds2;

            ds2 = bs2.SelectHoldEventTypeOnlineFormB(dropEvent);
            radioEventType.DataSource = ds2;
            radioEventType.DataTextField = "EventTypeName";
            radioEventType.DataValueField = "EventTypeID";
            radioEventType.DataBind();

            ErP5.Visible = false;
        }
        else if (dropEvent == "0")
        {
            //string dropEvent = DropDownListEvent.SelectedValue;
            ErP5.Visible = true;
            Currency.Text = "";
            Amount.Text = "";
            lblEventID.Value = dropEvent;
            lblEventTitle.Value = "";
            ProdDesc.Text = "";
            ShowDropDownEvent();
        }
    }

    protected void ShowDropDownEvent()
    {
        bizEvent  bt = new bizEvent ();

        DataSet ds_template = bt.Selectevent ();

        ListItem li;
        DropDownListEvent.Items.Clear();

        if (ds_template.Tables[0].Rows.Count > 0)
        {
            li = new ListItem();
            li.Value = "0";
            li.Text = "---------Please Select---------";
            DropDownListEvent.Items.Add(li);

            for (int i = 0; i < ds_template.Tables[0].Rows.Count; i++)
            {
                li = new ListItem();
                li.Value = ds_template.Tables[0].Rows[i]["EventID"].ToString();
                li.Text = ds_template.Tables[0].Rows[i]["EventTitle"].ToString();
                DropDownListEvent.Items.Add(li);
            }
        }
    }

    protected void refCode()
    {
        string tryRefcode;
        bizCode bRefCode = new bizCode();
        DataSet ds;
        ds = bRefCode.SelectReferenceCode();

        string refcode = ds.Tables[0].Rows[0]["Approval_Code"].ToString();
        int refintcode= Convert .ToInt32(refcode)  + 1;
        RefNo.Value = Convert.ToString(refintcode);
        tryRefcode = Convert.ToString(refintcode);

        bizCode updaterefCoed = new bizCode();
        int saverefcode = updaterefCoed.Updaterefcode(tryRefcode);
        
        //return tryRefcode;
    }

    public static bool IsInteger(string theValue)
    {
        try
        {
            Convert.ToInt64(theValue);
            return true;
        }
        catch
        {
            return false;
        }
    }

     public static bool IsValidEmailAddress(string sEmail)
        {
            if (sEmail == null)
            {
                return false;
            }
            else
            {
              return Regex.IsMatch(sEmail, @"^[-a-zA-Z0-9_][-_.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.
            (com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|[a-zA-Z]{2})$",
                RegexOptions.IgnorePatternWhitespace);
            }
        }

    protected void Btnsubmit_Click(object sender, EventArgs e)
     {
         if (lblEventID.Value !=null )
         {
             if (radioEventType.SelectedValue != null)
             {
                 if (lblDesignation.Value != "" && lblDesignation.Value != "0")
                 {

                     if (UserContact.Text != "" && IsInteger(UserContact.Text) == true)
                     {
                         if (UserEmail.Text != "" && IsValidEmailAddress(UserEmail.Text) == true)
                         {

                             if (CheckBoxConfirm.Checked == true)
                             {

                                 //if (RefNo.Value != "")
                                 //{
                                     bizpaymentform insertPForm = new bizpaymentform();
                                     refCode();
                                     int insertdetailForm = insertPForm.Insertpaymentform(UserName.Text, lblDesignation.Value, txtIC.Text, txtplcPractice.Text, txtAddress.Value, txtCity.Text, lblState.Value, txtPoscode.Text, UserEmail.Text, UserContact.Text, Amount.Text, txtpaymentoption.Text, RefNo.Value, HideDateTime.Value, lblEventID.Value, radioEventType.SelectedValue, lblEventTitle.Value);
                                     
                                     Response.Write("<script>window.location='OnlineFormConfirm.aspx';</script>");

                                 //}
                                 lblEP.Visible = false;
                             }
                             else
                             {
                                 lblEP.Visible = true;
                             }
                             ErPemail.Visible = false;
                         }
                         else
                         {

                             ErPemail.Visible = true;
                         }
                         ErP.Visible = false;
                     }
                     else
                     {

                         ErP.Visible = true;
                     }
                     //lblEP.Visible = false;
                     //lblEP.Visible = false;
                     //ErPemail.Visible = false;
                     ErP8.Visible = false;
                 }
                 else
                 {

                     ErP8.Visible = true;

                 }
                 ErP9.Visible = false;
             }
             else
             {

                 ErP9.Visible = true;

             }
             ErP5.Visible = false;
         }
         else
         {
             ErP5.Visible = true;

         }
        
        //Response.Write(radioEventType.SelectedValue);
    }

    
}
