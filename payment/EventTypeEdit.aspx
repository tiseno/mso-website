﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventTypeEdit.aspx.cs" Inherits="EventTypeEdit" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/VEventadd.js" ></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
     <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    
    <tr><td><table style="width:100%; background-color :#d9d6d6; height:20px;" align="center">
      <tr><td style="color:#323232;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Event Type</td></tr></table>  <br />
      </td></tr>
      
      <tr><td align ="left" style="padding-left:10px;" >
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="60%" >
      <%--<tr><td align ="left" class="style1" >Event</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle1">
      <asp:DropDownList ID="DropEventType" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="EventType_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
      <asp:Label ID="ErEventType" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
          <asp:HiddenField ID="lblEventTypeID" runat="server"  />
      </td></tr>--%>
      
      <tr><td align ="left" class="style1">Event Type Name</td> 
      <td class="style2">:  <td align ="left" style =" width :450px;">
      <asp:TextBox ID="txtEventTypeTitle" Width="200px" runat="server"></asp:TextBox>
      </td></tr>
      
      <tr><td align ="left" class="style1" >Currency</td><td class="style2">:</td>
      <td  align ="left" style ="width:400px;">
      <asp:DropDownList ID="txtCurrency" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
    
          <asp:Label ID="ErP7" runat="server" 
            style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
            Font-Bold="true" Text="Please Choose Currency!"></asp:Label> 
      <asp:HiddenField ID="lblCurrency" runat="server"  />
      </td></tr> 
      
      <tr><td align ="left" class="style1" >Fees</td><td class="style2">:</td>
      <td  align ="left">
      <asp:TextBox ID="txtFees" Width="200px" runat="server"></asp:TextBox>
      <asp:Label ID="ErP" runat="server" style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" Font-Bold="true" Text="Invalid Value!"></asp:Label></td>
      </tr> 
      
      <tr><td align ="left" class="style1" >Status</td><td class="style2">:</td>
      <td  align ="left">
      <asp:RadioButtonList ID="radioStatus" Runat="server" Font-Size="8pt" Font-Names="Verdana" BorderWidth="0" CellPadding="0" CellSpacing="0" RepeatLayout="flow" RepeatColumns="3">
            <asp:ListItem Text="Yes" Value="True" ></asp:ListItem>
            <asp:ListItem Text="No" Value="False"></asp:ListItem>
        </asp:RadioButtonList>   
        <asp:Label ID="lblStatus" runat="server" Visible ="false"  Text=""></asp:Label> 
      </td>
      </tr> 
      
      <tr><td align ="left" colspan ="3" style ="padding :10px 20px 10px 0px;">
      <asp:Button ID="btnEventTypeUpdate" runat="server" Text="Update" 
              onclick="btnEventTypeUpdate_Click"/> 
      <asp:HiddenField ID="lblTEventID" runat="server"  />
          </td>
      </tr> 
      
      </table></td></tr>
      
      
      </table> 
    
    
    </div>
    </form>
</body>
</html>
