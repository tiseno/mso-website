﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;

public partial class FillUserDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            DateTime Now = DateTime.Now;
            txtDate.Text = Now.ToShortDateString();
            dropdownlist();

            MerchantKey.Value = Request.Form["MerchantKey"];
            RefNo.Value  = Request.Form["RefNo"];
            Amount.Value  = Request.Form["Amount"];
            Currency.Value  = Request.Form["Currency"];
            ProdDesc.Value = Request.Form["ProdDesc"];

            paymentoption.Value = Request.Form["txtpaymentoption"];
            lblEventID.Value = Request.Form["lblEventID"];
            lblEventTitle.Value = Request.Form["lblEventTitle"];

            Response.Write(RefNo.Value);
            Response.Write(lblEventTitle.Value);
        }

    }

    protected void Prod_EditChanged(object sender, EventArgs e)
    {
        lblDesignation.Text = txtDesignation.SelectedValue;
    }

    protected void dropdownlist()
    {
        txtDesignation.Items.Clear();
        txtDesignation.Items.Add(new ListItem("---------Please Select---------", "0"));
        txtDesignation.Items.Add(new ListItem("Trainee", "Trainee"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Govt/University)", "Ophthalmologist(Govt/University)"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Private)", "Ophthalmologist(Private)"));
        txtDesignation.Items.Add(new ListItem("Eyecare Proffessional", "Eyecare_Proffessional"));
    }  

    protected void Btnsubmit_Click(object sender, EventArgs e)
    {
        
        if (RefNo.Value  != "")
        {
            
            bizpaymentform insertPForm = new bizpaymentform();
            int i = insertPForm.Insertpaymentform(UserName.Text, lblDesignation.Text, txtIC.Text, txtplcPractice.Text, txtAddress.Value, txtCity.Text, txtState.Text, txtPoscode.Text, UserEmail.Text, UserContact.Text, Amount.Value, paymentoption.Value, RefNo.Value, txtDate.Text, lblEventID.Value , lblEventID.Value, lblEventTitle.Value);

            //Response.Write("<script>alert('Submit Successful!');</script>");
            //Response.Write("<script>window.location='tmppage.aspx';</script>");

        }

    }
}
