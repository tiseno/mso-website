﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;

public partial class EventDetailEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblEventTypeID.Value = Request.QueryString["EventID"];
            dropdownlist();
            ViewDetail();
        }
    }

    //protected void Prod_EditChanged(object sender, EventArgs e)
    //{
    //    lblCurrency.Value = txtCurrency.SelectedValue;
    //}

    //protected void dropdownlist()
    //{
    //    txtCurrency.Items.Clear();
    //    txtCurrency.Items.Add(new ListItem("Please Select", "0"));
    //    txtCurrency.Items.Add(new ListItem("MYR", "MYR"));
    //    txtCurrency.Items.Add(new ListItem("USD", "USD"));

    //}

    protected void ViewDetail()
    {
        //string gtxtEventTitle = "";
        bizEvent  bs = new bizEvent ();
        DataSet ds;
        ds = bs.SelectDetailevent (Request.QueryString["EventID"]);

        if (ds.Tables[0].Rows.Count > 0)
        {

            txtEventTitle.Text = ds.Tables[0].Rows[0]["EventTitle"].ToString();
            txtEventDescription.Value = ds.Tables[0].Rows[0]["EventDescription"].ToString();
            //txtCurrency.Text = ds.Tables[0].Rows[0]["ECurrency"].ToString();
            //txtFees.Text = ds.Tables[0].Rows[0]["EFees"].ToString();
            radioStatus.Text = ds.Tables[0].Rows[0]["EStatus"].ToString();
        }

        bizEvent bsT = new bizEvent();
        DataSet dsT;

        dsT = bs.SelectHoldEventTypeB(Request.QueryString["EventID"]);
        dg_SearchControl.DataSource = dsT;
        dg_SearchControl.DataBind();
        
    }

       

    public static bool IsInteger(string theValue)
    {
        try
        {
            Convert.ToInt32(theValue);
            return true;
        }
        catch
        {
            return false;
        }
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        //string gamountFees = txtFees.Text;
        //string setamount = gamountFees.Replace(".", "");

        //if (setamount != "" && IsInteger(setamount) == true)
        //{
            //string gfees = txtFees.Text;
            //string gfeesdot = Convert.ToDecimal(String.Format("{0:0.00}", Convert.ToDecimal(gfees))).ToString();

            string EventID = Request.QueryString["EventID"];
            bizEvent UpdatePForm = new bizEvent();
            int i = UpdatePForm.EditDetailEvent(EventID, txtEventTitle.Text, txtEventDescription.Value, radioStatus.SelectedValue);

            //Response.Write("<script>alert('Update Successful!');</script>");
            //Response.Write("<script>window.location='EventDetailEdit.aspx?EventID=" + EventID + ";</script>");Eventpage.aspx
            //Response.Write("<script>window.location='EventDetailEdit.aspx?EventID=" + EventID + ";</script>");
            Alert.Show("Update Successful!");
            //ViewDetail();
        //}
        //else
        //{
        //    //ErP.Visible = true;

        //}

    }
    protected void btnback_Click1(object sender, EventArgs e)
    {
        string EventID = Request.QueryString["EventID"];
        Response.Redirect("EventView.aspx?EventID=" + EventID);
    }
    protected void btndelete_Click(object sender, EventArgs e)
    {
        //bizEvent  bdlt = new bizEvent ();
        //string EventID = Request.QueryString["EventID"];
        //int tmpdlt = bdlt.DeleteEventDetail (EventID);
        //Response.Write("<script>alert('Delete Successful!');</script>");
        //Response.Write("<script>window.location='Eventpage.aspx';</script>");
    }
    protected void btnEventTypeAdd_Click(object sender, EventArgs e)
    {
        //if (lblEventTypeID.Value != "")
        //{
            if (txtEventTypeTitle.Text != "")
            {
                //if (lblCurrency.Value != "")
                //{
                    string gamountFees = txtFees.Text;
                    string setamount = gamountFees.Replace(".", "");

                    if (setamount != "" && IsInteger(setamount) == true)
                    {

                        string gfees = txtFees.Text;
                        string gfeesdot = Convert.ToDecimal(String.Format("{0:0.00}", Convert.ToDecimal(gfees))).ToString();

                        bizEvent addevent = new bizEvent();
                        int i = addevent.insertEventType(txtEventTypeTitle.Text, "MYR", gfeesdot, lblEventTypeID.Value, "True");

                        bizEvent bs = new bizEvent();
                        DataSet ds;

                        ds = bs.SelectHoldEventTypeB(lblEventTypeID.Value);
                        dg_SearchControl.DataSource = ds;
                        dg_SearchControl.DataBind();

                        //Response.Write("<script>alert('Add Successful!');</script>");
                        Alert.Show("Add Successful!");


                        ErP.Visible = false;
                    }
                    else
                    {
                        ErP.Visible = true;

                    }
                //    ErP7.Visible = false;
                //}

                //else
                //{
                //    ErP7.Visible = true;
                //}
                ErP5.Visible = false;
            }
            else
            {
                ErP5.Visible = true;
            }
        //    ErP6.Visible = false;
        //}
        //else
        //{
        //    ErP6.Visible = true;
        //}
    }

    protected string URL(object EventTypeID)
    {
        return "EventTypeEdit.aspx?EventTypeID=" + EventTypeID;
    }

    protected void dg_SearchControl_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dg_SearchControl.CurrentPageIndex = e.NewPageIndex;
        bizEvent bs = new bizEvent();
        DataSet ds;

        ds = bs.SelectHoldEventTypeB(lblEventTypeID.Value);
        dg_SearchControl.DataSource = ds;
        dg_SearchControl.DataBind();
    }

    protected void Prod_CurrencyEditChanged(object sender, EventArgs e)
    {
        //lblCurrency.Value = txtCurrency.SelectedValue;
        string dropCurrency = txtCurrency.SelectedValue;
        lblCurrency.Value = dropCurrency;

        if (dropCurrency != "0")
        {
            ErP7.Visible = false;
        }
        else if (dropCurrency == "0")
        {
            ErP7.Visible = true;
        }
    }

    protected void dropdownlist()
    {
        txtCurrency.Items.Clear();
        //txtCurrency.Items.Add(new ListItem("Please Select", "0"));
        txtCurrency.Items.Add(new ListItem("MYR", "MYR"));
        //txtCurrency.Items.Add(new ListItem("USD", "USD"));

    }

    //protected void EventType_EditChanged(object sender, EventArgs e)
    //{
    //    string dropEvent = DropEventType.SelectedValue;
    //    lblEventTypeID.Value = dropEvent;

    //    if (dropEvent != "0")
    //    {
    //        bizEvent bs = new bizEvent();
    //        DataSet ds;

    //        ds = bs.SelectHoldEventTypeB(dropEvent);
    //        dg_SearchControl.DataSource = ds;
    //        dg_SearchControl.DataBind();
    //        ErP6.Visible = false;
    //    }
    //    else if (dropEvent == "0")
    //    {
    //        ErP6.Visible = true;
    //        ShowDropDownEvent();
    //    }
    //}

    //protected void ShowDropDownEvent()
    //{
    //    bizEvent bt = new bizEvent();

    //    DataSet ds_template = bt.SelectAllEventInTypeB();

    //    ListItem li;
    //    DropEventType.Items.Clear();

    //    if (ds_template.Tables[0].Rows.Count > 0)
    //    {
    //        li = new ListItem();
    //        li.Value = "0";
    //        li.Text = "---------Please Select---------";
    //        DropEventType.Items.Add(li);

    //        for (int i = 0; i < ds_template.Tables[0].Rows.Count; i++)
    //        {
    //            li = new ListItem();
    //            li.Value = ds_template.Tables[0].Rows[i]["EventID"].ToString();
    //            li.Text = ds_template.Tables[0].Rows[i]["EventTitle"].ToString();
    //            DropEventType.Items.Add(li);
    //        }
    //    }
    //}



    protected void Status_EventEditchange(object sender, EventArgs e)
    {
        lblStatus1.Text = radioStatus.SelectedValue;

    }

    protected void Status_EventTypeEditchange(object sender, EventArgs e)
    {
        lblStatus.Text = RadioEventType.SelectedValue;

    }

    
}

