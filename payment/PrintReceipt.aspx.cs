﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;
using System.Data.OleDb;

public partial class PrintReceipt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DateTime Now = DateTime.Now;
            string str = Now.ToString("dd/MM/yyyy");
            txtDate.Text = str;
            string Gtime = Request.Form["HideDateTime"];

            lblTime.Text = FEndDate(Gtime);

            MerchantCode.Value = Request.Form["MerchantCode"];
            PaymentId.Value = Request.Form["PaymentId"];
            RefNo.Text = Request.Form["RefNo"];
            Amount.Text = Request.Form["Amount"];
            eCurrency.Text = Request.Form["eCurrency"];
            lblPaymentName.Text = Request.Form["PFullName"];
            lblEventName.Text = Request.Form["pEventName"];
            Remark.Text = Request.Form["Remark"];
            //TransId.Text = Request.Form["TransId"];
            //AuthCode.Text = Request.Form["AuthCode"];
            eStatus.Value = Request.Form["eStatus"];
            ErrDesc.Text = Request.Form["ErrDesc"];
            Signature.Value = Request.Form["Signature"];

            txtreceiptNo.Text = Request.Form["txtreceiptNo"];

            //ViewDetail();
            ViewErrorMsg(); 
        }

    }

    protected string FEndDate(string setdataenddate)
    {
        string datecombile = setdataenddate;
        string HourNow = datecombile.Substring(11, 2);
        string mmnow = datecombile.Substring(14, 2);
        string ssnow = datecombile.Substring(17, 2);
        string APMnow = datecombile.Substring(20, 2);

        string TimeNow = HourNow + ":" + mmnow + ":" + ssnow + " " + APMnow;
        string gdate = TimeNow;

        return gdate;
    }


    protected void ViewErrorMsg()
    {
    //    if (eStatus.Value != "")
    //    {
        //eStatus.Value = Request.Form["eStatus"];
        int gStatus = Convert.ToInt32(eStatus.Value);
        
        if (gStatus == 1)
            //if (eStatus.Value != "0")
            {
                lblErrorDescTitle.Visible = false;
                lblDot.Visible = false;
                ErrDesc.Visible = false;
                lblThankYouPayment.Visible = true;
            }
            else
            {
                lblErrorDescTitle.Visible = true;
                lblDot.Visible = true;
                ErrDesc.Visible = true;
                lblThankYouPayment.Visible = false;
            }
        //}
        //else
        //{
        //    eStatus.Value = "0";
        //    lblErrorDescTitle.Visible = true;
        //    lblDot.Visible = true;
        //    ErrDesc.Visible = true;
        //    //string gErrDesc = "Fail, Try Again!";

        //    //Response.Write("<script>alert('" + gErrDesc + "!" + "');</script>");
        //    //Response.Write("<script>window.location='OnlineForm.aspx';</script>");
        //}
    }

    //protected void ViewDetail()
    //{

    //    bizpaymentform bs = new bizpaymentform();
    //    DataSet ds;
    //    ds = bs.SelectPaymentDetail();

    //    if (ds.Tables[0].Rows.Count > 0)
    //    {

    //        UserName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();
    //        txtDesignation.Text = ds.Tables[0].Rows[0]["Designation"].ToString();
    //        txtIC.Text = ds.Tables[0].Rows[0]["IC_Passport_No"].ToString();
    //        txtplcPractice.Text = ds.Tables[0].Rows[0]["Place_Practice"].ToString();

    //        txtAddress.Value = ds.Tables[0].Rows[0]["User_Address"].ToString();
    //        txtCity.Text = ds.Tables[0].Rows[0]["City"].ToString();
    //        txtState.Text = ds.Tables[0].Rows[0]["State"].ToString();

    //        txtPoscode.Text = ds.Tables[0].Rows[0]["PostCode"].ToString();
    //        UserEmail.Text = ds.Tables[0].Rows[0]["User_Email"].ToString();
    //        UserContact.Text = ds.Tables[0].Rows[0]["Hand_Phone_No"].ToString();
    //        eCurrency.Text = ds.Tables[0].Rows[0]["ECurrency"].ToString();
    //        Amount.Text = ds.Tables[0].Rows[0]["Payment_Amount"].ToString();

    //        txtpaymentoption.Text = ds.Tables[0].Rows[0]["Payment_Option"].ToString();
    //        RefNo.Text = ds.Tables[0].Rows[0]["Reference_ID"].ToString();
    //        txtreceiptNo = ds.Tables[0].Rows[0]["Receipt_No"].ToString();
    //        txtDate.Text = ds.Tables[0].Rows[0]["PaymentDate"].ToString();
    //        lblEventTitle.Text = ds.Tables[0].Rows[0]["UserDetail.EventTitle"].ToString();
    //        ProdDesc.Value = ds.Tables[0].Rows[0]["EventDescription"].ToString();
    //    }
    //}
}
