﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;


public partial class PaymentDetailEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            //DateTime Now = DateTime.Now;
            //txtDate.Text = Now.ToShortDateString();

            dropdownlist();
            ViewDetail();
        }

    }

    protected void ViewDetail()
    {
        bizpaymentform bs = new bizpaymentform();
        DataSet ds;
        ds = bs.SelectPaymentDetail(Request.QueryString["UserDetailID"]);

        if (ds.Tables[0].Rows.Count > 0)
        {

            UserName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();
            txtDesignation.Text = ds.Tables[0].Rows[0]["Designation"].ToString();
            txtIC.Text = ds.Tables[0].Rows[0]["IC_Passport_No"].ToString();
            txtplcPractice.Text = ds.Tables[0].Rows[0]["Place_Practice"].ToString();

            txtAddress.Value = ds.Tables[0].Rows[0]["User_Address"].ToString();
            txtCity.Text = ds.Tables[0].Rows[0]["City"].ToString();
            txtState.Text = ds.Tables[0].Rows[0]["State"].ToString();

            txtPoscode.Text = ds.Tables[0].Rows[0]["PostCode"].ToString();
            UserEmail.Text = ds.Tables[0].Rows[0]["User_Email"].ToString();
            UserContact.Text = ds.Tables[0].Rows[0]["Hand_Phone_No"].ToString();
            amount.Text = ds.Tables[0].Rows[0]["Payment_Amount"].ToString();

            txtpaymentoption.Text = ds.Tables[0].Rows[0]["Payment_Option"].ToString();
            txtReferenceCode.Text = ds.Tables[0].Rows[0]["Reference_ID"].ToString();
            string PStatus = ds.Tables[0].Rows[0]["paymentStatus"].ToString();
            if (PStatus == "True")
            { 
                lblPaymentStatus.Text = "Success";
            }
            else if (PStatus == "False")
            {
                lblPaymentStatus.Text = "Fail";
            }
            
            txtReceiptCode.Text = ds.Tables[0].Rows[0]["Receipt_No"].ToString();
            txtDate.Text = ds.Tables[0].Rows[0]["PaymentDate"].ToString();
            lblevent.Text = ds.Tables[0].Rows[0]["UserDetail.EventTitle"].ToString();

        }
    }

    protected void Prod_EditChanged(object sender, EventArgs e)
    {
        lblDesignation.Value = txtDesignation.SelectedValue;
    }

    protected void dropdownlist()
    {
        txtDesignation.Items.Clear();
        txtDesignation.Items.Add(new ListItem("---------Please Select---------", "0"));
        txtDesignation.Items.Add(new ListItem("Trainee", "Trainee"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Govt/University)", "Ophthalmologist(Govt/University)"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Private)", "Ophthalmologist(Private)"));
        txtDesignation.Items.Add(new ListItem("Eyecare Professional", "Eyecare Professional"));
    }

    //protected string refCode()
    //{
    //    string tryRefcode;
    //    bizCode bRefCode = new bizCode();
    //    DataSet ds;
    //    ds = bRefCode.SelectReferenceCode();

    //    string refcode = ds.Tables[0].Rows[0]["Approval_Code"].ToString();
    //    int refintcode = Convert.ToInt32(refcode) + 1;
    //    //txtReferenceCode.Text = Convert .ToString ( refintcode);
    //    tryRefcode = Convert.ToString(refintcode);

    //    return tryRefcode;
    //}

    protected void receiptNo()
    {
        bizCode bRecCode = new bizCode();
        DataSet ds;
        ds = bRecCode.SelectReceiptCode();

        string reccode = ds.Tables[0].Rows[0]["Receipt_ID"].ToString();
        int recintcode = Convert.ToInt32(reccode) + 1;
        txtReceiptCode.Text = Convert.ToString(recintcode);
    }

    //protected void Updateform()
    //{
    //    string UserDetailID = Request.QueryString["UserDetailID"];
    //    bizpaymentform UpdatePForm = new bizpaymentform();
    //    int i = UpdatePForm.Editpaymentform(UserDetailID, UserName.Text, lblDesignation.Value, txtIC.Text, txtplcPractice.Text, txtAddress.Value, txtCity.Text, txtState.Text, txtPoscode.Text, UserEmail.Text, UserContact.Text, amount.Text, txtpaymentoption.Text, txtReceiptCode.Text);

    //    Response.Write("<script>alert('Update Success!');</script>");
    //    Response.Write("<script>window.location='ListAllPayment.aspx';</script>");
    //}
    
    protected void BtnDelete_Click(object sender, EventArgs e)
    {
        //bizpaymentform bdlt = new bizpaymentform();
        //string UserDetailID = Request.QueryString["UserDetailID"];
        //int tmpdlt = bdlt.deletepaymentdetail(UserDetailID);
        //Response.Write("<script>alert('Delete Successful!');</script>");
        //Response.Write("<script>window.location='ListAllPayment.aspx';</script>");
        //Response.Write("<script>javascript:return confirm('Are you sure you want to delete subject :  ?');</script>");
    }

    public static bool IsValidEmailAddress(string sEmail)
    {
        if (sEmail == null)
        {
            return false;
        }
        else
        {
            return Regex.IsMatch(sEmail, @"
            ^
            [-a-zA-Z0-9_][-_.a-zA-Z0-9]*
            @
            [-.a-zA-Z0-9]+
            (\.[-.a-zA-Z0-9]+)*
            \.
            (
            com|edu|info|gov|int|mil|net|org|biz|
            name|museum|coop|aero|pro
            |
            [a-zA-Z]{2}
            )
            $",
              RegexOptions.IgnorePatternWhitespace);
        }
    }

    public static bool IsInteger(string theValue)
    {
        try
        {
            Convert.ToInt64(theValue);
            return true;
        }
        catch
        {
            return false;
        }
    }

    protected void Btnupdate_Click(object sender, EventArgs e)
    {
        if (UserContact.Text != "" && IsInteger(UserContact.Text) == true)
        {
            if (UserEmail.Text != "" && IsValidEmailAddress(UserEmail.Text) == true)
            {
                string UserDetailID = Request.QueryString["UserDetailID"];
                bizpaymentform UpdatePForm = new bizpaymentform();
                int i = UpdatePForm.Editpaymentform(UserDetailID, UserName.Text, lblDesignation.Value, txtIC.Text, txtplcPractice.Text, txtAddress.Value, txtCity.Text, txtState.Text, txtPoscode.Text, UserEmail.Text, UserContact.Text, amount.Text, txtpaymentoption.Text, txtReceiptCode.Text, txtDate.Text);

                //Response.Write("<script>alert('Update Success!');</script>");
                //Response.Write("<script>window.location='ListAllPayment.aspx';</script>");
                Alert.Show("Update Success!");
                ErPemail.Visible = false;
            }
            else
            {
                ErPemail.Visible = true;

            }
            ErP.Visible = false;
        }
        else
        {
            ErP.Visible = true;

        }
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        string UserDetailID = Request.QueryString["UserDetailID"];
        Response.Redirect("PaymentDetailView.aspx?UserDetailID=" + UserDetailID);
    }
}
