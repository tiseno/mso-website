﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Eventadd.aspx.cs" Inherits="Eventadd" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/VEventadd.js" ></script>
</head>
<script type="text/javascript">
function toggle2(showHideDiv, switchTextDiv) {
	var ele = document.getElementById(showHideDiv);
	var text = document.getElementById(switchTextDiv);
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "restore";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "collapse";
	}
}

</script>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
 <%-- <tr><td ><table border="0" style="width:100%; height:20px;" align="center">
<tr><td style="color: #323232; font-size:9pt;font-weight:bold; padding:3px 10px 3px 8px" align="left">Search Criteria</td></tr></table></td></tr>--%>
	
    <tr><td><table style="width:100%; background-color :#d9d6d6; height:20px;" align="center">
      <tr><td style="color:#323232;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left">Event Detail</td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :10px 10px 10px 0px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%" >
    <%--<tr><td  class ="formheader"align ="left" >Event Add</td></tr>--%>
    
    <%--<tr><td ><table class="Dline"  align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      <%--<tr><td ><table style="width:100%; height:1px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold;" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      <tr><td align ="left" style="padding-left:10px;">
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="60%" >
      
      <tr><td align ="left" class="style1" >Event Title</td><td class="style2">:</td>
      <td  align ="left" style =" width :450px;">
      <asp:TextBox ID="txtEventTitle" Width="200px" runat="server"></asp:TextBox>
    
          <asp:Label ID="ErP0" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
    
      <asp:Label ID="ErP6" runat="server" 
              style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
              Font-Bold="true" Text="Please Insert Event Name!"></asp:Label> 
          </td>
      </tr> 
      
      <tr><td align ="left" valign ="top" class="style1" >Event Description</td><td class="style2" valign ="top">:</td>
      <td  align ="left" class ="Estyle1">
      <textarea id ="txtEventDescription" runat="server" cols="80" name="elm1" rows="20" style="width: 250px;height:60px; "></textarea>
      </td></tr> 
      
      <tr><td align ="left" class="style1" >&nbsp;</td><td class="style2">&nbsp;</td>
      <td  align ="left" class ="Estyle1">
      <asp:RadioButtonList ID="radioStatus" Runat="server" Font-Size="10pt" Visible ="false" Font-Names="Verdana" BorderWidth="0" CellPadding="0" CellSpacing="0" RepeatLayout="flow" RepeatColumns="3">
            <asp:ListItem Text="Yes" Value="True" ></asp:ListItem>
            <asp:ListItem Text="No" Value="False"></asp:ListItem>
        </asp:RadioButtonList>    
    
      </td>
      </tr>
      
      
      
      </table>
      </td></tr>
      
      
       
       
      
      
      
      
      
      <tr><td align ="left" style="padding-left:10px;"><div id ="TypeNonMember" visible ="false" >
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="60%" >
      

       
      
      <tr><td align="left" colspan="6" style=" font-family:Verdana; font-size:8pt;">
    <br /><br />
          <asp:Label ID="ErP4" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> &nbsp;=Fields must choose be or insert.
            <br />
    
    <br /></td></tr>
    
          <%--<input id="Hidden1" type="hidden" runat="server" />--%>
      <tr><td align ="left" colspan ="3" style ="padding :10px 20px 10px 0px;">
      <asp:Button ID="btnAdd" runat="server" Text="Add" Width ="80px"  OnClientClick="return validateForm(this);" onclick="btnAdd_Click" /></td>
      </tr> <%----%>
      
      </table></div>
      </td></tr>
    
    <tr><td align ="center" >
    
    </td></tr>
    
    </table> 
    </td></tr>
    
    <%--<tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
   </table>
    
    </div>
    </form>
</body>
</html>
