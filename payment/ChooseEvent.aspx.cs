﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;

public partial class ChooseEvent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowDropDownEvent();
            refCode();
        }
    }

    protected void Event_EditChanged(object sender, EventArgs e)
    {
        string dropEvent = DropDownListEvent.SelectedValue;

        bizEvent bevent = new bizEvent();
        DataSet ds;
        ds = bevent.SelectDetailevent(dropEvent);

        if (ds.Tables[0].Rows.Count > 0)
        {
            Currency.Text = ds.Tables[0].Rows[0]["ECurrency"].ToString();
            Amount.Text = ds.Tables[0].Rows[0]["EFees"].ToString();
            lblEventID.Text = ds.Tables[0].Rows[0]["EventID"].ToString();
            lblEventTitle.Text = ds.Tables[0].Rows[0]["EventTitle"].ToString();
            ProdDesc.Value = ds.Tables[0].Rows[0]["EventDescription"].ToString();
        }
    }

    protected void ShowDropDownEvent()
    {
        bizEvent bt = new bizEvent();
        DataSet ds_template = bt.Selectevent();
        ListItem li;
        DropDownListEvent.Items.Clear();
        if (ds_template.Tables[0].Rows.Count > 0)
        {
            li = new ListItem();
            li.Value = "";
            li.Text = "---------Please Select---------";
            DropDownListEvent.Items.Add(li);

            for (int i = 0; i < ds_template.Tables[0].Rows.Count; i++)
            {
                li = new ListItem();
                li.Value = ds_template.Tables[0].Rows[i]["EventID"].ToString();
                li.Text = ds_template.Tables[0].Rows[i]["EventTitle"].ToString();
                DropDownListEvent.Items.Add(li);
            }
        }
    }

    protected string refCode()
    {
        string tryRefcode;
        bizCode bRefCode = new bizCode();
        DataSet ds;
        ds = bRefCode.SelectReferenceCode();

        string refcode = ds.Tables[0].Rows[0]["Approval_Code"].ToString();
        int refintcode = Convert.ToInt32(refcode) + 1;
        RefNo.Text = Convert.ToString(refintcode);
        tryRefcode = Convert.ToString(refintcode);

        bizCode updaterefCoed = new bizCode();
        int saverefcode = updaterefCoed.Updaterefcode(tryRefcode);

        return tryRefcode;
    }

    protected void BtnNext_Click(object sender, EventArgs e)
    {
        if (lblEventTitle.Text != "")
        {
            
        }
        else
        {
            Response.Write("title blank");
        }
    }
}
