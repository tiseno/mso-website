﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventView.aspx.cs" Inherits="EventView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Event Add</td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :10px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="80%" >
    <tr><td  class ="formheader"align ="center" >Event Add</td></tr>
    
    <tr><td ><table class="Dline"  align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
      
      <tr><td align ="center">
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="60%" >
      
      <tr><td align ="left" class="style1" >Event Title</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <asp:Label ID="txtEventTitle" runat="server" Visible ="true"  Text=""></asp:Label></td>
      </tr> 
      
      <tr><td align ="left" class="style1" >Event Description</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <textarea id ="txtEventDescription" runat="server" cols="80" name="elm1" rows="20" style="width: 250px;height:60px; "></textarea>
      </td></tr> 
      
      <tr><td align ="left" class="style1" >Currency</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <asp:Label ID="lblCurrency" runat="server" Visible ="true"  Text=""></asp:Label>
      </td></tr> 

      <tr><td align ="left" class="style1" >Fees</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <asp:Label ID="txtFees" runat="server" Visible ="true"  Text=""></asp:Label></td>
      </tr> 
      
      <%--<tr><td align ="left" class="style1" >Status</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <asp:RadioButtonList ID="radioStatus" Runat="server" Font-Size="8pt" Font-Names="Verdana" BorderWidth="0" CellPadding="0" CellSpacing="0" RepeatLayout="flow" RepeatColumns="3">
            <asp:ListItem Text="yes" Selected="True" Value="true" ></asp:ListItem>
            <asp:ListItem Text="No" Value="false"></asp:ListItem>
        </asp:RadioButtonList>    
      </td>
      </tr> --%>
      
      <tr><td align ="right" colspan ="3" style ="padding :10px 20px 10px 10px;">
      <asp:Button ID="btnback" runat="server" Text="Back" onclick="btnback_Click1" />
      <asp:Button ID="btnEdit" runat="server" Text="Edit" onclick="btnEdit_Click" /></td>
      </tr> 
      
      </table>
      </td></tr>
    
    <tr><td align ="center" >
    </td></tr>
    </table> 
    </td></tr>
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>
   </table>
    </div>
    </form>
</body>
</html>
