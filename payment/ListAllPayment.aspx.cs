﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Collections.Generic;
using commonlayer;
using System.Data.OleDb;

public partial class ListAllPayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack )
        {
            //alllistpayment();
            dropdownlist();
            Currencydropdownlist();
            ShowDropDownEvent();
        }
        
    }

    protected void alllistpayment()
    {
        bizpaymentform bs = new bizpaymentform();
        DataSet ds;

        ds = bs.SelectAllPayment();
        dg_SearchControl.DataSource = ds;
        dg_SearchControl.DataBind();
    }

    protected string URL(object UserDetailID)
    {
        return "PaymentDetailEdit.aspx?UserDetailID=" + UserDetailID;
    }

    protected void dg_SearchControl_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dg_SearchControl.CurrentPageIndex = e.NewPageIndex;
        //bizpaymentform bsC = new bizpaymentform();
        //DataSet dsC;

        //dsC = bsC.SelectAllPayment();
        //dg_SearchControl.DataSource = dsC;
        //dg_SearchControl.DataBind();

        bizpaymentform bsearch = new bizpaymentform();
        DataSet DsS;

        DsS = bsearch.BSearchRecordDetail(lblEventTypeID.Value, txtName.Text, lblDesignation.Text, txtDate.Text, txtReceiptNo.Text, lblCurrency.Text, lblStatus.Text);

        dg_SearchControl.DataSource = DsS;
        dg_SearchControl.DataBind();
    }

    protected void Status_editchange(object sender, EventArgs e)
    {
        lblStatus.Text = radioStatus.SelectedValue;
    }

    protected void Prod_EditChanged(object sender, EventArgs e)
    {
        lblDesignation.Text  = txtDesignation.SelectedValue;
    }

    protected void Currency_EditChanged(object sender, EventArgs e)
    {
        lblCurrency.Text = txtCurrency.SelectedValue;
    }

    protected void Currencydropdownlist()
    {
        txtCurrency.Items.Clear();
        //txtCurrency.Items.Add(new ListItem("Please Select", "0"));
        txtCurrency.Items.Add(new ListItem("MYR", "MYR"));
        //txtCurrency.Items.Add(new ListItem("USD", "USD"));
    }

    protected void dropdownlist()
    {
        txtDesignation.Items.Clear();
        txtDesignation.Items.Add(new ListItem("---------Please Select---------", "0"));
        txtDesignation.Items.Add(new ListItem("Trainee", "Trainee"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Govt/University)", "Ophthalmologist(Govt/University)"));
        txtDesignation.Items.Add(new ListItem("Ophthalmologist (Private)", "Ophthalmologist(Private)"));
        txtDesignation.Items.Add(new ListItem("Eyecare Professional", "Eyecare Professional"));
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bizpaymentform bsearch = new bizpaymentform();
        DataSet DsS;

        DsS = bsearch.BSearchRecordDetail(lblEventTypeID.Value, txtName.Text, lblDesignation.Text, txtDate.Text, txtReceiptNo.Text, txtCurrency.SelectedValue , lblStatus.Text);
        dg_SearchControl.CurrentPageIndex = 0;
        dg_SearchControl.DataSource = DsS;
        dg_SearchControl.DataBind();
        Extract_Selected_Result.Enabled = true;
    }

    protected void EventType_EditChanged(object sender, EventArgs e)
    {
        string dropEvent = DropEventType.SelectedValue;
        lblEventTypeID.Value = dropEvent;

        if (dropEvent != "0")
        {
            //bizEvent bs = new bizEvent();
            //DataSet ds;

            //ds = bs.SelectHoldEventTypeB(dropEvent);
            //dg_SearchControl.DataSource = ds;
            //dg_SearchControl.DataBind();
            //ErP6.Visible = false;
        }
        else if (dropEvent == "0")
        {
            //ErP6.Visible = true;
            ShowDropDownEvent();
        }
    }

    protected void ShowDropDownEvent()
    {
        bizEvent bt = new bizEvent();

        DataSet ds_template = bt.SelectAllEventInTypeB();

        ListItem li;
        DropEventType.Items.Clear();

        if (ds_template.Tables[0].Rows.Count > 0)
        {
            li = new ListItem();
            li.Value = "0";
            li.Text = "---------Please Select---------";
            DropEventType.Items.Add(li);

            for (int i = 0; i < ds_template.Tables[0].Rows.Count; i++)
            {
                li = new ListItem();
                li.Value = ds_template.Tables[0].Rows[i]["EventID"].ToString();
                li.Text = ds_template.Tables[0].Rows[i]["EventTitle"].ToString();
                DropEventType.Items.Add(li);
            }
        }
    }


    protected void Extract_Selected_Result_Click(object sender, EventArgs e)
    {
        System.Web.UI.Control ctl = this.dg_SearchControl;

        HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=Excel.xls");
        HttpContext.Current.Response.Charset = "UTF-8";
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Default;
        HttpContext.Current.Response.ContentType = "application/ms-excel";

        ctl.Page.EnableViewState = false;

        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

        int counter = 0;

        bizpaymentform dd = new bizpaymentform();
        DataSet ds;

        if (counter == 3)
        {
            ds = dd.SelectAllDetail();
        }
        else
        {
            ds = dd.BSearchRecordDetail(lblEventTypeID.Value, txtName.Text, lblDesignation.Text, txtDate.Text, txtReceiptNo.Text, lblCurrency.Text, lblStatus.Text);
        }

        HttpContext.Current.Response.Write("<table style='background-color:#000000;'><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></table>");
        HttpContext.Current.Response.Write("<Table border='1'>");
        HttpContext.Current.Response.Write("<tr>");
        HttpContext.Current.Response.Write("<td>No</td><td>Customer Name</td><td>Designation</td>");
        HttpContext.Current.Response.Write("<td>IC/Passport No</td><td>Place Practice</td>");
        HttpContext.Current.Response.Write("<td>Email</td><td>Hand Phone No</td>");
        HttpContext.Current.Response.Write("<td>Payment Amount</td>");
        HttpContext.Current.Response.Write("<td>Payment Option</td><td>Currency</td>");
        HttpContext.Current.Response.Write("<td>Address</td><td>City</td>");
        HttpContext.Current.Response.Write("<td>State</td><td>PostCode</td>");

        HttpContext.Current.Response.Write("<td>Reference ID</td><td>Receipt No</td><td>Payment Date</td>");
        HttpContext.Current.Response.Write("<td>Payment Status</td><td>Event Title</td>");
        
        HttpContext.Current.Response.Write("<td>EventTypeName</td>");
        HttpContext.Current.Response.Write("</tr>");

        string firstRecord = "firstRecord";
        string checkLastRecordSubmissionID = "";
        int x = 0;

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (firstRecord == "firstRecord" || checkLastRecordSubmissionID != ds.Tables[0].Rows[i]["UserDetailID"].ToString())
            {
                HttpContext.Current.Response.Write("<tr>");

                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(++x);
                HttpContext.Current.Response.Write("</td>");

                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["FullName"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["Designation"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["IC_Passport_No"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["Place_Practice"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["User_Email"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["Hand_Phone_No"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["Payment_Amount"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["Payment_Option"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["ECurrency"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["User_Address"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["City"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["State"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["PostCode"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["Reference_ID"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["Receipt_No"].ToString());
                HttpContext.Current.Response.Write("</td>");

                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["PaymentDate"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["paymentStatus"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["UserDetail.EventTitle"].ToString());
                HttpContext.Current.Response.Write("</td>");
                HttpContext.Current.Response.Write("<td>");
                HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["EventTypeName"].ToString());
                HttpContext.Current.Response.Write("</td>");
                

                
            }
            firstRecord = "";
            checkLastRecordSubmissionID = ds.Tables[0].Rows[i]["UserDetailID"].ToString();
        }

        HttpContext.Current.Response.Write("</tr>");

        HttpContext.Current.Response.Write("</table>");

        this.ClearControls(ctl);
        ctl.RenderControl(hw);

        //HttpContext.Current.Response.Write(tw.ToString());
        HttpContext.Current.Response.End();
    }

    private void ClearControls(Control control)
    {
        for (int i = control.Controls.Count - 1; i >= 0; i--)
        {
            ClearControls(control.Controls[i]);
        }
        if (!(control is TableCell))
        {
            if (control.GetType().GetProperty("SelectedItem") != null)
            {
                LiteralControl literal = new LiteralControl();
                control.Parent.Controls.Add(literal);
                try
                {
                    literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                }
                catch
                {
                }
                control.Parent.Controls.Remove(control);
            }
            else
                if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
        }
        return;
    }

    
}
