﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO; 
using bizlayer;
using System.Net;
using System.Data.OleDb;
using System.Security;
using System.Security.Cryptography;

public partial class response : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RefNo.Value = Request.Form["RefNo"];

            if (RefNo.Value != "")
            {
                DateTime Now = DateTime.Now;
                string str = Now.ToString("dd/MM/yyyy");
                txtDate.Text = str;

                MerchantCode.Value = Request.Form["MerchantCode"];
                PaymentId.Value = Request.Form["PaymentId"];

                
                RefNo.Value = Request.Form["RefNo"];
                //RefNo.Value = "1000531";
            
                lblRefNo.Text = RefNo.Value;

                Amount.Value = Request.Form["Amount"];
                lblAmount.Text = Amount.Value;

                eCurrency.Value = Request.Form["Currency"];
                lbleCurrency.Text = eCurrency.Value;

                Remark.Value = Request.Form["Remark"];
                lblRemark.Text = Remark.Value;

                TransId.Value = Request.Form["TransId"];

                AuthCode.Value = Request.Form["AuthCode"];
                eStatus.Value = Request.Form["Status"];
                //eStatus.Value = "0";
               

                ErrDesc.Value = Request.Form["ErrDesc"];
                lblErrDesc.Text = ErrDesc.Value;

                Signature.Value = Request.Form["Signature"];

                receiptNo();
                
                PaymentName();
        }
        else
        {
            Response.Redirect("http://mso.org.my/payment/OnlineForm.aspx");
            }
        }
    }

    protected int receiptNo()
    {
        //int gStatus = Convert .ToInt32 ( Request.Form["Status"]);
        if (eStatus.Value != "")
        {
            //string gamount = Amount.Value;

            //string setamount = gamount.Replace(".", "");

            //string concertsignature = MerchantKey.Value + MerchantCode.Value + RefNo.Value + setamount + eCurrency.Value;

            //if (Signature.Value == ComputeHash(concertsignature))
            //{ 

            //Response.Write(Signature.Value);

                int getStatus = Convert.ToInt32(Request.Form["Status"]);


                if (getStatus == 1)
                {
                    bizCode bRecCode = new bizCode();
                    DataSet ds;
                    ds = bRecCode.SelectReceiptCode();

                    string reccode = ds.Tables[0].Rows[0]["Receipt_ID"].ToString();
                    int recintcode = Convert.ToInt32(reccode) + 1;
                    txtreceiptNo.Value = Convert.ToString(recintcode);
                    lbltxtreceiptNo.Text = txtreceiptNo.Value;

                    string gsavereceiptcode = Convert.ToString(recintcode);
                    bizCode updatereceiptCode = new bizCode();
                    int savereceiptcode = updatereceiptCode.Updatereceiptcode(gsavereceiptcode);
                    saveReceiptPayment();

                    lblThankYouPayment.Visible = true;
                    Response.Write("<script>alert('Submit Successful!');</script>");

                    bizpaymentform ps = new bizpaymentform();
                    int pstd = ps.Bupdatepaymentstatus(RefNo.Value, eStatus.Value);
                    //Response.Write("123!!");
                }
                else
                {
                    string gErrDesc = Request.Form["ErrDesc"];

                    lblErrorDescTitle.Visible = true;
                    lblDot.Visible = true;
                    lblErrDesc.Visible = true;
                    Response.Write("<script>alert('" + gErrDesc + "!" + "');</script>");
                }
            //}
            //else
            //{
            //    string gErrDesc = Request.Form["ErrDesc"];

            //    lblErrorDescTitle.Visible = true;
            //    lblDot.Visible = true;
            //    lblErrDesc.Visible = true;
            //    Response.Write("<script>alert('" + gErrDesc + "!" + "');</script>");
            //}
            }
            else
            {
                eStatus.Value ="0";
                string gErrDesc = "Fail, Try Again!";

                Response.Write("<script>alert('" + gErrDesc + "!" + "');</script>");
                Response.Write("<script>window.location='OnlineForm.aspx';</script>");
            }
            return 0;
    }

    protected void PaymentName()
    {
        bizpaymentform gFullName = new bizpaymentform();
        DataSet ds;
        ds = gFullName.BPaymentName(RefNo.Value);

        PFullName.Value = ds.Tables[0].Rows[0]["FullName"].ToString();
        lblFullName.Text = PFullName.Value;
        pEventName.Value = ds.Tables[0].Rows[0]["EventTitle"].ToString();
        lblEventName.Text = pEventName.Value;

        HideDateTime.Value = ds.Tables[0].Rows[0]["PaymentDate"].ToString();
        

        lblTime.Text = FEndDate(HideDateTime.Value);
    }

    protected string FEndDate(string setdataenddate)
    {
        string datecombile = setdataenddate;
        string HourNow = datecombile.Substring(11, 2);
        string mmnow= datecombile.Substring(14, 2);
        string ssnow= datecombile.Substring(17, 2);
        string APMnow = datecombile.Substring(20, 2);

        string TimeNow = HourNow + ":" + mmnow + ":" + ssnow + " " + APMnow;
        string gdate = TimeNow;

        return gdate;
    }

    protected void saveReceiptPayment()
    {
        bizpaymentform saveRec = new bizpaymentform();
        
        int saverecNo = saveRec.BupdatereceiptNO(RefNo.Value, txtreceiptNo.Value);
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //Response.Redirect("PrintReceipt.aspx?RefNo=" + RefNo.Text);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("OnlineForm.aspx");
    }

    public static string ComputeHash(string Key)
    {
        SHA1CryptoServiceProvider objSHA1 = new SHA1CryptoServiceProvider();
        objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Key.ToCharArray()));
        byte[] buffer = objSHA1.Hash;
        string HashValue = System.Convert.ToBase64String(buffer);
        return HashValue;
    }





}
