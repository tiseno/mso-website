﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListEventSearch.aspx.cs" Inherits="ListEventSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSO</title>
    <link href="css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Event</td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :10px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="80%" >
    <tr><td  class ="formheader"align ="center" >Event</td></tr>
    
    <tr><td ><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
      
     
    
    <tr><td align ="center" >
    
 <asp:Datagrid ID="dg_SearchControl" runat="server" OnPageIndexChanged="dg_SearchControl_PageIndexChanged" Width ="100%" 
        AutoGenerateColumns="False" 
        BackColor="White" 
        BorderColor="#eeeeee" 
        BorderWidth="2px" 
        CellPadding="4" 
        ForeColor="Black" 
        ItemStyle-VerticalAlign="Top" 
        PageSize="5" 
        AllowPaging="true" 
        PagerStyle-Position="Bottom" 
        PagerStyle-Mode="NextPrev"
        ><%--tis is for paging--%>
        
            <FooterStyle BackColor="#CCCC99" />
            <SelectedItemStyle BackColor="#CE5D5A" Font-Size ="Small"  Font-Bold="False" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
            Mode="NumericPages" />
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#efefef" />
        
       <Columns>
        
            <asp:TemplateColumn HeaderText="No"><itemtemplate>
            <%#Container.ItemIndex + dg_SearchControl.CurrentPageIndex * dg_SearchControl.PageSize + 1%>
            </itemtemplate>
            <HeaderStyle  HorizontalAlign="Center"/>
            <ItemStyle HorizontalAlign="Center"/>
            </asp:TemplateColumn>

            <asp:TemplateColumn HeaderText="EventTitle" Visible ="True" >
            <ItemTemplate>
            <asp:Hyperlink runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventTitle")%>' NavigateUrl='<%#URL(DataBinder.Eval(Container.DataItem, "EventID"))%>'  ID="Hyperlink1"/></ItemTemplate>
            </asp:TemplateColumn>

            <asp:BoundColumn DataField="EventDescription" HeaderText="EventDescription"></asp:BoundColumn>
            <asp:BoundColumn DataField="ECurrency" HeaderText="ECurrency"></asp:BoundColumn>
            <asp:BoundColumn DataField="EFees" HeaderText="EFees"></asp:BoundColumn>
            <asp:BoundColumn DataField="EStatus" HeaderText="EStatus"></asp:BoundColumn>
            
            
            
            </Columns>
            <HeaderStyle BackColor="#2a718d" Font-Bold="False" Font-Size ="Small" ForeColor="White" />
            </asp:Datagrid>   
    
    </td></tr>
       
    </table> 
    </td></tr>
    
    
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>
      
   </table>
    </div>
    </form>
</body>
</html>
