﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="admin_Login.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AMI Student Tracking System</title>
</head>
<body>

<form id="form1" runat="server">
<div align="center" style="vertical-align:middle; height:500px; width:100%;">
<br /><br />

<div style="height: 300px; width: 450px;">
<br /><br />

<table><tr><td align=center;><img src="images/logo.png" border="0" alt="AMI"/></td></tr></table>
<br />
<table style="border: 1px solid #cccccc; width: 73%; text-align:left; padding-left: 0px; padding-right: 0px;" border="0">
<tr>
<td colspan="4" style="font-size:12pt;background-color:#000000; padding-left:33px;"><span style="font-size:14px;color:white"> Administration Login</span>&nbsp;</td></tr>

<%--<tr><td colspan="4" style="text-align:center; font-size:12pt;">&nbsp;</td></tr>
--%>
<tr>
<td style="width:130px; padding:8px 8px 8px 33px; font-size:smaller;">Username</td>
<td style="width:10px;">:</td>
<td style="width:130px;"><asp:TextBox ID="txt_username" runat="server" Width="150px" meta:resourcekey="txt_usernameResource2" Font-Size="Smaller"></asp:TextBox></td>
<td style="width:100px;">&nbsp;</td>
</tr>
<tr>
<td style="width:130px; padding-left:35px; font-size:smaller;">Password</td><td>:</td>
<td><asp:TextBox ID="txt_password" runat="server" TextMode="Password" Width="150px" meta:resourcekey="txt_passwordResource2" Font-Size="Smaller"></asp:TextBox></td>
<td>&nbsp;</td>
</tr>    

<tr>
<td ></td>
<td ></td>
<td align="right" style="padding:8px 8px 8px 8px;">
<asp:Button ID="btn_login" runat="server" Text="Login" Width="60px" onclick="btn_login_Click" meta:resourcekey="btn_loginResource2" /></td>
<td>&nbsp;</td>
</tr>
</table>
<table>
<tr>
<td></td>
<td>&nbsp;</td>
<td>
<asp:Label ID="lbl_invalidMessage" runat="server"  Font-Names="verdana" Font-Size="X-Small" ForeColor="Red" meta:resourcekey="lbl_invalidMessageResource2"></asp:Label></td>
<td>&nbsp;</td>
</tr>
</table>
</div>
</div>
</form>  
    
</body>
</html>
