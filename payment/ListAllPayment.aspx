﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListAllPayment.aspx.cs" Inherits="ListAllPayment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    <tr><td><table style="width:100%; background-color :#d9d6d6; height:20px; " align="center">
      <tr><td style="color:#323232;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">
          Payment Detail Search </td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :10px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%" >
    <%--<tr><td  class ="formheader"align ="left" >Search Detail</td></tr>--%>
    
    <%--<tr><td ><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      <%--<tr><td ><table style="width:100%; height:1px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold;" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <tr><td align ="left" style=" padding-left:0px;">
      <table border ="0" cellpadding ="0" cellspacing ="0" width ="40%" >
      
      <tr><td align ="left" class="style1" >Event</td><td class="style2">:</td>
      <td  align ="left">
      <asp:DropDownList ID="DropEventType" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="EventType_EditChanged" AutoPostBack="false" Width="200px" ></asp:DropDownList>
      <%--<asp:Label ID="ErEventType" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label>--%> 
      <%--<asp:Label ID="ErP6" runat="server" 
              style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
              Font-Bold="true" Text="Please Choose an Event!"></asp:Label>--%> 
          <asp:HiddenField ID="lblEventTypeID" runat="server"  />
      </td></tr>
      
      <tr><td align ="left" class="style1" >Name</td><td class="style2">:</td>
      <td  align ="left">
      <asp:TextBox ID="txtName" Width="200px" runat="server"></asp:TextBox></td>
      </tr> 
      
      <tr><td align ="left" class="style1" >Designation</td><td class="style2">:</td>
      <td  align ="left">
      <asp:DropDownList ID="txtDesignation" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
      <asp:Label ID="lblDesignation" runat="server" Visible ="false"  Text=""></asp:Label>
      </td></tr> 
      
      <tr><td align ="left" class="style1" >Date</td><td class="style2">:</td>
      <td  align ="left">
      <asp:TextBox ID="txtDate" Width="200px" runat="server"></asp:TextBox>
      
      </td>
      </tr> 
      
      <tr><td align ="left" class="style1" >Receipt No</td><td class="style2">:</td>
      <td  align ="left">
      <asp:TextBox ID="txtReceiptNo" Width="200px" runat="server"></asp:TextBox></td>
      </tr> 
      
      <tr><td align ="left" class="style1" >Currency</td><td class="style2">:</td>
      <td  align ="left">
      <asp:DropDownList ID="txtCurrency" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Currency_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
      <asp:Label ID="lblCurrency" runat="server" Visible ="false"  Text=""></asp:Label>
      </td></tr> 
      
      <tr><td align ="left" class="style1" >Payment Status</td><td class="style2">:</td>
      <td  align ="left" class ="Estyle">
      <asp:RadioButtonList ID="radioStatus" Runat="server" Font-Size="8pt" Font-Names="Verdana"  OnSelectedIndexChanged="Status_editchange" BorderWidth="0" CellPadding="0" CellSpacing="0" RepeatLayout="flow" RepeatColumns="3">
            <asp:ListItem Text="Success" Value="true" ></asp:ListItem>
            <asp:ListItem Text="Fail" Value="false"></asp:ListItem>
        </asp:RadioButtonList>   
        <asp:Label ID="lblStatus" runat="server" Visible ="false"  Text=""></asp:Label> 
      </td>
      </tr> 
      
      
      <tr><td align ="left" colspan ="3" style ="padding :10px 20px 10px 0px;">
      <asp:Button ID="btnsearch" runat="server" Text="Search" onclick="btnsearch_Click"/></td>
      </tr> 
      
      </table>
      </td></tr>
  
    </table> 
    </td></tr>
    
    <tr><td><table style="width:100%; background-color :#d9d6d6; height:20px;" align="center">
      <tr><td style="color:#323232;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Payment Detail</td></tr></table>  <br />
      </td></tr>
    
    <tr><td align ="center" style =" padding :0px 10px 10px 10px;">
    <table border ="0" cellpadding="0" cellspacing="0" width ="100%">
      
      <tr align ="left">
      <td style="padding:0px 0px 15px 0px;">
      <asp:Button ID="Extract_Selected_Result" runat="server" Text="Extract to Excel" 
              Enabled="false" onclick="Extract_Selected_Result_Click" /></td>
      </tr>
    
    <tr><td align ="center" >
    
 <asp:Datagrid ID="dg_SearchControl" runat="server" OnPageIndexChanged="dg_SearchControl_PageIndexChanged" Width ="100%" 
        AutoGenerateColumns="False" 
        BackColor="White" 
        BorderColor="#eeeeee" 
        BorderWidth="2px" 
        CellPadding="4" 
        ForeColor="Black" 
        ItemStyle-VerticalAlign="Top" 
        PageSize="50" 
        AllowPaging="true" 
        PagerStyle-Position="Bottom" 
        PagerStyle-Mode="NextPrev"
        ><%--tis is for paging--%>
        
            <FooterStyle BackColor="#CCCC99" />
            <SelectedItemStyle BackColor="#CE5D5A" Font-Size ="Small"  Font-Bold="False" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
            Mode="NumericPages" />
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#efefef" />
        
       <Columns>
        
            <asp:TemplateColumn HeaderText="No"><itemtemplate>
            <%#Container.ItemIndex + dg_SearchControl.CurrentPageIndex * dg_SearchControl.PageSize + 1%>
            </itemtemplate>
            <HeaderStyle  HorizontalAlign="Center"/>
            <ItemStyle HorizontalAlign="Center"/>
            </asp:TemplateColumn>

            <%--<asp:TemplateColumn HeaderText="FullName" Visible ="True" >
            <ItemTemplate>
            <asp:Hyperlink runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FullName")%>' NavigateUrl='<%#URL(DataBinder.Eval(Container.DataItem, "UserDetailID"))%>'  ID="Hyperlink1"/></ItemTemplate>
            </asp:TemplateColumn>--%>
            
            <asp:TemplateColumn HeaderText="Full Name" Visible ="True" >
            <ItemTemplate>
            <asp:Hyperlink runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FullName")%>' NavigateUrl='<%#URL(DataBinder.Eval(Container.DataItem, "UserDetailID"))%>'  ID="Hyperlink1"/></ItemTemplate>
            </asp:TemplateColumn>

            <asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="Designation" HeaderText="Designation"></asp:BoundColumn>
            <asp:BoundColumn DataField="IC_Passport_No" HeaderText="IC/Passport No"></asp:BoundColumn>
            <asp:BoundColumn DataField="Place_Practice" HeaderText="Place Practice"></asp:BoundColumn>
            
            <%--<asp:BoundColumn DataField="User_Address" HeaderText="User_Address"></asp:BoundColumn>--%>
            
            <%--<asp:BoundColumn DataField="City" HeaderText="City"></asp:BoundColumn>
            <asp:BoundColumn DataField="State" HeaderText="State"></asp:BoundColumn>
            <asp:BoundColumn DataField="PostCode" HeaderText="PostCode"></asp:BoundColumn>--%>
            
            <%--<asp:BoundColumn DataField="User_Email" HeaderText="User_Email"></asp:BoundColumn>
            <asp:BoundColumn DataField="Hand_Phone_No" HeaderText="Hand_Phone_No"></asp:BoundColumn>--%>
           <asp:BoundColumn DataField="ECurrency" HeaderText="Currency"></asp:BoundColumn>
            <asp:BoundColumn DataField="Payment_Amount" HeaderText="Payment Amount"></asp:BoundColumn>
            <%--<asp:BoundColumn DataField="Payment_Option" HeaderText="Payment_Option"></asp:BoundColumn>--%>
            <asp:BoundColumn DataField="Reference_ID" HeaderText="Reference_ID"></asp:BoundColumn>
            
            <asp:BoundColumn DataField="Receipt_No" HeaderText="Receipt No"></asp:BoundColumn>
            <asp:BoundColumn DataField="paymentStatus" HeaderText="Payment Status"></asp:BoundColumn>
            <asp:BoundColumn DataField="PaymentDate" HeaderText="Payment Date"></asp:BoundColumn>
            
            </Columns>
            <HeaderStyle BackColor ="#d9d6d6" Font-Bold="False" Font-Size ="Small" ForeColor="#323232" />
            </asp:Datagrid>
    
    
    </td></tr></table></td></tr>
    <%--<tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <%--<tr><td align="center">
      <table style ="padding:10px 10px 10px 10px; width :80%;"><tr><td align ="right" style =" padding-right :30px;" >
      <asp:Button ID="Btnsubmit" runat="server" Text="Submit" onclick="Btnsubmit_Click" /></td></tr></table>
          
      </td></tr>--%>
      
   </table>
    
    </div>
    </form>
</body>
</html>
