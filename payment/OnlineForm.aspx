﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlineForm.aspx.cs" Inherits="payment_OnlineForm" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSO</title>
    <link href="../css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/txtvalidation.js" ></script>
</head>
<script type="text/javascript">
function poponload()
{
//http://localhost:4756/MSOonline/OnlineForm.aspx
   testwindow = window.open ("http://localhost:4756/MSOonline/OnlineForm.aspx","mywindow","menubar=1,resizable=1,width=900,height=1200");
    //testwindow = //window.open("", "mywindow", "location=1,status=1,scrollbars=1,width=100,height=100");
    testwindow.moveTo(0, 0);
}
</script>
<body><%--onload="javascript: poponload()"--%>
    <form id="form1" runat="server" ><%--action='http://localhost/msotest/msotestpostback.php' method='POST'--%>
    <div>
    
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%"  >
    
    
    
    <tr><td align ="center" style ="padding:10px 0px 10px 0px;">
    <table style="width:78%;"><tr><td align ="center"><a href ="../index.html"><img src="../images/logo.jpg" style ="border:none;" alt="" /></a></td></tr></table>
    </td></tr>
    
    <%--<tr><td><table style="width:100%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 15px" align="left"></td></tr></table>
      </td></tr>--%>
      
      <tr><td align ="center" >
    <table border="0" width="100%;" style="background-color:#2a718d; color:White;">
    <tr><td align ="left" style ="color:White ; font-family :tahoma,arial ; font-size :10pt; font-weight :bold ; padding-left :220px;">3rd Annual Scientific Meeting </td></tr></table>
    </td></tr>
      
    <%--<tr><td align ="center" style ="background-color:#E0EBF1; padding:20px 10px 10px 10px;">--%>
    <tr><td align ="center" style ="padding:0px 10px 0px 10px;"><br /><br />
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="63%" >
    
    
    
    <%--<tr>
	 <td align="right" style="width:10px;"><img src="../images/box_left_top.png" /></td>
	 <td style="background-image:url(../images/box_line_vec.png); background-repeat:repeat-x;"></td>
	 <td style="width:10px;"><img src="../images/box_right_top.png" /></td>
</tr>--%>

<tr>
<%--<td style="background-image:url(../images/box_left_h.png); background-repeat:repeat-y; width:10px;"></td>--%>
<td style="background-image:url(../images/content_white.png); background-repeat:repeat;" align="center">

<table style ="border:2px #eeeeee solid;" width="100%" cellpadding ="0" cellspacing ="0">
<tr><td align ="right" style="padding:10px 3px 0px 0px; " colspan ="6">
              <a href ="http://mso.org.my/index.html"><span style ="font-family:tahoma,arial; font-size :8pt;">Back To Main Page</span></a>
                  </td></tr>
<tr><td colspan ="6" class ="formheader"align ="left" ><br />Event Registration</td></tr>
    <tr><td colspan ="6" class ="smallstep" align ="left" ><br /><br />Step 1 : Choose Event<br /><br /></td></tr>
    
    <%--<tr><td colspan ="6"><table class="Dline1" border="0" width="100%" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <tr><td align ="left" class="style1" >Event</td><td class="style2">:</td>
    <td align ="left" colspan="6">
    <asp:DropDownList ID="DropDownListEvent" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Event_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
    
          <asp:Label ID="ErP5" runat="server" 
            style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
            Font-Bold="true" Text="Please choose an event!"></asp:Label> 
    <asp:HiddenField ID="lblEventTitle" runat="server"  /><asp:HiddenField ID="RefNo" runat="server"  />
    <asp:HiddenField ID="lblEventID" runat="server"  />
    <%--<asp:Label ID="lblEventID" runat="server" Visible ="false"  Text=""></asp:Label>--%>
    </td>
    
    <%--<td align ="left" class="style1" >RefNo</td>
    <td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="RefNo" ReadOnly ="true" runat="server"></asp:TextBox></td>--%>
    </tr>
    
    <tr><td align ="left" class="style1" ></td><td class="style2"></td>
    <td align ="left" colspan ="6">
    <asp:RadioButtonList ID="radioEventType" Runat="server" Font-Size="8pt" Font-Names="Verdana" BorderWidth="0" CellPadding="0" CellSpacing="0" OnSelectedIndexChanged="radioEditEventTypeChange" AutoPostBack ="true" RepeatLayout="Table" RepeatColumns="3">
            
    </asp:RadioButtonList>    
    <asp:Label ID="ErP9" runat="server" 
            style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
            Font-Bold="true" Text="Please choose MemberType!"></asp:Label> 
    </td></tr>
    
    <tr><td align ="left"  style ="width:250px; padding :0px 0px 0px 3px; height :23px; font-family :tahoma,arial; font-size :10pt;">Event Description</td><td class="style2">:</td>
    <td align ="left" colspan ="6">
    <asp:Label ID="ProdDesc" runat="server" Text=""></asp:Label>
        <%--<textarea id ="ProdDesc" readonly ="readonly" runat="server" cols="80" name="elm1" rows="20" style="width: 250px; height:60px; "></textarea>--%>
    </td></tr>
     
    <tr><td align ="left" class="style1" >Fees</td><td class="style2">:</td>
    <td align ="left" colspan ="6">
    <asp:Label ID="Currency" runat="server" Text=""></asp:Label>
    <%--    <asp:TextBox ID="Currency" ReadOnly ="true"  runat="server"></asp:TextBox>--%>&nbsp;
    <asp:Label ID="Amount" runat="server" Text=""></asp:Label><asp:Label ID="txtpaymentoption" Visible ="false" runat="server" Text="Credit Card"></asp:Label>
    </td></tr>
    
    <%--<tr><td align ="left" class="style1" >&nbsp;</td>
    <td class="style2">&nbsp;</td>
    <td align ="left" colspan ="6">
    </td></tr>--%>
    
    <%--<tr><td align ="left" class="style1" ></td><td class="style2">&nbsp;</td>
    <td align ="left" colspan ="6">
    </td>
    </tr> --%>
    
    
    
    <%--<tr><td colspan ="6" class ="formheader"align ="center" >User Information</td></tr>--%>
    <tr><td colspan ="6" class ="smallstep" align ="left" ><br /><br />Step 2 : Fill in Your Personal Detail<br /><br /></td></tr>
    
    <%--<tr><td colspan ="6"><table class="Dline1"align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
    
    <tr><td align ="left" class="style1" >Name</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:TextBox ID="UserName" Width="200px" runat="server" ></asp:TextBox>
    
          <asp:Label ID="ErP0" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
    
    </td>
    <td align ="left" class="style1" >Date</td><td class="style2">:</td>
    <td  align ="left" >
    <asp:Label ID="txtDate" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="HideDateTime" runat="server"  /> 
    </td></tr> 
    
    <tr><td align ="left" class="style1" >Designation</td><td class="style2">:</td>
    <td class ="style3 " align ="left" style =" width :450px;">
    <asp:DropDownList ID="txtDesignation" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Prod_EditChanged" AutoPostBack="false" Width="220px" ></asp:DropDownList>
    
          <asp:Label ID="ErP7" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
    
          <asp:Label ID="ErP8" runat="server" 
            style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
            Font-Bold="true" Text="Please Select!"></asp:Label> 
    <asp:HiddenField ID="lblDesignation" runat="server"  /> </td>
    
    <td align ="left" class="style1" >IC/Passport No</td><td class="style2">:</td>
    <td align ="left" style=" width:180px;">
    <asp:TextBox ID="txtIC" runat="server"></asp:TextBox> <asp:Label ID="ErP3" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label>  </td>
    </tr> 
    
    <tr><td align ="left" class="style1" >Place of Practice</td>
        <td class="style2">:</td>
    <td colspan ="6" align ="left" >
    <asp:TextBox ID="txtplcPractice" Width="200px" runat="server"></asp:TextBox>
    
          <asp:Label ID="ErP6" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
    
          </td></tr> 
    
    <tr><td align ="left" valign="top" class="style1" rowspan ="2">Address</td><td valign="top"  class="style2" rowspan ="2">:</td>
    <td align ="left" rowspan ="2">
    <textarea id ="txtAddress" runat="server" cols="80" name="elm1" rows="20" style="width: 250px;height:50px; "></textarea> 
    </td>
    
    <td align ="left" class="style1" >State/Overseas</td><td class="style2">:</td>
    <td align ="left" style ="padding-right :3px;">
    <%--<asp:TextBox ID="txtState" runat="server"></asp:TextBox>--%>
    <asp:DropDownList ID="txtState" runat="server" Font-Size="8pt" 
            Font-Names="verdana" OnSelectedIndexChanged="State_EditChanged" 
            AutoPostBack="false" Width="160px" ></asp:DropDownList>
    
    <asp:HiddenField ID="lblState" runat="server"  /> 
    
                                        </td>
    </tr>
    
    <tr><td align ="left" class="style1" >City</td>
    <td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td></tr> 
    
    <tr>    
    <td align ="left" class="style1" >Hand Phone No.</td><td class="style2">:</td>
    <td align ="left" class="style3">
    <asp:TextBox ID="UserContact" MaxLength="15" runat="server"></asp:TextBox>
    
          <asp:Label ID="ErP1" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
    <asp:Label ID="Label1" runat="server"
            style="color:Gray ; vertical-align:bottom; font-family:Verdana; font-size:8pt;" 
            Font-Bold="false" Text="e.g:0123456789"></asp:Label> 
            
          <asp:Label ID="ErP" runat="server" style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" Font-Bold="true" Text="Invalid No.!"></asp:Label> 
          </td>
    
    <td align ="left" class="style1" >Postcode</td>
    <td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtPoscode" runat="server"></asp:TextBox></td>
    </tr> 
    
    
    
    <tr>
    
    <td align ="left" class="style1" >Email Address</td>
    <td class="style2">:</td>
    <td  align ="left" colspan ="6">
    <asp:TextBox ID="UserEmail" runat="server"></asp:TextBox>
    
          <asp:Label ID="ErP2" runat="server"
            style="color:Red; vertical-align:top; font-family:Verdana; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> 
    
          <asp:Label ID="ErPemail" runat="server" 
            style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" 
            Font-Bold="true" Text="Invalid e-mail Address!"></asp:Label> 
                                        </td></tr> 
    
    <%--<tr><td colspan ="6"><table class="Dline1" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>--%>
      
      <tr><td align="left" colspan="6" style=" font-family:tahoma,arial; font-size:8pt; padding:0px 0px 0px 5px;">
    <br /><br />
          <asp:Label ID="ErP4" runat="server"
            style="color:Red; vertical-align:top; font-family:tahoma,arial; font-size:8pt;" 
            Font-Bold="true" Text="*"></asp:Label> &nbsp;=Fields must be filled.
            <br />
    
    <br /></td></tr>
      
      <tr><td align="center" colspan ="6">
      <table style ="padding:0px 10px 10px 0px; width:100%;">
      <tr><td align ="left" style="width:5px; padding-left :3px;">
        <asp:CheckBox ID="CheckBoxConfirm" runat="server" />
        
      </td><td align="left" style="font-family:tahoma,arial; font-size:8pt;">Yes, I confirm the payment and details of above.<br /></td></tr>
      <tr><td align="left" colspan="6" style ="padding-left :3px;">
    <asp:Label ID="lblEP" runat="server" style="color:Red; font-family:Verdana; font-size:6pt;" Visible="false" Font-Bold="true" Text="Please Check To Confirm !" />
    </td></tr>
    
      <tr><td align ="left" style =" padding:20px 0px 10px 3px;" colspan="2" >
      <asp:Button ID="Btnsubmit" runat="server" Text="Proceed" OnClientClick ="return validateForm(this);" onclick="Btnsubmit_Click" /></td></tr></table>
      
          <%--PostBackUrl="http://localhost/msotest/msotestpostback.php"--%><%-- PostBackUrl="https://www.mobile88.com/ePayment/entry.asp"--%>
      </td></tr>
    
    

</table>

</td>
<%--<td style="background-image:url(../images/box_right_h.png); background-repeat:repeat-y;"></td>--%>
</tr>

<%--<tr>
     <td><img src="../images/box_left_btm.png" /></td>
     <td style="background-image:url(../images/box_line_vec2.png); background-repeat:repeat-x;"></td>
     <td><img src="../images/box_right_btm.png" /></td>
</tr>--%>

    
    </table> 
    </td></tr>
    
    
    
      
      
    
      
      
   </table>
    
    </div>
    </form>
</body>
</html>
