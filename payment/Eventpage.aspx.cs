﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;

public partial class Eventpage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //dropdownlist();
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bizEvent bs = new bizEvent();
        DataSet ds;

        ds = bs.BSearchEvent(txtEventTitle.Text, lblStatus.Text);
        dg_SearchControl.CurrentPageIndex = 0;
        dg_SearchControl.DataSource = ds;
        dg_SearchControl.DataBind();

    }
    //protected void Prod_EditChanged(object sender, EventArgs e)
    //{
    //    lblCurrency.Text = txtCurrency.SelectedValue;
    //}


    protected void Status_editchange(object sender, EventArgs e)
    {
        lblStatus.Text = radioStatus.SelectedValue;
    }

    //protected void dropdownlist()
    //{
    //    txtCurrency.Items.Clear();
    //    txtCurrency.Items.Add(new ListItem("Please Select", "0"));
    //    txtCurrency.Items.Add(new ListItem("MYR", "MYR"));
    //    //txtCurrency.Items.Add(new ListItem("USD", "USD"));

    //}

    protected void alllistEvent()
    {
        bizEvent bs = new bizEvent();
        DataSet ds;

        ds = bs.SelectAllEvent();
        dg_SearchControl.DataSource = ds;
        dg_SearchControl.DataBind();
    }

    protected string URL(object EventID)
    {
        return "EventDetailEdit.aspx?EventID=" + EventID;
    }

    protected void dg_SearchControl_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dg_SearchControl.CurrentPageIndex = e.NewPageIndex;
        //bizEvent bsC = new bizEvent();
        //DataSet dsC;

        //dsC = bsC.SelectAllEvent();
        //dg_SearchControl.DataSource = dsC;
        //dg_SearchControl.DataBind();

        bizEvent bs = new bizEvent();
        DataSet ds;

        ds = bs.BSearchEvent(txtEventTitle.Text, lblStatus.Text);
        dg_SearchControl.DataSource = ds;
        dg_SearchControl.DataBind();
    }
}
