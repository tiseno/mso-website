﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;

public partial class OnlineFormConfirm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            //DateTime Now = DateTime.Now;
            //string str = Now.ToString("dd/MM/yyyy");
            //txtDate.Text = str;
            
            PaymentId.Value = "2";
            Remark.Value = "";

            Usermaxid();
            serialnumbr();
        }

    }

    protected void serialnumbr()
    {
        string gamount = Amount.Value;

        string setamount = gamount.Replace(".", "");

        string concertsignature = MerchantKey.Value + MerchantCode.Value + RefNo.Value + setamount + Currency.Value;

        Signature.Value = ComputeHash(concertsignature);

        //Response.Write(Signature.Value);

    }

    public static string ComputeHash(string Key)
    {
        SHA1CryptoServiceProvider objSHA1 = new SHA1CryptoServiceProvider();
        objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Key.ToCharArray()));
        byte[] buffer = objSHA1.Hash;
        string HashValue = System.Convert.ToBase64String(buffer);
        return HashValue;
    }


    public void Usermaxid()
    {
        bizpaymentform maxid = new bizpaymentform();
        DataSet ds;
        ds = maxid.SelectMaxUserID();

        
            string usermaxid = ds.Tables[0].Rows[0]["UserDetailID"].ToString();
            //return usermaxid;
            ViewDetail(usermaxid);
        
    }

    protected void ViewDetail( string UserMaxID)
    {

        bizpaymentform bs = new bizpaymentform();
        DataSet ds;
        ds = bs.SelectPaymentDetail(UserMaxID);

        if (ds.Tables[0].Rows.Count > 0)
        {

            UserName.Value = ds.Tables[0].Rows[0]["FullName"].ToString();
            lblUserName.Text = UserName.Value;
            txtDesignation.Value = ds.Tables[0].Rows[0]["Designation"].ToString();
            lbltxtDesignation.Text = txtDesignation.Value;
            txtIC.Value = ds.Tables[0].Rows[0]["IC_Passport_No"].ToString();
            lbltxtIC.Text = txtIC.Value;
            txtplcPractice.Value = ds.Tables[0].Rows[0]["Place_Practice"].ToString();
            lbltxtplcPractice.Text = txtplcPractice.Value;

            txtAddress.Value = ds.Tables[0].Rows[0]["User_Address"].ToString();
            lbltxtAddress.Text = txtAddress.Value;
            txtCity.Value = ds.Tables[0].Rows[0]["City"].ToString();
            lbltxtCity.Text = txtCity.Value;
            txtState.Value = ds.Tables[0].Rows[0]["State"].ToString();
            lbltxtState.Text=txtState.Value;

            txtPoscode.Value = ds.Tables[0].Rows[0]["PostCode"].ToString();
            lbltxtPoscode.Text = txtPoscode.Value;
            UserEmail.Value = ds.Tables[0].Rows[0]["User_Email"].ToString();
            lblUserEmail.Text = UserEmail.Value;
            UserContact.Value = ds.Tables[0].Rows[0]["Hand_Phone_No"].ToString();
            lblUserContact.Text = UserContact.Value;
            Currency.Value = ds.Tables[0].Rows[0]["ECurrency"].ToString();
            lblCurrency.Text = Currency.Value;
            Amount.Value=ds.Tables[0].Rows[0]["Payment_Amount"].ToString();
            lblAmount.Text = Amount.Value;
            txtpaymentoption.Text = ds.Tables[0].Rows[0]["Payment_Option"].ToString();
            RefNo.Value = ds.Tables[0].Rows[0]["Reference_ID"].ToString();
            //txtReceiptCode.Text = ds.Tables[0].Rows[0]["Receipt_No"].ToString();

            string dateTimeNow = ds.Tables[0].Rows[0]["PaymentDate"].ToString();

            txtDate.Text = FEndDate(dateTimeNow);

            lblEventTitle.Value = ds.Tables[0].Rows[0]["UserDetail.EventTitle"].ToString();
            lblEventTitlelbl.Text = lblEventTitle.Value;
            ProdDesc.Value = ds.Tables[0].Rows[0]["EventDescription"].ToString();
            lblProdDesc.Text = ProdDesc.Value;
        }
    }
    protected string FEndDate(string setdataenddate)
    {
        string datecombile = setdataenddate;
     
        string Dyear = datecombile.Substring(6, 4);
        string Dmonth = datecombile.Substring(3, 2);
        string Dday = datecombile.Substring(0, 2);
        string DDD = Dday + "/" + Dmonth + "/" + Dyear;
        //DateTime dd = DateTime.Parse(DDD);
        //string str = dd.ToString("dd/MM/yyyy");
        string gdate = DDD;

        return gdate;
    }
    protected void Btnsubmit_Click(object sender, EventArgs e)
    {
        //string gRefCode = refCode();
        //RefNo.Text = refCode();
        //if (RefNo.Text != "")
        //{
        //    bizCode updaterefCoed = new bizCode();
        //    int refcode = updaterefCoed.Updaterefcode(gRefCode);
        //    bizpaymentform insertPForm = new bizpaymentform();
        //    int i = insertPForm.Insertpaymentform(UserName.Text, lblDesignation.Text, txtIC.Text, txtplcPractice.Text, txtAddress.Value, txtCity.Text, txtState.Text, txtPoscode.Text, UserEmail.Text, UserContact.Text, Amount.Text, txtpaymentoption.Text, gRefCode, txtDate.Text, lblEventID.Text, lblEventTitle.Text);

        //    //Response.Write("<script>alert('Submit Successful!');</script>");
        //    //Response.Write("<script>window.location='tmppage.aspx';</script>");

        //}

    }

}
