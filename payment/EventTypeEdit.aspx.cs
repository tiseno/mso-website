﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;

public partial class EventTypeEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dropdownlist();
            viewdetail();
        }
    }

    protected void Prod_EditChanged(object sender, EventArgs e)
    {
         string dropEvent = txtCurrency.SelectedValue;
         lblCurrency.Value = dropEvent;

        if (dropEvent != "0")
        {
            ErP7.Visible = false;
        }
        else if (dropEvent == "0")
        {
            ErP7.Visible = true ;
        }
    }

    protected void dropdownlist()
    {
        txtCurrency.Items.Clear();
        //txtCurrency.Items.Add(new ListItem("Please Select", "0"));
        txtCurrency.Items.Add(new ListItem("MYR", "MYR"));
        //txtCurrency.Items.Add(new ListItem("USD", "USD"));

    }

    protected void viewdetail()
    {
        //string dropEvent = Request.QueryString["EventTypeID"];
        
    
        bizEvent bs = new bizEvent();
        DataSet ds;

        ds = bs.SelectDetailEventTypeB(Request.QueryString["EventTypeID"]);
        if (ds.Tables[0].Rows.Count > 0)
        {

            txtEventTypeTitle.Text = ds.Tables[0].Rows[0]["EventTypeName"].ToString();
            txtCurrency.Text = ds.Tables[0].Rows[0]["ECurrency"].ToString();
            txtFees.Text = ds.Tables[0].Rows[0]["EFees"].ToString();
            radioStatus.SelectedValue = ds.Tables[0].Rows[0]["EventTypeAtivation"].ToString();
            lblTEventID.Value = ds.Tables[0].Rows[0]["TEventID"].ToString();

        }
    }

    public static bool IsInteger(string theValue)
    {
        try
        {
            Convert.ToInt32(theValue);
            return true;
        }
        catch
        {
            return false;
        }
    }

    protected void btnEventTypeUpdate_Click(object sender, EventArgs e)
    {
        //if (lblCurrency.Value != "")
        //{
            string gamountFees = txtFees.Text;
            string setamount = gamountFees.Replace(".", "");

            if (setamount != "" && IsInteger(setamount) == true)
            {

                string gfees = txtFees.Text;
                string gfeesdot = Convert.ToDecimal(String.Format("{0:0.00}", Convert.ToDecimal(gfees))).ToString();

                string EventTypeID = Request.QueryString["EventTypeID"];

                bizEvent UpdatePForm = new bizEvent();
                int i = UpdatePForm.EditEventMbmType(EventTypeID, txtEventTypeTitle.Text, txtCurrency.SelectedValue, gfeesdot, radioStatus.SelectedValue);

                Alert.Show("Update Successful!");
                //viewdetail();

                Response.Redirect("EventDetailEdit.aspx?EventID=" + lblTEventID.Value);
            }

            ErP7.Visible = false;
        //}
        //else
        //{
        //    ErP7.Visible = true;
        //}
    }
}
