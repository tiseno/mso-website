﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChooseEvent.aspx.cs" Inherits="ChooseEvent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/StyleSheet.css"  rel="Stylesheet" type="text/css" />
    <script type="text/javascript" src="js/txtvalidation.js" ></script>
</head>
<body>
    <form id="form1" runat="server" ><%--onsubmit="return validateForm(this)"--%>
    <div>
    
    
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%">
    
    <tr><td align ="center" style ="padding:10px 10px 10px 10px;">
    <table style="width:80%;"><tr><td align ="left"><img src="images/logo.jpg" /></td></tr></table>
    </td></tr>
    
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Online Form</td></tr></table>  <br />
      </td></tr>
      
    <tr><td align ="center" style =" padding :0px 10px 10px 10px;">
    <table border ="0" cellpadding ="0" cellspacing ="0" width ="80%" >
    
    <tr><td colspan ="6" class ="formheader"align ="center" ><br />Event</td></tr>
    <tr><td colspan ="6" class ="smallstep" align ="left" >Step 1 : Choose Event</td></tr>
    
    <tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
      
      <tr><td align ="left" class="style1" >Event</td><td class="style2">:</td>
    <td align ="left">
    <asp:DropDownList ID="DropDownListEvent" runat="server" Font-Size="8pt" Font-Names="verdana" OnSelectedIndexChanged="Event_EditChanged" AutoPostBack="true" Width="200px" ></asp:DropDownList>
    <asp:Label ID="lblEventTitle" runat="server" Visible ="false"  Text=""></asp:Label>
    <asp:Label ID="lblEventID" runat="server" Visible ="false"  Text=""></asp:Label>
    </td>
    
    <td align ="left" class="style1" >RefNo</td>
    <td class="style2">:</td>
    <td  align ="left" >
    <asp:TextBox ID="RefNo" ReadOnly ="true" runat="server"></asp:TextBox></td>
    </tr>
    
    <tr><td align ="left" class="style1" >Event Description</td><td class="style2">:</td>
    <td align ="left" colspan ="6">
        <textarea id ="ProdDesc" readonly ="readonly" runat="server" cols="80" name="elm1" rows="20" style="width: 250px; height:60px; "></textarea>
    </td></tr>
     
    <tr><td align ="left" class="style1" >Currency</td><td class="style2">:</td>
    <td align ="left" colspan ="6">
        <asp:TextBox ID="Currency" ReadOnly ="true"  runat="server"></asp:TextBox>
    </td></tr>
    
    <tr><td align ="left" class="style1" >Payment Amount</td><td class="style2">:</td>
    <td align ="left" class="style3">
    
    <asp:TextBox ID="Amount" ReadOnly ="true"  runat="server"></asp:TextBox>
    </td>
    <td align ="left" class="style1" >Payment Type</td><td class="style2">:</td>
    <td align ="left" >
    <asp:TextBox ID="txtpaymentoption" ReadOnly ="true" Text="Credit Card" runat="server"></asp:TextBox>
        <%--<asp:Label ID="txtpaymentoption" runat="server" ></asp:Label>--%> <br />
    </td>
    </tr> 
    
   <tr><td colspan ="6"><table class="Dline" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt; font-weight:bold; padding:3px 10px 3px 10px" align="left"></td></tr></table>  <br />
      </td></tr>
    
    
    
    <tr><td align="left" colspan ="6">
      <table style ="padding:0px 3px 0px 10px; width :80%;">
      
      <tr><td align ="left" style =" padding-left :5px;" >
      <asp:Button ID="BtnNext" runat="server" OnClientClick ="return validateForm(this);" PostBackUrl="http://localhost:4756/MSOonline/FillUserDetail.aspx" Text="Next"  />
      </td> </tr></table><%----%><%--"if(!confirm('Switch page?'))return false;"--%><%----%>
      <%-- --%><%-- --%>
      </td></tr>
    
    
    
    <tr><td colspan ="6"><table cellpadding ="0" cellspacing ="0">
    <tr>
    <td align ="left" class="style3">
    <%--<asp:HiddenField ID="MerchantKey" runat="server" Value="Fg0ESGVFKx" />--%>
    
    </td>
    </tr> 
    </table></td></tr>
    
    </table> 
    </td></tr>    
    
    <tr><td><table style="width:80%; height:20px; background-color:#2a718d;" align="center">
      <tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:10px 10px 3px 10px;" align="left"></td></tr></table>  <br />
      </td></tr>

      
   </table>
    
    </div>
    </form>
</body>
</html>

