﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btn_login_Click(object sender, EventArgs e)
    {
        BizAdminLogin bal = new BizAdminLogin();
        int check = 0;

        check = bal.AdminLogin(txt_username.Text, txt_password.Text);

        if (check == 0)
        {
            lbl_invalidMessage.Text = "Invalid username or password!";

        }
        else if(check == 1)
        {
            HttpCookie aCookie = new HttpCookie("UserType");
            aCookie.Value = "AdminUser";
            aCookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(aCookie);

            BizAdminLogin bal2 = new BizAdminLogin();
            string display_name = "";

            display_name = bal2.FetchDisplayName(txt_username.Text, txt_password.Text);

            Response.Redirect("admin_Control.aspx?display_name=" + display_name);
        }
        else if (check == 2)
        {
            HttpCookie aCookie = new HttpCookie("UserType");
            aCookie.Value = "User";
            aCookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(aCookie);

            BizAdminLogin bal2 = new BizAdminLogin();
            string display_name = "";

            display_name = bal2.FetchDisplayName(txt_username.Text, txt_password.Text);

            Response.Redirect("admin_Control.aspx?display_name=" + display_name);
        }

    }
}
