﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function placeFocus() {
if (document.forms.length > 0) {
var field = document.forms[0];
for (i = 0; i < field.length; i++) {
if ((field.elements[i].type == "text") || (field.elements[i].type == "textarea") || (field.elements[i].type.toString().charAt(0) == "s")) {
document.forms[0].elements[i].focus();
break;
         }
      }
   }
}
//  End -->
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY - Login</title>
</head>
<body bgcolor="gray" OnLoad="placeFocus()">
    <form id="form1" runat="server">
    <br /><br />
        <table width="100%">
        <tr>
            <td align="center"><table style="border-width:1px; border-style:dotted; border-color:Gray;" width="350px" bgcolor="white">
                <tr>
                    <td colspan="3" align="center"><br />
                    <img src="images/logo.jpg" alt="mso" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right" class="style1">
                        <span style="font-size: 8pt; font-family: Verdana">
                        Username</span></td>
                    <td>
                        :</td>
                    <td style="text-align: left; width: 175px;">
                        <asp:TextBox ID="txt_username" runat="server" Width="148px" MaxLength="15"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right" class="style1">
                        <span style="font-size: 8pt; font-family: Verdana">
                        Password</span></td>
                    <td>
                        :</td>
                    <td style="text-align: left; width: 175px;">
                        <asp:TextBox ID="txt_password" runat="server" TextMode="Password" Width="149px" MaxLength="15"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; " class="style2">
                        &nbsp;</td>
                    <td style="height: 21px">
                        &nbsp;</td>
                    <td style="text-align: left; height: 21px; width: 175px;">
                        <asp:Button ID="btn_login" runat="server" onclick="btn_login_Click" 
                            Text="Login" Width="97px" Font-Names="Verdana" Font-Size="9pt" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right" class="style1">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td style="text-align: left; width: 175px;">
                        <asp:Label ID="lbl_InvalidLogin" runat="server" Font-Names="Verdana" 
                            Font-Size="8pt" ForeColor="Red" Text="Invalid username or password." 
                            Visible="False" Width="228px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2" style="height: 20px; text-align: right">
                    </td>
                    <td style="height: 20px">
                    </td>
                    <td style="height: 20px; text-align: left; width: 175px;">
                    </td>
                </tr>
            </table></td>
        </tr>
        </table>
    <p align="center"><span style="font-family: Verdana; font-size:8pt; color: whitesmoke;">© 2010 Malaysia Society of Ophthalmology. All Rights Reserved. Powered by <a href="http://www.quality-direct.net" target="_blank"> Quality Direct</a></span></p>
    
    </form>
</body>
</html>
