﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminMain.aspx.cs" Inherits="AdminMain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY - Admin Control</title>
    <style type="text/css">
        .style1
        {
            width: 54%;
        }
        </style>
</head>
<body bgcolor="gray">
    <form id="form1" runat="server">
    <div>
        <div>
            <table class="style1" bgcolor="whitesmoke" style="width: 682px; height: 86px;">
                <tr>
                    <td style="font-size: 8pt; font-family: Verdana; height: 22px"><br />
                    <span style="font-family: Verdana; font-size:13pt;">&nbsp;<strong><span style="color: #323232">Malaysia Society of Ophthalmology</span></strong></span><br /><br />
                        <span style="font-family: Verdana; font-size:11pt;">&nbsp;Administration Control Panel</span><br />
                        <br />
                        <table style="height: 70px">
                            <tr>
                                <td style="width: 130px; height: 56px; border-right: lightgrey 1px dotted; border-top: lightgrey 1px dotted; border-left: lightgrey 1px dotted; border-bottom: lightgrey 1px dotted;width: 154px; height: 84px;" align="center" valign="bottom"><a href="Register.aspx"><img src="images/addusers.png" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none" /></a></td>
                                <td style="width: 130px; height: 56px; border-right: lightgrey 1px dotted; border-top: lightgrey 1px dotted; border-left: lightgrey 1px dotted; border-bottom: lightgrey 1px dotted;width: 127px; height: 84px;" align="center" valign="bottom"><a href="SearchMember.aspx"><img src="images/search_f2.png" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none" /></a></td>
                                <td style="width: 130px; height: 56px; border-right: lightgrey 1px dotted; border-top: lightgrey 1px dotted; border-left: lightgrey 1px dotted; border-bottom: lightgrey 1px dotted;width: 127px; height: 84px;" align="center" valign="bottom"><a href="newsletter.aspx"><img src="images/generic.png" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none" /></a></td>                                
                                <td style="width: 130px; height: 56px; border-right: lightgrey 1px dotted; border-top: lightgrey 1px dotted; border-left: lightgrey 1px dotted; border-bottom: lightgrey 1px dotted;" valign="bottom" align="center"><a href="login.aspx"><img src="images/cancel_f2.png" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none" /></a></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:LinkButton ID="LinkButton2" runat="server" Font-Names="Verdana" Font-Size="8pt" OnClick="Link_CreateNewMember_Click">Create New Member</asp:LinkButton></td>
                                <td align="center">
                                    <asp:LinkButton ID="LinkButton3" runat="server" onclick="Link_SearchMember_Click" Font-Names="Verdana" Font-Size="8pt">Search Member</asp:LinkButton></td>
                                <td align="center">                                  
                                    <asp:LinkButton ID="LinkButton4" runat="server" onclick="Link_newletter_Click" Font-Names="Verdana" Font-Size="8pt">Newsletter / Address</asp:LinkButton></td>                                
                                <td align="center">                                   
                                    <asp:LinkButton ID="LinkButton5" runat="server" Font-Names="Verdana" Font-Size="8pt" ForeColor="Red" OnClick="Link_Logout_Click">Logout</asp:LinkButton></td>
                            </tr>  
                            
                            <tr>
                                <td style="width: 130px; height: 56px; border-right: lightgrey 1px dotted; border-top: lightgrey 1px dotted; border-left: lightgrey 1px dotted; border-bottom: lightgrey 1px dotted;width: 154px; height: 84px;" align="center" valign="bottom"><a href="payment/Event_Control.aspx"><img src="images/event_management_logo.png" height ="48px" width ="48px" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none" /></a></td>
                                <td style="width: 130px; height: 56px; border-right: lightgrey 1px dotted; border-top: lightgrey 1px dotted; border-left: lightgrey 1px dotted; border-bottom: lightgrey 1px dotted;width: 127px; height: 84px;" align="center" valign="bottom"><a href="proposal/admin/search.aspx"><img src="images/proposal.jpg"height ="48px" width ="48px" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none" /></a></td>
                                <td style="width: 130px; height: 56px; border-right: lightgrey 1px dotted; border-top: lightgrey 1px dotted; border-left: lightgrey 1px dotted; border-bottom: lightgrey 1px dotted;width: 127px; height: 84px;" align="center" valign="bottom"></td>                                
                                <td style="width: 130px; height: 56px; border-right: lightgrey 1px dotted; border-top: lightgrey 1px dotted; border-left: lightgrey 1px dotted; border-bottom: lightgrey 1px dotted;" valign="bottom" align="center"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:LinkButton ID="BtnEventControl" runat="server" Font-Names="Verdana" 
                                        Font-Size="8pt" onclick="BtnEventControl_Click">Event Control</asp:LinkButton></td>
                                <td align="center">
                                    <asp:LinkButton ID="btnAbstractSubmission" runat="server" Font-Names="Verdana" 
                                        Font-Size="8pt" onclick="btnAbstractSubmission_Click">Abstract Submission</asp:LinkButton></td>
                                <td align="center">                                  
                                    </td>                                
                                <td align="center">                                   
                                    </td>
                            </tr>     
                                                  
                        </table>
                            <span style="font-size: 8pt; font-family: Verdana">
                                <br />
                            </span>
                        
                    </td>
                </tr>
                <tr>
                    <td style="height: 22px; font-size: 8pt; font-family: Verdana;">
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        © 2010 Malaysia Society of Ophthalmology.
                                All Rights Reserved. 
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel_Alert_Message" runat="server" Width="682px" BackColor="whitesmoke">
                <span style="font-family: Verdana; font-size:11pt;">&nbsp;Expired/Unpaid Member</span><br />
                <asp:Datagrid ID="Dg_AlertExpiredMember" runat="server" Width="90%" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">
                    <Columns>
                    <asp:TemplateColumn headertext="No"><itemtemplate><%#Container.ItemIndex + Dg_AlertExpiredMember.CurrentPageIndex * Dg_AlertExpiredMember.PageSize + 1%></itemtemplate>
                    <ItemStyle Width="35px" />
                    </asp:TemplateColumn> 
                        <asp:TemplateColumn HeaderText="Member Name">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Memb_Name")%>' NavigateUrl='<%# "./UpdateMemberProfile.aspx?id=" + DataBinder.Eval(Container.DataItem,"Profile_No") %>'></asp:HyperLink>
                        </ItemTemplate>
                            <HeaderStyle Width="200px" />
                     </asp:TemplateColumn>
                        <asp:BoundColumn DataField="NRIC" HeaderText="Member IC"></asp:BoundColumn>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                    <SelectedItemStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                    <ItemStyle Font-Names="Verdana" Font-Size="9pt" />
                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="9pt" Font-Strikeout="False" Font-Underline="False" BackColor="DimGray" ForeColor="White" />
                </asp:Datagrid><br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                </asp:Panel>
            <br />

        </div>
    <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
    </div>
    </form>
</body>
</html>
