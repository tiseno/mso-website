using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MSO_BizLayer;

public partial class memberlist_by_state : System.Web.UI.Page
{
    private BizMember bm = new BizMember();
    private BizMember bm2 = new BizMember();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bm = new BizMember();
            DataSet ds = new DataSet();
            string state;

            ds = bm.SelectDistinctState();

            td_member_by_state.InnerHtml = "<table><tr><td>";
            for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
            {
                state = ds.Tables[0].Rows[i]["office_state"].ToString();
                bm2 = new BizMember();
                DataSet ds2 = new DataSet();
                ds2 = bm2.SelectMemberByState(state);

                td_member_by_state.InnerHtml += "<br><br><a name='" + state + "'><span style='font-size:14pt;'>" + state + "</span></a>";
                td_member_by_state.InnerHtml += "<table width='820px' class='table_border' cellpadding='0' cellspacing='0'>";
                td_member_by_state.InnerHtml += "<tr><td class='td_border_start' width='50' bgcolor='#23607C'><span style='color:white;'>Title</span></td>";
                td_member_by_state.InnerHtml += "<td class='td_border_mid' width='250' bgcolor='#23607C'><span style='color:white;'>Name</span></td>";
                td_member_by_state.InnerHtml += "<td class='td_border_mid' width='250' bgcolor='#23607C'><span style='color:white;'>Address</span></td>";
                td_member_by_state.InnerHtml += "<td class='td_border_mid' width='120' bgcolor='#23607C'><span style='color:white;'>City/Town</span></td>";
                td_member_by_state.InnerHtml += "<td class='td_border_mid' width='120' bgcolor='#23607C'><span style='color:white;'>State</span></td>";
                td_member_by_state.InnerHtml += "<td class='td_border_mid' width='60' bgcolor='#23607C'><span style='color:white;'>Postcode</span></td>";
                td_member_by_state.InnerHtml += "<td class='td_border_end' width='150' bgcolor='#23607C'><span style='color:white;'>Office Tel.</span></td></tr>";
                for (int x = 0; x < ds2.Tables[0].Rows.Count; x++)
                {
                    if (x % 2 == 1)
                    {
                        td_member_by_state.InnerHtml += "<tr><td class='td_border_start' width='50' bgcolor='white'>" + ds2.Tables[0].Rows[x]["Memb_Title"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='250' bgcolor='white'>" + ds2.Tables[0].Rows[x]["Memb_Name"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='250' bgcolor='white'>" + ds2.Tables[0].Rows[x]["Office_Address"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='120' bgcolor='white'>" + ds2.Tables[0].Rows[x]["Office_City"].ToString() + "&nbsp;</td>";

                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='120' bgcolor='white'>" + ds2.Tables[0].Rows[x]["Office_State"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='60' bgcolor='white'>" + ds2.Tables[0].Rows[x]["Office_Code"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_end' width='150' bgcolor='white'>" + ds2.Tables[0].Rows[x]["O_Phone"].ToString() + "&nbsp;</td></tr>";
                    }
                    else {
                        td_member_by_state.InnerHtml += "<tr><td class='td_border_start' width='50' bgcolor='whitesmoke'>" + ds2.Tables[0].Rows[x]["Memb_Title"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='250' bgcolor='whitesmoke'>" + ds2.Tables[0].Rows[x]["Memb_Name"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='250' bgcolor='whitesmoke'>" + ds2.Tables[0].Rows[x]["Office_Address"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='120' bgcolor='whitesmoke'>" + ds2.Tables[0].Rows[x]["Office_City"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='120' bgcolor='whitesmoke'>" + ds2.Tables[0].Rows[x]["Office_State"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_mid' width='60' bgcolor='whitesmoke'>" + ds2.Tables[0].Rows[x]["Office_Code"].ToString() + "&nbsp;</td>";
                        td_member_by_state.InnerHtml += "<td class='td_border_end' width='150' bgcolor='whitesmoke'>" + ds2.Tables[0].Rows[x]["O_Phone"].ToString() + "&nbsp;</td></tr>";

                    }
                }
                td_member_by_state.InnerHtml += "</table><table width='820' cellpadding='0' cellspacing='0'><tr><td align='right'><br><a href='memberlist_by_state.aspx#top'>Back to Top</a></td></tr></table>";
            }
            td_member_by_state.InnerHtml += "</td></tr></table>";
        }
    }
}
