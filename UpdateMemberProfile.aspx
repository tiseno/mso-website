﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateMemberProfile.aspx.cs" Inherits="UpdateMemberProfile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY - Update Profile</title>
    <script type="text/javascript" src="ezcalendar.js"></script>
	<link rel="stylesheet" type="text/css" href="ezcalendar.css" />
	<%--added by kavi 03/04/2010--%>
	<script type ="text/javascript" >
    function CheckIsNumeric()
    {
    var AsciiCode = event.keyCode;
    if ((AsciiCode < 48) || (AsciiCode > 57))
    {
    //alert('Please enter only numbers.');
    event.cancelBubble = true;
    event.returnValue = false;
    }
    }
    </script>
    <%--end added by kavi 03/04/2010--%>
    <style type="text/css">

    </style>
</head>
<body bgcolor="whitesmoke">
    <form id="form1" runat="server">
    <div>
        <div style="width: 872px; height: 55px">
            <asp:Panel ID="Panel1" runat="server" Height="53px" Width="882px" Visible="false">
            <div>
                <table style="border: 1px solid; border-color:gray; width: 880px; font-family: Verdana; font-size: 8pt; padding-left: 5px;" bgcolor="white" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="5"><span style="font-family: Verdana; font-size:10pt;">Membership Particulars | <a href="#addpoint" class="faqlink">
                            <span style="font-size: 8pt">Addresses</span></a><span style="font-size: 8pt"> | </span>
                            <a href="#quapoint" class="faqlink"><span style="font-size: 8pt">Qualification</span></a><span
                                style="font-size: 8pt"> | </span><a href="#paymentpoint" class="faqlink"><span style="font-size: 8pt">
                                    Payment</span></a></span>
                            <br /><br /></td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            Membership No : 
                            <asp:TextBox ID="memberno" runat="server" Font-Size="8pt"></asp:TextBox>
                            Status : 
                            <asp:DropDownList ID="isactive" runat="server" Font-Names="verdana" Font-Size="8pt">
                                <asp:ListItem Value="0">Active</asp:ListItem>
                                <asp:ListItem Value="1">Resigned</asp:ListItem>
                                <asp:ListItem Value="2">Ceased</asp:ListItem>
                            </asp:DropDownList>
                            Join Date :  <asp:TextBox ID="joindt" runat="server" Width="109px" Font-Size="8pt"></asp:TextBox>
                            <a href="javascript: showCalendar('joindt')" ><img alt="" src="images/ew_calendar.gif" border="0" /></a>
                            <%--added by kavi 03/04/2010--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="joindt"
                                Display="dynamic" ErrorMessage="*Invalid Date Format." Font-Names="Verdana" Font-Size="8pt" SetFocusOnError="true"
                                ValidationExpression="([1-9]|0[1-9]|[12][0-9]|3[01])[- /.]([1-9]|0[1-9]|1[012])[- /.][0-9]{4}$">*Invalid Date Format.
                            </asp:RegularExpressionValidator>
                            <%--end added by kavi 03/04/2010--%>
                            Last Save: 
                            <asp:Label ID="lastsave" runat="server" Font-Names="verdana" Font-Size="8pt"></asp:Label></td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 241px; height: 21px; ">
                            Membership Type</td>
                        <td colspan="2" style="height: 21px; width: 241px;">
                            &nbsp;<asp:DropDownList ID="ddl_MemberShipType" runat="server" Font-Names="verdana" Font-Size="8pt">
                                <asp:ListItem Value="1">Ordinary Member</asp:ListItem>
                                <asp:ListItem Value="2">Life Member</asp:ListItem>
                                <asp:ListItem Value="3">Associate Member</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lbl_CurMembType" runat="server" Width="123px" Visible="true"></asp:Label></td>
                        <td style=" width: 241px; height: 21px">
                            </td>
                        <td style="width: 241px; height: 21px;">
                            </td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 241px; height: 21px; ">
                            Member of MMAOS</td>
                        <td colspan="2" style="height: 21px; width: 241px;">
                            &nbsp;<asp:DropDownList ID="ddl_mmaos" runat="server" Font-Names="verdana" Font-Size="8pt">
                                <asp:ListItem Value="NO">NO</asp:ListItem>
                                <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                <asp:ListItem Value="LAPSED">LAPSED</asp:ListItem>
                        </asp:DropDownList>
                            <asp:Label ID="lbl_ddl_mmaos" runat="server" Width="123px" Visible="true"></asp:Label></td>
                        <td style=" width: 241px; height: 21px">
                        </td>
                        <td style="width: 241px; height: 21px;">
                            </td>
                    </tr>                    

                    
                    <tr style="font-size: 8pt">
                        <td style="width: 241px; height: 21px; ">
                            User Type Selection</td>
                        <td colspan="2" style="height: 21px; width: 241px;">
                            <asp:RadioButton ID="Radio_userType1" runat="server" Text="Administrator" Checked="True" GroupName="userType" />
                            <asp:RadioButton ID="Radio_userType2" runat="server" Text="Member" GroupName="userType" Font-Size="8pt" /></td>
                        <td style=" width: 241px; height: 21px">
                        </td>
                        <td style="width: 241px; height: 21px;">
                            <asp:Label ID="lbl_userNo" runat="server" Visible="False"></asp:Label></td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 241px; height: 21px">
                        </td>
                        <td colspan="2" style="width: 241px; height: 21px">
                        </td>
                        <td style="width: 241px; height: 21px">
                        </td>
                        <td style="width: 241px; height: 21px">
                        </td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 269px; height: 21px;">
                            Username<span style="color: red">*</span></td>
                        <td colspan="2" style="height: 21px">
                            <asp:Label ID="txt_username" runat="server"></asp:Label>
                            </td>
                        <td style="height: 21px">
                        </td>
                        <td style="width: 241px; height: 21px">
                        </td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 269px; height: 21px;">
                            Password<span style="color: red">*</span></td>
                        <td colspan="2" style="height: 21px">
                            <asp:TextBox ID="txt_password" runat="server" Font-Size="8pt"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ErrorMessage="Invalid Password" ControlToValidate="txt_password" Font-Names="Verdana" Font-Size="7pt"></asp:RequiredFieldValidator></td>
                        <td>
                            </td>
                        <td style="width: 241px; height: 21px">
                        </td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 269px; height: 21px">
                        </td>
                        <td colspan="2" style="height: 21px">
                        </td>
                        <td>
                        </td>
                        <td style="width: 241px; height: 21px">
                        </td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 269px; height: 7px">
                            
                            Name <span style="color: red">*</span></td>
                        <td style="height: 7px" colspan="2">
                            <asp:TextBox ID="txt_Name" runat="server" Width="244px" Font-Size="8pt"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ErrorMessage="Invalid" ControlToValidate="txt_Name" Font-Names="Verdana" Font-Size="7pt"></asp:RequiredFieldValidator></td>
                        <td style="height: 7px" >
                            
                            Gender</td>
                        <td style="width: 241px; height: 7px">
                            <asp:RadioButton ID="Radio_Male" runat="server" GroupName="gend" Text="Male" Checked="True" />&nbsp;
                            <asp:RadioButton ID="Radio_Female" runat="server" GroupName="gend" Text="Female" /></td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 269px; height: 24px;">
                            
                            Title <span style="font-size: 7pt">
                            (eg. Dato')</span></td>
                        <td style="width: 169px; height: 24px;">
                            <asp:DropDownList ID="ddl_Title" runat="server" Width="102px" OnSelectedIndexChanged="ddl_Title_SelectedIndexChanged" Font-Size="8pt">
                                <asp:ListItem>Select Title</asp:ListItem>
                                <%--added by kavi 17/05/10--%>
                                <asp:ListItem>Datuk Dr.</asp:ListItem>
                                <asp:ListItem>Dato Dr.</asp:ListItem>
                                <asp:ListItem>Datin Dr.</asp:ListItem>
                                <asp:ListItem>Associate Prof. Dr.</asp:ListItem>
                                <%--end added by kavi 17/05/10--%>
                                <asp:ListItem>Dato</asp:ListItem>
                                <asp:ListItem>Prof</asp:ListItem>
                                <asp:ListItem>Dr</asp:ListItem>
                                <asp:ListItem>Others</asp:ListItem>
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txt_TitleOthers" runat="server" Width="43px" Visible="False"></asp:TextBox></td>
                        <td style="width: 141px; height: 24px;">
                        </td>
                        <td style="height: 24px">
                            
                            Date of Birth</td>
                        <td style="width: 241px; height: 24px;">
                            <asp:DropDownList ID="ddl_DOB_Day" runat="server" Width="47px" Font-Size="8pt">
                                <asp:ListItem>dd</asp:ListItem>
                            </asp:DropDownList><asp:DropDownList ID="ddl_DOB_Month" runat="server" Width="46px" Font-Size="8pt">
                                <asp:ListItem>mm</asp:ListItem>
                            </asp:DropDownList><asp:DropDownList ID="ddl_DOB_Year" runat="server" Width="59px" Font-Size="8pt">
                                <asp:ListItem>yyyy</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            
                            Nationality</td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_Nationality" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px;">
                        </td>
                        <td>
                            
                            NRIC Number (New)<span
                                style="color: red">*</span></td>
                        <td style="width: 241px">
                            <asp:TextBox ID="txt_IC" runat="server" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px">
                        </td>
                        <td style="width: 169px; height: 21px">
                        </td>
                        <td style="width: 141px;">
                        </td>
                        <td>
                        </td>
                        <td style="width: 241px; height: 21px"><a name="addpoint" id="addpoint"></a>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                ErrorMessage="Invalid IC Number or Passport No." ControlToValidate="txt_IC" Font-Names="Verdana" Font-Size="7pt"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 20px;">
                            
                            Mailing Address</td>
                        <td colspan="2" style="height: 20px">
                            <asp:RadioButton ID="Radio_MailingAddress1" runat="server" GroupName="mailing" Text="Office Address" Checked="True" />
                            <asp:RadioButton ID="Radio_MailingAddress2" runat="server" GroupName="mailing" Text="Home Address" />
                            <span style="color: #0000ff; text-decoration: underline"><a href="#top" class="toplink" >top</a></span></td>
                        <td style="height: 20px">
                        </td>
                        <td style="width: 241px; height: 20px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 18px;">
                            
                            Home Address</td>
                        <td style="height: 18px;" colspan="4">
                            <asp:TextBox ID="txt_HomeAddress" runat="server" Width="672px" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 16px;">
                            City/Town</td>
                        <td style="width: 169px; height: 16px;">
                            <asp:TextBox ID="txt_HomeCity" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 16px;">
                            </td>
                        <td style="height: 16px" >
                            &nbsp;Postcode</td>
                        <td style="width: 241px; height: 16px;">
                            <asp:TextBox ID="txt_HomeCode" runat="server" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 19px;">
                            State</td>
                        <td style="width: 169px; height: 19px;">
                            <asp:DropDownList ID="txt_HomeState" runat="server" Font-Size="8pt">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="JOHOR" Value="JOHOR"></asp:ListItem>
                                <asp:ListItem Text="KEDAH" Value="KEDAH"></asp:ListItem>
                                <asp:ListItem Text="KELANTAN" Value="KELANTAN"></asp:ListItem>
                                <asp:ListItem Text="KUALA LUMPUR" Value="KUALA LUMPUR"></asp:ListItem>
                                <asp:ListItem Text="LABUAN" Value="LABUAN"></asp:ListItem>
                                <asp:ListItem Text="MELAKA" Value="MELAKA"></asp:ListItem>
                                <asp:ListItem Text="NEGERI SEMBILAN" Value="NEGERI SEMBILAN"></asp:ListItem>
                                <asp:ListItem Text="PAHANG" Value="PAHANG"></asp:ListItem>
                                <asp:ListItem Text="PERAK" Value="PERAK"></asp:ListItem>
                                <asp:ListItem Text="PERLIS" Value="PERLIS"></asp:ListItem>
                                <asp:ListItem Text="PULAU PINANG" Value="PULAU PINANG"></asp:ListItem>
                                <asp:ListItem Text="SABAH" Value="SABAH"></asp:ListItem>
                                <asp:ListItem Text="SARAWAK" Value="SARAWAK"></asp:ListItem>
                                <asp:ListItem Text="SELANGOR" Value="SELANGOR"></asp:ListItem>
                                <asp:ListItem Text="SINGAPORE" Value="SINGAPORE"></asp:ListItem>
                                <asp:ListItem Text="TERENGGANU" Value="TERENGGANU"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 141px; height: 19px;">
                            </td>
                        <td style="height: 19px">
                            &nbsp;Country</td>
                        <td style="width: 241px; height: 19px;">
                            <asp:DropDownList ID="txt_HomeCountry" runat="server" Font-Size="8pt">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="Malaysia" Value="Malaysia"></asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 14px">
                            
                            Office Address</td>
                        <td style="height: 14px" colspan="4">
                            <asp:TextBox ID="txt_OfficeAddress" runat="server" Width="672px" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 18px">
                            City/Town</td>
                        <td style="width: 169px; height: 18px">
                            <asp:TextBox ID="txt_OfficeCity" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; font-size: 8pt; font-family: Verdana; height: 18px;">
                            </td>
                        <td style="height: 18px">
                            &nbsp;Postcode</td>
                        <td style="width: 241px; height: 18px">
                            <asp:TextBox ID="txt_OfficeCode" runat="server" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            State</td>
                        <td style="width: 169px">
                            <asp:DropDownList ID="txt_OfficeState" runat="server" Font-Size="8pt">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="JOHOR" Value="JOHOR"></asp:ListItem>
                                <asp:ListItem Text="KEDAH" Value="KEDAH"></asp:ListItem>
                                <asp:ListItem Text="KELANTAN" Value="KELANTAN"></asp:ListItem>
                                <asp:ListItem Text="KUALA LUMPUR" Value="KUALA LUMPUR"></asp:ListItem>
                                <asp:ListItem Text="LABUAN" Value="LABUAN"></asp:ListItem>
                                <asp:ListItem Text="MELAKA" Value="MELAKA"></asp:ListItem>
                                <asp:ListItem Text="NEGERI SEMBILAN" Value="NEGERI SEMBILAN"></asp:ListItem>
                                <asp:ListItem Text="PAHANG" Value="PAHANG"></asp:ListItem>
                                <asp:ListItem Text="PERAK" Value="PERAK"></asp:ListItem>
                                <asp:ListItem Text="PERLIS" Value="PERLIS"></asp:ListItem>
                                <asp:ListItem Text="PULAU PINANG" Value="PULAU PINANG"></asp:ListItem>
                                <asp:ListItem Text="SABAH" Value="SABAH"></asp:ListItem>
                                <asp:ListItem Text="SARAWAK" Value="SARAWAK"></asp:ListItem>
                                <asp:ListItem Text="SELANGOR" Value="SELANGOR"></asp:ListItem>
                                <asp:ListItem Text="SINGAPORE" Value="SINGAPORE"></asp:ListItem>
                                <asp:ListItem Text="TERENGGANU" Value="TERENGGANU"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 141px;">
                            </td>
                        <td >
                            &nbsp;Country</td>
                        <td style="width: 241px">
                            <asp:DropDownList ID="txt_OfficeCountry" runat="server" Font-Size="8pt">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="Malaysia" Value="Malaysia"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                        </td>
                        <td style="width: 169px">
                        </td>
                        <td style="width: 141px">
                        </td>
                        <td>
                        </td>
                        <td style="width: 241px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            Phone No. (Home)</td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_HomePhone" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                        </td>
                        <td>
                            &nbsp;Fax No. (Home)</td>
                        <td style="width: 241px">
                            <asp:TextBox ID="txt_HomeFax" runat="server" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            Phone No. (Office)</td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_OfficePhone" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                        </td>
                        <td>
                            &nbsp;Fax No. (Office)</td>
                        <td style="width: 241px">
                            <asp:TextBox ID="txt_OfficeFax" runat="server" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            
                            Phone No. (Mobile)</td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_MobilePhone" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                        </td>
                        <td>
                        </td>
                        <td style="width: 241px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                        </td>
                        <td style="width: 169px">
                        </td>
                        <td style="width: 141px">
                        </td>
                        <td>
                        </td>
                        <td style="width: 241px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 17px;">
                            
                            Email Address 1</td>
                        <td style="height: 17px;" colspan="2">
                            <asp:TextBox ID="txt_Email1" runat="server" Width="148px" Font-Size="8pt"></asp:TextBox></td>
                        <td style="height: 17px">
                            
                            Email Address 2</td>
                        <td style="width: 241px; height: 17px;">
                            <asp:TextBox ID="txt_Email2" runat="server" Width="150px" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 22px">
                            
                            </td>
                        <td style="width: 141px; height: 22px;">
                            </td>
                        <td style="height: 22px">
                        </td>
                        <td style="width: 241px; height: 22px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 11px">
                            
                            Preferred method of member communication:&nbsp;</td>
                        <td style="width: 141px">
                            </td>
                        <td >
                            <asp:RadioButton ID="Radio_CommMethod1" runat="server" GroupName="CommMethod" Text="By Post" Checked="True" /><asp:RadioButton
                                ID="Radio_CommMethod2" runat="server" GroupName="CommMethod" Text="By Email" /></td>
                        <td style="width: 241px; height: 11px">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px;" colspan="3">
                            Malaysia Medical Council Registration Number (required)</td>
                        <td style="height: 20px" colspan="2" >
                            <asp:TextBox ID="txt_MMC_No" runat="server" Width="146px" Font-Size="8pt"></asp:TextBox>
                            </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 10px;"><a name="quapoint" id="a1"></a>
                            Date of Registration<span
                                style="color: red">*</span></td>
                        <td style="width: 169px; height: 10px;">
                        </td>
                        <td style="width: 141px;">
                            </td>
                        <td colspan="2">
                        <asp:TextBox ID="txt_RegisDate" runat="server" Width="109px" Font-Size="8pt"></asp:TextBox>
                            <a href="javascript: showCalendar('txt_RegisDate')" ><img alt="" src="images/ew_calendar.gif" border="0" /></a>
                            <%--added by kavi 03/04/2010--%>
                            <asp:RegularExpressionValidator id="valRegEx" runat="server"
                                ControlToValidate="txt_RegisDate" SetFocusOnError="true"
                                ValidationExpression="([1-9]|0[1-9]|[12][0-9]|3[01])[- /.]([1-9]|0[1-9]|1[012])[- /.][0-9]{4}$"
                                ErrorMessage="*Invalid Date Format."
                                display="dynamic">*Invalid Date Format.
                            </asp:RegularExpressionValidator>
                            <%--end added by kavi 03/04/2010--%>
                            </td>
                    </tr>
                     <tr>
                        <td colspan="5" style="height: 37px">
                            
                                <br />
                            <span style="font-size: 10pt">
                                PROFESSIONAL QUALIFICATION </span><span style="color: #0000ff; text-decoration: underline"><a href="#top" class="toplink" >top</a></span></td>
                    </tr>
                     <tr>
                        <td colspan="2" style="height: 20px">
                            <span style="text-decoration: underline">
                            Basic Medical Degree</span></td>
                        <td style="width: 141px; height: 20px;">
                        </td>
                        <td style="height: 20px" >
                        </td>
                        <td style="width: 241px; height: 20px;">
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 269px; height: 24px;">
                            Degree (e.g MBBS, MD)</td>
                        <td style="width: 169px; height: 24px;">
                            <asp:TextBox ID="txt_Degree1" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 24px;">
                            University</td>
                        <td style="height: 24px">
                            <asp:TextBox ID="txt_University1" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 241px; height: 24px;">
                            <asp:Label ID="lbl_No1" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 269px; height: 4px;">
                            City/ Country</td>
                        <td style="width: 169px; height: 4px;">
                            <asp:TextBox ID="txt_Country1" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px;">
                            Date</td>
                        <td >
                            <asp:DropDownList ID="ddl_Month1" runat="server" Width="49px" Font-Size="8pt">
                            </asp:DropDownList><asp:DropDownList ID="ddl_Year1" runat="server" Width="64px" Font-Size="8pt">
                            </asp:DropDownList></td>
                        <td style="width: 241px; height: 4px;">
                        </td>
                    </tr>
                     <tr>
                        <td colspan="5">
                            <hr style="height: -12px" />
                        </td>
                    </tr>
                     <tr>
                        <td colspan="5" style="height: 19px">
                            <span style="text-decoration: underline">
                            Post Graduate
                                Qualifications in Ophthalmology</span></td>
                    </tr>
                     <tr>
                        <td colspan="5" valign="top">
                            <span style="font-size: 8pt">
                            Applicants applying for Ordinary
                                and Life Membership must complete this section</span><br />
                            <br />
                            </td>
                    </tr>
                     <tr>
                        <td style="width: 269px; height: 19px;">
                            I. &nbsp; Degree</td>
                        <td style="width: 169px; height: 19px;">
                            <asp:TextBox ID="txt_Degree2" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 19px;">
                            University/Institution</td>
                        <td style="height: 19px" >
                            <asp:TextBox ID="txt_University2" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 241px; height: 19px;">
                            <asp:Label ID="lbl_No2" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 20px;">
                            &nbsp; &nbsp;&nbsp; City
                                / Country</td>
                        <td style="width: 169px; height: 20px;">
                            <asp:TextBox ID="txt_Country2" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 20px;">
                            Date</td>
                        <td style="height: 20px" >
                            <asp:DropDownList ID="ddl_Month2" runat="server" Width="52px" Font-Size="8pt">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddl_Year2" runat="server" Width="65px" Font-Size="8pt">
                            </asp:DropDownList></td>
                        <td style="width: 241px; height: 20px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 20px;">
                            II.&nbsp; Degree</td>
                        <td style="width: 169px; height: 20px;">
                            <asp:TextBox ID="txt_Degree3" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 20px;">
                            University/Institution</td>
                        <td style="height: 20px" >
                            <asp:TextBox ID="txt_University3" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 241px; height: 20px;">
                            <asp:Label ID="lbl_No3" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px;">
                            &nbsp; &nbsp;&nbsp; City
                                / Country</td>
                        <td style="width: 169px; height: 21px;">
                            <asp:TextBox ID="txt_Country3" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 21px;">
                            Date</td>
                        <td style="height: 21px" >
                            <asp:DropDownList ID="ddl_Month3" runat="server" Width="51px" Font-Size="8pt">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddl_Year3" runat="server" Width="66px" Font-Size="8pt">
                            </asp:DropDownList></td>
                        <td style="width: 241px; height: 21px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            III. Degree</td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_Degree4" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                            University/Institution</td>
                        <td>
                            <asp:TextBox ID="txt_University4" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 241px">
                            <asp:Label ID="lbl_No4" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px;">
                            &nbsp; &nbsp;&nbsp;City
                                / Country</td>
                        <td style="width: 169px;">
                            <asp:TextBox ID="txt_Country4" runat="server" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                            Date</td>
                        <td>
                            <asp:DropDownList ID="ddl_Month4" runat="server" Width="52px" Font-Size="8pt">
                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddl_Year4" runat="server" Width="65px" Font-Size="8pt">
                            </asp:DropDownList></td>
                        <td style="width: 241px;">
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 269px; height: 10px;">
                        </td>
                        <td style="width: 169px; height: 10px;">
                        </td>
                        <td style="width: 141px; height: 10px;">
                        </td>
                        <td style="height: 10px" >
                        </td>
                        <td style="width: 241px; height: 10px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <hr style="height: -12px" />
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td colspan="5" style="height: 37px">
                         <span style="font-size: 10pt">Verification by Head of Department</span>&nbsp;&nbsp;<span style="color: #0000ff; text-decoration: underline"><a href="#top" class="toplink" >top</a></span></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 12px" colspan="4">The Applicant is currently 
                            <asp:TextBox ID="veriapp" runat="server" Font-Size="8pt" Width="476px"></asp:TextBox></td>
                        <td style="width: 241px; height: 12px"></td>
                    </tr>
                     <tr>
                        <td style="width: 269px; height: 10px;">Full Name</td>
                        <td style="width: 169px; height: 10px;"><asp:TextBox ID="veriname" runat="server" Font-Size="8pt" Width="143px"></asp:TextBox></td>
                        <td style="width: 141px; height: 10px;">Institution</td>
                        <td style="height: 10px" colspan="2"><asp:TextBox ID="veriins" runat="server" Font-Size="8pt" Width="311px"></asp:TextBox></td>
                    </tr>                                       
                    <tr>
                        <td colspan="5" style="height: 29px">
                            <hr style="height: -12px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 37px">
                         <span style="font-size: 10pt">Statement of Support</span>&nbsp;&nbsp;<span style="color: #0000ff; text-decoration: underline"><a href="#top" class="toplink" >top</a></span></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 12px" colspan="2">Full Name&nbsp;&nbsp;<asp:TextBox ID="supfullname1" runat="server" Font-Size="8pt" Width="213px"></asp:TextBox></td>
                        <td style="width: 241px; height: 12px" colspan="2">
                            Full Name&nbsp;<asp:TextBox ID="supsign1" runat="server" Font-Size="8pt" Width="213px"></asp:TextBox></td>
                        <td style="height: 10px">
                            &nbsp;&nbsp;</td>
                    </tr>
                   <tr>
                        <td style="width: 269px; height: 12px" colspan="2">Membership No&nbsp;&nbsp;<asp:TextBox ID="supfullname2" runat="server" Font-Size="8pt" Width="213px"></asp:TextBox></td>
                        <td style="width: 241px; height: 12px" colspan="2">
                            Membership No&nbsp;<asp:TextBox ID="supsign2" runat="server" Font-Size="8pt" Width="213px"></asp:TextBox></td>
                        <td style="height: 10px">
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <hr style="height: -12px" />
                        </td>
                    </tr>                    
                    <tr>
                        <td valign="top">Remark &nbsp;&nbsp;</td>
                        <td colspan="4" valign="top">
                            <asp:TextBox ID="remark" runat="server" TextMode="MultiLine" Height="55px" Width="546px"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Button ID="btn_Submit" runat="server" OnClick="btn_Submit_Click" Text="Update Membership" Font-Size="8pt" /><a name="paymentpoint" id="a2"></a></td>
                    </tr>
                </table>
                </div>
            <asp:Panel ID="Panel_UpdateMemberType" runat="server" Height="50px" Width="880px">
                <table style="padding: 5px; border: 1px solid gray; width: 880px; font-size: 8pt; font-family: Verdana;" 
                    bgcolor="white" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="font-size: 12pt">
                            <span style="font-size: 10pt">Payment Details<span style="font-family: Arial"><span
                                style="font-size: 8pt"> <span style="color: #0000ff; text-decoration: underline"><a href="#top" class="toplink" >top</a></span></span></span></span></td>
                            <td style="width: 195px; height: 10px; font-size: 8pt;">
                        </td>
                        <td style="width: 436px; height: 10px; font-size: 8pt;" align="right">
                            <asp:Button ID="btn_Payment" runat="server" Text="Payment" OnClick="btn_Payment_Click" Font-Size="8pt" />
                        </td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td style="width: 144px; height: 9px;">
                        </td>
                        <td style="width: 195px; height: 9px;">
                        </td>
                        <td style="width: 436px; height: 9px;">
                        </td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td colspan="3" style="height: 10px">
                            <asp:GridView ID="gvPayment" runat="server" AutoGenerateColumns="False" OnLoad="gvPayment_DataBound" Width="100%" BackColor="whitesmoke">
                                <RowStyle Font-Names="verdana" Font-Size="8pt" HorizontalAlign="Center" BackColor="whitesmoke" />
                                <HeaderStyle Font-Names="verdana" Font-Size="8pt" HorizontalAlign="Center" BackColor="DimGray" ForeColor="white" />
                                <Columns>
                                    <asp:BoundField DataField="PaymentYear" HeaderText="Year" />
                                    <asp:TemplateField HeaderText="Received Date">
                                        <ItemTemplate>
                                            <asp:Label ID="rDate" runat="server" Text='<%# changeDate(DataBinder.Eval(Container.DataItem,"ReceivedDate")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStat" runat="server" Text='<%# getStatus(DataBinder.Eval(Container.DataItem,"Status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PaymentMethod" HeaderText="Method" />
                                    <asp:BoundField DataField="AmountReceived" HeaderText="Amount" />
                                    <asp:BoundField DataField="chequeNo" HeaderText="Cheque" />
                                    <asp:BoundField DataField="ReceiptNo" HeaderText="ReceiptNo" />
                                    <asp:BoundField DataField="membertype" HeaderText="Type" />
                                    <asp:BoundField DataField="lastsave" HeaderText="LastSave" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:HyperLinkField DataNavigateUrlFields="id,MemberID" DataNavigateUrlFormatString="Payment.aspx?mid={1}&id={0}" Text="Edit" />
                                </Columns>
                            </asp:GridView>
                            <asp:Label ID="txtNoInfo" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr style="font-size: 8pt">
                        <td colspan="3" style="height: 10px">
                        </td>
                    </tr>
                </table>
              </asp:Panel>
                <br /><br /><br /><br />
            </asp:Panel>
            
            </div>
    
    </div>
    </form>

</body>
</html>
