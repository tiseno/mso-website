using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MSO_BizLayer;

public partial class Renew : System.Web.UI.Page
{
    private BizMember bm = new BizMember();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserCookies"] == null)
        {
            Response.Redirect("LostCookies.aspx");
        }
        else
        {
            string ProfileID;

            ProfileID = Request.Cookies["UserCookies"].Value;
        }

        if (IsPostBack == false)
        {
            string id = Request.QueryString["id"];
            int memid = Convert.ToInt16(id);
            int MaxID = Convert.ToInt16(bm.maxID(memid));

            DataSet get_detail = new DataSet();
            get_detail = bm.MemberDetail(id);
            lbl_id.Text = get_detail.Tables[0].Rows[0]["Profile_No"].ToString();
            lbl_MemberName.Text = get_detail.Tables[0].Rows[0]["Memb_Name"].ToString();
            lbl_MemberIC.Text = get_detail.Tables[0].Rows[0]["NRIC"].ToString();

            int tid = Convert.ToInt16(get_detail.Tables[0].Rows[0]["AppType"].ToString());
            DataSet getPtype = new DataSet();
            getPtype = bm.memberPayType(tid);

            lbl_CurrentMemberType.Text = getPtype.Tables[0].Rows[0]["Application_Name"].ToString();

            txt_StartDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            //lbl_fee.Text = app.Tables[0].Rows[0]["normal_fee"].ToString();

            //lbl_CurMembType.Text = get_detail.Tables[0].Rows[0]["AppType"].ToString();

            lbl_CurrentEntryFee.Text = getPtype.Tables[0].Rows[0]["entrance_fee"].ToString();
            lbl_CurrentNormalFee.Text = getPtype.Tables[0].Rows[0]["normal_fee"].ToString();

            DataSet mPay = new DataSet();
            int mid = Convert.ToInt16(id);
            mPay = bm.memberPayment2(MaxID);

            if (mPay.Tables[0].Rows.Count == 0)
            {
                lbl_CurrentExpiredDate.Text = " ";
                lbl_feeStat.Text = " ";
                lbl_CurrentEntryFee.Text = " ";
                lbl_CurrentNormalFee.Text = " ";
                ddl_RenewYear.Items.Add("2009");
                ddl_RenewYear.Items.Add("2010");
                ddl_RenewYear.Items.Add(DateTime.Now.ToString("yyyy"));
                ddl_FeeStatus.SelectedValue = "3";
                txtAmntRecv.Text = Convert.ToString(Convert.ToInt16(getPtype.Tables[0].Rows[0]["entrance_fee"]) + Convert.ToInt16(getPtype.Tables[0].Rows[0]["normal_fee"]));
                //lbl_ExpiredDate.Text = Convert.ToDateTime(txt_StartDate.Text).AddYears(1);
                lbl_fee.Text = getPtype.Tables[0].Rows[0]["normal_fee"].ToString();
                lbl_Entryfee.Text = getPtype.Tables[0].Rows[0]["entrance_fee"].ToString();
            }
            else
            {
                txtAmntRecv.Text = getPtype.Tables[0].Rows[0]["normal_fee"].ToString();
                lbl_fee.Text = getPtype.Tables[0].Rows[0]["normal_fee"].ToString();
                lbl_Entryfee.Text = "0";
                Int16 rcvDate = Convert.ToInt16(mPay.Tables[0].Rows[0]["PaymentYear"].ToString());
                ddl_RenewYear.Items.Add("2009");
                ddl_RenewYear.Items.Add("2010");
                ddl_RenewYear.Items.Add((rcvDate+1).ToString());
                lbl_CurrentExpiredDate.Text = mPay.Tables[0].Rows[0]["ReceivedDate"].ToString();
                string FeeStatus = mPay.Tables[0].Rows[0]["Status"].ToString();
                if (FeeStatus == "1")
                {
                    lbl_feeStat.Text = "Cancelled";
                }
                if (FeeStatus == "2")
                {
                    lbl_feeStat.Text = "Unpaid";
                }
                if (FeeStatus == "3")
                {
                    lbl_feeStat.Text = "Paid";
                }

            }

            if (lbl_CurrentMemberType.Text == "Life Member")
            {
                //lbl_WarningMessage.Text = "Life Member is not required to renew membership anymore!";
                //lbl_WarningMessage.Visible = true;
                //ddl_FeeStatus.Enabled = false;
                //ddl_RenewYear.Enabled = false;
                //lbl_fee.Enabled = false;
                //txt_StartDate.Enabled = false;
                //btn_Renew.Enabled = false;
                btn_Renew.Text = "Confirm One Time Payment!";
            }
            else
            {
                btn_Renew.Text = "Confirm!";
            }
            

        }
  
    }
    protected void btn_Renew_Click(object sender, EventArgs e)
    {

        string id = Request.QueryString["id"];
        int mid = Convert.ToInt16(id);
        //int memberTye;
        if (txt_StartDate.Text != "")
        {
            if (ddl_RenewYear.SelectedValue == "")
            {
                Response.Write("<script type='text/javascript'>alert('Invalid Year!')</script>");
            }
            else
            {
                //if (lbl_CurrentExpiredDate.Text != " ")
                //{
                //    if (Convert.ToDateTime(txt_StartDate.Text) <= Convert.ToDateTime(lbl_CurrentExpiredDate.Text))
                //    {
                //        Response.Write("<script type='text/javascript'>alert('Invalid Date!')</script>");
                //        //ddl_RenewYear.SelectedValue = "0";
                //    }
                //    else
                //    {
                bm.insertMemberFee(mid, ddl_RenewYear.SelectedValue.ToString(), txt_StartDate.Text.Substring(6, 4) + "/" + txt_StartDate.Text.Substring(0, 2) + "/" + txt_StartDate.Text.Substring(3, 2), Convert.ToInt32(ddl_FeeStatus.SelectedValue), ddlPayMet.SelectedValue.ToString(), txtAmntRecv.Text, txtRcpt.Text, taRmrk.Value, txtCheq.Text, Request.Cookies["UserCookies"].Value, lbl_CurrentMemberType.Text);
                        Response.Write("<script type='text/javascript'>alert('Payment has been made, thank you!')</script>");
                        Response.Write("<script type='text/javascript'>window.location='UpdateMemberProfile.aspx?id=" + id + "';</script>");
                //    }
                //}
                //else
                //{
                //    bm.insertMemberFee(mid, ddl_RenewYear.SelectedValue.ToString(), txt_StartDate.Text.Substring(6, 4) + "/" + txt_StartDate.Text.Substring(0, 2) + "/" + txt_StartDate.Text.Substring(3, 2), Convert.ToInt32(ddl_FeeStatus.SelectedValue), ddlPayMet.SelectedValue.ToString(), txtAmntRecv.Text, txtRcpt.Text, taRmrk.Value);
                //    Response.Write("<script type='text/javascript'>alert('Renew membership has been done, thank you!')</script>");
                //    Response.Write("<script type='text/javascript'>window.location='UpdateMemberProfile.aspx?id=" + id + "';</script>");

                //}
            }
        }
        else
        {
            Response.Write("<script type='text/javascript'>alert('Invalid Date!')</script>");
        }
    }
    protected void Link_Back_Click(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];
        Response.Redirect("UpdateMemberProfile.aspx?id=" + id);
    }
}
