<%@ Page Language="C#" AutoEventWireup="true" CodeFile="newsletterstop2.aspx.cs" Inherits="SearchMember" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="ezcalendar.js"></script>
	<link rel="stylesheet" type="text/css" href="ezcalendar.css" />
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY - Newsletter / Address</title>
    <style type="text/css">
        .style1
        {
        }
        .style2
        {
            width: 12px;
        }
        .style3
        {
            width: 182px;
        }
        .style4
        {
            font-family: Verdana;
            color: #333333;
            font-weight: bold;
        }
    </style>
</head>
<body bgcolor="gray">
    <form id="form1" runat="server">
    <div>
        <div>
                <table style="width: 95%; height:95%; border-right: black 1px solid; border-top: black 1px solid; padding-left: 5px; border-left: black 1px solid; border-bottom: black 1px solid; background-color: whitesmoke;" border="0" cellpadding="0" cellspacing="0">
                <tr valign="top">
                    <td colspan="2"><br />
                    <span style="font-family: Verdana; font-size:13pt;">&nbsp;<span style="color: #323232"><strong>Malaysia Society of Ophthalmology</strong></span></span> | 
                        &nbsp;<a href="AdminMain.aspx"><span style="font-family: Verdana; font-size:9pt;"><span
                            style="font-size: 8pt">Back to Previous</span></a> |
                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Size="8pt" OnClick="LinkButton1_Click">Expiry Membership Check</asp:LinkButton>
                        <asp:Label ID="searchtypelbl" runat="server" Visible="false"></asp:Label><asp:Label ID="sortlbl" runat="server" Visible="false"></asp:Label><br /><br />
                        <span style="font-size: 8pt">ID:
                            <asp:TextBox ID="MembershipID" runat="server" Font-Size="8pt" Width="22px"></asp:TextBox>
                            Name :</span>
                        <asp:TextBox ID="txt_MemberName" runat="server" Width="146px" Font-Size="8pt"></asp:TextBox>
                        <span style="font-size: 8pt">
                    NRIC :</span>
                        <asp:TextBox ID="txt_IC" runat="server" Width="124px" Font-Size="8pt"></asp:TextBox>
                        <span style="font-size: 8pt">
                        Type :</span>
                        <asp:DropDownList ID="ddl_memb_type" runat="server" Font-Size="8pt">
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem Value="1">Ordinary Member</asp:ListItem>
                            <asp:ListItem Value="2">Life Member</asp:ListItem>
                            <asp:ListItem Value="3">Associate Member</asp:ListItem>
                        </asp:DropDownList>
                        <span style="font-size: 8pt">State :</span>
                        <asp:DropDownList ID="memb_state" runat="server" Font-Size="8pt">
                            <asp:ListItem Value="" Text=""></asp:ListItem>
                            <asp:ListItem Value="JOHOR" Text="JOHOR"></asp:ListItem>
                            <asp:ListItem Value="KEDAH" Text="KEDAH"></asp:ListItem>
                            <asp:ListItem Value="KELANTAN" Text="KELANTAN"></asp:ListItem>
                            <asp:ListItem Value="KUALA LUMPUR" Text="KUALA LUMPUR"></asp:ListItem>
                            <asp:ListItem Value="LABUAN" Text="LABUAN"></asp:ListItem>
                            <asp:ListItem Value="MELAKA" Text="MELAKA"></asp:ListItem>
                            <asp:ListItem Value="NEGERI SEMBILAN" Text="NEGERI SEMBILAN"></asp:ListItem>
                            <asp:ListItem Value="PAHANG" Text="PAHANG"></asp:ListItem>
                            <asp:ListItem Value="PERAK" Text="PERAK"></asp:ListItem>
                            <asp:ListItem Value="PERLIS" Text="PERLIS"></asp:ListItem>
                            <asp:ListItem Value="PULAU PINANG" Text="PULAU PINANG"></asp:ListItem>
                            <asp:ListItem Value="SABAH" Text="SABAH"></asp:ListItem>
                            <asp:ListItem Value="SARAWAK" Text="SARAWAK"></asp:ListItem>
                            <asp:ListItem Value="SELANGOR" Text="SELANGOR"></asp:ListItem>
                            <asp:ListItem Value="SINGAPORE" Text="SINGAPORE"></asp:ListItem>
                            <asp:ListItem Value="TERENGGANU" Text="TERENGGANU"></asp:ListItem>
                        </asp:DropDownList>
                        <span style="font-size: 8pt">Address</span>
                        <asp:TextBox ID="addres" runat="server" Font-Size="8pt" Width="124px"></asp:TextBox>
                        <asp:Button ID="btn_Search" runat="server" onclick="btn_Search_Click" 
                            Text="Search" Font-Size="8pt" /></td>

                </tr>            
                <tr valign="top">
                    <td colspan="2">
                        <asp:Datagrid ID="Dg_MemberList" runat="server" AllowPaging="True" OnPageIndexChanged="Dg_MemberList_pageIndexChanged" PageSize="8" AutoGenerateColumns="False" Width="95%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" ForeColor="Black" GridLines="Horizontal" AllowSorting="True" OnSortCommand="SortCommand">
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <SelectedItemStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <PagerStyle HorizontalAlign="Left" BackColor="Gainsboro" ForeColor="Black" Font-Bold="False" Font-Names="verdana" Font-Size="8pt" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Mode="NumericPages"/>
                        <ItemStyle Font-Names="Verdana" Font-Size="8pt" />
                        <Columns>
                        <asp:TemplateColumn headertext="No"><itemtemplate><%#Container.ItemIndex + Dg_MemberList.CurrentPageIndex * Dg_MemberList.PageSize + 1%></itemtemplate>
                        </asp:TemplateColumn> 
                            <asp:BoundColumn DataField="membershipid" HeaderText="ID" SortExpression="membershipid"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Member Name" SortExpression="Memb_Name">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Memb_Name")%>' NavigateUrl='<%# "./UpdateMemberProfile.aspx?id=" + DataBinder.Eval(Container.DataItem,"Profile_No") %>' Target="main"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Application_Name" HeaderText="Membership Type" SortExpression="Application_Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Office_State" HeaderText="State" SortExpression="Office_State"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Email1" HeaderText="E-mail" SortExpression="Email1" ItemStyle-Width="100"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Office_Address" HeaderText="House Address" SortExpression="Office_Address" ItemStyle-Width="200"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Home_Address" HeaderText="Office Address" SortExpression="Home_Address" ItemStyle-Width="200"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Names="Verdana" Font-Overline="False" Font-Size="8pt" Font-Strikeout="False" Font-Underline="False" BackColor="Dimgray" ForeColor="White" />
                    </asp:Datagrid>
                        <br />
                        <table>
                            <tr>
                                <td style="height: 40px" valign="top">
                                    <span style="font-size: 10pt; font-family: Verdana">To: </span>
                                </td>
                                <td style="width: 184px; height: 40px">
                                    <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Height="72px" Width="471px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size: 10pt; font-family: Verdana">Subject :</span></td>
                                <td style="width: 184px">
                                    <asp:TextBox ID="TextBox2" runat="server" Width="467px" Text="Malaysia Society of Ophthalmology : Generate Information"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="height: 66px"></td>
                                <td style="width: 184px; height: 66px"><asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Height="144px" Width="602px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 184px">
                                    <asp:Button ID="Button1" runat="server" Text="Send Newsletter" /></td>
                            </tr>                                                                                    
                        </table>
                   </td>
                </tr>
            </table>            
        </div>
    
    </div>
    </form>
</body>
</html>
