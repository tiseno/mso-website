﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class MemberMain : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserCookies"] == null)
        {
            Response.Redirect("LostCookies.aspx");
        }
        else
        {
            string ProfileID;

            ProfileID = Request.Cookies["UserCookies"].Value;
        }
    }
    protected void Link_1_Click(object sender, EventArgs e)
    {
        string ProfileID;

        ProfileID = Request.Cookies["UserCookies"].Value;

        if (ProfileID == null)
        {
            Response.Redirect("login.aspx");
        }
        else
        {
            Response.Redirect("MemberProfile.aspx");
        }

    }
    protected void Link_Logout_Click(object sender, EventArgs e)
    {
        Response.Cookies["UserCookies"].Value = "";
        Response.Cookies["UserCookies"].Expires = DateTime.Now.AddDays(-1);
 
        Response.Redirect("login.aspx");
    }
}
