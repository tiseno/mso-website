using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MSO_BizLayer;

public partial class ChangeMembershipType : System.Web.UI.Page
{
    private BizMember bm = new BizMember();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserCookies"] == null)
        {
            Response.Redirect("LostCookies.aspx");
        }
        else
        {
            string ProfileID;

            ProfileID = Request.Cookies["UserCookies"].Value;
        }

        if (IsPostBack == false)
        {
            string id = Request.QueryString["id"];
            DataSet get_detail = new DataSet();

            get_detail = bm.MemberDetail(id);
            lbl_id.Text = get_detail.Tables[0].Rows[0]["Profile_No"].ToString();
            lbl_MemberName.Text = get_detail.Tables[0].Rows[0]["Memb_Name"].ToString();
            lbl_MemberIC.Text = get_detail.Tables[0].Rows[0]["NRIC"].ToString();
            lbl_CurrentMemberType.Text = get_detail.Tables[0].Rows[0]["Application"].ToString();

            if (get_detail.Tables[0].Rows[0]["Application"].ToString() == "Life Member")
            {
                ddl_MemberShipType.SelectedValue ="2";
            }
            if (get_detail.Tables[0].Rows[0]["Application"].ToString() == "Ordinary Member")
            {
                ddl_MemberShipType.SelectedValue ="1";
            }
            if (get_detail.Tables[0].Rows[0]["Application"].ToString() == "Associate Member")
            {
                ddl_MemberShipType.SelectedValue = "3";
            }

            lbl_CurrentExpiredDate.Text = get_detail.Tables[0].Rows[0]["Expired_Date"].ToString();
            lbl_CurrentStartDate.Text = get_detail.Tables[0].Rows[0]["Start_Date"].ToString();
            txt_StartDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
            
            lbl_CurMembType.Text = get_detail.Tables[0].Rows[0]["Application"].ToString();
            lbl_ExpiredDate.Text = get_detail.Tables[0].Rows[0]["Expired_Date"].ToString();

            string FeeStatus = get_detail.Tables[0].Rows[0]["Status_id"].ToString();
            ddl_FeeStatus.SelectedValue = FeeStatus;

            if (FeeStatus == "1")
            {
                lbl_feeStat.Text = "Cancelled";
            }
            if (FeeStatus == "2")
            {
                lbl_feeStat.Text = "Unpaid";
            }
            if (FeeStatus == "3")
            {
                lbl_feeStat.Text = "Paid";
            }
           

            lbl_CurrentEntryFee.Text = get_detail.Tables[0].Rows[0]["Entrance_fee"].ToString();
            lbl_CurrentNormalFee.Text = get_detail.Tables[0].Rows[0]["Normal_fee"].ToString();


            if (lbl_CurrentMemberType.Text == "Life Member")
            {
                lbl_WarningMessage.Text = "Sorry, 'Life Member' are not allowed to do any changing!";
                lbl_WarningMessage.Visible = true;
                ddl_FeeStatus.Enabled = false;
                ddl_MemberShipType.Enabled = false;
                lbl_ExpiredDate.Enabled = false;
                lbl_fee.Enabled = false;
                txt_StartDate.Enabled = false;
                btn_Renew.Enabled = false;

            }
            else
            {
                if (Convert.ToDateTime(lbl_CurrentExpiredDate.Text) <= DateTime.Now)
                {
                    //lbl_WarningMessage.Visible = true;
                    lbl_WarningMessage.Visible = true;
                    lbl_WarningMessage.Text = "Sorry, This record is expired and you are not allowed to do any changing!";
                    ddl_FeeStatus.Enabled = false;
                    ddl_MemberShipType.Enabled = false;
                    lbl_ExpiredDate.Enabled = false;
                    lbl_fee.Enabled = false;
                    txt_StartDate.Enabled = false;
                    btn_Renew.Enabled = false;
                }
                else
                {

                    ddl_FeeStatus.Enabled = true;
                    ddl_MemberShipType.Enabled = true;
                    lbl_ExpiredDate.Enabled = true;
                    lbl_fee.Enabled = true;
                    txt_StartDate.Enabled = true;
                    btn_Renew.Enabled = true;
                }
                
                

              
            }

            if (lbl_feeStat.Text == "Unpaid")
            {
                lbl_WarningMessage.Visible = true;
                lbl_WarningMessage.Text = "Sorry, your current fee status is 'Unpaid', you are not allowed to do any changing!";
                ddl_MemberShipType.Enabled = false;
                lbl_ExpiredDate.Enabled = false;
                lbl_fee.Enabled = false;
                txt_StartDate.Enabled = false;
                btn_Renew.Enabled = false;
                ddl_FeeStatus.Enabled = false;
            }
            if (lbl_feeStat.Text == "Cancelled")
            {
                lbl_WarningMessage.Visible = true;
                lbl_WarningMessage.Text = "Sorry, your current fee status is 'Cancelled', you are not allowed to do any changing!";
                ddl_MemberShipType.Enabled = false;
                lbl_ExpiredDate.Enabled = false;
                lbl_fee.Enabled = false;
                txt_StartDate.Enabled = false;
                btn_Renew.Enabled = false;
                ddl_FeeStatus.Enabled = false;
            }

            
            

        }

    }
    protected void btn_Renew_Click(object sender, EventArgs e)
    {

        string id = Request.QueryString["id"];
        
        if (ddl_MemberShipType.SelectedItem.Text == "Life Member")
        {
            int memberTye =2;
            if (lbl_CurrentMemberType.Text == "Ordinary Member")
            {
                memberTye = 1;
            }
            if (lbl_CurrentMemberType.Text == "Associate Member")
            {
                memberTye = 3;
            }
            bm.updateMembershipFee(Convert.ToInt32(id), memberTye, lbl_CurrentStartDate.Text, lbl_CurrentExpiredDate.Text, lbl_CurrentNormalFee.Text, 1, DateTime.Now.ToString("yyyy/MM/dd"));

            DataSet checkfee = new DataSet();
            checkfee = bm.CheckEntranceFee(id, memberTye);
            if (checkfee.Tables[0].Rows.Count > 0)
            {
                //bm.insertMemberFee(Convert.ToInt32(lbl_id.Text), Convert.ToInt32(ddl_MemberShipType.SelectedValue), "-", lbl_TotalRefund.Text, DateTime.Now.ToString("yyyy/MM/dd"), "", Convert.ToInt32(ddl_FeeStatus.SelectedValue));
                Response.Write("<script type='text/javascript'>alert('Membership type has been changed!')</script>");
                Response.Write("<script type='text/javascript'>window.location='SearchMember.aspx';</script>");
                //Response.Redirect("SearchMember.aspx");
            }
            else
            {
                //bm.insertMemberFee(Convert.ToInt32(lbl_id.Text), Convert.ToInt32(ddl_MemberShipType.SelectedValue), lbl_Entryfee.Text, lbl_TotalRefund.Text, DateTime.Now.ToString("yyyy/MM/dd"), "", Convert.ToInt32(ddl_FeeStatus.SelectedValue));
                Response.Write("<script type='text/javascript'>alert('Membership type has been changed!')</script>");
                Response.Write("<script type='text/javascript'>window.location='SearchMember.aspx';</script>");
            }
        }
        else
        {

            if (ddl_MemberShipType.SelectedItem.Text == lbl_CurMembType.Text)
            {
                Response.Write("<script type='text/javascript'>alert('Sorry, you are not allowed to change the same membership type!')</script>");
            }
            else
            {

            DataSet searchDate = new DataSet();
            searchDate = bm.check_expiredDate(Convert.ToInt32(id));
            
                int memberTye = 0;
                if (lbl_CurrentMemberType.Text == "Ordinary Member")
                {
                    memberTye = 1;
                }
                if (lbl_CurrentMemberType.Text == "Life Member")
                {
                    memberTye = 2;
                }
                if (lbl_CurrentMemberType.Text == "Associate Member")
                {
                    memberTye = 3;
                }
                bm.updateMembershipFee(Convert.ToInt32(id), memberTye, lbl_CurrentStartDate.Text, lbl_CurrentExpiredDate.Text, lbl_CurrentNormalFee.Text, 1, DateTime.Now.ToString("yyyy/MM/dd"));

                DataSet checkfee = new DataSet();
                checkfee = bm.CheckEntranceFee(id, memberTye);
                if (checkfee.Tables[0].Rows.Count > 0)
                {
                    //bm.insertMemberFee(Convert.ToInt32(lbl_id.Text), Convert.ToInt32(ddl_MemberShipType.SelectedValue), "-", lbl_TotalRefund.Text, DateTime.Now.ToString("yyyy/MM/dd"), lbl_ExpiredDate.Text, Convert.ToInt32(ddl_FeeStatus.SelectedValue));
                    Response.Write("<script type='text/javascript'>alert('Membership type has been changed!')</script>");
                    Response.Write("<script type='text/javascript'>window.location='SearchMember.aspx';</script>");
                }
                else
                {
                    //bm.insertMemberFee(Convert.ToInt32(lbl_id.Text), Convert.ToInt32(ddl_MemberShipType.SelectedValue), lbl_Entryfee.Text, lbl_TotalRefund.Text, DateTime.Now.ToString("yyyy/MM/dd"), lbl_ExpiredDate.Text, Convert.ToInt32(ddl_FeeStatus.SelectedValue));
                    Response.Write("<script type='text/javascript'>alert('Membership type has been changed!')</script>");
                    Response.Write("<script type='text/javascript'>window.location='SearchMember.aspx';</script>");
                }
                
                                  
            }
           
            
        }
    }
    protected void ddl_MemberShipType_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataSet app = new DataSet();
        app = bm.get_Application(ddl_MemberShipType.SelectedItem.Text);
        lbl_fee.Text = app.Tables[0].Rows[0]["normal_fee"].ToString();
        lbl_Entryfee.Text = app.Tables[0].Rows[0]["entrance_fee"].ToString();

        //double amount;
        if (ddl_MemberShipType.SelectedValue == "2")
        {
            lbl_TotalRefund.Text = lbl_fee.Text;
        }
        else
        {
            DateTime firstDate = new DateTime(Convert.ToInt32(lbl_CurrentExpiredDate.Text.Substring(0, 4)), Convert.ToInt32(lbl_CurrentExpiredDate.Text.Substring(5, 2)), Convert.ToInt32(lbl_CurrentExpiredDate.Text.Substring(8, 2)));
            DateTime SecondDate = DateTime.Now.AddDays(-1);
            TimeSpan diff = firstDate.Subtract(SecondDate);
            //DateTime secondDate = new DateTime(Convert.ToInt32((DateTime.Now.ToString("yyyy/MM/dd").Substring(0,4))), Convert.ToInt32((DateTime.Now.ToString("yyyy/MM/dd").Substring(5,2))), Convert.ToInt32(DateTime.Now.ToString("yyyy/MM/dd").Substring(8,2)));


            double fund;

            fund = (Convert.ToDouble(diff.Days) / 365) * Convert.ToDouble(lbl_fee.Text);
            fund = Math.Round(fund, 2);
            if (ddl_MemberShipType.SelectedValue != lbl_CurMembType.Text)
            {
                //amount = fund * Convert.ToDouble(SecondDate);

                lbl_TotalRefund.Text = fund.ToString();
            }
            else
            {

            }
        }

    }

    protected void Link_Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchMember.aspx");
    }
    protected void ddl_FeeStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}

