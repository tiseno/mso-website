using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MSO_CommonLayer;
using MSO_BizLayer;
using MyDate_Val;

public partial class _Default : System.Web.UI.Page
{

    private BizMember bm = new BizMember();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            DateAutoGenerate();
        // added by kavi 03/04/10
        memberno.Attributes.Add("onKeypress", "CheckIsNumeric()");
        // end added by kavi
    }
    protected void btn_Submit_Click(object sender, EventArgs e)
    {
        // added by kavi 17/05/10
        if (memberno.Text == "")
        {
            Response.Write("<script type='text/javascript'>alert('Fill Membership No!');</script>");
            memberno.Focus();
        }
        else
        {
        // end added by kavi 17/05/10
            string app_type = "";
            string mmaos = "";
            string birth_date;
            string gend = "";
            string mailing_add = "";
            string user_type = "";
            string comm_way = "";
            string strregisdt = "";
            string strjoindt = "";
            DataSet app;


            if (Radio_Application1.Checked == true)
            {
                app_type = "1";
            }
            if (Radio_Application2.Checked == true)
            {
                app_type = "2";
            }
            if (Radio_Application3.Checked == true)
            {
                app_type = "3";
            }

            if (RadioButton1.Checked == true)
            {
                mmaos = "NO";
            }
            if (RadioButton2.Checked == true)
            {
                mmaos = "ACTIVE";
            }
            if (RadioButton3.Checked == true)
            {
                mmaos = "LAPSED";
            }

            if (Radio_Male.Checked == true)
            {
                gend = Radio_Male.Text;
            }
            if (Radio_Female.Checked == true)
            {
                gend = Radio_Female.Text;
            }
            if (Radio_MailingAddress1.Checked == true)
            {
                mailing_add = Radio_MailingAddress1.Text;
            }
            if (Radio_MailingAddress2.Checked == true)
            {
                mailing_add = Radio_MailingAddress2.Text;
            }

            if (Radio_userType1.Checked == true)
            {
                user_type = "Admin";
            }
            if (Radio_userType2.Checked == true)
            {
                user_type = "Member";
            }
            if (Radio_CommMethod1.Checked == true)
            {
                comm_way = "By Post";
            }
            if (Radio_CommMethod2.Checked == true)
            {
                comm_way = "By Mail";
            }

            DataSet checkUniqueUsername = new DataSet();
            checkUniqueUsername = bm.CheckUsername(txt_username.Text);

            if (checkUniqueUsername.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script type='text/javascript'>alert('Sorry! Your username might used by other users, please try again!');</script>");
            }
            else
            {
                //    if (checkUniqueUsername.Tables[0].Rows.Count < 0)
                //    {
                //if (ddl_DOB_Day.SelectedItem.Text.ToString() == "dd")
                //{
                //    Response.Write("<script type='text/javascript'>alert('Invalid Date of Birth!');</script>");
                //    ddl_DOB_Day.Focus();
                //}
                //else
                //{
                //if (ddl_DOB_Month.SelectedItem.Text.ToString() == "mm")
                //{
                //    Response.Write("<script type='text/javascript'>alert('Invalid Date of Birth!');</script>");
                //    ddl_DOB_Month.Focus();
                //}
                //else
                //{
                //if (ddl_DOB_Year.SelectedItem.Text.ToString() == "yyyy")
                //{
                //    Response.Write("<script type='text/javascript'>alert('Invalid Date of Birth!');</script>");
                //    ddl_DOB_Year.Focus();
                //}
                //else
                //{
                if (txt_ConfirmPass.Text != txt_password.Text)
                {
                    Response.Write("<script type='text/javascript'>alert('Invalid password! Your confirmation password is incorrect, please check again!')</script>");
                    txt_ConfirmPass.Text = "";
                    txt_password.Text = "";
                }
                else
                {
                    //if (Date_Val.ShowDate_Val(txt_RegisDate.Text) == "False")
                    //{
                    //    Response.Write("<script type='text/javascript'>alert('Invalid Registration Date!');</script>");
                    //}
                    //else
                    //{
                    //if (Date_Val.ShowDate_Val(joindt.Text) == "False")
                    //{
                    //    Response.Write("<script type='text/javascript'>alert('Invalid Join Date!');</script>");
                    //}
                    //else
                    //{
                    if (bm.CheckMembershipID(memberno.Text, "0") == "1")
                    {
                        Response.Write("<script type='text/javascript'>alert('Same Membership found or no membership was found, Kindly Amend Membership No!');</script>");
                        memberno.Focus();
                    }
                    else
                    {
                        birth_date = ddl_DOB_Year.SelectedValue + ddl_DOB_Month.SelectedValue + ddl_DOB_Day.SelectedValue;
                        if (txt_RegisDate.Text != "")
                        {
                            strregisdt = txt_RegisDate.Text.ToString().Substring(6, 4) + txt_RegisDate.Text.ToString().Substring(3, 2) + txt_RegisDate.Text.ToString().Substring(0, 2);
                        }
                        if (joindt.Text != "")
                        {
                            strjoindt = joindt.Text.ToString().Substring(6, 4) + joindt.Text.ToString().Substring(3, 2) + joindt.Text.ToString().Substring(0, 2);
                        }
                        app = bm.get_Application(app_type);

                        bm.insertMember(txt_MMC_No.Text, user_type, txt_Name.Text, ddl_Title.SelectedValue, birth_date, gend,
                          txt_Nationality.Text, txt_IC.Text, mailing_add, txt_HomeAddress.Text, txt_HomeCity.Text, txt_HomeState.Text, txt_HomeCountry.Text, txt_HomeCode.Text, txt_OfficeAddress.Text,
                          txt_OfficeCity.Text, txt_OfficeState.Text, txt_OfficeCode.Text, txt_OfficeCountry.Text, txt_HomePhone.Text, txt_OfficePhone.Text, txt_MobilePhone.Text, txt_HomeFax.Text, txt_OfficeFax.Text, txt_Email1.Text, txt_Email2.Text, strregisdt, comm_way, txt_username.Text, txt_password.Text, mmaos, app_type, memberno.Text, isactive.SelectedValue, strjoindt, veriapp.Text, veriname.Text, veriins.Text, supfullname1.Text, supsign1.Text, supfullname2.Text, supsign2.Text, remark.Text, Request.Cookies["UserCookies"].Value);

                        DataSet getNo;
                        getNo = bm.get_No(txt_username.Text);

                        string No;
                        No = getNo.Tables[0].Rows[0]["Profile_No"].ToString();
                        string date_graduate1;
                        date_graduate1 = ddl_Year1.SelectedValue + "-" + ddl_Month1.SelectedValue;
                        string date_graduate2;
                        date_graduate2 = ddl_Year2.SelectedValue + "-" + ddl_Month2.SelectedValue;
                        string date_graduate3;
                        date_graduate3 = ddl_Year3.SelectedValue + "-" + ddl_Month3.SelectedValue;
                        string date_graduate4;
                        date_graduate4 = ddl_Year4.SelectedValue + "-" + ddl_Month4.SelectedValue;

                        if (txt_Degree1.Text != "")
                        {
                            if (ddl_Month1.SelectedItem.Text.ToString() == "mm")
                            {
                                date_graduate1 = "";

                            }
                            else
                            {
                                if (ddl_Year1.SelectedItem.Text.ToString() == "yyyy")
                                {
                                    date_graduate1 = "";
                                }
                                else
                                {
                                    bm.insertEducation(No, "Basic Medical Degree", txt_Degree1.Text, txt_Country1.Text, txt_University1.Text, date_graduate1);

                                }
                            }
                        }
                        if (txt_Degree2.Text != "")
                        {
                            if (ddl_Month2.SelectedItem.Text.ToString() == "mm")
                            {
                                date_graduate2 = "";
                            }
                            else
                            {
                                if (ddl_Year2.SelectedItem.Text.ToString() == "yyyy")
                                {
                                    date_graduate1 = "";
                                }
                                else
                                {
                                    bm.insertEducation(No, "Post Graduate in Ophthalmology", txt_Degree2.Text, txt_Country2.Text, txt_University2.Text, date_graduate2);
                                }
                            }
                        }
                        if (txt_Degree3.Text != "")
                        {
                            if (ddl_Month3.SelectedItem.Text.ToString() == "mm")
                            {
                                date_graduate3 = "";
                            }
                            else
                            {
                                if (ddl_Year3.SelectedItem.Text.ToString() == "yyyy")
                                {
                                    date_graduate3 = "";
                                }
                                else
                                {
                                    bm.insertEducation(No, "Post Graduate in Ophthalmology", txt_Degree3.Text, txt_Country3.Text, txt_University3.Text, date_graduate3);
                                }
                            }
                        }
                        if (txt_Degree4.Text != "")
                        {
                            if (ddl_Month4.SelectedItem.Text.ToString() == "mm")
                            {
                                date_graduate4 = "";
                            }
                            else
                            {
                                if (ddl_Year4.SelectedItem.Text.ToString() == "yyyy")
                                {
                                    date_graduate4 = "";
                                }
                                else
                                {
                                    bm.insertEducation(No, "Post Graduate in Ophthalmology", txt_Degree4.Text, txt_Country4.Text, txt_University4.Text, date_graduate4);
                                }
                            }
                        }

                        //if (Radio_Application1.Checked == true)
                        //{
                        //if (Radio_Application2.Checked == true)
                        //{
                        //    bm.insertMemberFee(Convert.ToInt32(No), Convert.ToInt32(app.Tables[0].Rows[0]["ApplyID"].ToString()), app.Tables[0].Rows[0]["entrance_fee"].ToString(), app.Tables[0].Rows[0]["normal_fee"].ToString(), DateTime.Now.ToString("yyyy/MM/dd"), "", 2);
                        //}
                        //else
                        //{
                        //    bm.insertMemberFee(Convert.ToInt32(No), Convert.ToInt32(app.Tables[0].Rows[0]["ApplyID"].ToString()), app.Tables[0].Rows[0]["entrance_fee"].ToString(), app.Tables[0].Rows[0]["normal_fee"].ToString(), DateTime.Now.ToString("yyyy/MM/dd"), Convert.ToDateTime(DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd")).AddYears(1).ToString("yyyy/MM/dd"), 2);

                        //}
                        //}
                        //if (Radio_Application2.Checked == true)
                        //{
                        //    bm.insertMemberFee(Convert.ToInt32(No), Convert.ToInt32(app.Tables[0].Rows[0]["ApplyID"].ToString()), app.Tables[0].Rows[0]["entrance_fee"].ToString(), app.Tables[0].Rows[0]["normal_fee"].ToString(), DateTime.Now.ToString("yyyy/MM/DD"), DateTime.Now.AddYears(1).ToString("yyyy/MM/DD"), 2);
                        //}
                        //if (Radio_Application3.Checked == true)
                        //{
                        //    bm.insertMemberFee(Convert.ToInt32(No), Convert.ToInt32(app.Tables[0].Rows[0]["ApplyID"].ToString()), app.Tables[0].Rows[0]["entrance_fee"].ToString(), app.Tables[0].Rows[0]["normal_fee"].ToString(), DateTime.Now.ToString("yyyy/MM/DD"), DateTime.Now.AddYears(1).ToString("yyyy/MM/DD"), 2);
                        //}
                        Response.Write("<script type='text/javascript'>alert('Member profile has been registered!')</script>");
                        Response.Write("<script type='text/javascript'>window.location='Register.aspx';</script>");
                    }


                    //}


                    //}
                    //Response.Redirect("AdminMain.aspx");
                }
                //}

                //}
                //}
                //}
            }
        // added by kavi 17/05/10
        }
        // end added by kavi 17/05/10

        //Response.Redirect("login.aspx");
    }
    protected void ddl_Title_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_Title.SelectedItem.Value.ToString() == "Others")
        {
            txt_TitleOthers.Visible = true;
        }
        else
        {
            txt_TitleOthers.Visible = false;
        }
    }

    public void DateAutoGenerate()
    {
        ddl_DOB_Day.Items.Clear();
        ddl_DOB_Month.Items.Clear();
        ddl_DOB_Year.Items.Clear();
        ddl_Month1.Items.Clear();
        ddl_Month2.Items.Clear();
        ddl_Month3.Items.Clear();
        ddl_Month4.Items.Clear();
        ddl_Year1.Items.Clear();
        ddl_Year2.Items.Clear();
        ddl_Year3.Items.Clear();
        ddl_Year4.Items.Clear();

        ddl_DOB_Day.Items.Add("dd");
        ddl_DOB_Month.Items.Add("mm");
        ddl_DOB_Year.Items.Add("yyyy");
        ddl_Month1.Items.Add("mm");
        ddl_Month2.Items.Add("mm");
        ddl_Month3.Items.Add("mm");
        ddl_Month4.Items.Add("mm");
        ddl_Year1.Items.Add("yyyy");
        ddl_Year2.Items.Add("yyyy");
        ddl_Year3.Items.Add("yyyy");
        ddl_Year4.Items.Add("yyyy");

        int i;
        for (i = 1; i <= 31; i++)
        {
            if (i < 10)
            {
                ddl_DOB_Day.Items.Add("0" + i + "");
            }
            else
            {
                ddl_DOB_Day.Items.Add("" + i + "");
            }
            
        }
        for (i = 1; i <= 12; i++)
        {
            if (i < 10)
            {
                ddl_DOB_Month.Items.Add("0" + i + "");
                ddl_Month1.Items.Add("0" + i + "");
                ddl_Month2.Items.Add("0" + i + "");
                ddl_Month3.Items.Add("0" + i + "");
                ddl_Month4.Items.Add("0" + i + "");
            }
            else
            {
                ddl_DOB_Month.Items.Add("" + i + "");
                ddl_Month1.Items.Add("" + i + "");
                ddl_Month2.Items.Add("" + i + "");
                ddl_Month3.Items.Add("" + i + "");
                ddl_Month4.Items.Add("" + i + "");
            }
        }
        // emended by kavi 17/05/10
        // changed from 1940 to 1930
        for (i = 1930; i <= 2011; i++)
        // end emended by kavi 17/05/10
        {
            ddl_DOB_Year.Items.Add("" + i + "");
        }
        for (i = 1955; i <= 2011; i++)
        {
            ddl_Year1.Items.Add("" + i + "");
            ddl_Year2.Items.Add("" + i + "");
            ddl_Year3.Items.Add("" + i + "");
            ddl_Year4.Items.Add("" + i + "");
        }
      
    }
    protected void Link_Back_Click(object sender, EventArgs e)
    {
        Response.Write("<script type='text/javascript'>window.location='AdminMain.aspx';</script>");
    }
}
