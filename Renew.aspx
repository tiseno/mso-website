<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Renew.aspx.cs" Inherits="Renew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 <script type="text/javascript" src="ezcalendar.js"></script>
	<link rel="stylesheet" type="text/css" href="ezcalendar.css" />
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY - Membership Renew</title>
    <style type="text/css">
        .style1
        {
            width: 210px;
        }
        .style2
        {
            width: 195px;
        }
    </style>
    <script type="text/javascript">
        function num(e)
        {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 47 && k < 58) || k == 46);
        }
    </script>
</head>
<body bgcolor="whitesmoke">
    <form id="form1" runat="server">
    <div>
        <table style="width: 638px; font-size: 8pt; font-family: Verdana;" bgcolor="white">
            <tr bgcolor="#666666" style="color: #FFFFFF">
                <td style="font-size: 10pt; font-family: Verdana;" colspan="3">
                    Membership Record</td>
            </tr>
            <tr>
                <td style="width: 210px; height: 15px;">
                    <A HREF="javascript:history.back()"><span style="font-size: 8pt; font-family: Verdana">
                       Back to previous</span></A></td>
                <td style="width: 204px; height: 15px;">
                    <asp:Label ID="lbl_id" runat="server" Text="" Visible="False"></asp:Label></td>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="width: 210px">
                    <span style="font-size: 8pt">
                    Member Name :</span></td>
                <td style="width: 204px">
                    <asp:Label ID="lbl_MemberName" runat="server" Text=""></asp:Label></td>
                <td>
                    </td>
            </tr>
            <tr>
                <td style="width: 210px">
                    <span style="font-size: 8pt">
                    Member IC :</span></td>
                <td style="width: 204px">
                    <asp:Label ID="lbl_MemberIC" runat="server" Text=""></asp:Label></td>
                <td>
                    </td>
            </tr>
            <tr>
                <td style="width: 210px">
                    Membership Type :</td>
                <td style="width: 204px">
                    <asp:Label ID="lbl_CurrentMemberType" runat="server" Text=""></asp:Label></td>
                <td>
                    </td>
            </tr>
            <tr>
                <td style="width: 210px">
                </td>
                <td style="width: 204px">
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 210px">
                    Current Entry Fee :</td>
                <td style="width: 204px">
                    <asp:Label ID="lbl_CurrentEntryFee" runat="server" Text=""></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 210px">
                    Current Normal Fee :</td>
                <td style="width: 204px">
                    <asp:Label ID="lbl_CurrentNormalFee" runat="server" Text=""></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 210px">
                    Current Fee Status :</td>
                <td style="width: 204px">
                    <asp:Label ID="lbl_feeStat" runat="server" Text=""></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 210px">
                    Last Payment Received Date :
                </td>
                <td style="width: 204px">
                    <asp:Label ID="lbl_CurrentExpiredDate" runat="server" Text=""></asp:Label></td>
                <td>
                </td>
            </tr>
           
            
           
            <tr>
                <td colspan="3">
                    <br />
                    <asp:Label ID="lbl_WarningMessage" runat="server" 
                        Text="You are not allowed to renew the membership before it is expired!" 
                        ForeColor="Red" Visible="False" Font-Size="8pt"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="height: 12px">
                </td>
            </tr>
            <tr bgcolor="#666666" style="color: #FFFFFF">
                <td style="font-size: 10pt;" colspan="3">
                    Payment Option</td>
            </tr>
            <tr>
                <td style="width: 210px">
                    Payment Year</td>
                <td style="width: 204px">
                    <asp:DropDownList ID="ddl_RenewYear" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td>
                    </td>
            </tr>
            <tr>
                <td style="width: 210px; height: 8px;">
                    Received Date</td>
                <td style="width: 204px; height: 8px;">
                    <asp:TextBox ID="txt_StartDate" runat="server" ReadOnly="true"></asp:TextBox>&nbsp;&nbsp; <a 
                        href="javascript: showCalendar('txt_StartDate')"><img alt="" border="0" 
                        src="images/ew_calendar.gif" /></a>&nbsp; </td>
                <td style="height: 8px">
                </td>
            </tr>
            <tr>
                <td style="width: 210px; height: 24px;">
                    Fee Status</td>
                <td style="width: 204px; height: 24px;">
                    <asp:DropDownList ID="ddl_FeeStatus" runat="server">
                        <asp:ListItem Value="2">Unpaid</asp:ListItem>
                        <asp:ListItem Value="3" Selected="True">Paid</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="height: 24px">
                </td>
            </tr>
            <tr>
                <td style="width: 210px; height: 15px;">
                    Payment Method</td>
                <td style="width: 204px; height: 15px;">
                    <asp:DropDownList ID="ddlPayMet" runat="server">
                    <asp:ListItem Value="Cash">Cash</asp:ListItem>
                    <asp:ListItem Value="Cheque">Cheque</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="width: 210px; height: 15px">
                    Amount Received</td>
                <td style="width: 204px; height: 15px" colspan="2">
                    <asp:TextBox ID="txtAmntRecv" runat="server" Width="70px" MaxLength="6"></asp:TextBox>
                    Fee RM<asp:Label ID="lbl_fee" runat="server" Text=""></asp:Label>&nbsp; Entrance <asp:Label ID="lbl_Entryfee" runat="server"></asp:Label>
                    <asp:RegularExpressionValidator ID="regexpName" runat="server"     
                                    ErrorMessage="Enter Value" 
                                    ControlToValidate="txtAmntRecv"     
                                    ValidationExpression="^\d+(\.\d\d)?$" Font-Size="8pt" />

                </td>
            </tr>
            <tr>
                <td style="width: 210px; height: 10px">
                    Cheque No.</td>
                <td style="width: 204px; height: 10px">
                    <asp:TextBox ID="txtCheq" runat="server"></asp:TextBox></td>
                <td style="height: 10px">
                </td>
            </tr>            
            <tr>
                <td style="width: 210px; height: 10px">
                    Receipt No.</td>
                <td style="width: 204px; height: 10px">
                    <asp:TextBox ID="txtRcpt" runat="server"></asp:TextBox></td>
                <td style="height: 10px">
                </td>
            </tr>
            <tr>
                <td style="width: 210px; height: 15px" valign="top">
                    Remarks</td>
                <td style="width: 204px; height: 15px">
                    <textarea id="taRmrk" cols="20" rows="2" runat="server"></textarea><br />
                    <br />
                    <asp:Button ID="btn_Renew" runat="server" Text="Renew" OnClick="btn_Renew_Click" /></td>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 14px">
                    <asp:Label ID="lbl_TotalRefund" runat="server" Visible="False"></asp:Label></td>
            </tr>
        </table>
        <br />
    </div> 
    </form>
</body>
</html>
