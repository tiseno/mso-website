﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using MSO_BizLayer;
public partial class AdminMain : System.Web.UI.Page
{
    private BizMember bm = new BizMember();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserCookies"] == null)
        {
            Response.Redirect("LostCookies.aspx");
        }
        else
        {
            string ProfileID;

            ProfileID = Request.Cookies["UserCookies"].Value;
        }

        DataSet alert = new DataSet();
        alert = bm.ExpiredMemberList2();

        Dg_AlertExpiredMember.DataSource = alert;
        Dg_AlertExpiredMember.DataBind();
        Panel_Alert_Message.Visible = false;

        if (alert.Tables[0].Rows.Count > 0)
        {
            //Panel_Alert_Message.Visible = true;
        }
        else
        {
            Panel_Alert_Message.Visible = false;
        }
    }
    protected void Link_ViewProfile_Click(object sender, EventArgs e)
    {
        Response.Redirect("MemberProfile.aspx");
    }
    protected void Link_SearchMember_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchMember.aspx");
    }
    protected void Link_newletter_Click(object sender, EventArgs e)
    {
        Response.Redirect("newsletter.aspx");
    }
    protected void Link_Logout_Click(object sender, EventArgs e)
    {
        Response.Cookies["UserCookies"].Value = "";
        Response.Cookies["UserCookies"].Expires = DateTime.Now.AddDays(-1);
        Response.Redirect("login.aspx");
    }
    protected void Link_CreateNewMember_Click(object sender, EventArgs e)
    {
        Response.Redirect("Register.aspx");
    }
    
    protected void btnAbstractSubmission_Click(object sender, EventArgs e)
    {
        Response.Redirect("proposal/admin/search.aspx");
    }
    protected void BtnEventControl_Click(object sender, EventArgs e)
    {
        Response.Redirect("payment/Event_Control.aspx");
    }
}
