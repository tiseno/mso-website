﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="detail_form.aspx.cs" Inherits="detail_form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY</title>
    <style type="text/css">
        body
        {
            
            font-family :tahoma,arial ;
            margin:0px 0px 0px 0px;
        }
    </style>
    
<script type="text/javascript"><!--
var wordLen = 250; // Maximum word length
function checkWordLen(obj){
var len = obj.value.split(/[\s]+/);
if(len.length > wordLen){
alert("You cannot put more than "+wordLen+" words in this text area.");
obj.oldValue = obj.value!=obj.oldValue?obj.value:obj.oldValue;
obj.value = obj.oldValue?obj.oldValue:"";
return false;
}
return true;
}
//--></script>
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table border ="0" cellpadding="0" cellspacing="0"  width ="100%">
    
    <tr><td align ="center" style ="padding:10px 0px 20px 0px;">
    <table style="width:50%;"><tr><td align ="center"><a href ="../index.html"> <img src="../images/logo.jpg" style ="border:none;" alt="" /></a> </td></tr></table>
    </td></tr>
    
    <tr><td align ="center" >
    <table border="0" width="100%;" style="background-color:#2a718d; color:White;">
    <tr><td align ="left" style ="color:White ; font-family :tahoma,arial ; font-size :10pt; font-weight :bold ; padding:0px 0px 0px 335px">3rd Annual Scientific Meeting </td></tr></table>
    </td></tr>
    
    <tr><td align ="center" >
    <table style ="border:2px #eeeeee solid;" width ="500px"  cellpadding ="0" cellspacing ="0">

    <%--<table border="0" width ="500px" cellpadding="0" cellspacing="0" >--%>
    <tr><td align="left">
    <%--<table border="0" width="500px;" style="background-color:#2a718d; color:White;"><tr><td>Online Abstract Submission</td></tr></table>--%>
    
    <table border="0" width="500px;">
    <tr><td align ="right" style="padding:10px 3px 0px 0px; " colspan ="3">
              <a href ="http://mso.org.my/index.html"><span style ="font-family:tahoma,arial; font-size :8pt;">Back To Main Page</span></a>
                  </td></tr>
    <tr><td style="padding:5px 0px 0px 10px; font-size:9pt;">Submitting Author</td><td>:</td>
    <td style="padding:5px 0px 0px 0px;"><asp:TextBox ID="Author" runat="server"></asp:TextBox><span style="color:Red;">*&nbsp;<asp:Label ID="author_error" Font-Size ="8pt" runat="server" Text=""></asp:Label></span></td></tr>
    
    
    <tr><td style="padding-left:10px; padding-top:10px;font-size:9pt;font-family:Arial;" valign="top" >Co-Authors</td><td style="padding-top:10px;" valign="top" >:</td><td>
    <textarea rows="5" cols="30" id="coAuthor" runat="server" onchange="checkWordLen(this);"></textarea></td></tr>
    
    <tr><td style="padding-left:10px;font-size:9pt;">Hospital Name</td><td>:</td><td><asp:TextBox ID="Hospital" runat="server"></asp:TextBox></td></tr>
    
    
    
    <tr><td style="padding-left:10px;font-size:9pt;">Email</td><td>:</td><td><asp:TextBox ID="Email" runat="server"></asp:TextBox><span style="color:Red;">*</span><asp:RegularExpressionValidator
        id="regEmail"
        Font-Size ="8pt"
        ControlToValidate="Email"
        Text="(Invalid email)"
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
        Runat="server" /></td></tr>
    
    <tr><td style="padding-left:10px;font-size:9pt;">Mobile Phone</td><td>:</td><td><asp:TextBox ID="Mobile" runat="server"></asp:TextBox><span style="color:Red;">*&nbsp<asp:Label ID="mobile_error" Font-Size ="8pt" runat="server" Text=""></asp:Label></span></td></tr>
    
    <tr><td style="padding-left:10px; font-size:9pt;">Category</td><td>:</td>
    <td>
    <asp:DropDownList ID="DropDownList1" runat="server">
    <asp:ListItem>Cataract</asp:ListItem>
    <asp:ListItem>Oculoplastics and Orbit</asp:ListItem>
    <asp:ListItem>Retina</asp:ListItem>
    <asp:ListItem>Neuro Ophthalmology</asp:ListItem>
    <asp:ListItem>Cornea and Anterior Segment</asp:ListItem>
    <asp:ListItem>Glaucoma</asp:ListItem>
    <asp:ListItem>Paediatric Ophthalmology</asp:ListItem>
    <asp:ListItem>Refractive Surgery</asp:ListItem>
    <asp:ListItem>Others</asp:ListItem>
    </asp:DropDownList>
    </td></tr>
    
    <tr><td style="padding-left:10px;font-size:9pt;">Abstract Title</td><td>:</td><td><asp:TextBox ID="Abstact" runat="server"></asp:TextBox></td></tr>
   
    <tr><td style="padding-left:10px; padding-top:10px;font-size:9pt;" valign="top" >Remark</td><td style="padding-top:10px;" valign="top" >:</td><td>
    <textarea rows="10" cols="40" id="Submission" runat="server" onchange="checkWordLen(this);"></textarea></td></tr>
      
      
      
    <tr>
    <td style="padding-left:10px;font-size:9pt;">File Upload</td>
    <td>:</td>     
    <td><asp:FileUpload ID="FileUpload_ExcelFile" runat="server" />&nbsp;<asp:Label ID="lbl_error_document" runat="server" Font-Size ="8pt" Text="" ForeColor="#ff0000"></asp:Label></td>
    </tr>
    <%--<tr>
    <td style="padding-left:10px;font-size:9pt;">&nbsp;</td>
    <td>&nbsp;</td>     
    <td><asp:FileUpload ID="FileUpload_ExcelFile2" runat="server" />&nbsp;
    <asp:Label ID="lbl_error_document2" runat="server" Text="" Font-Size ="8pt" ForeColor="#ff0000"></asp:Label></td>
    </tr> 
    <tr>
    <td style="padding-left:10px;font-size:9pt;">&nbsp;</td>
    <td>&nbsp;</td>     
    <td><asp:FileUpload ID="FileUpload_ExcelFile3" runat="server" />&nbsp;<asp:Label ID="lbl_error_document3" runat="server" Font-Size ="8pt" Text="" ForeColor="#ff0000"></asp:Label></td>
    </tr> 
    <tr>
    <td style="padding-left:10px;font-size:9pt;">&nbsp;</td>
    <td>&nbsp;</td>     
    <td><asp:FileUpload ID="FileUpload_ExcelFile4" runat="server" />&nbsp;<asp:Label ID="lbl_error_document4" runat="server" Text="" Font-Size ="8pt" ForeColor="#ff0000"></asp:Label></td>
    </tr> 
    <tr>
    <td style="padding-left:10px;font-size:9pt;">&nbsp;</td>
    <td>&nbsp;</td>     
    <td><asp:FileUpload ID="FileUpload_ExcelFile5" runat="server" />&nbsp;<asp:Label ID="lbl_error_document5" runat="server" Text="" Font-Size ="8pt" ForeColor="#ff0000"></asp:Label></td>
    </tr>  --%>
       
       
       
        
    <tr><td></td><td></td>
    <td style="padding:10px 0px 10px 0px;"><asp:Button ID="submit" runat="server" Text="Submit" 
    onclick="submit_Click" />
    <span style="color:Red;"><asp:Label ID="lbl_file_insert_error" Font-Size ="8pt" runat="server" Text=""></asp:Label></span>
    </td></tr>
   
    
    </table>
    </td></tr></table>
    </td></tr></table>
    </div>
    </form>
</body>
</html>
