<asp:TableRow>    
<asp:TableCell VerticalAlign="top" HorizontalAlign="Right"><asp:Label ID="Label51" runat="server" Text="Case Type :" Font-Names="verdana" Font-Size="8pt"></asp:Label></asp:TableCell>
<asp:TableCell VerticalAlign="top">
        <asp:RadioButtonList ID="CaseRadioButtonList" Runat="server" Font-Size="8pt" Font-Names="Verdana" BorderWidth="0" CellPadding="0" CellSpacing="0" RepeatLayout="flow" RepeatColumns="2">            
            <asp:ListItem Value="APS" Text="APS"></asp:ListItem>
            <asp:ListItem Value="Normal Case" Text="Normal Case" Selected="True"></asp:ListItem>
        </asp:RadioButtonList>
    </asp:TableCell>
</asp:TableRow>