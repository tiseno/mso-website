﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_control : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Request.Cookies["AdminID"] == null)
        //{
        //    Response.Redirect("LostCookies.aspx");

        //}
        //else
        //{
        //}

    }
    protected void Link_Logout_Click(object sender, EventArgs e)
    {
        HttpCookie aCookie = default(HttpCookie);
        int i = 0;
        string cookieName = null;
        int limit = Request.Cookies.Count - 1;
        for (i = 0; i <= limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }

        Response.Redirect("Login.aspx");

    }
}
