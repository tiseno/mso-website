﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="documentDelete.aspx.cs" Inherits="documentDelete" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/style1.css" rel="Stylesheet" type="text/css" />
</head>
<body onunload="opener.location.reload();">
    <form id="form1" runat="server">
    <div>
    <span class="PageTitle">Delete Document Page</span><br /><br />
    <table>
    
    <tr>
    <td>Are you sure you want to delete
    <asp:Label ID="lbl_Bannertitle" runat="server" Text=""></asp:Label>
     ?<br />
     </td>
     </tr>
    
     <tr>
     <td>
     <asp:Label ID="lbl_imgpath" runat="server" Text="" Visible="false"></asp:Label>
     </td>
     </tr>
     
     <tr>
     <td align="right"><br />
     <asp:Button ID="btn_Yes" runat="server" Text="Yes" Width="45px" 
             onclick="btn_Yes_Click"/>
     &nbsp;<asp:Button ID="btn_No" runat="server" Text="No" Width="45px" 
             style="height: 26px" onclick="btn_No_Click" />
     </td>
     </tr>
     </table>
    </div>
    </form>
</body>
</html>
