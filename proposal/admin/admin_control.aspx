﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="admin_control.aspx.cs" Inherits="admin_control" %>

<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="en" />
	<meta name="robots" content="noindex,nofollow" />
    <title>MSO Administrator Login</title>
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../../css/reset1.css" /> <!-- RESET -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../../css/main1.css" /> <!-- MAIN STYLE SHEET -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../../css/2col1.css" title="2col" /> <!-- DEFAULT: 2 COLUMNS -->
	<link rel="alternate stylesheet" media="screen,projection" type="text/css" href="../../css/1col1.css" title="1col" /> <!-- ALTERNATE: 1 COLUMN -->
	<!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]--> <!-- MSIE6 -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../../css/style21.css" /> <!-- GRAPHIC THEME -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="../../css/mystyle1.css" /> <!-- WRITE YOUR CSS CODE HERE -->
	<script type="text/javascript" src="../js/jquery1.js"></script>
	<script type="text/javascript" src="../js/switcher1.js"></script>
	<script type="text/javascript" src="../js/toggle1.js"></script>
	<script type="text/javascript" src="../js/ui.core1.js"></script>
	<script type="text/javascript" src="../js/ui.tabs1.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".tabs > ul").tabs();
	});
	</script>
<script language="javascript" type="text/javascript">
<!--
    function autoResize(id) {
        var newheight;
        var newwidth;

        if (document.getElementById) {
            newheight = document.getElementById(id).contentWindow.document.body.scrollHeight;
            newwidth = document.getElementById(id).contentWindow.document.body.scrollWidth;
        }

        document.getElementById(id).height = (newheight) + "px";
        document.getElementById(id).width = (newwidth) + "px";
    }
//-->
</script>

</head>

<body>
<form id="form1" runat="server">
<div id="main">

	<!-- Tray -->
	<div id="tray" class="box">
        &nbsp;<p class="f-right"><%--Language Version:--%><strong>
               <%-- <asp:LinkButton ID="Link_English" runat="server">English</asp:LinkButton>--%></strong>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><%--<asp:LinkButton ID="Link_Logout" 
                runat="server" onclick="Link_Logout_Click">Logout</asp:LinkButton>--%></strong></p>

	</div> <!--  /tray -->

	<hr class="noscreen" />

	<!-- Menu -->
	<div id="menu" class="box">

		<ul class="box f-right">
			<li></li>
		</ul>

		<ul class="box">
			<li id="menu-active"></li> <!-- Active -->
		</ul>
        </div> <!-- /header -->

	<hr class="noscreen" />

	<!-- Columns -->
	<div id="cols" class="box">

		<!-- Aside (Left Column) -->
		<div id="aside" class="box">

			<div class="padding box" align="center">

				<!-- Logo (Max. width = 200px) -->
				<p id="logo"><img src="../../images/smalllogo.jpg" border="0" alt="Motor" /></p>

                <!-- Search -->
				<!-- Create a new project -->
                <!--<table><tr><td ><p id="btn-create"><a href="#" target="_blank" >&nbsp;&nbsp;&nbsp;Website 
                    Preview</a></p></td></tr></table>-->
                    </div><!-- /padding -->
			    <ul class="box">
				<li></li>
				<li id="submenu-active"><table><tr><td><a></a></td></tr></table>  <%--Active--%> 
					<ul>
					<li><asp:HyperLink ID="HyperLink1" runat="server" target="main" NavigateUrl="search.aspx" >Search Manager</asp:HyperLink></li>
					<li><asp:HyperLink ID="HyperLink2" runat="server" target="main" NavigateUrl="../detail_form.aspx">Form Insert</asp:HyperLink></li>
					<%--<li><asp:HyperLink ID="HyperLink2" runat="server" target="main" NavigateUrl="menuView.aspx">Menu Manager</asp:HyperLink></li>
					<li><asp:HyperLink ID="HyperLink3" runat="server" target="main" NavigateUrl="homebannerView.aspx">Home banner</asp:HyperLink></li>
				    <li><asp:HyperLink ID="HyperLink4" runat="server" target="main" NavigateUrl="newsView.aspx" >Home News</asp:HyperLink></li>
				    <li><asp:HyperLink ID="HyperLink5" runat="server" target="main" NavigateUrl="dealerView.aspx" >Dealer</asp:HyperLink></li>--%>

				<%--	<li><asp:HyperLink ID="HyperLink5" runat="server" target="main" NavigateUrl="category_view.aspx">Category Manager</asp:HyperLink></li>
		--%>
					</ul>
				</li>
			</ul>

		</div> <!-- /aside -->

		<hr class="noscreen" />

		<!-- Content (Right Column) -->
		<div id="content" class="box">

			<h1>MSO Administrator Control</h1>

			<!-- Headings -->
            <p></p>
            
            <iframe id="AdminControl" runat="server" name="main" frameborder="0" scrolling="yes" width="100%" height="970"></iframe>

            </div> <!-- /content -->

	</div> <!-- /cols -->

	<hr class="noscreen" />

	<!-- Footer -->
	<div id="footer" class="box">

		<p class="f-left">&copy; 2011 <a href="#">MSO</a>, All Rights Reserved &reg;</p>

	</div> <!-- /footer -->

</div> <!-- /main -->
</form>
</body>
</html>