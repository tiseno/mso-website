﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using commonlayer;
using System.Data.OleDb;

public partial class admin_updatedetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != "") 
            {

            }


            bizdocument bd = new bizdocument();
            bizdetail bhb = new bizdetail();

            DataSet ds;
            DataSet ds2;

            ds = bhb.SelectDetailByID(Request.QueryString["id"]);
            ds2 = bd.SelectAllDocumentByID(Request.QueryString["id"]);

            //dg_SearchControl.DataSource = ds2;
            //dg_SearchControl.DataBind();


            if (ds.Tables[0].Rows.Count > 0)
            {

                Author.Text = ds.Tables[0].Rows[0]["author_name"].ToString();
                coAuthor.Value = ds.Tables[0].Rows[0]["co_author"].ToString();
                hospital.Text = ds.Tables[0].Rows[0]["hospital"].ToString();
                category.SelectedValue = ds.Tables[0].Rows[0]["category"].ToString();
                email.Text = ds.Tables[0].Rows[0]["email"].ToString();
                mobile.Text = ds.Tables[0].Rows[0]["mobile_phone"].ToString();
                title.Text = ds.Tables[0].Rows[0]["abstract_title"].ToString();
                submission.Text = ds.Tables[0].Rows[0]["abstract_submission"].ToString();


            }

            if (ds2.Tables[0].Rows.Count > 0)
            {
                document.InnerHtml = "<a href='../UploadedExcel/" + ds2.Tables[0].Rows[0]["document_name"].ToString() + "'>" + ds2.Tables[0].Rows[0]["document_name"].ToString() + "</a>";
                
            }

            //int i = 0;
            //foreach (DataGridItem dr in dg_SearchControl.Items)
            //{
            //    HtmlAnchor link_download = (HtmlAnchor)dr.FindControl("hyperlink_Download");
            //    link_download.HRef = "../UploadedExcel/" + ds2.Tables[0].Rows[i]["document_name"].ToString();
                
            //    i = i + 1;
            //}
        }
    }
    protected void submit_Click(object sender, EventArgs e)
    {
        bizdetail bhb = new bizdetail();

        bhb.updateDetail(Author.Text, hospital.Text, category.Text, email.Text, mobile.Text, title.Text, submission.Text, coAuthor.Value , Request.QueryString["id"]);
       
        Response.Write("<script>alert('Detail has been saved!');</script>");
        Response.Write("<script>window.location='search.aspx';</script>");
    }

    //protected void upload_Click(object sender, EventArgs e)
    //{
    //    //upload document here.
    //    //bool fileSelected = false;
    //    int x = 0;
    //    //if (FileUpload_ExcelFile.PostedFile.ContentLength > 1)
    //    //{
    //       // fileSelected = true;
    //        int test = FileUpload_ExcelFile1.PostedFile.ContentLength;
    //        int AbsSize = test / 1024;
    //        if (AbsSize <= 1024)
    //        {
    //            x = 1;
    //        }
    //   // }

    //   // if (fileSelected)
    //               // {
    //                    if (x == 1)
    //                    {
    //    if (FileUpload_ExcelFile1.PostedFile.ContentLength < 1)
    //    {
    //        //lbl_error_document.Text = "You must browse your document!";
    //    }
    //    else
    //    {
    //        OleDbConnection conn = new OleDbConnection();
    //        OleDbCommand cmd = new OleDbCommand();
    //        OleDbDataAdapter da = new OleDbDataAdapter();
    //        DataSet ds = new DataSet();

    //        string strFileName = DateTime.Now.ToString("ddMMyyyy_HHmmss");
    //        string RealFileName = FileUpload_ExcelFile1.FileName;
    //        string strFileType = System.IO.Path.GetExtension(FileUpload_ExcelFile1.FileName).ToString().ToLower();

    //        FileUpload_ExcelFile1.SaveAs(Server.MapPath("~/UploadedExcel/" + strFileName + "1" + RealFileName));

    //        //save file into UploadedExcel folder
    //        string strNewPath = Server.MapPath("~/UploadedExcel/" + strFileName + strFileType);


    //        bizdocument bd2 = new bizdocument();
    //        bd2.insertDocument(strFileName + "1" + RealFileName,Convert.ToInt32(Request.QueryString["id"]));
            
    //        Response.Write("<script>window.location='updatedetail.aspx?id=" + Request.QueryString["id"] + "';</script>");
    //    }
    //                    }
    //                    else
    //                    {
    //                        //Response.Write("<script>window.location='detail_form.aspx';</script>");
    //                        Response.Write("<script>alert('File size should not be greater than 1 MB.!');</script>");
    //                    }

    //               // }
    //  //  else
    //    //{
    //        //lbl_file_insert_error.Text = "You must insert at least one file!";
    //   // }

    //}

    protected void dg_SearchControl_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void back_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.location='search.aspx';</script>");
    }
  
}
