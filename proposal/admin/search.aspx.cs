﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using bizlayer;
using commonlayer;
using System.Data;
using System.Data.OleDb;




public partial class search: System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //after batch insert
            //if (Request.QueryString["batchID"].ToString() != "")
            //{
            //    bizstudent bs = new bizstudent();
            //    DataSet ds;

            //    ds = bs.SelectAllByBatchID(Request.QueryString["batchID"].ToString());
            //    dg_SearchControl.DataSource = ds;
            //    dg_SearchControl.DataBind();

            //}

            //after edit student info
            //if (Request.QueryString["stud_id"].ToString() != "")
            //{
            //    bizstudent bs2 = new bizstudent();
            //    DataSet ds2;

            //    ds2 = bs2.SelectStudentByID(Request.QueryString["stud_id"].ToString());
            //    dg_SearchControl.DataSource = ds2;
            //    dg_SearchControl.DataBind();
            //}
        }

    }

    protected void btn_Reset_Click(object sender, EventArgs e)
    {
        txt_studentName.Text = null;
        txt_studentID.Text = null;
   
    }

    protected void btn_Submit_Click(object sender, EventArgs e)
    {
        dg_SearchControl.CurrentPageIndex = 0;

        Save_Selected_Result.Enabled = true; 

        bizdetail bs = new bizdetail();
        bizdocument bd = new bizdocument();
        
        DataSet ds;
        DataSet ds_menu;
        DataSet ds2;
        
        ds = bs.SelectRecordByCustomizeInput(txt_studentName.Text.ToString(), txt_studentID.Text.ToString(), txt_submission_id.Text);

        DataTable retDT = new DataTable();
        DataColumn idcol = new DataColumn("submission_ID", Type.GetType("System.String"));
        DataColumn authorcol = new DataColumn("author_name", Type.GetType("System.String"));
        DataColumn hostcol = new DataColumn("hospital", Type.GetType("System.String"));
        DataColumn catcol = new DataColumn("category", Type.GetType("System.String"));
        DataColumn emailcol = new DataColumn("email", Type.GetType("System.String"));
        DataColumn mobilecol = new DataColumn("mobile_phone", Type.GetType("System.String"));
        DataColumn titlecol = new DataColumn("abstract_title", Type.GetType("System.String"));
        DataColumn title2col = new DataColumn("document_name", Type.GetType("System.String"));
        DataColumn subcol = new DataColumn("abstract_submission", Type.GetType("System.String"));   
        DataColumn datecol = new DataColumn("detail_date", Type.GetType("System.String"));
        DataColumn coauthorcol = new DataColumn("co_author", Type.GetType("System.String"));

        DataRow retdr;

        retDT.Columns.Add(idcol);
        retDT.Columns.Add(authorcol);
        retDT.Columns.Add(hostcol);
        retDT.Columns.Add(catcol);
        retDT.Columns.Add(emailcol);
        retDT.Columns.Add(mobilecol);
        retDT.Columns.Add(titlecol);
        retDT.Columns.Add(title2col);
        retDT.Columns.Add(subcol);
        retDT.Columns.Add(datecol);
        retDT.Columns.Add(coauthorcol);

        if (ds.Tables[0].Rows.Count > 0)
        {
            string firstRecord2 = "firstRecord";
            string checkLastRecordSubmissionID2 = "";

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (firstRecord2 == "firstRecord" || checkLastRecordSubmissionID2 != ds.Tables[0].Rows[i]["submission_ID"].ToString())
                {
                    retdr = retDT.NewRow();

                    retdr[0] = ds.Tables[0].Rows[i]["submission_ID"].ToString();
                    retdr[1] = ds.Tables[0].Rows[i]["author_name"].ToString();
                    retdr[2] = ds.Tables[0].Rows[i]["hospital"].ToString();
                    retdr[3] = ds.Tables[0].Rows[i]["category"].ToString();
                    retdr[4] = ds.Tables[0].Rows[i]["email"].ToString();
                    retdr[5] = ds.Tables[0].Rows[i]["mobile_phone"].ToString();
                    
                    retdr[6] = ds.Tables[0].Rows[i]["abstract_title"].ToString();
                    //retdr[7] = ds.Tables[0].Rows[i]["abstract_submission"].ToString();
                    retdr[8] = ds.Tables[0].Rows[i]["abstract_submission"].ToString();

                    string iinsert_date = ds.Tables[0].Rows[i]["detail_date"].ToString().Substring(0, 2) + "/" + ds.Tables[0].Rows[i]["detail_date"].ToString().Substring(2, 2) + "/" + ds.Tables[0].Rows[i]["detail_date"].ToString().Substring(4, 4);
                    retdr[9] = iinsert_date;//ds.Tables[0].Rows[i]["detail_date"].ToString();
                    retdr[10] = ds.Tables[0].Rows[i]["co_author"].ToString();

                    ds2 = bd.SelectAllDocumentByID(retdr[0].ToString());

                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        retdr[7] = ds2.Tables[0].Rows[0]["document_name"].ToString();
                    }

                    retDT.Rows.Add(retdr);
                }
                    firstRecord2 = "";
                    checkLastRecordSubmissionID2 = ds.Tables[0].Rows[i]["submission_ID"].ToString();
            }
        }     

        ds_menu = new DataSet();
        ds_menu.Tables.Add(retDT);

        dg_SearchControl.DataSource = ds_menu;
        dg_SearchControl.DataBind();
    }

    protected void dg_SearchControl_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        System.Web.UI.Control ctl = this.dg_SearchControl;
        
        HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=Excel.xls");

        HttpContext.Current.Response.Charset = "UTF-8";
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Default;
        HttpContext.Current.Response.ContentType = "application/ms-excel";

        ctl.Page.EnableViewState = false;

        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

        int counter = 0;

        bizdetail dd = new bizdetail();
        DataSet ds;

        if (counter == 3)
        {
            ds = dd.SelectAllDetail();
        }
        else 
        {
            ds = dd.SelectRecordByCustomizeInput(txt_studentName.Text.ToString(), txt_studentID.Text.ToString(), txt_submission_id.Text);
        }

        HttpContext.Current.Response.Write("<table style='background-color:#000000;'><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></table>");
            HttpContext.Current.Response.Write("<Table border='1'>");
            HttpContext.Current.Response.Write("<tr>");
            HttpContext.Current.Response.Write("<td>No</td><td>Author Name</td><td>Co Author Name</td><td>Hospital Name</td>");
            HttpContext.Current.Response.Write("<td>Category</td><td>E-mail</td>");
            HttpContext.Current.Response.Write("<td>Mobile-Phone</td><td>Abstract Title</td>");
            HttpContext.Current.Response.Write("<td>Abstract Submission</td>");
            HttpContext.Current.Response.Write("<td>Date</td><td>Document</td>");
            HttpContext.Current.Response.Write("<td>Submission ID</td>");
            HttpContext.Current.Response.Write("</tr>");
            
            string firstRecord = "firstRecord";
            string checkLastRecordSubmissionID = "";
            int x = 0;

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++ )
            {
                if (firstRecord == "firstRecord" || checkLastRecordSubmissionID != ds.Tables[0].Rows[i]["submission_ID"].ToString())
                {
                    HttpContext.Current.Response.Write("<tr>");
                    
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(++x);
                    HttpContext.Current.Response.Write("</td>");
                    
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["author_name"].ToString());
                    HttpContext.Current.Response.Write("</td>");
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["co_author"].ToString());
                    HttpContext.Current.Response.Write("</td>");
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["hospital"].ToString());
                    HttpContext.Current.Response.Write("</td>");
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["category"].ToString());
                    HttpContext.Current.Response.Write("</td>");
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["email"].ToString());
                    HttpContext.Current.Response.Write("</td>");
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["mobile_phone"].ToString());
                    HttpContext.Current.Response.Write("</td>");
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["abstract_title"].ToString());
                    HttpContext.Current.Response.Write("</td>");
                    HttpContext.Current.Response.Write("<td>");
                    HttpContext.Current.Response.Write(ds.Tables[0].Rows[i]["abstract_submission"].ToString());
                    HttpContext.Current.Response.Write("</td>");
                    HttpContext.Current.Response.Write("<td>");

                    string iinsert_date = ds.Tables[0].Rows[i]["detail_date"].ToString().Substring(0, 2) + "/" + ds.Tables[0].Rows[i]["detail_date"].ToString().Substring(2, 2) + "/" + ds.Tables[0].Rows[i]["detail_date"].ToString().Substring(4, 4);


                    HttpContext.Current.Response.Write(iinsert_date);

                    HttpContext.Current.Response.Write("</td>");

                    bizdocument dd2 = new bizdocument();
                    DataSet ds2 = dd2.SelectAllDocumentByID(ds.Tables[0].Rows[i]["submission_ID"].ToString());

                    //start from 0
                    if(ds2.Tables[0].Rows.Count > 0)
                    {
                        HttpContext.Current.Response.Write("<td>");
                        HttpContext.Current.Response.Write(ds2.Tables[0].Rows[0]["document_name"].ToString());
                        HttpContext.Current.Response.Write("</td>");
                        HttpContext.Current.Response.Write("<td>");
                        HttpContext.Current.Response.Write(ds2.Tables[0].Rows[0]["submission_ID"].ToString());
                        HttpContext.Current.Response.Write("</td>");

                        //loop start from 1
                        for (int a = 1; a < ds2.Tables[0].Rows.Count; a++)
                        {
                            HttpContext.Current.Response.Write("<tr>");
                            HttpContext.Current.Response.Write("<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>");

                            HttpContext.Current.Response.Write(ds2.Tables[0].Rows[a]["document_name"].ToString());

                            HttpContext.Current.Response.Write("</td>");
                            HttpContext.Current.Response.Write("<td>");

                            HttpContext.Current.Response.Write(ds2.Tables[0].Rows[a]["submission_ID"].ToString());

                            HttpContext.Current.Response.Write("</td>");
                            HttpContext.Current.Response.Write("</tr>");
                        }
                    }                 
                }
                    firstRecord = "";
                    checkLastRecordSubmissionID = ds.Tables[0].Rows[i]["submission_ID"].ToString();
            }
         
            HttpContext.Current.Response.Write("</tr>");

            HttpContext.Current.Response.Write("</table>");
          
           this.ClearControls(ctl);
           ctl.RenderControl(hw);

       //HttpContext.Current.Response.Write(tw.ToString());
        HttpContext.Current.Response.End();
    }

    private void ClearControls(Control control)
    {
        for (int i = control.Controls.Count - 1; i >= 0; i--)
        {
            ClearControls(control.Controls[i]);
        }
        if (!(control is TableCell))
        {
            if (control.GetType().GetProperty("SelectedItem") != null)
            {
                LiteralControl literal = new LiteralControl();
                control.Parent.Controls.Add(literal);
                try
                {
                    literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                }
                catch
                {
                }
                control.Parent.Controls.Remove(control);
            }
            else
                if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                    control.Parent.Controls.Remove(control);
                }
        }
        return;
    }


    protected void dg_SearchControl_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        dg_SearchControl.CurrentPageIndex = e.NewPageIndex;

        bizdetail bs5 = new bizdetail();
        DataSet ds5;
        DataSet ds_menu;
        bizdocument bd = new bizdocument();
        DataSet ds2;

        ds2 = bd.SelectAllDocument();

        ds5 = bs5.SelectRecordByCustomizeInput(txt_studentName.Text.ToString(), txt_studentID.Text.ToString(), txt_submission_id.Text);

        DataTable retDT = new DataTable();
        DataColumn idcol = new DataColumn("submission_ID", Type.GetType("System.String"));
        DataColumn authorcol = new DataColumn("author_name", Type.GetType("System.String"));
        DataColumn hostcol = new DataColumn("hospital", Type.GetType("System.String"));
        DataColumn catcol = new DataColumn("category", Type.GetType("System.String"));
        DataColumn emailcol = new DataColumn("email", Type.GetType("System.String"));
        DataColumn mobilecol = new DataColumn("mobile_phone", Type.GetType("System.String"));
        DataColumn titlecol = new DataColumn("abstract_title", Type.GetType("System.String"));
        DataColumn title2col = new DataColumn("document_name", Type.GetType("System.String"));
        DataColumn subcol = new DataColumn("abstract_submission", Type.GetType("System.String"));
        DataColumn datecol = new DataColumn("detail_date", Type.GetType("System.String"));
        DataColumn coauthorcol = new DataColumn("co_author", Type.GetType("System.String"));

        retDT.Columns.Add(idcol);
        retDT.Columns.Add(authorcol);
        retDT.Columns.Add(hostcol);
        retDT.Columns.Add(catcol);
        retDT.Columns.Add(emailcol);
        retDT.Columns.Add(mobilecol);
        retDT.Columns.Add(titlecol);
        retDT.Columns.Add(title2col);
        retDT.Columns.Add(subcol);
        retDT.Columns.Add(datecol);
        retDT.Columns.Add(coauthorcol);

        int count = ds5.Tables[0].Rows.Count;
        int i = 0;

        if (ds5.Tables[0].Rows.Count != 0)
        {
            if (ds2.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in ds5.Tables[0].Rows)
                {
                    //foreach (DataRow dr2 in ds2.Tables[0].Rows)
                    //{
                        DataRow retdr = retDT.NewRow();
                        retdr[0] = dr[0];
                        retdr[1] = dr[1];
                        retdr[2] = dr[2];
                        retdr[3] = dr[3];
                        retdr[4] = dr[4];
                        retdr[5] = dr[5];
                        retdr[6] = dr[6];
                        retdr[7] = ds2.Tables[0].Rows[i]["document_name"];
                        retdr[8] = dr[7];
                        string iinsert_date = ds5.Tables[0].Rows[i]["detail_date"].ToString().Substring(0, 2) + "/" + ds5.Tables[0].Rows[i]["detail_date"].ToString().Substring(2, 2) + "/" + ds5.Tables[0].Rows[i]["detail_date"].ToString().Substring(4, 4);
                        retdr[9] = iinsert_date;
                        retdr[10] = dr[9];

                        retDT.Rows.Add(retdr);

                        i++;
                   // }
                }
            }
        }

            ds_menu = new DataSet();
            ds_menu.Tables.Add(retDT);

            dg_SearchControl.DataSource = ds_menu;
            dg_SearchControl.DataBind();
    }
}

