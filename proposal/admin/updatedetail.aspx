﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="updatedetail.aspx.cs" Inherits="admin_updatedetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY</title>
    <style type="text/css">
        .style1
        {
            width: 126px;
            font-family :tahoma,arial ;
            font-size :10pt;
        }
    </style>
</head>
<body bgcolor="gray">
    <form id="form1" runat="server">
    <div>
    <table style="width: 95%; border-right: black 1px solid; border-top: black 1px solid; padding-left: 5px; border-left: black 1px solid; border-bottom: black 1px solid; background-color: whitesmoke;" border="0" cellpadding="0" cellspacing="0">
                <tr><td align ="left" style ="padding :10px 10px 10px 10px">
    
    <%--<table border='0' width="580px;" cellpadding="2" cellspacing="0">
    <tr><td style="font-size:12pt;background-color:#2a718d; color:#ffffff;" >Detail</td></tr>
    </table>--%>
    <table border="0" style="width:65%; height:20px;"><tr><td align ="left" >
    <table border="0" style="width:95%; height:20px;">
    
    <tr><td style ="padding :20px 10px 10px 0px;"><strong ><span style="font-family: Verdana; font-size:12pt;">&nbsp;<span style="color: #323232">
	Malaysia Society of Ophthalmology</span></span> | 
                        &nbsp;<a href="search.aspx"><span style="font-family: Verdana; font-size:8pt;"><span
                            style="font-size: 8pt">Back to Previous</span></span> </a>
	
	</strong></td></tr>
	
<tr><td style="color: #323232; font-size:12pt;font-weight:bold; padding:15px 10px 0px 8px" align="left">Detail</td></tr></table>
    
    <table border='0' cellpadding="0" cellspacing="2" style="padding:5px 10px 15px 10px;" width ="95%" >
    <tr><td class="style1">Submitting Author</td><td>:</td><td><asp:TextBox ID="Author" runat="server"></asp:TextBox></td></tr>
    <tr><td class="style1" valign ="top">Co Author Name</td><td valign ="top" >:</td><td>
    <textarea rows="5" cols="30" id="coAuthor" runat="server" onchange="checkWordLen(this);" name="S1"></textarea></td></tr>
    <tr><td class="style1">Hospital Name</td><td>:</td><td><asp:TextBox ID="hospital" runat="server"></asp:TextBox></td></tr>
    
    <tr><td class="style1">Category</td><td>:</td>
    <td>
    <asp:DropDownList ID="category" runat="server">
    <asp:ListItem>Cotaract</asp:ListItem>
    <asp:ListItem>Oculoplastics and Orbit</asp:ListItem>
    <asp:ListItem>Retina</asp:ListItem>
    <asp:ListItem>Neuro Ophthalmology</asp:ListItem>
    <asp:ListItem>Anteroir Segment</asp:ListItem>
    <asp:ListItem>Glaucoma</asp:ListItem>
    <asp:ListItem>Paediatric Ophthalmology</asp:ListItem>
    <asp:ListItem>Others</asp:ListItem>
    </asp:DropDownList>
    </td>
    </tr>
    <!-- need a drop down list -->
    
    <tr><td class="style1">Email</td><td>:</td><td><asp:TextBox ID="email" runat="server"></asp:TextBox></td></tr>
    <tr><td class="style1">Mobile Phone</td><td>:</td><td><asp:TextBox ID="mobile" runat="server"></asp:TextBox></td></tr>
    <tr><td class="style1">Abstract Title</td><td>:</td><td><asp:TextBox ID="title" runat="server"></asp:TextBox></td></tr>
    <tr><td class="style1">Abstract Submission</td><td>:</td><td><asp:TextBox ID="submission" runat="server"></asp:TextBox></td></tr>
   
    <tr><td class="style1"></td><td></td><td></td></tr>
    
    <%--<tr><td colspan="3">
    <table border="0" style="width:100%; height:20px;" align="center">
    <tr><td style="color: #323232; font-size:9pt;font-weight:bold; padding:10px 0px 15px 0px" align="left">Document</td></tr></table>
    </td></tr>--%>
    <tr><td class="style1">Document</td><td>:</td>
    <td><div id="document" runat="server"></div></td></tr>
   
    <tr><td class="style1"></td><td></td><td>
    <%--<asp:Datagrid ID="dg_SearchControl" runat="server" 
    AutoGenerateColumns="False" 
    onselectedindexchanged="dg_SearchControl_SelectedIndexChanged" 
    BackColor="White" 
    BorderColor="#eeeeee" 
     
    BorderWidth="2px" 
    CellPadding="4" 
    ForeColor="Black" 
    
    
    ItemStyle-VerticalAlign="Top" 
    >
        
    <Columns>
    
    <asp:TemplateColumn HeaderText="No"><itemtemplate>
    <%#Container.ItemIndex + dg_SearchControl.CurrentPageIndex * dg_SearchControl.PageSize + 1%>
    </itemtemplate>
    <HeaderStyle Width="17px" HorizontalAlign="Center"/>
    <ItemStyle HorizontalAlign="Center"/>
    </asp:TemplateColumn>
    
    <asp:BoundColumn DataField="document_name" HeaderText="Document Name">
    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
    </asp:BoundColumn>
    
    <asp:TemplateColumn HeaderText="Delete">
    <ItemTemplate>
    <a href="" runat="server" style="text-decoration:none;" id="hyperlink_delete" >Delete</a>
    </ItemTemplate>
    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
    </asp:TemplateColumn>
   

    
    </Columns>
    <HeaderStyle BackColor="#2a718d" Font-Bold="True" ForeColor="White" />
    </asp:Datagrid>--%>
    </td></tr>
    
    <%--<tr><td class="style1" valign ="top">File</td><td valign ="top">:</td><td><asp:FileUpload ID="FileUpload_ExcelFile1" runat="server" />
    
    <asp:RegularExpressionValidator 
    id="RegularExpressionValidator1" runat="server" 
    ErrorMessage="Only PDF and DOC!" 
    ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.doc|.docx)$" 
    ControlToValidate="FileUpload_ExcelFile1">
    </asp:RegularExpressionValidator>
    
    <asp:Label ID="Label1" runat="server" Text="" ForeColor="#ff0000"></asp:Label></td></tr>
    
    <tr><td class="style1"></td><td></td><td align ="left" style =" padding:0px 10px 10px 0px;" >
<asp:Button ID="upload" runat="server" Text="upload" 
    onclick="upload_Click" />
    </td></tr>--%>
    
    <tr><td colspan="3" style="height:20px;"></td></tr>
    
    <tr><td colspan ="3" style =" padding:10px 10px 20px 0px;" align ="left" ><asp:Button ID="submit" runat="server" Text="Update" 
            onclick="submit_Click" />&nbsp;<asp:Button ID="back" runat="server" Visible ="false"  Text="Back" 
            onclick="back_Click" /></td></tr>
    </table>
    </td> </tr> 
   </table>  </td></tr></table></div>
    </form>
</body>
</html>
