﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using commonlayer;
using System.IO;

public partial class documentDelete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bizdocument bhb = new bizdocument();

            DataSet ds;

            ds = bhb.SelectDocumentByTitle(Request.QueryString["title"]);
            lbl_Bannertitle.Text = ds.Tables[0].Rows[0]["document_name"].ToString();
            
        }
        string scriptString = "<script language=JavaScript> " + "window.opener.document.forms(0).submit(); </script>";

        if (!Page.ClientScript.IsClientScriptBlockRegistered(scriptString))
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", scriptString);
        }
    }
    protected void btn_No_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.close()</script>");
    }
    protected void btn_Yes_Click(object sender, EventArgs e)
    {
        bizdocument bhb = new bizdocument();
        bhb.DeleteDocumentByTitle(Request.QueryString["title"]);
        File.Delete(Server.MapPath(("../UploadedExcel/" + Request.QueryString["title"])));
       // File.Delete(Server.MapPath(("../UploadedExcel/" + Request.QueryString["title"])));
        Response.Write("<script>alert('Document has been deleted!');</script>");
        Response.Write("<script>window.close()</script>");
    }
}
