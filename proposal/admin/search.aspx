﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search.aspx.cs" Inherits="search" %>

<%--<%@ Register TagPrefix="chkbox" Namespace="DataGridControls" Assembly="DataGridCheckbox" %>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY</title>
<style>
    body
    {
        font-family:tahoma,arial;
        font-size:9pt;
    }
</style>    
<link href="../css/link.css" rel="Stylesheet" type="text/css" />    
<link href="../css/style.css" rel="Stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" media="all" href="../jsDatePick_ltr.min.css" />
															
<script type="text/javascript" src="../jsDatePick.min.1.3.js"></script>			   
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target: "txt_enrollDate",
			cellColorScheme: "ocean_blue",
			dateFormat:"%m/%d/%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	};
</script>
</head>
<body bgcolor="gray">
    <form id="form1" runat="server">
    <div>
    <table style="width: 93%; border-right: black 1px solid; border-top: black 1px solid; padding-left: 5px; border-left: black 1px solid; border-bottom: black 1px solid; background-color: whitesmoke;" border="0" cellpadding="0" cellspacing="0">
                <tr><td>
    
  
<table style="width:95%;" border="0">
<tr><td colspan ="3" style="padding:20px 0px 10px 0px;"><strong ><span style="font-family: Verdana; font-size:12pt;">&nbsp;<span style="color: #323232">
	Malaysia Society of Ophthalmology</span></span> | 
                        &nbsp;<a href="../../AdminMain.aspx"><span style="font-family: Verdana; font-size:8pt;"><span
                            style="font-size: 8pt">Back to Previous</span></span> </a>
	
	</strong></td></tr>

</table>
<br />
<table cellpadding="0" cellspacing="0" style="border:2px #eeeeee solid; width:95%">

<tr><td style="font-family: Verdana;; font-size:11pt;font-weight:bold; padding:10px 10px 15px 10px; color: #323232" align="left">Search Manager</td></tr>
<tr>
<td>

<table border="0" style="width:100%; height:20px;" align="center">
<tr><td style="color: #323232; font-size:9pt;font-weight:bold; padding:3px 10px 3px 8px" align="left">Search Criteria</td></tr></table>
<br />

<table border="0">
<tr>
<td style="padding-left:10px;">Author Name</td>
<td>:</td>
<td><asp:TextBox ID="txt_studentName" runat="server" width="250px" meta:resourcekey="txt_usernameResource2"></asp:TextBox></td>
</tr>

<tr>
<td style="padding-left:10px;">Hospital</td>
<td>:</td>
<td><asp:TextBox ID="txt_studentID" runat="server" width="200px" meta:resourcekey="txt_usernameResource2"></asp:TextBox></td>
</tr>

<tr>
<td style="padding-left:10px;">Submission ID</td>
<td>:</td>
<td><asp:TextBox ID="txt_submission_id" runat="server" meta:resourcekey="txt_usernameResource2"></asp:TextBox></td>
</tr>

</table>

<br />
<table align="left">
<tr>
<td style="padding-left:8px;"><asp:Button ID="btn_Reset" runat="server" Text="Reset" onclick="btn_Reset_Click" /></td>
<td><asp:Button ID="btn_Submit" runat="server" Text="Search" onclick="btn_Submit_Click" /></td>
</tr>
</table>
<br />


<%--<table style="width:100%; height:20px; background-color:#2a718d;" align="center">
<tr><td style="color:#FFFFFF;font-size:9pt;font-weight:bold; padding:3px 10px 3px 10px" align="left">Search Results</td></tr></table>--%>


</td>
</tr>
</table>
<br />
<table border="0" style="width:95%; height:20px;">
<tr><td style="color: #323232; font-size:9pt;font-weight:bold; padding:20px 10px 3px 8px" align="left">Search Results</td></tr></table>

<%--testing shopping cart--%>
<table border="0" cellpadding="0" cellspacing="0" style ="padding:10px 10px 10px 0px;">
<tr>
<td style="padding-left:8px;" align ="left" >
<asp:Button ID="Save_Selected_Result" runat="server" Text="Extract to Excel" onclick="btn_Save_Click" Enabled="false" /></td>
<%--<td style="padding-right:10px;"><asp:Button ID="Display_Selected_Result" runat="server" Text="Display Selected Result" onclick="btn_Display_Click" /></td>
<td><asp:Button ID="Clear_Selected_Result" runat="server" Text="Clear Selected Results" onclick="btn_Clear_Click" /></td>--%>
</tr>
</table>
<table border ="0" cellpadding="0" cellspacing="0" width ="100%" style ="padding :10px 8px 10px 8px;"><tr><td align ="center"  >


       <asp:Datagrid ID="dg_SearchControl" runat="server" 
        AutoGenerateColumns="False" 
        onselectedindexchanged="dg_SearchControl_SelectedIndexChanged" 
        BackColor="White" 
        BorderColor="#eeeeee" 
         
        BorderWidth="2px" 
        CellPadding="4" 
        ForeColor="Black" 
        
        
        ItemStyle-VerticalAlign="Top" 
        PageSize="10" 
        OnPageIndexChanged="dg_SearchControl_PageIndexChanged" 
        AllowPaging="true" 
        PagerStyle-Position="Bottom" 
        PagerStyle-Mode="NextPrev" 
        ><%--tis is for paging--%>
        
            <FooterStyle BackColor="#CCCC99" />
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
            Mode="NumericPages" />
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#e0ebf1" />
            
        
       <Columns>
        
            <asp:TemplateColumn HeaderText="No"><itemtemplate>
            <%#Container.ItemIndex + dg_SearchControl.CurrentPageIndex * dg_SearchControl.PageSize + 1%>
            </itemtemplate>
            <HeaderStyle Width="17px" HorizontalAlign="Center"/>
            <ItemStyle HorizontalAlign="Center"/>
            </asp:TemplateColumn>
            
<%--checkbox--%>
<%--<asp:TemplateColumn>
<HeaderTemplate>Select</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox Runat="server" ID="checkbox1"></asp:CheckBox> 
</ItemTemplate>
</asp:TemplateColumn>--%>
            
            
            <asp:TemplateColumn HeaderText="Edit">
            <ItemTemplate>
            <asp:HyperLink ID="hyperlink_citysetting" runat="server" Font-Underline="false" NavigateUrl='<%#"updatedetail.aspx?id=" + Eval("submission_ID") %>'>View</asp:HyperLink><%--<%#"view_and_edit_studentInfo.aspx?user_id=" + Eval("user_id") %>--%>
            </ItemTemplate> 
            <HeaderStyle Width="10px" HorizontalAlign="Center"/>
            <ItemStyle HorizontalAlign="Center"/>          
            </asp:TemplateColumn>

            <asp:BoundColumn DataField="author_name" HeaderText="Author Name">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>
            
            <asp:BoundColumn DataField="co_author" HeaderText="Co-Author">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>

            <asp:BoundColumn DataField="hospital" HeaderText="Hospital Name">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>

            <asp:BoundColumn DataField="category" HeaderText="Category">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>
            
            <asp:BoundColumn DataField="email" HeaderText="E-mail">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>
            
            <asp:BoundColumn DataField="mobile_phone" HeaderText="Mobile Phone">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>
            
           
            
            <asp:BoundColumn DataField="abstract_title" HeaderText="Abstract Title">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>
            
            
            
            <asp:BoundColumn DataField="abstract_submission" HeaderText="Abstract Submission">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>
            
           
            
           <asp:BoundColumn DataField="document_name" HeaderText="Document Name">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>
            
            <asp:BoundColumn DataField="detail_date" HeaderText="Date">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>
            
           <%--<asp:BoundColumn DataField="submission_ID" HeaderText="Submission Code">
            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
            </asp:BoundColumn>--%>
            
          <%--  <asp:TemplateColumn HeaderText="Delete">
            <ItemTemplate>
            <a href="" runat="server" style="text-decoration:none;" id="hyperlink_delete" >Delete</a>
            </ItemTemplate>

            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
            </asp:TemplateColumn>
--%>
            </Columns>
            <HeaderStyle BackColor="#d9d6d6" Font-Bold="True" ForeColor="#323232" />
            </asp:Datagrid>
          </td></tr></table>
    </td> </tr> </table> </div> 
    </form>
</body>
</html>
