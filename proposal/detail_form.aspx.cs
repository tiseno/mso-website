﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using commonlayer;
using System.Data.OleDb;

public partial class detail_form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
        
    }

    protected void submit_Click(object sender, EventArgs e)
    {
        bizdetail bd = new bizdetail();

        if (Author.Text != "")
        {
            author_error.Text = "";

                if (Mobile.Text != "")
                {
                    mobile_error.Text = "";

                    //validate uploader to c whether there is doc being uploaded
                    bool fileSelected = false;
                    int x = 0;

                    if (FileUpload_ExcelFile.PostedFile.ContentLength > 1)
                    {
                        fileSelected = true;
                        int test = FileUpload_ExcelFile.PostedFile.ContentLength;
                        int AbsSize = test / 1024;
                        if (AbsSize <= 1024)
                        {
                            x = 1;
                        }
                    }

                    //if (FileUpload_ExcelFile2.PostedFile.ContentLength > 1)
                    //{
                    //    fileSelected = true;

                    //    int test = FileUpload_ExcelFile2.PostedFile.ContentLength;
                    //    int AbsSize = test / 1024;
                    //    if (AbsSize <= 1024)
                    //    {
                    //        x = 1;
                    //    }

                    //    //if (FileUpload_ExcelFile2.FileBytes.Length < 1024)
                    //    //{
                    //    //    x = 1;
                    //    //}
                    //}

                    //if (FileUpload_ExcelFile3.PostedFile.ContentLength > 1)
                    //{
                    //    fileSelected = true;

                    //    int test = FileUpload_ExcelFile3.PostedFile.ContentLength;
                    //    int AbsSize = test / 1024;
                    //    if (AbsSize <= 1024)
                    //    {
                    //        x = 1;
                    //    }


                    //    //if (FileUpload_ExcelFile3.FileBytes.Length < 1024)
                    //    //{
                    //    //    x = 1;
                    //    //}
                    //}

                    //if (FileUpload_ExcelFile4.PostedFile.ContentLength > 1)
                    //{
                    //    fileSelected = true;

                    //    int test = FileUpload_ExcelFile4.PostedFile.ContentLength;
                    //    int AbsSize = test / 1024;
                    //    if (AbsSize <= 1024)
                    //    {
                    //        x = 1;
                    //    }

                    //    //if (FileUpload_ExcelFile4.FileBytes.Length < 1024)
                    //    //{
                    //    //    x = 1;
                    //    //}
                    //}

                    //if (FileUpload_ExcelFile5.PostedFile.ContentLength > 1)
                    //{
                    //    fileSelected = true;

                    //    int test = FileUpload_ExcelFile5.PostedFile.ContentLength;
                    //    int AbsSize = test / 1024;
                    //    if (AbsSize <= 1024)
                    //    {
                    //        x = 1;
                    //    }
                    //    //if (FileUpload_ExcelFile5.FileBytes.Length < 1024)
                    //    //{
                    //    //    x = 1;
                    //    //}
                    //}

                    if (fileSelected)
                    {
                        if (x == 1)
                        {
                            //if (checkdate(detail_date.Text) == true)
                            //{
                                string strdate = DateTime.Now.ToString("ddMMyyyy");
                                //string iinsert_date = Convert.ToDateTime(detail_date.Text).ToString("MM/dd/yyyy").Substring(6, 4) + Convert.ToDateTime(detail_date.Text).ToString("MM/dd/yyyy").Substring(0, 2) + Convert.ToDateTime(detail_date.Text).ToString("MM/dd/yyyy").Substring(3, 2);
                                int id = bd.insertDetail(Author.Text, Hospital.Text, DropDownList1.SelectedValue, Email.Text, Mobile.Text, Abstact.Text, Submission.Value, strdate, coAuthor.Value);

                                if (FileUpload_ExcelFile.PostedFile.ContentLength < 1)
                                {
                                    lbl_error_document.Text = "You must browse your document!";
                                }
                                else
                                {
                                    OleDbConnection conn = new OleDbConnection();
                                    OleDbCommand cmd = new OleDbCommand();
                                    OleDbDataAdapter da = new OleDbDataAdapter();
                                    DataSet ds = new DataSet();

                                    string strFileName = DateTime.Now.ToString("ddMMyyyy_HHmmss");
                                    string RealFileName = FileUpload_ExcelFile.FileName;
                                    string strFileType = System.IO.Path.GetExtension(FileUpload_ExcelFile.FileName).ToString().ToLower();


                                    FileUpload_ExcelFile.SaveAs(Server.MapPath("~/proposal/UploadedExcel/" + id + "-1-" + RealFileName));

                                    //save file into UploadedExcel folder
                                    string strNewPath = Server.MapPath("~/proposal/UploadedExcel/" + id + strFileType);


                                    bizdocument bd2 = new bizdocument();
                                    bd2.insertDocument(id + "-1-" + RealFileName, id);
                                }

                                //if (FileUpload_ExcelFile2.PostedFile.ContentLength < 1)
                                //{
                                //    lbl_error_document2.Text = "You must browse your document!";
                                //}
                                //else
                                //{

                                //    OleDbConnection conn = new OleDbConnection();
                                //    OleDbCommand cmd = new OleDbCommand();
                                //    OleDbDataAdapter da = new OleDbDataAdapter();
                                //    DataSet ds = new DataSet();

                                //    string strFileName = DateTime.Now.ToString("ddMMyyyy_HHmmss");
                                //    string RealFileName = FileUpload_ExcelFile2.FileName;
                                //    string strFileType = System.IO.Path.GetExtension(FileUpload_ExcelFile2.FileName).ToString().ToLower();

                                //    FileUpload_ExcelFile2.SaveAs(Server.MapPath("~/UploadedExcel/" + id + "-2-" + RealFileName));

                                //    //save file into UploadedExcel folder
                                //    string strNewPath = Server.MapPath("~/UploadedExcel/" + strFileName + strFileType);

                                //    bizdocument bd2 = new bizdocument();
                                //    bd2.insertDocument(id + "-2-" + RealFileName, id);

                                //}

                                //if (FileUpload_ExcelFile3.PostedFile.ContentLength < 1)
                                //{
                                //    lbl_error_document3.Text = "You must browse your document!";
                                //}
                                //else
                                //{

                                //    OleDbConnection conn = new OleDbConnection();
                                //    OleDbCommand cmd = new OleDbCommand();
                                //    OleDbDataAdapter da = new OleDbDataAdapter();
                                //    DataSet ds = new DataSet();

                                //    string strFileName = DateTime.Now.ToString("ddMMyyyy_HHmmss");
                                //    string RealFileName = FileUpload_ExcelFile3.FileName;
                                //    string strFileType = System.IO.Path.GetExtension(FileUpload_ExcelFile3.FileName).ToString().ToLower();

                                //    FileUpload_ExcelFile3.SaveAs(Server.MapPath("~/UploadedExcel/" + id + "-3-" + RealFileName));

                                //    //save file into UploadedExcel folder
                                //    string strNewPath = Server.MapPath("~/UploadedExcel/" + strFileName + strFileType);

                                //    bizdocument bd2 = new bizdocument();
                                //    bd2.insertDocument(id + "-3-" + RealFileName, id);


                                //}

                                //if (FileUpload_ExcelFile4.PostedFile.ContentLength < 1)
                                //{
                                //    lbl_error_document4.Text = "You must browse your document!";
                                //}
                                //else
                                //{

                                //    OleDbConnection conn = new OleDbConnection();
                                //    OleDbCommand cmd = new OleDbCommand();
                                //    OleDbDataAdapter da = new OleDbDataAdapter();
                                //    DataSet ds = new DataSet();

                                //    string strFileName = DateTime.Now.ToString("ddMMyyyy_HHmmss");
                                //    string RealFileName = FileUpload_ExcelFile4.FileName;
                                //    string strFileType = System.IO.Path.GetExtension(FileUpload_ExcelFile4.FileName).ToString().ToLower();

                                //    FileUpload_ExcelFile4.SaveAs(Server.MapPath("~/UploadedExcel/" + id + "-4-" + RealFileName));

                                //    //save file into UploadedExcel folder
                                //    string strNewPath = Server.MapPath("~/UploadedExcel/" + strFileName + strFileType);

                                //    bizdocument bd2 = new bizdocument();
                                //    bd2.insertDocument(id + "-4-" + RealFileName, id);


                                //}

                                //if (FileUpload_ExcelFile5.PostedFile.ContentLength < 1)
                                //{
                                //    lbl_error_document5.Text = "You must browse your document!";
                                //}
                                //else
                                //{

                                //    OleDbConnection conn = new OleDbConnection();
                                //    OleDbCommand cmd = new OleDbCommand();
                                //    OleDbDataAdapter da = new OleDbDataAdapter();
                                //    DataSet ds = new DataSet();

                                //    string strFileName = DateTime.Now.ToString("ddMMyyyy_HHmmss");
                                //    string RealFileName = FileUpload_ExcelFile5.FileName;
                                //    string strFileType = System.IO.Path.GetExtension(FileUpload_ExcelFile5.FileName).ToString().ToLower();

                                //    FileUpload_ExcelFile5.SaveAs(Server.MapPath("~/UploadedExcel/" + id + "-5-" + RealFileName));

                                //    //save file into UploadedExcel folder
                                //    string strNewPath = Server.MapPath("~/UploadedExcel/" + strFileName + strFileType);

                                //    bizdocument bd2 = new bizdocument();
                                //    bd2.insertDocument(id + "-5-" + RealFileName, id);


                                //}
                                //int id = bd.insertDetail(Author.Text, Hospital.Text, DropDownList1.SelectedValue, Email.Text, Mobile.Text, Abstact.Text, Submission.Value);
                                Response.Write("<script>alert('Detail has been inserted!');</script>");
                                Response.Write("<script>window.location='summary.aspx?id=" + id + "';</script>");
                            //}
                            //else 
                            //{
                                //txt_errordate.Text = "Date format invalid!";
                            //}
                        }
                        else
                        {
                            //Response.Write("<script>window.location='detail_form.aspx';</script>");
                            Response.Write("<script>alert('File size should not be greater than 1 MB.!');</script>");
                        }
                       
                    }
                    else
                    {
                        lbl_file_insert_error.Text = "You must insert at least one file!";
                    }
                }
                else
                {
                    mobile_error.Text = "Please Enter Your Number";
                }
        }
        else
        {
            author_error.Text = "Please Enter Your Name";
        }
    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (FileUpload_ExcelFile.FileBytes.Length > 1024) // 1024*KB of file size
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
    //public static bool checkdate(string idate)
    //{
    //    try
    //    {
    //        Convert.ToDateTime(idate);
    //        return true;
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}
}
