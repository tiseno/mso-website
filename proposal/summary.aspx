﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="summary.aspx.cs" Inherits="summary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MSO</title>
    <style>
    body
    {
        font-family:tahoma,arial;
        font-size:9pt;
         margin:0px 0px 0px 0px;
    }
    .style1
    {
    	padding :0px 0px 0px 10px;
    }
</style>  
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table border='0' width ="100%" cellpadding="2" cellspacing="0"><tr><td align ="center" >
    <tr><td align ="center" style ="padding:10px 0px 20px 0px;">
    <table style="width:50%;"><tr><td align ="center"><a href ="../index.html"> <img src="../images/logo.jpg" style ="border:none;" alt="" /></a> </td></tr></table>
    </td></tr>
    
    <%--<tr><td align ="center" >
    <table border="0" width="100%;" style="background-color:#2a718d; color:White;">
    <tr><td align ="left" >&nbsp;</td></tr></table>
    </td></tr>--%>
    
    <tr><td align ="center" >
    <table border="0" width="100%;" style="background-color:#2a718d; color:White;">
    <tr><td align ="left" style ="color:White ; font-family :tahoma,arial ; font-size :10pt; font-weight :bold ; padding:0px 0px 0px 355px">3rd Annual Scientific Meeting </td></tr></table>
    </td></tr>
    
    <table border='0' align="center" style ="border:2px #eeeeee solid;" width ="40%" cellpadding="2" cellspacing="0">
    
    <tr><td align="left" style="padding:10px 0px 0px 3px; "><asp:ImageButton ID="ImageButton1" 
              ImageUrl="../images/print_images.jpg" Width="40px" Height="20px" BorderStyle="Groove" PostBackUrl="http://mso.org.my/proposal/PrintSummary.aspx" runat="server" 
              onclick="ImageButton1_Click" />
              </td>
    <td style ="font-family:tahoma,arial; font-size:9pt;" align="right" > <a href ="detail_form.aspx" ><span style ="font-family:tahoma,arial; font-size :8pt;">Back To Abstract Submission Page</span> </a> | <a href ="http://mso.org.my/index.html"><span style ="font-family:tahoma,arial; font-size :8pt;">Back To Main Page</span></a></td></tr>
    <tr><td align ="right" style="padding:10px 3px 0px 0px; " colspan ="2">
              
                  </td></tr>
    
    <tr><td align ="left" colspan ="2" ><table border='0' cellpadding="2" cellspacing="0">
    <tr><td style="font-size:10pt; font-family:tahoma,arial; padding:5px 5px 5px 5px" >Thank you for your submission,<br />We will inform you of the outcome by 14th Febuary 2012.<br />Your reference ID is :&nbsp;
    <span style="color:Red; font-size:10pt; font-family:tahoma,arial; font-weight :bold ;">
    <asp:Label ID="reference"  runat="server" Text=""></asp:Label><asp:HiddenField ID="hidereference" runat="server" /></span><br /></td></tr>
    </table></td></tr>
    
    <tr><td colspan ="2"><table border='0' width ="100%" cellpadding="2" cellspacing="0">
    <tr><td style="font-size:11pt;font-weight :bold ;" class ="style1" align ="left" >Detail</td></tr>
    </table>
    </td></tr>
    
    <tr><td align ="center" colspan ="2"><table border='0' width ="100%" cellpadding="2" cellspacing="0">
    <tr><td align ="left" class ="style1" style =" width :150px;">Submitting Author</td><td style =" width :10px;">:</td>
    <td align ="left">
    <asp:Label ID="Author" runat="server" Text=""></asp:Label></td></tr>
    
    <tr><td align ="left" class ="style1" style =" width :150px;">Co-Author Name</td><td>:</td>
    <td align ="left">
    <asp:Label ID="coAuthor" runat="server"  Text=""></asp:Label></td></tr>
    
    <tr><td align ="left" class ="style1">Hospital Name</td><td>:</td><td align ="left"><asp:Label ID="hospital" runat="server" Text=""></asp:Label></td></tr>
    
    <tr><td align ="left" class ="style1">Category</td><td>:</td >
    <td align ="left"><asp:Label ID="category" runat="server" Text=""></asp:Label>
    </td>
    </tr>
    <!-- need a drop down list -->
    
    <tr><td align ="left" class ="style1">Email</td><td>:</td><td align ="left"><asp:Label ID="email" runat="server" Text=""></asp:Label></td></tr>
    <tr><td align ="left" class ="style1">Mobile Phone</td><td>:</td><td align ="left"><asp:Label ID="mobile" runat="server" Text=""></asp:Label></td></tr>
    <tr><td align ="left" class ="style1">Abstract Title</td><td>:</td><td align ="left"><asp:Label ID="title" runat="server" Text=""></asp:Label></td></tr>
    <tr><td align ="left" class ="style1">Remark</td><td>:</td><td align ="left"><asp:Label ID="submission" runat="server" Text=""></asp:Label></td></tr>
   
    <tr><td></td><td></td><td></td></tr>
    </table>
    </td></tr>
    
    <tr><td colspan ="2"><table border='0' width ="100%" cellpadding="2" cellspacing="0">
    <tr><td style="font-size:11pt;font-weight :bold ;" class ="style1" align ="left" >Document</td></tr>
    </table>
    </td></tr>
    <%--<tr><td colspan="3" style="font-size:11pt; font-weight :bold ;" align ="left" class ="style1">Document</td></tr>--%>
    
   
    <tr><td colspan="2" align="left" style ="padding:0px 0px 10px 5px;">
    <asp:Datagrid ID="dg_SearchControl" runat="server" 
    AutoGenerateColumns="False" 
    onselectedindexchanged="dg_SearchControl_SelectedIndexChanged" 
    BackColor="White" 
    BorderColor="#eeeeee" 
     
    BorderWidth="2px" 
    CellPadding="4" 
    ForeColor="Black" 
    
    
    ItemStyle-VerticalAlign="Top" 
    >
        
    <Columns>
    
    <asp:TemplateColumn HeaderText="No"><itemtemplate>
    <%#Container.ItemIndex + dg_SearchControl.CurrentPageIndex * dg_SearchControl.PageSize + 1%>
    </itemtemplate>
    <HeaderStyle Width="17px" HorizontalAlign="Center"/>
    <ItemStyle HorizontalAlign="Center"/>
    </asp:TemplateColumn>
    
    <asp:BoundColumn DataField="document_name" HeaderText="Document Name">
    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
    </asp:BoundColumn>
   
    
    </Columns>
    <HeaderStyle BackColor="#d9d6d6" Font-Bold="True" Font-Size ="Small" ForeColor="#323232" />
    </asp:Datagrid>
    </td></tr>

    
    
    
    
    
    
   
    
    </table>
    
    
    </td></tr></table>
    
    
    </div>
    </form>
</body>
</html>
