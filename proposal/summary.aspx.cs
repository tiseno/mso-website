﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using bizlayer;
using commonlayer;
using System.Data.OleDb;

public partial class summary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != "")
            {

            }


            bizdocument bd = new bizdocument();
            bizdetail bhb = new bizdetail();

            DataSet ds;
            DataSet ds2;

            ds = bhb.SelectDetailByID(Request.QueryString["id"]);
            ds2 = bd.SelectAllDocumentByID(Request.QueryString["id"]);

            dg_SearchControl.DataSource = ds2;
            dg_SearchControl.DataBind();
            reference.Text = Request.QueryString["id"];
            hidereference.Value = Request.QueryString["id"];

            if (ds.Tables[0].Rows.Count > 0)
            {
                string iinsert_date = ds.Tables[0].Rows[0]["detail_date"].ToString().Substring(6, 2) + "/" + ds.Tables[0].Rows[0]["detail_date"].ToString().Substring(4, 2) + "/" + ds.Tables[0].Rows[0]["detail_date"].ToString().Substring(0, 4);

                Author.Text = ds.Tables[0].Rows[0]["author_name"].ToString();
                coAuthor.Text = ds.Tables[0].Rows[0]["co_author"].ToString();
                hospital.Text = ds.Tables[0].Rows[0]["hospital"].ToString();
                category.Text = ds.Tables[0].Rows[0]["category"].ToString();
                email.Text = ds.Tables[0].Rows[0]["email"].ToString();
                mobile.Text = ds.Tables[0].Rows[0]["mobile_phone"].ToString();
               // detail_date.Text = iinsert_date;
                title.Text = ds.Tables[0].Rows[0]["abstract_title"].ToString();
                submission.Text = ds.Tables[0].Rows[0]["abstract_submission"].ToString();

            }

            //int i = 0;
            //foreach (DataGridItem dr in dg_SearchControl.Items)
            //{
            //    HtmlAnchor link_delete = (HtmlAnchor)dr.FindControl("hyperlink_delete");
            //    link_delete.HRef = "javascript:void(window.open('documentDelete.aspx?id=" + Request.QueryString["id"] + "&title=" + ds2.Tables[0].Rows[i]["document_name"].ToString() + "', 'myWindow', 'status = 1, height = 150, width = 400, resizable = 0'))";
            //    i = i + 1;
            //}
        }
    }


    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //Response.Redirect("PrintReceipt.aspx?RefNo=" + RefNo.Text);
    }

    protected void dg_SearchControl_SelectedIndexChanged(object sender, EventArgs e)
    {

    }


}
