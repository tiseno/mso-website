<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletterstop.aspx.cs" Inherits="Newsletter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    Newsletter
        <table style="width: 828px; font-size: 8pt; font-family: Verdana; height: 184px;">
        
            <tr>
                <td style="width: 130px" >
                    Search Member</td>
                <td style="width: 12px" >
                </td>
                <td style="width: 171px">
                </td>
                <td >
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                </td>
                <td style="width: 12px">
                </td>
                <td style="width: 171px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                    Member Name</td>
                <td style="width: 12px">
                    :</td>
                <td style="width: 171px">
                    <asp:TextBox ID="txt_MemberName" runat="server"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                    Member IC</td>
                <td style="width: 12px">
                    :</td>
                <td style="width: 171px">
                    <asp:TextBox ID="txt_MemberIC" runat="server"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                    Membership Type</td>
                <td style="width: 12px">
                    :</td>
                <td style="width: 171px">
                    <asp:DropDownList ID="ddl_MembershipType" runat="server">
                    </asp:DropDownList></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                    Starting Date</td>
                <td style="width: 12px">
                    :</td>
                <td style="width: 171px">
                    <asp:TextBox ID="txt_StartDate1" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_StartDate2" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 130px">
                    Expired Date</td>
                <td style="width: 12px">
                    :</td>
                <td style="width: 171px">
                    <asp:TextBox ID="txt_ExpiredDate1" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_ExpiredDate2" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBox ID="ck_AlmostExpiredMember" runat="server" Text="To search only member who are expired soon." /></td>
            </tr>
            <tr>
                <td style="width: 130px; ">
                </td>
                <td colspan="2" style="text-align:center;">
                    <asp:Button ID="btn_search" runat="server" Text="Search Member" /></td>
                <td>
                </td>
            </tr>
           
        </table>
    
    </div>
        <br />
        <div style="width: 830px; height: 100px">
            <table style="width: 825px">
                <tr>
                    <td style="width: 420px">
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 420px">
                        <asp:Datagrid ID="GridView1" runat="server" Width="415px">
                        </asp:Datagrid>
                    </td>
                    <td>
                    </td>
                </tr>
               
            </table>
        </div>
    </form>
</body>
</html>
