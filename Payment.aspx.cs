using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MSO_BizLayer;

public partial class Payment : System.Web.UI.Page
{
    private BizMember bm = new BizMember();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserCookies"] == null)
        {
            Response.Redirect("LostCookies.aspx");
        }
        else
        {
            string ProfileID;

            ProfileID = Request.Cookies["UserCookies"].Value;
        }

        if (IsPostBack == false)
        {
            ddl_RenewYear.Enabled = false;
            txt_RcvdDate.Enabled = false;
            //ddlPayMet.Enabled = false;
            //txtAmnt.Enabled = false;
            //txtRcptNo.Enabled = false;

            string id = Request.QueryString["id"];
            DataSet mPay = new DataSet();
            int mid = Convert.ToInt16(id);
            mPay = bm.memberPayment2(mid);

            Int16 rcvDate = Convert.ToInt16(mPay.Tables[0].Rows[0]["PaymentYear"].ToString());
            ddl_RenewYear.Items.Add((rcvDate).ToString());
            txt_RcvdDate.Text = mPay.Tables[0].Rows[0]["ReceivedDate"].ToString();
            ddlStatus.SelectedValue = mPay.Tables[0].Rows[0]["Status"].ToString();
            ddlPayMet.SelectedValue = mPay.Tables[0].Rows[0]["PaymentMethod"].ToString();
            txtAmnt.Text = mPay.Tables[0].Rows[0]["AmountReceived"].ToString();
            txtRcptNo.Text = mPay.Tables[0].Rows[0]["ReceiptNo"].ToString();
            taRemarks.Value = mPay.Tables[0].Rows[0]["Remarks"].ToString();
            txtCheqNo.Text = mPay.Tables[0].Rows[0]["ChequeNo"].ToString();
        }

    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string mid = Request.QueryString["mid"];
        string id = Request.QueryString["id"];
        string pstat = ddlStatus.SelectedValue;
        string prmrk = taRemarks.Value;

        bm.updatePayment(id, pstat, prmrk, ddlPayMet.SelectedValue, txtAmnt.Text, txtRcptNo.Text, txtCheqNo.Text, Request.Cookies["UserCookies"].Value);
        Response.Write("<script type='text/javascript'>alert('Record updated!')</script>");
        Response.Write("<script type='text/javascript'>window.location='UpdateMemberProfile.aspx?id=" + mid + "';</script>");
    }
}
