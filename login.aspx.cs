﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using MSO_BizLayer;

public partial class login : System.Web.UI.Page
{
    private BizMember bm = new BizMember();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_login_Click(object sender, EventArgs e)
    {
        DataSet log = new DataSet();
        //string user_type;

        log = bm.login(txt_username.Text, txt_password.Text);
        //user_type = log.Tables[0].Rows[0]["userType"].ToString();
        if (log.Tables[0].Rows.Count > 0)
        {
            if (log.Tables[0].Rows[0]["userType"].ToString() == "Member")
            {
                DataSet getNo;
                getNo = bm.get_No(txt_username.Text);

                string No;
                No = getNo.Tables[0].Rows[0]["Profile_No"].ToString();

                HttpCookie ProfileID = new System.Web.HttpCookie("UserCookies", txt_username.Text);
                ProfileID.Expires.AddHours(8);
                Response.Cookies.Add(ProfileID);

                Response.Redirect("AdminMain.aspx");

            }
            else
            {
                DataSet getNo;
                getNo = bm.get_No(txt_username.Text);

                string No;
                No = getNo.Tables[0].Rows[0]["Profile_No"].ToString();

                HttpCookie ProfileID = new System.Web.HttpCookie("UserCookies", txt_username.Text);
                ProfileID.Expires.AddHours(8);
                Response.Cookies.Add(ProfileID);
                Response.Redirect("AdminMain.aspx");
            }

            lbl_InvalidLogin.Visible = false;
        }
        else
        {
            lbl_InvalidLogin.Visible = true;
        }
          
    }
}
