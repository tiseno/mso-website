<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangeMembershipType.aspx.cs" Inherits="ChangeMembershipType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY - Membership Changing Type</title>
    <style type="text/css">
        .style1
        {
            width: 229px;
        }
        .style2
        {
            height: 21px;
            width: 229px;
        }
    </style>
</head>
<body style="font-size: 12pt">
    <form id="form1" runat="server">
        <div>
            <span style="font-family: Verdana"><strong style="color: #333333">Change Membership Type<br />
            </strong></span>
            <br />
            <table style="width: 681px; font-size: 8pt; font-family: Verdana;" 
                bgcolor="#CCCCCC">
                <tr bgcolor="#666666" style="color: #FFFFFF">
                    <td style="font-size: 12pt;" colspan="3">
                        Current
                        Membership Record</td>
                </tr>
                <tr>
                    <td style="width: 243px">
                    </td>
                    <td class="style1">
                        <asp:Label ID="lbl_id" runat="server" Text="Label" Visible="False"></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Member Name</td>
                    <td class="style1">
                        <asp:Label ID="lbl_MemberName" runat="server"></asp:Label></td>
                    <td>
                        
                        </td>
                </tr>
                <tr>
                    <td style="width: 243px; height: 21px;">
                        Member IC</td>
                    <td class="style2">
                        <asp:Label ID="lbl_MemberIC" runat="server"></asp:Label></td>
                    <td style="height: 21px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Membership Type</td>
                    <td class="style1">
                        <asp:Label ID="lbl_CurrentMemberType" runat="server"></asp:Label></td>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                    </td>
                    <td class="style1">
                        </td>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Starting Date</td>
                    <td class="style1">
                        <asp:Label ID="lbl_CurrentStartDate" runat="server"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Expired Date</td>
                    <td class="style1">
                        <asp:Label ID="lbl_CurrentExpiredDate" runat="server" Text=""></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Current Entry Fee</td>
                    <td class="style1">
                        
                        <asp:Label ID="lbl_CurrentEntryFee" runat="server" Text=""></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Current Normal Fee</td>
                    <td class="style1">
                        <asp:Label ID="lbl_CurrentNormalFee" runat="server" Text=""></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Current Fee Status
                    </td>
                    <td class="style1">
                        <asp:Label ID="lbl_feeStat" runat="server" Text=""></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                    </td>
                    <td class="style1">
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lbl_WarningMessage" runat="server" ForeColor="Red" Text="The renew membership has been added, you are not allowed to renew anymore!"
                            Visible="False"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 29px">
                    </td>
                </tr>
                <tr bgcolor="#666666" style="color: #FFFFFF">
                    <td style="font-size: 12pt;" colspan="3">
                        Change Option</td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Change Membership Type</td>
                    <td class="style1">
                        <asp:DropDownList ID="ddl_MemberShipType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_MemberShipType_SelectedIndexChanged">
                            <asp:ListItem Value="1">Ordinary Member</asp:ListItem>
                            <asp:ListItem Value="3">Associate Member</asp:ListItem>
                            <asp:ListItem Value="2">Life Member</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lbl_CurMembType" runat="server" Width="123px" Visible="False"></asp:Label></td>
                    <td>
                        Fee RM<asp:Label ID="lbl_fee" runat="server"></asp:Label>
                        &nbsp;
                        Entrance
                        <asp:Label ID="lbl_Entryfee" runat="server" Visible="False"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 243px; height: 17px;">
                        Starting Date</td>
                    <td class="style1" style="height: 17px">
                        <asp:Label ID="txt_StartDate" runat="server"></asp:Label>&nbsp;</td>
                    <td style="height: 17px">
                        Expired Date
                        <asp:Label ID="lbl_ExpiredDate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        </td>
                    <td class="style1">
                        </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        Fee Status</td>
                    <td class="style1">
                        <asp:DropDownList ID="ddl_FeeStatus" runat="server">
                            <asp:ListItem Value="2">Unpaid</asp:ListItem>
                            <asp:ListItem Value="3">Paid</asp:ListItem>
                        </asp:DropDownList></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br />
                        <asp:Label ID="lbl_TotalRefund" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 243px">
                    </td>
                    <td class="style1">
                        <asp:Button ID="btn_Renew" runat="server" OnClick="btn_Renew_Click" Text="Submit"
                            Width="111px" /></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        &nbsp;</td>
                    <td class="style1">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 243px">
                        &nbsp;</td>
                    <td class="style1">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            <br />
        </div>
                        <asp:LinkButton ID="Link_Back" runat="server" Font-Names="Verdana" 
                    OnClick="Link_Back_Click">Back to Previous</asp:LinkButton>
    </form>
</body>
</html>
