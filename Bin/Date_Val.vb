Imports System

Namespace MyDate_Val

    Public Class Date_Val

        Public Shared Function ShowDate_Val(ByVal DateStr As String) As String

            If Len(DateStr) = 10 Then
                If InStr(Mid(DateStr, 3, 1), "/") > 0 And InStr(Mid(DateStr, 6, 1), "/") > 0 Then
                    If IsNumeric(Left(DateStr, 2)) And IsNumeric(Mid(DateStr, 4, 2)) And IsNumeric(Right(DateStr, 4)) Then
                        If Mid(DateStr, 4, 2) = "01" Or Mid(DateStr, 4, 2) = "03" Or Mid(DateStr, 4, 2) = "05" Or Mid(DateStr, 4, 2) = "07" Or Mid(DateStr, 4, 2) = "08" Or Mid(DateStr, 4, 2) = "10" Or Mid(DateStr, 4, 2) = "12" Then
                            If Left(DateStr, 2) = "01" Or Left(DateStr, 2) = "02" Or Left(DateStr, 2) = "03" Or Left(DateStr, 2) = "04" Or Left(DateStr, 2) = "05" Or Left(DateStr, 2) = "06" Or Left(DateStr, 2) = "07" Or Left(DateStr, 2) = "08" Or Left(DateStr, 2) = "09" Or Left(DateStr, 2) = "10" Or Left(DateStr, 2) = "11" Or Left(DateStr, 2) = "12" Or Left(DateStr, 2) = "13" Or Left(DateStr, 2) = "14" Or Left(DateStr, 2) = "15" Or Left(DateStr, 2) = "16" Or Left(DateStr, 2) = "17" Or Left(DateStr, 2) = "18" Or Left(DateStr, 2) = "19" Or Left(DateStr, 2) = "20" Or Left(DateStr, 2) = "21" Or Left(DateStr, 2) = "22" Or Left(DateStr, 2) = "23" Or Left(DateStr, 2) = "24" Or Left(DateStr, 2) = "25" Or Left(DateStr, 2) = "26" Or Left(DateStr, 2) = "27" Or Left(DateStr, 2) = "28" Or Left(DateStr, 2) = "29" Or Left(DateStr, 2) = "30" Or Left(DateStr, 2) = "31" Then
                                Return True
                            Else
                                Return False
                            End If
                        ElseIf Mid(DateStr, 4, 2) = "04" Or Mid(DateStr, 4, 2) = "06" Or Mid(DateStr, 4, 2) = "09" Or Mid(DateStr, 4, 2) = "11" Then
                            If Left(DateStr, 2) = "01" Or Left(DateStr, 2) = "02" Or Left(DateStr, 2) = "03" Or Left(DateStr, 2) = "04" Or Left(DateStr, 2) = "05" Or Left(DateStr, 2) = "06" Or Left(DateStr, 2) = "07" Or Left(DateStr, 2) = "08" Or Left(DateStr, 2) = "09" Or Left(DateStr, 2) = "10" Or Left(DateStr, 2) = "11" Or Left(DateStr, 2) = "12" Or Left(DateStr, 2) = "13" Or Left(DateStr, 2) = "14" Or Left(DateStr, 2) = "15" Or Left(DateStr, 2) = "16" Or Left(DateStr, 2) = "17" Or Left(DateStr, 2) = "18" Or Left(DateStr, 2) = "19" Or Left(DateStr, 2) = "20" Or Left(DateStr, 2) = "21" Or Left(DateStr, 2) = "22" Or Left(DateStr, 2) = "23" Or Left(DateStr, 2) = "24" Or Left(DateStr, 2) = "25" Or Left(DateStr, 2) = "26" Or Left(DateStr, 2) = "27" Or Left(DateStr, 2) = "28" Or Left(DateStr, 2) = "29" Or Left(DateStr, 2) = "30" Then
                                Return True
                            Else
                                Return False
                            End If
                        ElseIf Mid(DateStr, 4, 2) = "02" And (Right(DateStr, 4) Mod 4) = 0 Then
                            If Left(DateStr, 2) = "01" Or Left(DateStr, 2) = "02" Or Left(DateStr, 2) = "03" Or Left(DateStr, 2) = "04" Or Left(DateStr, 2) = "05" Or Left(DateStr, 2) = "06" Or Left(DateStr, 2) = "07" Or Left(DateStr, 2) = "08" Or Left(DateStr, 2) = "09" Or Left(DateStr, 2) = "10" Or Left(DateStr, 2) = "11" Or Left(DateStr, 2) = "12" Or Left(DateStr, 2) = "13" Or Left(DateStr, 2) = "14" Or Left(DateStr, 2) = "15" Or Left(DateStr, 2) = "16" Or Left(DateStr, 2) = "17" Or Left(DateStr, 2) = "18" Or Left(DateStr, 2) = "19" Or Left(DateStr, 2) = "20" Or Left(DateStr, 2) = "21" Or Left(DateStr, 2) = "22" Or Left(DateStr, 2) = "23" Or Left(DateStr, 2) = "24" Or Left(DateStr, 2) = "25" Or Left(DateStr, 2) = "26" Or Left(DateStr, 2) = "27" Or Left(DateStr, 2) = "28" Or Left(DateStr, 2) = "29" Then
                                Return True
                            Else
                                Return False
                            End If
                        ElseIf Mid(DateStr, 4, 2) = "02" And (Right(DateStr, 4) Mod 4) <> 0 Then
                            If Left(DateStr, 2) = "01" Or Left(DateStr, 2) = "02" Or Left(DateStr, 2) = "03" Or Left(DateStr, 2) = "04" Or Left(DateStr, 2) = "05" Or Left(DateStr, 2) = "06" Or Left(DateStr, 2) = "07" Or Left(DateStr, 2) = "08" Or Left(DateStr, 2) = "09" Or Left(DateStr, 2) = "10" Or Left(DateStr, 2) = "11" Or Left(DateStr, 2) = "12" Or Left(DateStr, 2) = "13" Or Left(DateStr, 2) = "14" Or Left(DateStr, 2) = "15" Or Left(DateStr, 2) = "16" Or Left(DateStr, 2) = "17" Or Left(DateStr, 2) = "18" Or Left(DateStr, 2) = "19" Or Left(DateStr, 2) = "20" Or Left(DateStr, 2) = "21" Or Left(DateStr, 2) = "22" Or Left(DateStr, 2) = "23" Or Left(DateStr, 2) = "24" Or Left(DateStr, 2) = "25" Or Left(DateStr, 2) = "26" Or Left(DateStr, 2) = "27" Or Left(DateStr, 2) = "28" Then
                                Return True
                            Else
                                Return False
                            End If
                        Else
                            Return False
                        End If
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            Else
                Return False
            End If

        End Function

    End Class

End Namespace
