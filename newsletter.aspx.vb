Imports System.Data
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports MSO_BizLayer
Imports MyDate_Val
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Data.OleDb

Partial Class newsletter
    Inherits System.Web.UI.Page
    Dim bm As BizMember = New BizMember()
    
    Sub Page_Load()
        If Not IsPostBack Then
            If Request.Cookies("UserCookies") Is Nothing Then
                Response.Redirect("LostCookies.aspx")
            Else
                Dim ProfileID As String
                ProfileID = Request.Cookies("UserCookies").Value
            End If

        End If
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Dim Capemail As String = ""
        searchtypelbl.Text = "1"
        Dim Ds_MemberList As DataSet
        Dg_MemberList.CurrentPageIndex = 0
        Ds_MemberList = bm.MemberList(txt_MemberName.Text, txt_IC.Text, ddl_memb_type.SelectedValue, "false", memb_state.SelectedValue, addres.Text, searchtypelbl.Text, sortlbl.Text, MembershipID.Text)
        Dg_MemberList.DataSource = Ds_MemberList
        Dg_MemberList.DataBind()

        For intRow As Integer = 0 To Dg_MemberList.Items.Count - 1
            If CType(Dg_MemberList.Items(intRow).FindControl("emailcap"), Label).Text <> "" Then
                Capemail = Capemail & CType(Dg_MemberList.Items(intRow).FindControl("emailcap"), Label).Text & ", "
            End If
        Next
        If Capemail <> "" Then
            Toemail.Text = Left(Capemail, Len(Capemail) - 2)
        Else
            Toemail.Text = ""
        End If
    End Sub

    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click
        Dim Capemail As String = ""
        searchtypelbl.Text = "0"
        Dim Ds_MemberList As DataSet
        Dg_MemberList.CurrentPageIndex = 0
        Ds_MemberList = bm.MemberList(txt_MemberName.Text, txt_IC.Text, ddl_memb_type.SelectedValue, "true", memb_state.SelectedValue, addres.Text, searchtypelbl.Text, sortlbl.Text, MembershipID.Text)
        Dg_MemberList.DataSource = Ds_MemberList
        Dg_MemberList.DataBind()

        For intRow As Integer = 0 To Dg_MemberList.Items.Count - 1
            If CType(Dg_MemberList.Items(intRow).FindControl("emailcap"), Label).Text <> "" Then
                Capemail = Capemail & CType(Dg_MemberList.Items(intRow).FindControl("emailcap"), Label).Text & ", "
            End If
        Next
        If Capemail <> "" Then
            Toemail.Text = Left(Capemail, Len(Capemail) - 2)
        Else
            Toemail.Text = ""
        End If

    End Sub

    Sub SortCommand(ByVal s As Object, ByVal e As DataGridSortCommandEventArgs)
        Dim Ds_MemberList As DataSet
        sortlbl.Text = e.SortExpression
        Ds_MemberList = bm.MemberList(txt_MemberName.Text, txt_IC.Text, ddl_memb_type.SelectedValue, "false", memb_state.SelectedValue, addres.Text, searchtypelbl.Text, sortlbl.Text, MembershipID.Text)
        Dg_MemberList.DataSource = Ds_MemberList
        Dg_MemberList.DataBind()
    End Sub

    Sub Dg_MemberList_pageIndexChanged(ByVal s As Object, ByVal e As DataGridPageChangedEventArgs)
        Dim Ds_MemberList As DataSet
        Ds_MemberList = bm.MemberList(txt_MemberName.Text, txt_IC.Text, ddl_memb_type.SelectedValue, "false", memb_state.SelectedValue, addres.Text, searchtypelbl.Text, sortlbl.Text, MembershipID.Text)

        Dg_MemberList.CurrentPageIndex = e.NewPageIndex
        Dg_MemberList.DataSource = Ds_MemberList
        Dg_MemberList.DataBind()
    End Sub

    Function getStatus(ByVal stat) As String
        If stat = "1" Then
            Return "Active"
        Else
            Return "Deactivate"
        End If
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim mm As New MailMessage()
            mm.Subject = Subj.Text
            mm.To.Add(Toemail.Text)
            mm.From = New MailAddress("admin@mso.org.my")
            Dim body As String = content.Text

            mm.Body = body
            mm.IsBodyHtml = False

            'Attach the file
            mm.Attachments.Add(New Attachment(AttachmentFile.PostedFile.InputStream, AttachmentFile.FileName))

            Dim smtp As New SmtpClient("mail.mso.org.my", 25)
            smtp.Send(mm)
            Response.Write("<SCR" + "IPT language='javascript'>alert('All Emails has been sent out!');</SCR" + "IPT>")
            'Response.Redirect("index.html")
        Catch ex As Exception
            Response.Write("<SCR" + "IPT language='javascript'>alert('Email Failure! Please check your E-mail! ');</SCR" + "IPT>")
        End Try
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Dim SQL As String = "SELECT * FROM MemberProfile left join Application on MemberProfile.AppType=Application.ApplyID where Profile_No<>0"
        Dim PaymentSQL As String = "SELECT MembershipFee.*, MemberProfile.Memb_Name, Application.Application_Name, * FROM (MembershipFee LEFT JOIN MemberProfile ON MembershipFee.MemberID = MemberProfile.Profile_No) LEFT JOIN Application ON MemberProfile.AppType = Application.ApplyID where Profile_No<>0"


        If txt_IC.Text <> "" Then
            SQL = SQL + " And MemberProfile.NRIC like '%" & Replace(txt_IC.Text, "'", "''") & "%'"
            PaymentSQL = PaymentSQL + " And MemberProfile.NRIC like '%" & Replace(txt_IC.Text, "'", "''") & "%'"
        End If
        If ddl_memb_type.Text <> "" Then
            SQL = SQL + " And Application.ApplyID = " & Replace(ddl_memb_type.Text, "'", "''") & ""
            PaymentSQL = PaymentSQL + " And Application.ApplyID = " & Replace(ddl_memb_type.Text, "'", "''") & ""
        End If
        If memb_state.Text <> "" Then
            SQL = SQL + " And MemberProfile.Office_State like '%" & Replace(memb_state.Text, "'", "''") & "%'"
            PaymentSQL = PaymentSQL + " And MemberProfile.Office_State like '%" & Replace(memb_state.Text, "'", "''") & "%'"
        End If
        If addres.Text <> "" Then
            SQL = SQL + " And MemberProfile.Office_Address like '%" & Replace(addres.Text, "'", "''") & "%'"
            PaymentSQL = PaymentSQL + " And MemberProfile.Office_Address like '%" & Replace(addres.Text, "'", "''") & "%'"
        End If
        If MembershipID.Text <> "" Then
            SQL = SQL + " And MemberProfile.membershipid = " & Replace(MembershipID.Text, "'", "''") & ""
            PaymentSQL = PaymentSQL + " And MemberProfile.membershipid = " & Replace(MembershipID.Text, "'", "''") & ""
        End If
        PaymentSQL = PaymentSQL + " order by memb_name, PaymentYear"
        Dim FileName As String = Request.Cookies("UserCookies").Value & Format(Day(DateTime.Now()), "00") & Format(Month(DateTime.Now()), "00") & Format(Year(DateTime.Now()), "00") & Format(Hour(DateTime.Now()), "00") & Format(Minute(DateTime.Now()), "00") & Format(Second(DateTime.Now()), "00")

        'Response.Write(PaymentSQL)
        'Response.End()
        CreateRO_RptSort(SQL, PaymentSQL, FileName)
        Response.Redirect("~/Reports/MSO_" & FileName & ".xls")
    End Sub

    Sub CreateRO_RptSort(ByVal SQLStr As String, ByVal PaymentSQL As String, ByVal UserN As String)
        Dim strPath As String
        Dim strmFile As StreamWriter
        Dim MCConn As OleDbConnection
        Dim cmdAction, cmdAction2 As OleDbCommand
        'Dim QualificationSQL As String = "Select * from Qualification where id ="
        Dim RS, RS2 As OleDbDataReader

        Dim memberid As String = ""
        Dim PathStr As String = "C:\Domains\quality1\mso.org.my\wwwroot\Reports\MSO_"
        'Dim PathStr As String = "C:\Documents and Settings\Quality2\My Documents\Visual Studio 2008\Projects\MSO\MSO\Reports\MSO_"
        'Dim PathStr As String = "C:\MSO\MSO\Reports\MSO_"
        Dim Counter As Integer = 0
        Dim Counter2 As Integer = 0
        If File.Exists(PathStr & UserN & ".xls") Then
            strPath = PathStr & UserN & ".xls"
            File.Delete(strPath)
        End If

        strPath = PathStr & UserN & ".xls"
        strmFile = File.CreateText(strPath)

        strmFile.Write("<html xmlns:x=""urn:schemas-microsoft-com:office:excel"">")
        strmFile.Write("<head>")
        strmFile.Write("<STYLE> BR {mso-data-placement:same-cell}</STYLE>")
        strmFile.Write("<!--[if gte mso 9]><xml>")
        strmFile.Write("<x:ExcelWorkbook>")
        strmFile.Write("<x:ExcelWorksheets>")
        strmFile.Write("<x:ExcelWorksheet>")
        strmFile.Write("<x:Name>report</x:Name>")
        strmFile.Write("<x:WorksheetOptions>")
        strmFile.Write("<x:Print>")
        strmFile.Write("<x:ValidPrinterInfo/>")
        strmFile.Write("</x:Print>")
        strmFile.Write("</x:WorksheetOptions>")
        strmFile.Write("</x:ExcelWorksheet>")
        strmFile.Write("</x:ExcelWorksheets>")
        strmFile.Write("</x:ExcelWorkbook>")
        strmFile.Write("</xml>")
        strmFile.Write("<![endif]--> ")
        strmFile.Write("</head>")
        strmFile.Write("<body>")
        strmFile.Write("<table>")
        strmFile.Write("<tr>")
        strmFile.Write("<td>")
        strmFile.Write("<b>No.</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>ID</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Application</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Name</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Status</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>IC</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Email</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>mobile</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Mailing Address</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Office Address</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Office City</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>State</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>PostCode</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>House Address</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>House City</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>State</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Potcode</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>MMC No.</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Title</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Gender</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>D.O.B.</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Nationality</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Office Tel.</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Home Tel.</b>")
        strmFile.Write("</td>")       
        strmFile.Write("<td>")
        strmFile.Write("<b>MMAOS</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Joint Date</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write(("<b>veritext</b>"))
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Joined Date</b>")
        strmFile.Write("</td>")
        
        strmFile.Write("<td>")
        strmFile.Write("<b>verified Name</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>verfied Institution</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Support Full Name 1</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Support Membership No. 1</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Support Full Name 2</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Support Membership No. 2</b>")
        strmFile.Write("</td>")

        strmFile.Write("<td>")
        strmFile.Write("<b>Degree</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Type</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>University</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Country</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Date_Join</b>")
        strmFile.Write("</td>")

        strmFile.Write("<td>")
        strmFile.Write("<b>Degree</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Type</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>University</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Country</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Date_Join</b>")
        strmFile.Write("</td>")

        strmFile.Write("<td>")
        strmFile.Write("<b>Degree</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Type</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>University</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Country</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Date_Join</b>")
        strmFile.Write("</td>")

        strmFile.Write("<td>")
        strmFile.Write("<b>Degree</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Type</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>University</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Country</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Date_Join</b>")
        strmFile.Write("</td>")

        strmFile.Write("<td>")
        strmFile.Write("<b>Degree</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Type</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>University</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Country</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Date_Join</b>")
        strmFile.Write("</td>")

        'generate 4 qf

        strmFile.Write("</tr>")

        MCConn = New OleDbConnection(ConfigurationManager.ConnectionStrings("MSOConnectionString").ConnectionString)
        MCConn.Open()
        cmdAction = New OleDbCommand(SQLStr, MCConn)
        RS = cmdAction.ExecuteReader()
        While RS.Read()

            memberid = RS("membershipid")

            Counter = Counter + 1
            strmFile.Write("<tr>")
            strmFile.Write("<td>")
            strmFile.Write(Counter)
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("membershipid"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Application_Name"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Memb_Name"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            If RS("isactive") = "0" Then
                strmFile.Write("Active")
            ElseIf RS("isactive") = "1" Then
                strmFile.Write("Resigned")
            ElseIf RS("isactive") = "2" Then
                strmFile.Write("Ceased")
            Else
                strmFile.Write("")
            End If

            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write("'" & RS("NRIC"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("email1"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write("'" & RS("M_Phone"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Mailing_Address"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Office_Address"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Office_City"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Office_State"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Office_Code"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Home_Address"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Home_City"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Home_State"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Home_Code"))
            strmFile.Write("</td>")

            'added by zack 17-07-2010
            strmFile.Write("<td>")
            strmFile.Write(RS("MMC_No"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Memb_Title"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Gend"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("DOB"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Nationality"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("O_Phone"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("H_Phone"))
            strmFile.Write("</td>")            
            strmFile.Write("<td>")                       
            strmFile.Write(RS("MMAOS"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("joindate"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("veritext"))
            strmFile.Write("<td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("verifullname"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("veriins"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("supfullname1"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("supmemno1"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("supfullname2"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("supmemno2"))
            strmFile.Write("</td>")

            '/---------------------------------------------added 10/may/2011 seng-----------------/
            'here is to display qualificaiton
            'function(rs(memberid))
            'dim sql_qly= 0;


            Counter2 = 0
            If memberid = "" Then

            Else
                cmdAction2 = New OleDbCommand("Select * from Qualification Where id = '" & memberid & "'", MCConn)
                RS2 = cmdAction2.ExecuteReader()
                While RS2.Read()

                    Counter2 = Counter2 + 1

                    strmFile.Write("<td>")
                    strmFile.Write(RS2("Degree"))
                    strmFile.Write("</td>")
                    strmFile.Write("<td>")
                    strmFile.Write(RS2("Type"))
                    strmFile.Write("</td>")
                    strmFile.Write("<td>")
                    strmFile.Write(RS2("University"))
                    strmFile.Write("</td>")
                    strmFile.Write("<td>")
                    strmFile.Write(RS2("Country"))
                    strmFile.Write("</td>")
                    strmFile.Write("<td>")
                    strmFile.Write(RS2("Date_Join"))
                    strmFile.Write("</td>")

                End While
                RS2.Close()
                RS2 = Nothing
            End If
            strmFile.Write("</tr>")
        End While





        RS.Close()
        RS = Nothing

        Counter = 0
        strmFile.Write("</tr>")
        strmFile.Write("<td>")
        strmFile.Write("")
        strmFile.Write("</td>")
        strmFile.Write("</tr>")
        strmFile.Write("<tr>")
        strmFile.Write("<td>")
        strmFile.Write("")
        strmFile.Write("</td>")
        strmFile.Write("</tr>")
        strmFile.Write("<tr>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Payment Details</b>")
        strmFile.Write("</td>")
        strmFile.Write("</tr>")
        strmFile.Write("<tr>")
        strmFile.Write("<td>")
        strmFile.Write("<b>No</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Member Name</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>PaymentYear</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>ReceivedDate</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Status</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>PaymentMethod</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>AmountReceived</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>ReceiptNo</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>ChequeNo</b>")
        strmFile.Write("</td>")
        strmFile.Write("<td>")
        strmFile.Write("<b>Member Type</b>")
        strmFile.Write("</td>")
        strmFile.Write("</tr>")

        cmdAction = New OleDbCommand(PaymentSQL, MCConn)
        RS = cmdAction.ExecuteReader()
        While RS.Read()
            Counter = Counter + 1
            strmFile.Write("<tr>")
            strmFile.Write("<td>")
            strmFile.Write(Counter)
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Memb_Name"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("PaymentYear"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("ReceivedDate"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("Status"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("PaymentMethod"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("AmountReceived"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("ReceiptNo"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("chequeNo"))
            strmFile.Write("</td>")
            strmFile.Write("<td>")
            strmFile.Write(RS("membertype"))
            strmFile.Write("</td>")



            strmFile.Write("</tr>")

        End While

        RS.Close()
        RS = Nothing


        MCConn.Close()
        MCConn = Nothing

        strmFile.Write("</table>")
        strmFile.Write("</body>")
        strmFile.Write("</html>")

        strmFile.Close()
        strmFile = Nothing
    End Sub
End Class
