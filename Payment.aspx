<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Payment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY - Update Payment</title>
</head>
<body bgcolor="whitesmoke">
    <form id="form1" runat="server">
    <div>
        <table style="width: 400px; border-right: black 1px solid; border-top: black 1px solid; padding-left: 5px; border-left: black 1px solid; border-bottom: black 1px solid; background-color: white;" border="0" cellpadding="0" cellspacing="0">
           <tr>
                <td style="width: 117px; height: 16px;">
                   <A HREF="javascript:history.back()"><span style="font-size: 8pt; font-family: Verdana">
                       Back to previous</span></A> </td>
                <td style="width: 10px; height: 16px;">
                    </td>
                <td style="width: 230px; height: 16px;">
                </td>
            </tr>
            <tr>
                <td style="width: 117px; height: 16px;">
                    <span style="font-size: 8pt; font-family: Verdana">
                    Payment Year</span></td>
                <td style="width: 10px; height: 16px;">
                    :</td>
                <td style="width: 230px; height: 16px;">
                <asp:DropDownList ID="ddl_RenewYear" runat="server" Width="59px"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 117px; height: 11px;">
                    <span style="font-size: 8pt; font-family: Verdana">
                    Received Date</span></td>
                <td style="width: 10px; height: 11px;">
                    :</td>
                <td style="width: 230px; height: 11px;">
                <asp:TextBox ID="txt_RcvdDate" runat="server" Enabled="false"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 117px; height: 10px">
                    <span style="font-size: 8pt; font-family: Verdana">
                    Status</span></td>
                <td style="width: 10px; height: 10px">
                    :</td>
                <td style="width: 230px; height: 10px">
                <asp:DropDownList ID="ddlStatus" runat="server" Width="130px">
                <asp:ListItem Value="2">Unpaid</asp:ListItem>
                <asp:ListItem Value="3">Paid</asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 117px; height: 8px">
                    <span style="font-size: 8pt; font-family: Verdana">
                    Payment Method</span></td>
                <td style="width: 10px; height: 8px">
                    :</td>
                <td style="width: 230px; height: 8px">
                <asp:DropDownList ID="ddlPayMet" runat="server" Width="130px">
                <asp:ListItem Value="Cash">Cash</asp:ListItem>
                <asp:ListItem Value="Cheque">Cheque</asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 117px; height: 10px">
                    <span style="font-size: 8pt; font-family: Verdana">
                    Amount Received</span></td>
                <td style="width: 10px; height: 10px">
                    :</td>
                <td style="width: 230px; height: 10px">
                    <asp:TextBox ID="txtAmnt" runat="server" Width="76px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 117px; height: 3px">
                    <span style="font-size: 8pt; font-family: Verdana">
                    Cheque No.</span></td>
                <td style="width: 10px; height: 3px">
                    :</td>
                <td style="width: 230px; height: 3px">
                    <asp:TextBox ID="txtCheqNo" runat="server"></asp:TextBox></td>
            </tr>            
            <tr>
                <td style="width: 117px; height: 3px">
                    <span style="font-size: 8pt; font-family: Verdana">
                    Receipt No.</span></td>
                <td style="width: 10px; height: 3px">
                    :</td>
                <td style="width: 230px; height: 3px">
                    <asp:TextBox ID="txtRcptNo" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 117px; height: 26px" valign="top">
                    <span style="font-size: 8pt; font-family: Verdana">
                    Remarks</span></td>
                <td style="width: 10px; height: 26px" valign="top">
                    :</td>
                <td style="width: 230px; height: 26px">
                    <textarea id="taRemarks" runat="server" style="width: 221px; height: 71px"></textarea></td>
            </tr>
            <tr>
                <td style="width: 117px; height: 35px;">
                </td>
                <td style="width: 10px; height: 35px;">
                </td>
                <td style="width: 230px; height: 35px;">
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" /><br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="width: 117px">
                    </td>
                <td style="width: 10px">
                </td>
                <td style="width: 230px">
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
