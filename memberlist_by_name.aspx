<%@ Page Language="C#" AutoEventWireup="true" CodeFile="memberlist_by_name.aspx.cs" Inherits="memberlist_by_name" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>MALAYSIAN SOCIETY OF OPHTHAlMOLOGY</title>
<link a href="style.css" rel="stylesheet" type="text/css" />
<link a href="css/navmenu.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!-- DO NOT MOVE! The following AllWebMenus linking code section must always be placed right AFTER the BODY tag-->
<!-- ******** BEGIN ALLWEBMENUS CODE FOR menu ******** -->
<!--<script type="text/javascript">var MenuLinkedBy="AllWebMenus [4]",awmMenuName="menu",awmBN="754";awmAltUrl="";</script><script charset="UTF-8" src="menu.js" type="text/javascript"></script><script type="text/javascript">awmBuildMenu();</script>-->
<!-- ******** END ALLWEBMENUS CODE FOR menu ******** -->
<div><form runat="server" id="form1">
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="71%" style="padding:10px 10px 10px 10px;"><img src="images/logo.jpg" width="500" height="60" /></td>
    <td width="29%"></td>
  </tr>
</table>
<table background="images/menu_bg.jpg" style="background-repeat:repeat-x; background-position:bottom" width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="25" valign="top">
           <div class="mainmenu">
                <ul>
                	<li><a href="index.html">Home</a></li>
                    <li><a href="mission_and_history.html">About Us</a>
                    	<ul>
                            <li><a href="mission_and_history.html">About MSO</a></li>
                            <li><a href="committee.html">Committee</a></li>
                            <li><a href="contact_us.html">Contact us</a></li>
                            <li><a href="constitution.html">Constitution</a></li>
                            <li><a href="membership_benefits.html">Membership</a></li>
                        </ul>
                    </li>
                    <li><a href="memberlist_by_name.aspx">Find an Eye Doctor</a>
                    	<ul>
                            <li><a href="#">By Alphabetical</a></li>
                            <li><a href="memberlist_by_state.aspx">By State</a></li>
                        </ul>
                    </li>
                    <li><a href="Current_Event_Page.html">Events</a></li>
                    <li><a href="KSO_fund.html">Funds / Grants</a>
                    	<ul>
                            <li><a href="trainee.html">MSO Grants/Subsidies</a></li>
                            <li><a href="KSO_fund.html">KSO Fund</a></li>
                            <li><a href="MMA_foundation_eye_fund.html">MMA Foundation Eye Fund</a></li>
                        </ul>
                    </li>
                    <li><a href="CPGs.html">Reference For Doctors</a>
                    	<ul>
                            <li><a href="CPGs.html">CPGs/HTAs</a></li>
                            <li><a href="masters_in_mos.html">Master in Ophthalmology training</a></li>
                            <li><a href="Driving_License.html">Driving License</a></li>
                            <li><a href="O_K_U.html">Orang Kurang Upaya (OKU)</a></li>
                            <li><a href="guidelines_as.html">29th MSJOC 2013 - Guidelines for Abstract Submission</a></li>
                        </ul>
                    </li>
                    <li><a href="photo_gallery.html">Photo Gallery</a></li>
                </ul>
            </div>  
        </td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E0EBF1" >
  <tr><td>
<table width="960"  style="background-image:url(images/body_bg.gif); background-repeat:repeat-x"border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><img src="images/alphabetical.jpg" /></td>
  </tr>
  <tr >
    <td height="368" valign="top" bgcolor="#E0EBF1" style="padding-left:60px; padding-top:20px;">
	<h1 class="sub_pg_body_title"><img src="images/bullet.jpg" />&nbsp;&nbsp;Find Eye Doctor By Alphabetical Order</h1>
      
     
      <table class="sub_pg_body"><tr>
      <td><a href="memberlist_by_name.aspx#A">A</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#B">B</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#C">C</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#D">D</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#E">E</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#F">F</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#G">G</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#H">H</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#I">I</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#J">J</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#K">K</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#L">L</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#M">M</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#N">N</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#O">O</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#P">P</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#Q">Q</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#R">R</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#S">S</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#T">T</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#U">U</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#V">V</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#W">W</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#X">X</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#Y">Y</a></td><td>|</td>
      <td><a href="memberlist_by_name.aspx#Z">Z</a></td>
      
 </tr></table>
      <table class="sub_pg_body"><tr><td id="td_member_by_state" runat="server"></td></tr></table></td>
    </tr>
  </table></td></tr></table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="d7dee1">
  <tr><td>
  <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
    	<td class="copy_right" ><a href="index.html">Home</a> | <a href="mission_and_history.html">About Us</a> | <a href="memberlist_by_name.aspx">Find An Eye Doctor</a> | <a href="scientific_meetings.html">Events</a> | <a href="KSO_fund.html">Funds</a> | <a href="CPGs.html">Guidelines</a> | <a href="photo_gallery.html">Photo Gallery</a></td>
    	<td height="30px"colspan="3" align="right" bgcolor="d7dee1" class="copy_right" style="padding-right:10px">MALAYSIAN SOCIETY OF OPHTHALMOLOGY 2011, Web Designed by <a href="http://www.quality-direct.net" target="_blank" title="Web Design Company">Quality Direct</a>
        </td>
	</tr>
   </table>
  </td>
 </tr>
</table>
</form></div>
</body>
</html>
