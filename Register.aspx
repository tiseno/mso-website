﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MALAYSIA SOCIETY OF OPHTHALMOLOGY - Registration</title>
    <script type="text/javascript" src="ezcalendar.js"></script>
	<link rel="stylesheet" type="text/css" href="ezcalendar.css" />
	<%--added by kavi 03/04/2010--%>
	<script type ="text/javascript" >
    function CheckIsNumeric()
    {
    var AsciiCode = event.keyCode;
    if ((AsciiCode < 48) || (AsciiCode > 57))
    {
    //alert('Please enter only numbers.');
    event.cancelBubble = true;
    event.returnValue = false;
    }
    }
    </script>
    <%--end added by kavi 03/04/2010--%>
    <style type="text/css">

    </style>   
</head>
<body bgcolor="gray">
    <form id="form1" runat="server">
    <div>
        <div style="width: 869px; height: 100px">
            <div >
            <div style="width: 954px; height: 513px">
                <table style="border: 1px solid #000000; width: 874px; font-family: Verdana; font-size: 8pt;" 
                    border="0" cellpadding="3" cellspacing="3" bgcolor="whitesmoke">
                    <tr>                   
                        <td colspan="4" valign="top">
                    <span style="font-family: Verdana; font-size:13pt;">&nbsp;<span style="color: #323232"><strong>Malaysia Society of Ophthalmology</strong></span></span><br />
                                &nbsp;<span style="font-size: 8pt; color: gray">40a, Jalan SS21/58, Damansara Utama, 47400, Petaling Jaya, 
                                <br />
                                &nbsp;Selangor D. E, Malaysia
                                &nbsp;Tel: 012-2403318, Fax/Phone: 03-77107282, email:: msophth@gmail.com</span></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span style="font-family: Verdana; font-size:11pt;">&nbsp;<br />
                                &nbsp;Membership categories<span
                                style="font-size: 10pt">
                            | 
                        &nbsp;</span><a href="AdminMain.aspx"><span style="font-family: Verdana; font-size:8pt;">Back to Previous</a></span><br /> <br />
                            &nbsp;<span style="text-decoration: underline; font-size: 9pt; font-family: Arial;">Ordinary Member<br />
                            </span><span style="font-size: 9pt"><span style="font-family: Arial">
                            <span>&nbsp;For Ophthalmologists practising in Malaysia who possess a recognized postgraduate
                                qualification in Ophthalmology.
                                <br />
                                &nbsp;An Entrance Fee + Annual Subscription Fee apply.<br />
                            </span>
                            <br />
                            
                            &nbsp;<span style="text-decoration: underline">Life Member</span><br />
                            </span></span><span style="font-size: 9pt"><span style="font-family: Arial">
                            <span>&nbsp;For Ophthalmologists practising in Malaysia who possess a recognized postgraduate
                                qualification in Ophthalmology.
                                <br />
                                &nbsp;An Entrance Fee + a single lump sum Membership Fee apply.<br />
                            </span>
                            <br />
                            
                            &nbsp;</span></span><span style="text-decoration: underline; font-size: 9pt; font-family: Arial;">Associate Member<br />
                            </span>
                            <span style="font-size: 9pt; font-family: Arial">&nbsp;For Medical Doctors without a registrable postgraduate qualification in Ophthalmology, but who are either enrolled in a formal training programme in Ophthalmology, or who are practicing Ophthalmology. An Entrance Fee + Annual Subscription Fee apply.<br />
                                &nbsp;Associate Members shall enjoy full membership benefits except for the right to vote and hold office.<br />
                            </span>
                            <br />

                            
                        </td>
                    </tr>
                </table>
                <table style="border: 1px solid #000000; width: 874px; padding-left: 5px;" border="0" cellpadding="0" cellspacing="0" frame="border" bgcolor="whitesmoke" id="TABLE1">
                    <tr>
                        <td colspan="5" 
                            
                            
                            style="height: 21px; font-size: 12pt; font-weight: bold; text-align: center; color: #333333;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><span style="font-family: Verdana; font-size:11pt;">Personal Particulars</span></td>
                    </tr>
                    <tr>
                        <td style="width: 241px; height: 21px">
                        </td>
                        <td colspan="2" style="width: 241px; height: 21px">
                        </td>
                        <td style="font-size: 8pt; width: 182px">
                        </td>
                        <td c="" style="width: 250px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 22px">
                            <span style="font-size: 8pt; font-family: Verdana">
                            Membership No :</span>
                            <asp:TextBox ID="memberno" runat="server" Font-Size="8pt"></asp:TextBox>
                            
                            <span style="font-family: Verdana">
                                <span style="font-size: 8pt">
                            Isactive :</span> </span>
                            <asp:DropDownList ID="isactive" runat="server" Font-Names="verdana" Font-Size="8pt">
                                <asp:ListItem Value="0">Active</asp:ListItem>
                                <asp:ListItem Value="1">Resigned</asp:ListItem>
                                <asp:ListItem Value="2">Ceased</asp:ListItem>
                            </asp:DropDownList><span style="font-size: 8pt; font-family: Verdana">
                            Join Date :</span>
                            <asp:TextBox ID="joindt" runat="server" Width="109px" Font-Size="8pt"></asp:TextBox>
                            <a href="javascript: showCalendar('joindt')" ><img alt="" src="images/ew_calendar.gif" border="0" /></a>
                            <%--added by kavi 03/04/2010--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="joindt"
                                Display="dynamic" ErrorMessage="*Invalid Date Format." Font-Names="Verdana" Font-Size="8pt" SetFocusOnError="true"
                                ValidationExpression="([1-9]|0[1-9]|[12][0-9]|3[01])[- /.]([1-9]|0[1-9]|1[012])[- /.][0-9]{4}$">*Invalid Date Format.
                            </asp:RegularExpressionValidator>
                            <%--end added by kavi 03/04/2010--%>
                            </td>
                    </tr>                    
                    <tr>
                        <td>
                            <span style="font-family: Verdana"><span style="font-size: 8pt">Application for :</span></span></td>
                        <td colspan="4">
                        <asp:RadioButton ID="Radio_Application1" runat="server" GroupName="Application" Text="Ordinary Member" Checked="True" Font-Names="verdana" Font-Size="8pt" />&nbsp;
                            <asp:RadioButton ID="Radio_Application2" runat="server" GroupName="Application" Text="Life Member" Font-Names="verdana" Font-Size="8pt" />&nbsp;
                            <asp:RadioButton ID="Radio_Application3" runat="server" GroupName="Application" Text="Associate Member" Font-Names="verdana" Font-Size="8pt" />
                        
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 241px; height: 21px">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">Member of MMAOS :</span></span></td>
                        <td colspan="4" style="width: 241px; height: 21px">
                            <asp:RadioButton ID="RadioButton1" runat="server" GroupName="MMAOS" Text="No" Checked="True" Font-Names="verdana" Font-Size="8pt" />
                            <asp:RadioButton ID="RadioButton2" runat="server" GroupName="MMAOS" Text="Yes (active)" Font-Names="verdana" Font-Size="8pt" />
                            <asp:RadioButton ID="RadioButton3" runat="server" GroupName="MMAOS" Text="Yes (lapsed)" Font-Names="verdana" Font-Size="8pt" /></td>
                    </tr>
                    <tr>
                        <td style="width: 241px; height: 21px; ">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            User Type Selection</span></span></td>
                        <td colspan="2" style="height: 21px; width: 241px;">
                            <asp:RadioButton ID="Radio_userType1" runat="server" Text="Administrator" Checked="True" GroupName="userType" Font-Names="Verdana" Font-Size="8pt" />
                            <asp:RadioButton ID="Radio_userType2" runat="server" Text="Member" GroupName="userType" Font-Names="Verdana" Font-Size="8pt" /></td>
                        <td style="width: 182px" >
                        </td>
                        <td style="width: 250px" >
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 241px; height: 21px">
                        </td>
                        <td colspan="2" style="width: 241px; height: 21px">
                        </td>
                        <td style="width: 182px">
                        </td>
                        <td style="width: 250px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px;">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            Username<span style="color: red">*</span></span></span></td>
                        <td colspan="2" style="height: 21px">
                            <asp:TextBox ID="txt_username" runat="server" Width="149px" MaxLength="30" Font-Size="8pt"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Invalid Username" ControlToValidate="txt_username" Font-Size="8pt" Font-Names="Verdana"></asp:RequiredFieldValidator></td>
                        <td style="width: 182px" >
                        </td>
                        <td style="width: 250px" >
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px;">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            Password<span style="color: red">*</span></span></span></td>
                        <td colspan="2" style="height: 21px">
                            <asp:TextBox ID="txt_password" runat="server" TextMode="Password" Width="149px" MaxLength="30" Font-Size="8pt"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Invalid Password" ControlToValidate="txt_password" Font-Size="8pt" Font-Names="Verdana"></asp:RequiredFieldValidator></td>
                        <td style="width: 182px">
                        </td>
                        <td style="width: 250px" >
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            Confirm Password<span style="color: red">*</span></span></span></td>
                        <td colspan="2" style="height: 21px">
                            <asp:TextBox ID="txt_ConfirmPass" runat="server" TextMode="Password" Width="148px" MaxLength="30" Font-Size="8pt"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txt_ConfirmPass"
                                ErrorMessage="Invalid Password" Font-Size="8pt" Font-Names="Verdana"></asp:RequiredFieldValidator></td>
                        <td style="width: 182px" >
                        </td>
                        <td style="width: 250px" >
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px">
                        </td>
                        <td colspan="2" style="height: 21px">
                        </td>
                        <td style="width: 182px">
                        </td>
                        <td style="width: 250px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px;">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            
                            Name <span style="color: red">*</span></span></span></td>
                        <td colspan="2" style="height: 21px">
                            <asp:TextBox ID="txt_Name" runat="server" Width="211px" MaxLength="80" Font-Size="8pt"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Invalid Name" ControlToValidate="txt_Name" Font-Size="8pt" Font-Names="Verdana"></asp:RequiredFieldValidator></td>
                        <td style="width: 182px; height: 21px;" >
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Gender</span></td>
                        <td style="width: 250px; height: 21px;">
                            <asp:RadioButton ID="Radio_Male" runat="server" GroupName="gend" Text="Male" Checked="True" Width="61px" Font-Names="Verdana" Font-Size="8pt" /><asp:RadioButton ID="Radio_Female" runat="server" GroupName="gend" Text="Female" Font-Names="Verdana" Font-Size="8pt" /></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 18px;">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            
                            Title <span>
                            (eg. Dato', Prof)</span></span></span></td>
                        <td style="width: 169px; height: 18px;">
                            <asp:DropDownList ID="ddl_Title" runat="server" Width="102px" OnSelectedIndexChanged="ddl_Title_SelectedIndexChanged" Font-Size="8pt">
                                <asp:ListItem>Select Title</asp:ListItem>
                                <%--added by kavi 17/05/10--%>
                                <asp:ListItem>Datuk Dr.</asp:ListItem>
                                <asp:ListItem>Dato Dr.</asp:ListItem>
                                <asp:ListItem>Datin Dr.</asp:ListItem>
                                <asp:ListItem>Associate Prof. Dr.</asp:ListItem>
                                <%--end added by kavi 17/05/10--%>
                                <asp:ListItem>Dato</asp:ListItem>
                                <asp:ListItem>Prof</asp:ListItem>
                                <asp:ListItem>Dr</asp:ListItem>
                                <asp:ListItem>Others</asp:ListItem>
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txt_TitleOthers" runat="server" Width="43px" Visible="False" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 18px;">
                        </td>
                        <td style="width: 182px; height: 18px;" >
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Date of Birth </span></td>
                        <td style="width: 250px; height: 18px;" >
                            <asp:DropDownList ID="ddl_DOB_Day" runat="server" Width="47px" Font-Size="8pt">
                            </asp:DropDownList><asp:DropDownList ID="ddl_DOB_Month" runat="server" Width="46px" Font-Size="8pt">
                            </asp:DropDownList><asp:DropDownList ID="ddl_DOB_Year" runat="server" Width="59px" Font-Size="8pt">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px;">
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Nationality</span></td>
                        <td style="width: 169px; height: 21px;">
                            <asp:TextBox ID="txt_Nationality" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 21px;">
                        </td>
                        <td style="width: 182px; height: 21px;" >
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            
                            NRIC Number (New)<span
                                style="color: red">*</span></span></span></td>
                        <td style="width: 250px; height: 21px;" >
                            <asp:TextBox ID="txt_IC" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px">
                        </td>
                        <td style="width: 169px; height: 21px">
                        </td>
                        <td style="width: 141px;">
                        </td>
                        <td style="width: 182px" >
                        </td>
                        <td style="width: 250px" >
                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Invalid IC" ControlToValidate="txt_IC" Font-Size="8pt" Font-Names="Verdana"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Mailing Address</span></td>
                        <td colspan="2">
                            <asp:RadioButton ID="Radio_MailingAddress1" runat="server" GroupName="mailing" Text="Office Address" Checked="True" Font-Names="Verdana" Font-Size="8pt" />
                            <asp:RadioButton ID="Radio_MailingAddress2" runat="server" GroupName="mailing" Text="Home Address" Font-Names="Verdana" Font-Size="8pt" /></td>
                        <td style="width: 182px" >
                        </td>
                        <td style="width: 250px" >
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 19px;">
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Home Address</span></td>
                        <td style="height: 19px;" colspan="4">
                            <asp:TextBox ID="txt_HomeAddress" runat="server" Width="670px" MaxLength="200" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            <span style="font-size: 8pt; font-family: Verdana">City/Town</span></td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_HomeCity" runat="server" MaxLength="40" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px;">
                            </td>
                        <td style="width: 182px"  >
                            <span style="font-size: 8pt; font-family: Verdana">Postcode</span></td>
                        <td style="width: 250px">
                            <asp:TextBox ID="txt_HomeCode" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            <span style="font-size: 8pt; font-family: Verdana">State</span></td>
                        <td style="width: 169px">
                            <asp:DropDownList ID="txt_HomeState" runat="server" Font-Size="8pt">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="JOHOR" Value="JOHOR"></asp:ListItem>
                                <asp:ListItem Text="KEDAH" Value="KEDAH"></asp:ListItem>
                                <asp:ListItem Text="KELANTAN" Value="KELANTAN"></asp:ListItem>
                                <asp:ListItem Text="KUALA LUMPUR" Value="KUALA LUMPUR"></asp:ListItem>
                                <asp:ListItem Text="LABUAN" Value="LABUAN"></asp:ListItem>
                                <asp:ListItem Text="MELAKA" Value="MELAKA"></asp:ListItem>
                                <asp:ListItem Text="NEGERI SEMBILAN" Value="NEGERI SEMBILAN"></asp:ListItem>
                                <asp:ListItem Text="PAHANG" Value="PAHANG"></asp:ListItem>
                                <asp:ListItem Text="PERAK" Value="PERAK"></asp:ListItem>
                                <asp:ListItem Text="PERLIS" Value="PERLIS"></asp:ListItem>
                                <asp:ListItem Text="PULAU PINANG" Value="PULAU PINANG"></asp:ListItem>
                                <asp:ListItem Text="SABAH" Value="SABAH"></asp:ListItem>
                                <asp:ListItem Text="SARAWAK" Value="SARAWAK"></asp:ListItem>
                                <asp:ListItem Text="SELANGOR" Value="SELANGOR"></asp:ListItem>
                                <asp:ListItem Text="SINGAPORE" Value="SINGAPORE"></asp:ListItem>
                                <asp:ListItem Text="TERENGGANU" Value="TERENGGANU"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 141px; ">
                            </td>
                        <td style="width: 182px" >
                            <span style="font-size: 8pt; font-family: Verdana">Country</span></td>
                        <td style="width: 250px" >
                            <asp:DropDownList ID="txt_HomeCountry" runat="server" Font-Size="8pt">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="Malaysia" Value="Malaysia"></asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px">
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Office Address</span></td>
                        <td style="height: 21px" colspan="4">
                            <asp:TextBox ID="txt_OfficeAddress" runat="server" Width="670px" MaxLength="180" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 21px">
                            <span style="font-size: 8pt; font-family: Verdana">City/Town</span></td>
                        <td style="width: 169px; height: 21px">
                            <asp:TextBox ID="txt_OfficeCity" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; font-size: 8pt; font-family: Verdana;">
                            </td>
                        <td style="width: 182px" >
                            <span style="font-size: 8pt; font-family: Verdana">Postcode</span></td>
                        <td style="width: 250px" >
                            <asp:TextBox ID="txt_OfficeCode" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            <span style="font-size: 8pt; font-family: Verdana">State</span></td>
                        <td style="width: 169px">
                            <asp:DropDownList ID="txt_OfficeState" runat="server" Font-Size="8pt">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="JOHOR" Value="JOHOR"></asp:ListItem>
                                <asp:ListItem Text="KEDAH" Value="KEDAH"></asp:ListItem>
                                <asp:ListItem Text="KELANTAN" Value="KELANTAN"></asp:ListItem>
                                <asp:ListItem Text="KUALA LUMPUR" Value="KUALA LUMPUR"></asp:ListItem>
                                <asp:ListItem Text="LABUAN" Value="LABUAN"></asp:ListItem>
                                <asp:ListItem Text="MELAKA" Value="MELAKA"></asp:ListItem>
                                <asp:ListItem Text="NEGERI SEMBILAN" Value="NEGERI SEMBILAN"></asp:ListItem>
                                <asp:ListItem Text="PAHANG" Value="PAHANG"></asp:ListItem>
                                <asp:ListItem Text="PERAK" Value="PERAK"></asp:ListItem>
                                <asp:ListItem Text="PERLIS" Value="PERLIS"></asp:ListItem>
                                <asp:ListItem Text="PULAU PINANG" Value="PULAU PINANG"></asp:ListItem>
                                <asp:ListItem Text="SABAH" Value="SABAH"></asp:ListItem>
                                <asp:ListItem Text="SARAWAK" Value="SARAWAK"></asp:ListItem>
                                <asp:ListItem Text="SELANGOR" Value="SELANGOR"></asp:ListItem>
                                <asp:ListItem Text="SINGAPORE" Value="SINGAPORE"></asp:ListItem>
                                <asp:ListItem Text="TERENGGANU" Value="TERENGGANU"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 141px;">
                            </td>
                        <td style="width: 182px" >
                            <span style="font-size: 8pt; font-family: Verdana">Country</span></td>
                        <td style="width: 250px">
                            <asp:DropDownList ID="txt_OfficeCountry" runat="server" Font-Size="8pt">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                <asp:ListItem Text="Malaysia" Value="Malaysia"></asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                        </td>
                        <td style="width: 169px">
                        </td>
                        <td style="width: 141px">
                        </td>
                        <td style="width: 182px" >
                        </td>
                        <td style="width: 250px" >
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            <span style="font-size: 8pt; font-family: Verdana">Phone No. (Home)</span></td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_HomePhone" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                        </td>
                        <td style="width: 182px" >
                            <span style="font-size: 8pt; font-family: Verdana">Fax No. (Home)</span></td>
                        <td style="width: 250px">
                            <asp:TextBox ID="txt_HomeFax" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            <span style="font-size: 8pt; font-family: Verdana">Phone No. (Office)</span></td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_OfficePhone" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                        </td>
                        <td style="width: 182px">
                            <span style="font-size: 8pt; font-family: Verdana">Fax No. (Office)</span></td>
                        <td style="width: 250px">
                            <asp:TextBox ID="txt_OfficeFax" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Phone No. (Mobile)</span></td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_MobilePhone" runat="server" MaxLength="30" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                        </td>
                        <td style="width: 182px">
                        </td>
                        <td style="width: 250px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 14px;">
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Email Address 1</span></td>
                        <td style="height: 14px;" colspan="2">
                            <asp:TextBox ID="txt_Email1" runat="server" Width="148px" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 182px; height: 14px;">
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Email Address 2</span></td>
                        <td style="width: 250px; height: 14px;">
                            <asp:TextBox ID="txt_Email2" runat="server" Width="150px" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 31px;">
                            
                            </td>
                        <td style="height: 31px;" colspan="2">
                            </td>
                        <td style="width: 182px; height: 31px;">
                            
                            </td>
                        <td style="width: 250px; height: 31px;">
                            </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 20px">
                            <span style="font-size: 8pt; font-family: Verdana">
                            
                            Preferred method of member communication:&nbsp;</span></td>
                        <td style="width: 182px; height: 20px;" colspan="1">
                            <asp:RadioButton ID="Radio_CommMethod1" runat="server" GroupName="CommMethod" Text="By Post" Checked="True" Font-Names="Verdana" Font-Size="8pt" /><asp:RadioButton
                                ID="Radio_CommMethod2" runat="server" GroupName="CommMethod" Text="By Email" Font-Names="Verdana" Font-Size="8pt" /></td>
                        <td style="width: 250px; height: 20px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 1px;" colspan="3">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            
                            Malaysia Medical Council Registration Number (required)</span></span></td>
                        <td style="width: 182px" ><span
                                style="color: red">
                            <asp:TextBox ID="txt_MMC_No" runat="server" Width="146px" MaxLength="50" Font-Size="8pt"></asp:TextBox></span></td>
                        <td style="width: 250px">
                            </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 10px;">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            Date of Registration<span
                                style="color: red">*</span></span></span></td>
                        <td style="width: 169px; height: 10px;">
                        </td>
                        <td style="width: 141px;">
                        
                            </td>
                        <td style="width: 182px">
                        <asp:TextBox ID="txt_RegisDate" runat="server" Width="121px" MaxLength="50" Font-Size="8pt"></asp:TextBox>
                            <a href="javascript: showCalendar('txt_RegisDate')" ><img alt="" src="images/ew_calendar.gif" border="0" /></a>&nbsp;
                            </td>
                        <td style="width: 250px">
                        <%--added by kavi 03/04/2010--%>
                            <asp:RegularExpressionValidator id="valRegEx" runat="server" Font-Size="8pt" Font-Names="Verdana"
                                ControlToValidate="txt_RegisDate" SetFocusOnError="true"
                                ValidationExpression="([1-9]|0[1-9]|[12][0-9]|3[01])[- /.]([1-9]|0[1-9]|1[012])[- /.][0-9]{4}$"
                                ErrorMessage="*Invalid Date Format."
                                display="dynamic">*Invalid Date Format.
                            </asp:RegularExpressionValidator>
                            <%--end added by kavi 03/04/2010--%>
                            </td>
                    </tr>
                     <tr>
                        <td colspan="5">
                            
                                <br />
                            <span style="font-family: Verdana; font-size:11pt;">Professional Qualification</span></td>
                    </tr>
                     <tr>
                        <td colspan="2" style="height: 28px">
                            <span style="text-decoration: underline; font-size: 8pt; font-family: Verdana;">
                            Basic Medical Degree</span></td>
                        <td style="width: 141px; height: 28px;">
                        </td>
                        <td style="width: 182px; height: 28px;" >
                        </td>
                        <td style="width: 250px; height: 28px;">
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 269px">
                            <span style="font-size: 8pt; font-family: Verdana">
                            Degree (e.g MBBS, MD)</span></td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_Degree1" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px;">
                            <span style="font-size: 8pt; font-family: Verdana">
                            University</span></td>
                        <td style="width: 182px">
                            <asp:TextBox ID="txt_University1" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 250px">
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 269px; height: 4px;">
                            <span style="font-size: 8pt; font-family: Verdana">City/Country</span></td>
                        <td style="width: 169px; height: 4px;">
                            <asp:TextBox ID="txt_Country1" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px;">
                            <span style="font-size: 8pt; font-family: Verdana">Date</span></td>
                        <td style="width: 182px" >
                            <asp:DropDownList ID="ddl_Month1" runat="server" Width="49px" Font-Size="8pt">
                            </asp:DropDownList><asp:DropDownList ID="ddl_Year1" runat="server" Width="64px" Font-Size="8pt">
                            </asp:DropDownList></td>
                        <td style="width: 250px">
                        </td>
                    </tr>
                     <tr>
                        <td colspan="5" style="height: 28px">
                        </td>
                    </tr>
                     <tr>
                        <td colspan="5" style="height: 19px">
                            <span style="text-decoration: underline; font-size: 8pt; font-family: Verdana;">
                            Post Graduate
                                Qualifications in Ophthalmology</span></td>
                    </tr>
                     <tr>
                        <td colspan="5">
                            <span style="font-size: 8pt; font-family: Verdana;">
                            Applicants applying for Ordinary
                                and Life Membership must complete this section<br />
                            </span>
                            <br />
                            </td>
                    </tr>
                     <tr>
                        <td style="width: 269px; height: 8px;">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            I. &nbsp; &nbsp; Degree <span>(e.g. FRCS)</span></span></span></td>
                        <td style="width: 169px; height: 8px;">
                            <asp:TextBox ID="txt_Degree2" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                            <span style="font-size: 8pt; font-family: Verdana">University/Institution</span></td>
                        <td style="width: 182px" >
                            <asp:TextBox ID="txt_University2" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 250px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 14px;">
                            &nbsp; &nbsp;&nbsp; <span style="font-size: 8pt; font-family: Verdana">City/Country</span></td>
                        <td style="width: 169px; height: 14px;">
                            <asp:TextBox ID="txt_Country2" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 14px;">
                            <span style="font-size: 8pt; font-family: Verdana">Date</span></td>
                        <td style="width: 182px; height: 14px;" >
                            <asp:DropDownList ID="ddl_Month2" runat="server" Width="52px" Font-Size="8pt">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddl_Year2" runat="server" Width="65px" Font-Size="8pt">
                            </asp:DropDownList></td>
                        <td style="width: 250px; height: 14px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 18px;">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            II.&nbsp; Degree <span>(e.g. FRCS)</span></span></span></td>
                        <td style="width: 169px; height: 18px;">
                            <asp:TextBox ID="txt_Degree3" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 18px;">
                            <span style="font-size: 8pt; font-family: Verdana">University/Institution</span></td>
                        <td style="width: 182px; height: 18px;" >
                            <asp:TextBox ID="txt_University3" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 250px; height: 18px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px;">
                            &nbsp; &nbsp;&nbsp; <span style="font-size: 8pt; font-family: Verdana">City/Country</span></td>
                        <td style="width: 169px;">
                            <asp:TextBox ID="txt_Country3" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                            <span style="font-size: 8pt; font-family: Verdana">Date</span></td>
                        <td style="width: 182px" >
                            <asp:DropDownList ID="ddl_Month3" runat="server" Width="51px" Font-Size="8pt">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddl_Year3" runat="server" Width="66px" Font-Size="8pt">
                            </asp:DropDownList></td>
                        <td style="width: 250px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px">
                            <span style="font-family: Verdana"><span style="font-size: 8pt">
                            III. Degree <span>(e.g. FRCS)</span></span></span></td>
                        <td style="width: 169px">
                            <asp:TextBox ID="txt_Degree4" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px">
                            <span style="font-size: 8pt; font-family: Verdana">University/Institution</span></td>
                        <td style="width: 182px">
                            <asp:TextBox ID="txt_University4" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 250px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 22px;">
                            &nbsp; &nbsp;&nbsp; <span style="font-size: 8pt; font-family: Verdana">City/Country</span></td>
                        <td style="width: 169px; height: 22px;">
                            <asp:TextBox ID="txt_Country4" runat="server" MaxLength="50" Font-Size="8pt"></asp:TextBox></td>
                        <td style="width: 141px; height: 22px;">
                            <span style="font-size: 8pt; font-family: Verdana">Date</span></td>
                        <td style="width: 182px; height: 22px;">
                            <asp:DropDownList ID="ddl_Month4" runat="server" Width="52px" Font-Size="8pt">
                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddl_Year4" runat="server" Width="65px" Font-Size="8pt">
                            </asp:DropDownList></td>
                        <td style="width: 250px; height: 22px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <hr style="height: -12px" />
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td colspan="5" style="height: 37px">
                         <span style="font-size: 11pt; font-family: Verdana;">Verification By Head of Department</span></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 12px" colspan="4">
                            <span style="font-size: 8pt; font-family: Verdana">The Applicant is currently </span>
                            <asp:TextBox ID="veriapp" runat="server" Font-Size="8pt" Width="476px"></asp:TextBox></td>
                        <td style="width: 241px; height: 12px"></td>
                    </tr>
                     <tr>
                        <td style="width: 269px; height: 10px;">
                            <span style="font-size: 8pt; font-family: Verdana">Full Name</span></td>
                        <td style="width: 169px; height: 10px;"><asp:TextBox ID="veriname" runat="server" Font-Size="8pt" Width="143px"></asp:TextBox></td>
                        <td style="width: 141px; height: 10px;">
                            <span style="font-size: 8pt; font-family: Verdana">Institution</span></td>
                        <td style="height: 10px" colspan="2"><asp:TextBox ID="veriins" runat="server" Font-Size="8pt" Width="311px"></asp:TextBox></td>
                    </tr>                                       
                    <tr>
                        <td colspan="5" style="height: 37px">
                         <span style="font-size: 10pt; font-family: Verdana;">Statement of Support</span></td>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 12px" colspan="2">
                            <span style="font-size: 8pt; font-family: Verdana">Full Name </span>&nbsp;<asp:TextBox ID="supfullname1" runat="server" Font-Size="8pt" Width="213px"></asp:TextBox></td>
                        <td style="width: 241px; height: 12px" colspan="2">
                            <span style="font-size: 8pt; font-family: Verdana">Full Name </span>
                            <asp:TextBox ID="supsign1" runat="server" Font-Size="8pt" Width="213px"></asp:TextBox></td>
                        <td style="height: 10px">
                            &nbsp;&nbsp;</td>
                    </tr>
                   <tr>
                        <td style="width: 269px; height: 12px" colspan="2">
                            <span style="font-size: 8pt; font-family: Verdana">Membership No </span>&nbsp;<asp:TextBox ID="supfullname2" runat="server" Font-Size="8pt" Width="213px"></asp:TextBox></td>
                        <td style="width: 241px; height: 12px" colspan="2">
                            <span style="font-size: 8pt; font-family: Verdana">Membership No </span>
                            <asp:TextBox ID="supsign2" runat="server" Font-Size="8pt" Width="213px"></asp:TextBox></td>
                        <td style="height: 10px">
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <hr style="height: -12px" />
                        </td>
                    </tr>                    
                    <tr>
                        <td valign="top">
                            <span style="font-size: 8pt; font-family: Verdana">Remark</span> &nbsp;&nbsp;</td>
                        <td colspan="4" valign="top">
                            <asp:TextBox ID="remark" runat="server" TextMode="MultiLine" Height="55px" Width="546px"></asp:TextBox>
                    </tr>
                    <tr>
                        <td style="width: 269px; height: 31px;">
                        </td>
                        <td style="width: 169px; height: 31px;">
                            &nbsp;<asp:Button ID="btn_Submit" runat="server" Text="Add New Member" Font-Size="8pt" OnClick="btn_Submit_Click" /></td>
                        <td style="width: 141px; height: 31px;">
                        </td>
                        <td style="width: 182px; height: 31px;">
                        </td>
                        <td style="width: 250px; height: 31px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 21px;" colspan="5">
                            &nbsp;<br />
                            <br />
                        </td>
                    </tr>
                </table>
                </div></div>
        </div>
    
    </div>
    </form>
</body>
</html>
