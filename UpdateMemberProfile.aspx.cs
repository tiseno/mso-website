﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using MSO_BizLayer;
using MyDate_Val;

public partial class UpdateMemberProfile : System.Web.UI.Page
{
    private BizMember bm = new BizMember();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserCookies"] == null)
        {
            Response.Redirect("LostCookies.aspx");
        }
        else
        {
            string ProfileID;

            ProfileID = Request.Cookies["UserCookies"].Value;
        }

        if (!IsPostBack && Request.QueryString["id"] != null)
        {
            display_member_detail();
            Panel1.Visible = true;
        }
        // added by kavi 03/04/10
        memberno.Attributes.Add("onKeypress", "CheckIsNumeric()");
        // end added by kavi
    }
    protected void btn_Submit_Click(object sender, EventArgs e)
    {
        // added by kavi 17/05/10
        if (memberno.Text == "")
        {
            Response.Write("<script type='text/javascript'>alert('Fill Membership No!');</script>");
            memberno.Focus();
        }
        else
        {
        // end added by kavi 17/05/10
            string birth_date;
            string gend = "";
            string mailing_add = "";
            string user_type = "";
            string comm_way = "";
            string strregisdt = "";
            string strjoindt = "";

            if (Radio_Male.Checked == true)
            {
                gend = Radio_Male.Text;
            }
            if (Radio_Female.Checked == true)
            {
                gend = Radio_Female.Text;
            }
            if (Radio_MailingAddress1.Checked == true)
            {
                mailing_add = Radio_MailingAddress1.Text;
            }
            if (Radio_MailingAddress2.Checked == true)
            {
                mailing_add = Radio_MailingAddress2.Text;
            }

            if (Radio_userType1.Checked == true)
            {
                user_type = "Admin";
            }
            if (Radio_userType2.Checked == true)
            {
                user_type = "Member";
            }
            if (Radio_CommMethod1.Checked == true)
            {
                comm_way = "By Post";
            }
            if (Radio_CommMethod2.Checked == true)
            {
                comm_way = "By Mail";
            }


            //if (ddl_DOB_Day.SelectedValue != "dd")
            //{
            //    if (ddl_DOB_Month.SelectedValue != "mm")
            //    {
            //        if (ddl_DOB_Year.SelectedValue != "yyyy")
            //        {
            //if (Date_Val.ShowDate_Val(txt_RegisDate.Text) == "False")
            //{
            //    Response.Write("<script type='text/javascript'>alert('Invalid Registration Date!');</script>");
            //}
            //else
            //{
            //if (Date_Val.ShowDate_Val(joindt.Text) == "False")
            //{
            //    Response.Write("<script type='text/javascript'>alert('Invalid Join Date!');</script>");
            //}
            //else
            //{
            if (bm.CheckMembershipID(memberno.Text, Request.QueryString["id"]) == "1")
            {
                Response.Write("<script type='text/javascript'>alert('Same Membership found or no membership was found, Kindly Amend Membership No!');</script>");
                memberno.Focus();
            }
            else
            {

                birth_date = ddl_DOB_Year.SelectedValue + ddl_DOB_Month.SelectedValue + ddl_DOB_Day.SelectedValue;
                if (txt_RegisDate.Text != "")
                {
                    strregisdt = txt_RegisDate.Text.ToString().Substring(6, 4) + txt_RegisDate.Text.ToString().Substring(3, 2) + txt_RegisDate.Text.ToString().Substring(0, 2);
                }
                if (joindt.Text != "")
                {
                    strjoindt = joindt.Text.ToString().Substring(6, 4) + joindt.Text.ToString().Substring(3, 2) + joindt.Text.ToString().Substring(0, 2);
                }
                bm.updateMember(lbl_userNo.Text, txt_MMC_No.Text, user_type, txt_Name.Text, ddl_Title.SelectedValue, birth_date, gend,
                  txt_Nationality.Text, txt_IC.Text, mailing_add, txt_HomeAddress.Text, txt_HomeCity.Text, txt_HomeState.Text, txt_HomeCountry.Text, txt_HomeCode.Text, txt_OfficeAddress.Text,
                  txt_OfficeCity.Text, txt_OfficeState.Text, txt_OfficeCode.Text, txt_OfficeCountry.Text, txt_HomePhone.Text, txt_OfficePhone.Text, txt_MobilePhone.Text, txt_HomeFax.Text, txt_OfficeFax.Text, txt_Email1.Text, txt_Email2.Text, strregisdt, comm_way, txt_username.Text, txt_password.Text, ddl_MemberShipType.SelectedValue, ddl_mmaos.SelectedValue, memberno.Text, isactive.SelectedValue, strjoindt, veriapp.Text, veriname.Text, veriins.Text, supfullname1.Text, supsign1.Text, supfullname2.Text, supsign2.Text, remark.Text, Request.Cookies["UserCookies"].Value);


                //DataSet getNo;
                //getNo = bm.get_No(txt_username.Text);

                //string No;
                //No = getNo.Tables[0].Rows[0]["No"].ToString();
                string date_graduate1;
                date_graduate1 = ddl_Year1.SelectedValue + "-" + ddl_Month1.SelectedValue;
                string date_graduate2;
                date_graduate2 = ddl_Year2.SelectedValue + "-" + ddl_Month2.SelectedValue;
                string date_graduate3;
                date_graduate3 = ddl_Year3.SelectedValue + "-" + ddl_Month3.SelectedValue;
                string date_graduate4;
                date_graduate4 = ddl_Year4.SelectedValue + "-" + ddl_Month4.SelectedValue;




                if (lbl_No1.Text != "")
                {
                    if (txt_Degree1.Text != "")
                    {
                        bm.updateEducation(lbl_userNo.Text, "Basic Medical Degree", txt_Degree1.Text, txt_Country1.Text, txt_University1.Text, date_graduate1, Convert.ToInt32(lbl_No1.Text));
                    }
                }
                else
                {
                    if (txt_Degree1.Text != "")
                    {
                        bm.insertEducation(lbl_userNo.Text, "Basic Medical Degree", txt_Degree1.Text, txt_Country1.Text, txt_University1.Text, date_graduate1);
                    }
                }
                if (lbl_No2.Text != "")
                {
                    if (txt_Degree2.Text != "")
                    {
                        bm.updateEducation(lbl_userNo.Text, "Post Graduate in Ophthalmology", txt_Degree2.Text, txt_Country2.Text, txt_University2.Text, date_graduate2, Convert.ToInt32(lbl_No2.Text));
                    }
                }
                else
                {
                    if (txt_Degree2.Text != "")
                    {
                        bm.insertEducation(lbl_userNo.Text, "Post Graduate in Ophthalmology", txt_Degree2.Text, txt_Country2.Text, txt_University2.Text, date_graduate2);
                    }
                }
                if (lbl_No3.Text != "")
                {
                    if (txt_Degree3.Text != "")
                    {
                        bm.updateEducation(lbl_userNo.Text, "Post Graduate in Ophthalmology", txt_Degree3.Text, txt_Country3.Text, txt_University3.Text, date_graduate3, Convert.ToInt32(lbl_No3.Text));
                    }
                }
                else
                {
                    if (txt_Degree3.Text != "")
                    {
                        bm.insertEducation(lbl_userNo.Text, "Post Graduate in Ophthalmology", txt_Degree3.Text, txt_Country3.Text, txt_University3.Text, date_graduate3);
                    }
                }
                if (lbl_No4.Text != "")
                {
                    if (txt_Degree4.Text != "")
                    {
                        bm.updateEducation(lbl_userNo.Text, "Post Graduate in Ophthalmology", txt_Degree4.Text, txt_Country4.Text, txt_University4.Text, date_graduate4, Convert.ToInt32(lbl_No4.Text));
                    }
                }
                else
                {
                    if (txt_Degree4.Text != "")
                    {
                        bm.insertEducation(lbl_userNo.Text, "Post Graduate in Ophthalmology", txt_Degree4.Text, txt_Country4.Text, txt_University4.Text, date_graduate4);
                    }
                }

                string no = Request.QueryString["id"];

                // bm.updateMembershipFee(Convert.ToInt32(no), Convert.ToInt32(ddl_MemberShipType.SelectedValue), txt_StartDate.Text, lbl_ExpiredDate.Text, lbl_fee.Text, Convert.ToInt32(ddl_CurrentStatus.SelectedValue), "");
                Response.Write("<script type='text/javascript'>alert('Member profile has been updated!')</script>");
                //Response.Write("<script type='text/javascript'>window.location='AdminMain.aspx';</script>");
            }

            //}
            //}

            //}
            //        else
            //        {
            //            Response.Write("<script type='text/javascript'>alert('Invalid Date of Birth!');</script>");
            //            display_member_detail();
            //        }

            //    }
            //    else
            //    {
            //        Response.Write("<script type='text/javascript'>alert('Invalid Date of Birth!');</script>");
            //        display_member_detail();
            //    }
            //}
            //else
            //{
            //    Response.Write("<script type='text/javascript'>alert('Invalid Date of Birth!');</script>");
            //    display_member_detail();
            //}
        // added by kavi 17/05/10
        }
        // end added by kavi 17/05/10
        
    }

    public void DateAutoGenerate()
    {
        ddl_DOB_Day.Items.Clear();
        ddl_DOB_Month.Items.Clear();
        ddl_DOB_Year.Items.Clear();
        ddl_Month1.Items.Clear();
        ddl_Month2.Items.Clear();
        ddl_Month3.Items.Clear();
        ddl_Month4.Items.Clear();
        ddl_Year1.Items.Clear();
        ddl_Year2.Items.Clear();
        ddl_Year3.Items.Clear();
        ddl_Year4.Items.Clear();

        ddl_DOB_Day.Items.Add("dd");
        ddl_DOB_Month.Items.Add("mm");
        ddl_DOB_Year.Items.Add("yyyy");
        ddl_Month1.Items.Add("mm");
        ddl_Month2.Items.Add("mm");
        ddl_Month3.Items.Add("mm");
        ddl_Month4.Items.Add("mm");
        ddl_Year1.Items.Add("yyyy");
        ddl_Year2.Items.Add("yyyy");
        ddl_Year3.Items.Add("yyyy");
        ddl_Year4.Items.Add("yyyy");

        int i;
        for (i = 1; i <= 31; i++)
        {
            if (i < 10)
            {
                ddl_DOB_Day.Items.Add("0" + i + "");
            }
            else
            {
                ddl_DOB_Day.Items.Add("" + i + "");
            }
        }
        for (i = 1; i <= 12; i++)
        {
            if (i < 10)
            {
                ddl_DOB_Month.Items.Add("0" + i + "");
                ddl_Month1.Items.Add("0" + i + "");
                ddl_Month2.Items.Add("0" + i + "");
                ddl_Month3.Items.Add("0" + i + "");
                ddl_Month4.Items.Add("0" + i + "");
            }
            else
            {
                ddl_DOB_Month.Items.Add("" + i + "");
                ddl_Month1.Items.Add("" + i + "");
                ddl_Month2.Items.Add("" + i + "");
                ddl_Month3.Items.Add("" + i + "");
                ddl_Month4.Items.Add("" + i + "");
            }
            
        }
        // emended by kavi 17/05/10
        // changed from 1940 to 1930
        ///////////////////////////////
        for (i = 1930; i <= 2011; i++)
        // end emended by kavi 17/05/10
        {
            ddl_DOB_Year.Items.Add("" + i + "");
        }
        for (i = 1955; i <= 2011; i++)
        {
            ddl_Year1.Items.Add("" + i + "");
            ddl_Year2.Items.Add("" + i + "");
            ddl_Year3.Items.Add("" + i + "");
            ddl_Year4.Items.Add("" + i + "");
        }

    }
    protected void Link_Back_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchMember.aspx");
    }
    protected void ddl_MemberShipType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet app = new DataSet();
        app = bm.get_Application(ddl_MemberShipType.SelectedItem.Text);
        //lbl_fee.Text = app.Tables[0].Rows[0]["normal_fee"].ToString();

        if (ddl_MemberShipType.SelectedValue != lbl_CurMembType.Text)
        {

        }
        else
        {

        }
    }

    protected void ddl_Title_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public void display_member_detail()
    {
        DateAutoGenerate();

        string id = Request.QueryString["id"];

        DataSet get_detail = new DataSet();

        get_detail = bm.Search_MemberDetail(Convert.ToInt32(id));


        //ddl_CurrentStatus.SelectedValue = get_detail.Tables[0].Rows[0]["Status_id"].ToString();
        //ddl_MemberShipType.SelectedItem.Text = get_detail.Tables[0].Rows[0]["Application"].ToString();
        //for (int i = 0; i < ddl_MemberShipType.Items.Count; i++)
        //{
            //if (ddl_MemberShipType.Items[i].Text == get_detail.Tables[0].Rows[0]["AppType"].ToString())
            //{
                //ddl_MemberShipType.SelectedIndex = i;
                //break;
            //}
        //}
        ddl_MemberShipType.SelectedValue = get_detail.Tables[0].Rows[0]["AppType"].ToString();
        ddl_mmaos.SelectedValue = get_detail.Tables[0].Rows[0]["MMAOS"].ToString();

        if (get_detail.Tables[0].Rows[0]["userType"].ToString() == "Member")
        {
            Radio_userType1.Checked = true;
        }
        if (get_detail.Tables[0].Rows[0]["userType"].ToString() == "Administrator")
        {
            Radio_userType2.Checked = true;
        }

        txt_username.Text = get_detail.Tables[0].Rows[0]["username"].ToString();
        txt_password.Text = get_detail.Tables[0].Rows[0]["pw"].ToString();
   
        txt_Name.Text = get_detail.Tables[0].Rows[0]["Memb_Name"].ToString();
        txt_Nationality.Text = get_detail.Tables[0].Rows[0]["Nationality"].ToString();
        txt_IC.Text = get_detail.Tables[0].Rows[0]["NRIC"].ToString();

        ddl_Title.SelectedValue = get_detail.Tables[0].Rows[0]["Memb_Title"].ToString();

        if (get_detail.Tables[0].Rows[0]["DOB"].ToString() == "")
        {
        }
        else
        {
            ddl_DOB_Day.SelectedValue = get_detail.Tables[0].Rows[0]["DOB"].ToString().Substring(6, 2);
            ddl_DOB_Month.SelectedValue = get_detail.Tables[0].Rows[0]["DOB"].ToString().Substring(4, 2);
            ddl_DOB_Year.SelectedValue = get_detail.Tables[0].Rows[0]["DOB"].ToString().Substring(0, 4);
        }
        string gender;
        gender = get_detail.Tables[0].Rows[0]["Gend"].ToString();
        if (gender == "Male")
        {
            Radio_Male.Checked = true;
        }
        else
        {
            Radio_Female.Checked = true;
        }

        txt_HomeAddress.Text = get_detail.Tables[0].Rows[0]["Home_Address"].ToString();
        txt_HomeCity.Text = get_detail.Tables[0].Rows[0]["Home_City"].ToString();
        txt_HomeCode.Text = get_detail.Tables[0].Rows[0]["Home_Code"].ToString();
        txt_HomeState.SelectedValue = get_detail.Tables[0].Rows[0]["Home_State"].ToString();
        txt_HomeCountry.SelectedValue = get_detail.Tables[0].Rows[0]["Home_country"].ToString();
        txt_HomeFax.Text = get_detail.Tables[0].Rows[0]["H_Fax"].ToString();
        txt_HomePhone.Text = get_detail.Tables[0].Rows[0]["H_Phone"].ToString();
        txt_MobilePhone.Text = get_detail.Tables[0].Rows[0]["M_Phone"].ToString();

        if (get_detail.Tables[0].Rows[0]["Mailing_Address"].ToString() == "Office Address")
        {
            Radio_MailingAddress1.Checked = true;
        }
        if (get_detail.Tables[0].Rows[0]["Mailing_Address"].ToString() == "Home Address")
        {
            Radio_MailingAddress2.Checked = true;
        }

        if (get_detail.Tables[0].Rows[0]["Comm_Method"].ToString() == "Post")
        {
            Radio_CommMethod1.Checked = true;
        }
        if (get_detail.Tables[0].Rows[0]["Comm_Method"].ToString() == "Email")
        {
            Radio_CommMethod2.Checked = true;
        }

        txt_MMC_No.Text = get_detail.Tables[0].Rows[0]["MMC_No"].ToString();


        txt_OfficeAddress.Text = get_detail.Tables[0].Rows[0]["Office_Address"].ToString();
        txt_OfficeCity.Text = get_detail.Tables[0].Rows[0]["Office_City"].ToString();
        txt_OfficeCode.Text = get_detail.Tables[0].Rows[0]["Office_Code"].ToString();
        txt_OfficeState.SelectedValue = get_detail.Tables[0].Rows[0]["Office_State"].ToString();
        txt_OfficeCountry.SelectedValue = get_detail.Tables[0].Rows[0]["Office_Country"].ToString();
        txt_OfficeFax.Text = get_detail.Tables[0].Rows[0]["O_Fax"].ToString();
        txt_OfficePhone.Text = get_detail.Tables[0].Rows[0]["O_Phone"].ToString();
        if (get_detail.Tables[0].Rows[0]["DOR"].ToString() != "")
        { 
            txt_RegisDate.Text = get_detail.Tables[0].Rows[0]["DOR"].ToString().Substring(6, 2) + "/" + get_detail.Tables[0].Rows[0]["DOR"].ToString().Substring(4, 2) + "/" + get_detail.Tables[0].Rows[0]["DOR"].ToString().Substring(0, 4);
        }
        txt_Email1.Text = get_detail.Tables[0].Rows[0]["Email1"].ToString();
        txt_Email2.Text = get_detail.Tables[0].Rows[0]["Email2"].ToString();

        memberno.Text = get_detail.Tables[0].Rows[0]["membershipid"].ToString();
        isactive.SelectedValue = get_detail.Tables[0].Rows[0]["isactive"].ToString();
        remark.Text = get_detail.Tables[0].Rows[0]["remark"].ToString();
        if (get_detail.Tables[0].Rows[0]["joindate"].ToString() != "")
        {
            joindt.Text = get_detail.Tables[0].Rows[0]["joindate"].ToString().Substring(6, 2) + "/" + get_detail.Tables[0].Rows[0]["joindate"].ToString().Substring(4, 2) + "/" + get_detail.Tables[0].Rows[0]["joindate"].ToString().Substring(0, 4);
        }
        lastsave.Text = get_detail.Tables[0].Rows[0]["lastsave"].ToString();
        veriapp.Text = get_detail.Tables[0].Rows[0]["veritext"].ToString();
        veriname.Text = get_detail.Tables[0].Rows[0]["verifullname"].ToString();
        veriins.Text = get_detail.Tables[0].Rows[0]["veriins"].ToString();
        supfullname1.Text = get_detail.Tables[0].Rows[0]["supfullname1"].ToString();
        supsign1.Text = get_detail.Tables[0].Rows[0]["supmemno1"].ToString();
        supfullname2.Text = get_detail.Tables[0].Rows[0]["supfullname2"].ToString();
        supsign2.Text = get_detail.Tables[0].Rows[0]["supmemno2"].ToString();

        string no;
        no = get_detail.Tables[0].Rows[0]["Profile_No"].ToString();
        DataSet get_Memb_Edu = new DataSet();
        get_Memb_Edu = bm.MemberEducation(no);
        lbl_userNo.Text = no;

        if (get_Memb_Edu.Tables["Level"].Rows.Count > 0)
        {
            txt_Country1.Text = get_Memb_Edu.Tables["Level"].Rows[0]["Country"].ToString();
            txt_Degree1.Text = get_Memb_Edu.Tables["Level"].Rows[0]["Degree"].ToString();
            txt_University1.Text = get_Memb_Edu.Tables["Level"].Rows[0]["University"].ToString();
            ddl_Month1.SelectedValue = get_Memb_Edu.Tables["Level"].Rows[0]["Date_Join"].ToString().Substring(5, 2);
            ddl_Year1.SelectedValue = get_Memb_Edu.Tables["Level"].Rows[0]["Date_Join"].ToString().Substring(0, 4);
            lbl_No1.Text = get_Memb_Edu.Tables["Level"].Rows[0]["NO"].ToString();
        }
        if (get_Memb_Edu.Tables["Level"].Rows.Count > 1)
        {
            txt_Country2.Text = get_Memb_Edu.Tables["Level"].Rows[1]["Country"].ToString();
            txt_Degree2.Text = get_Memb_Edu.Tables["Level"].Rows[1]["Degree"].ToString();
            txt_University2.Text = get_Memb_Edu.Tables["Level"].Rows[1]["University"].ToString();
            ddl_Month2.SelectedValue = get_Memb_Edu.Tables["Level"].Rows[1]["Date_Join"].ToString().Substring(5, 2);
            ddl_Year2.SelectedValue = get_Memb_Edu.Tables["Level"].Rows[1]["Date_Join"].ToString().Substring(0, 4);
            lbl_No2.Text = get_Memb_Edu.Tables["Level"].Rows[1]["NO"].ToString();
        }
        if (get_Memb_Edu.Tables["Level"].Rows.Count > 2)
        {
            txt_Country3.Text = get_Memb_Edu.Tables["Level"].Rows[2]["Country"].ToString();
            txt_Degree3.Text = get_Memb_Edu.Tables["Level"].Rows[2]["Degree"].ToString();
            txt_University3.Text = get_Memb_Edu.Tables["Level"].Rows[2]["University"].ToString();
            ddl_Month3.SelectedValue = get_Memb_Edu.Tables["Level"].Rows[2]["Date_Join"].ToString().Substring(5, 2);
            ddl_Year3.SelectedValue = get_Memb_Edu.Tables["Level"].Rows[2]["Date_Join"].ToString().Substring(0, 4);
            lbl_No3.Text = get_Memb_Edu.Tables["Level"].Rows[2]["NO"].ToString();
        }

        if (get_Memb_Edu.Tables["Level"].Rows.Count > 3)
        {
            txt_Country4.Text = get_Memb_Edu.Tables["Level"].Rows[3]["Country"].ToString();
            txt_Degree4.Text = get_Memb_Edu.Tables["Level"].Rows[3]["Degree"].ToString();
            txt_University4.Text = get_Memb_Edu.Tables["Level"].Rows[3]["University"].ToString();
            ddl_Month4.SelectedValue = get_Memb_Edu.Tables["Level"].Rows[3]["Date_Join"].ToString().Substring(5, 2);
            ddl_Year4.SelectedValue = get_Memb_Edu.Tables["Level"].Rows[3]["Date_Join"].ToString().Substring(0, 4);
            lbl_No4.Text = get_Memb_Edu.Tables["Level"].Rows[3]["NO"].ToString();
        }
    }
    protected void gvPayment_DataBound(Object sender, EventArgs e)
    {
        int mid = Convert.ToInt16(Request.QueryString["id"]);
        DataSet dpayment;
        dpayment = bm.memberPayment(mid);

        if (dpayment.Tables[0].Rows.Count == 0)
        {
            txtNoInfo.Text = "No Payment Information Found For This Member!";
        }
        else
        {
            gvPayment.DataSource = dpayment;
            gvPayment.DataBind();
        }
    }
    public string changeDate(object dte)
    {
        String ccdte = Convert.ToString(dte);
        String cdate = (ccdte.Substring(5, 2) + "/" + ccdte.Substring(8, 2) + "/" + ccdte.Substring(0, 4));
        return cdate;
    }
    public string getStatus(object sid)
    {
        String statusid = Convert.ToString(sid);
        string stat = "";
        if (statusid == "2")
        {
            stat = "Unpaid";
        }
        else if (statusid == "3")
        {
            stat = "Paid";
        }
        return stat;
    }
    protected void btn_Payment_Click(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];
        Response.Redirect("Renew.aspx?id=" + id );
    }
}
